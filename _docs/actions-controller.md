# Actions & Controllers

## ADR Pattern

The ADR *(Action-Domain-Responder)* pattern is an alternative interpretation of MVC, using a single *Action* class for each operation instead of a *Controller* grouping several actions.

For a basic example of how ADR can look like in Laravel, head over to this website: http://martinbean.co.uk/blog/2016/10/20/implementing-adr-in-laravel/

## Controllers vs. Actions

We use ADR for all endpoints *except BREAD operations in the **admin** scope*. The reason for this is the individual nature of most custom actions. Only basic BREAD on all models for admins are handled by Controllers, as they are completely abstract and lack any model-specific logic.
