# Documentation on how to document ;)

- Document everything that a peer otherwise would have to ask you, when they work on the respective part of the project
- Update the documentation when the respective code or concept changes
- Keep everything organized inside the `_docs` directory. Add to existing topics, if they're similar enough. Create new topics when necessary
- Put a reference to every topic in the projec's `README.md` so they can easily be found
- For long or complex articles, put a table of contents at the very top of each file and keep it up to date
- You can use `bin/gh-md-toc` to generate a table of contents for a single .md file, eg. like so `bin/gh-md-toc _docs/meta.md`. It will print a TOC for the given file to the console. Copy that toc to the `## Table of contents` section of your file, replacing the old toc.
