# Image Attachments
Images must be uploaded as binary (not multipart) with the mime type as header, e.g. `Content-Type: image/jpeg`.

## Defining image attachments for a model
First create a string column (or multiple for multiple attachments) for the model's database table with the name of the attachment like this: `$table->string('avatar')->nullable();`. Then use the `HasImageAttachments`-Trait in the model. Define the attachment(s) configuration in your model via the `$image_attachments`-Array (use the column name as array key):

```php
protected $image_attachments = [
    'avatar' => [
        'path' => 'uploads/user/avatar', // partitioned object id + style will be appended, e.g. '/000/000/001/small/generated-filename.jpg'
        'styles' => [
            'small' => '100', // This relates to the image's width, ratio will remain
            'medium' => '500',
            'large' => '1000'
        ]
    ],
    'background_image' => [
        'path' => 'uploads/user/background',
        // ...
    ]
];
```

Create a Laravel style magic getter for every attachment like this:

```php
/**
 * Accessor providing an `avatar` attribute which contains an array of image urls for every defined dimension.
 *
 * @return array
 */
public function getAvatarAttribute()
{
    return $this->getImageAttachmentPaths('background_image'); // use db snake case style here
}
```

## Uploading images through API
Uploads can be added/updated through binary `POST` or `PUT` requests.
First create endpoint(s), e.g.:

```php
Route::post('/users/me/avatar', 'Actions\UploadMyAvatarAction');
Route::put('/users/me/avatar', 'Actions\UploadMyAvatarAction'); // optional
```

Then create your handling action like:

```php
public function __invoke(FileUploadRequest $request)
{
    $me = $this->userService->get(Auth::id());
    $me->updateImageAttachment('avatar', $request->fileContent(), $request->fileType());

    return $this->responder->send($me, UserResource::class);
}
```

Add the attributes to the responding resource if desired.

Content-Type validation and file extension is extracted from the Request-Headers, so make sure you set something similar to: `Content-Type: image/jpeg` in your Request-Headers and add the image data as body.

An example can be found for the user's avatar in the [API Reference](https://documenter.getpostman.com/view/830159/volunteer-vision-api-v2/7LuZeQf)

