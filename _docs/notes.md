# Notes

## Data Model

### Why removing `user_match`
- The frontend needs an endpoint to show all the enrollments/matches (unified as one model). Each enrollment has its own status etc.
- An enrollment shouldn't be a one-time event: Enrollments need the users role attached. Future business logic might entail doing the same program twice, with different roles.
- The functionality to (re-)start a program should be unified. In the old model, it was an `Enrollment` the first time around and a `Match` for the following times.
- The question 'Which enrollments are matched?' should be easy to answer.
