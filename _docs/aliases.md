
> ~/.zshrc

add at the end:

source ~/.aliases


> ~/.aliases

add :


alias gs="git status" 
alias gl="git log"  
alias gaa="git add ." 
alias gac="git add . && git commit -m"   
alias gp="git push"  
alias lcontroller="php artisan make:controller"  
alias lmodel="php artisan make:model"  
alias lmigration="php artisan make:migration"  

alias artisan="php artisan"  
  
alias lservice="php artisan make:provider"  
alias ltest="./vendor/bin/phpunit"  
  
alias gffeature="git flow feature start "  
alias gffinish="git flow feature finish "  
alias gfpublish="git flow feature publish "  



