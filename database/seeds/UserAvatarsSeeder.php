<?php

use Illuminate\Database\Seeder;
use GuzzleHttp\Client;
use Intervention\Image\ImageManagerStatic as Image;
use Modules\Core\Domain\Models\User;

class UserAvatarsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        return;
        // Only users with even id get an avatar, so we have users without avatar too ...
        $evenUsers = User::whereRaw('MOD(id, 2) = 0')->get();

        foreach ($evenUsers as $user) {
            self::seedProfilePicture($user);
        }

        self::seedDefaultPictures();

    }

    public static function seedProfilePicture(User $user)
    {

        $client = new Client();

        $cacheFolder = storage_path('cache');
        if (!file_exists($cacheFolder)) {
            mkdir($cacheFolder);
        }

        $cacheFile = $cacheFolder . '/' . $user->id . '.jpg';
        if ($user->email == 'alexandra.graf@volunteer-vision.com'){
            $cacheFile = __DIR__ . '/avatars/alex_800.jpg';
        }

        if (!file_exists($cacheFile)) {
            $response = $client->get("https://randomuser.me/api/?inc=picture&gender={$user->gender}");
            $responseAsJson = json_decode($response->getBody()->getContents());
            $imageUrl = $responseAsJson->results[0]->picture->large;
            $image = Image::make($imageUrl);
            $image->save($cacheFile);
        } else {
            $image = Image::make($cacheFile);
        }

        $user->updateImageAttachment('avatar', $image->stream(), $image->mime);
    }

    private static function seedDefaultPictures() {
        $src = __DIR__ . '/sampledata/avatar.jpg';
        $folder = __DIR__ . '/../../storage/app/public/uploads/user/avatar/defaults/';


        if (!File::exists($folder)) {
            File::makeDirectory($folder, 0755, true);
        }


        foreach (['large', 'medium', 'small'] as $style) {
            copy($src, $folder . $style .'.jpg');
        }


}
}
