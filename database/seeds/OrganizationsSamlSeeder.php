<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\OrganizationBooking;
use Modules\Partnerapi\Domain\Models\OrganizationSaml;

class OrganizationsSamlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\Faker\Generator $generator)
    {
        $organization = Organization::query()->first();

        factory(OrganizationSaml::class)->create(['organization_id' => $organization]);

    }
}
