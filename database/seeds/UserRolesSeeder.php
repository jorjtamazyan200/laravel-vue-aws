<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $roles = Role::all();
        $count = $roles->count();

        $userRole = $roles->first(function ($role) {
            return $role->name == Role::ROLES['user'];
        });
        $adminRole = $roles->first(function ($role) {
            return $role->name == Role::ROLES['admin'];
        });
        $supervisorRole = $roles->first(function ($role) {
            return $role->name == Role::ROLES['supervisor'];
        });

        foreach ($users as $user) {

            if ($user->primary_role === Role::ROLES['coordinator']) {
                continue;
                }
            if ($user->primary_role === Role::ROLES['supervisor']) {
                $user->roles()->attach($supervisorRole);
                continue;
            }


            $user->roles()->attach($userRole);

            if ($user->email == 'user@volunteer-vision.com'){
                continue;
            }

            if (strrpos($user->email, '@volunteer-vision.com') !== false  ) {
                $user->roles()->attach($adminRole);
                continue;
            }

            if ($user->email === 'supervisor@volunteer-vision.com') {
                $user->roles()->attach($supervisorRole);
                continue;
            }
            $role = $roles->get($user->id % $count);
            if ($role->name != Role::ROLES['user']) {
                $user->roles()->attach($role);
            }

        }
    }
}
