<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\Webinar;

use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class TestingUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//

        $user = factory(User::class)->create([
            'first_name' => 'Community',
            'last_name' => 'Member',
            'email' => 'community@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
            'avatar' => 'sample.png'
        ]);
        UsersTableSeeder::copySampleAvatar($user->id);

        $email = 'client@volunteer-vision.com';
        $user = factory(User::class)->create([
            'email' => $email,
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
        ]);
        UsersTableSeeder::copySampleAvatar($user->id);
        // Important: Other seeders rely on this user to exist!

    }


}
