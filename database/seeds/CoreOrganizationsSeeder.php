<?php


use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;

class CoreOrganizationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $organization = factory(Organization::class)->create([
            'name' => 'Volunteer Vision',
            'type' => 'company',
        ]);


        // Create a Group within the Organizaton
        factory(Group::class)->create([
            'name' => 'Employees',
            'organization_id' => $organization->id,
        ])->first();



    }
}
