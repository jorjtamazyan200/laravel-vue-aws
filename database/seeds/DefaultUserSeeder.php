<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Disable Model lifecycle events during seeding
        Match::flushEventListeners();


        // ----------------------------------------
        // Manager user
        // ----------------------------------------
        /** @var User $manager */
        $manager = factory(User::class)->create([
            'email' => 'manager@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
            'first_name' => 'Michael',
            'last_name' => 'Manager',
            'primary_role' => Role::ROLES['manager']
        ]);

        $role = Role::where('name', '=', Role::ROLES['manager'])->firstOrFail();
        $manager->roles()->attach($role->id);

        // ----------------------------------------
        // Coordinator
        // ----------------------------------------

        $role = Role::where('name', '=', Role::ROLES['coordinator'])->firstOrFail();



        for ($i = 1; $i < 20; $i++) {
            /** @var User $manager */
            $manager = factory(User::class)->create([
                'email' => 'coordinator' . $i . '@volunteeringmatters.org.uk',
                'password' => bcrypt('start123'),
                'organization_id' => 1,
                'brand_id' => 1,
                'last_name' => 'Coordinator - ' . $i,
                'primary_role' => Role::ROLES['coordinator']
            ]);
            $manager->roles()->attach($role->id);
        }



    }

}
