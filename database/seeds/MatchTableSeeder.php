<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class MatchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable Model lifecycle events during seeding
        Match::flushEventListeners();

        $primaryUser = $this->generateUserWithEnrollment('client@volunteer-vision.com', 'mentor');

        $user2 = $this->generateUserWithEnrollment('example1@volunteer-vision.com', 'mentee'); // user has an unconfirmed match
        $user3 = $this->generateUserWithEnrollment('example2@volunteer-vision.com', 'mentee');
        $user4 = $this->generateUserWithEnrollment('example3@volunteer-vision.com', 'mentee');

        // incomplete, has no match
        factory(Participation::class)->create([
            'enrollment_id' => $primaryUser['enrollment']->id,
            'match_id' => null,
            'match_confirmed_at' => null
        ]);

        // ----------------------------------------------------------------
        // Participation 2 : matched, but confonrimed
        // ----------------------------------------------------------------
        $match = factory(Match::class)->create(
            [
                'program_id' => 1
                //'state' => 'UNCONFIRMED' // @todo: add state= ACTIVE, if statemachine is available;
            ]
        // @todo: add state= UNCONFIRMED, if statemachine is available;
        );

        factory(Participation::class)->create([
            'enrollment_id' => $primaryUser['enrollment']->id,
            'match_id' => $match->id,
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $user2['enrollment']->id,
            'match_id' => $match->id,
        ]);

        // ----------------------------------------------------------------
        // Participation 3 : matched, active, has no appointments
        // ----------------------------------------------------------------
        $match = factory(Match::class)->create(
            ['program_id' => 1,
                //'state' => 'ACTIVE' // @todo: add state= ACTIVE, if statemachine is available;
                ]
        );

        factory(Participation::class)->create([
            'enrollment_id' => $primaryUser['enrollment']->id,
            'match_id' => $match->id,
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $user3['enrollment']->id,
            'match_id' => $match->id,
        ]);

        // ----------------------------------------------------------------
        // Participation 4 : matched, active, has appointments and feedbacks
        // ----------------------------------------------------------------
        $match = factory(Match::class)->create(
            ['program_id' => 1]
            // @todo: add state= ACTIVE, if statemachine is available;
        );

        factory(Participation::class)->create([
            'enrollment_id' => $primaryUser['enrollment']->id,
            'match_id' => $match->id,
        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $user4['enrollment']->id,
            'match_id' => $match->id,
        ]);
        factory(Appointment::class, 20)->create(['match_id' => $match->id]);
    }

    /**
     * @param $email
     * @return array
     */
    private function generateUserWithEnrollment($email, $role): array
    {
        $tenant = Tenant::first();

        $user = factory(User::class)->create([
            'email' => $email,
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
            'tenant_id' => $tenant->id,
        ]);
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $user->id,
            'program_id' => 1,
            'role' => $role,
        ]);

        return ['user' => $user, 'enrollment' => $enrollment];
    }
}
