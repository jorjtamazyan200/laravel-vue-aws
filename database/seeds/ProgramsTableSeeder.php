<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Program;

class ProgramsTableSeeder extends Seeder
{

    /**
     * eSister (Leadership Mentoring | Women)
    eCareer (Professional Orientation | Young Adults)
    eAbility (Professional Orientation | Disabled People)
    eStart (Language Training | Refugees)
    eStudy (Study Mentoring | Refugees)
     */

    const PROGRAM_IDS_TO_SEED = [2,3,4,5];

    /**
     * Pre-defined Programs.
     *
     * @var array
     */
    private $programs = [
        [
            'code' => 'eSister',
            'language' => 'en',
            'title' => 'eSister',
            'description' => 'eSister is a career mentoring program for young women.',
            'conference_entry_url' => 'esister',
            'lections_total' => 6

        ],
        [
            'code' => 'eStartA1',
            'language' => 'de',
            'title' => 'eStart Sprachpatenprogram',
            'description' => 'eStart ist ein 1:1 Sprachpatenprogramm. ',
            'conference_entry_url' => 'estart'
        ],
        [
            'code' => 'eStartB1',
            'language' => 'de',
            'old_id'=>'4028329a53bd448c0153bd44ce660000',
            'title' => 'eStart B1 Sprachpatenprogram',
            'description' => 'eStart ist ein 1:1 Sprachpatenprogramm. ',
            'conference_entry_url' => 'estartb1'
        ],
        [
            'code' => 'eStudy',
            'language' => 'de',
            'title' => 'eStudy',
            'old_id'=> 'ff8080815876966301589bc18c0c0223',
            'description' => 'Studiy Mentoring for Refugees and Academics.',
            'conference_entry_url' => 'estudy'
        ],
        [
            'code' => 'eAbility',
            'language' => 'en',
            'title' => 'eAbility',
            'description' => 'eAbility is a mentoring program for young professionals with disabilities to (re-) enter the job market.',
            'conference_entry_url' => 'eability'

        ],
        [
            'code' => 'eCareer',
            'language' => 'en',
            'title' => 'eCareer',
            'description' => ' eCareer is a mentoring program for young adults under the care of SOS Children’s Villages.',
            'conference_entry_url' => 'ecareer'
        ]
    ];

    /**
     * Run the database seeds.
     *§
     * @return void
     */
    public function run()
    {

        $Parsedown = new Parsedown();

        foreach ($this->programs as $program) {
            $program = factory(Program::class)->create($program);
            $program->organizations()->attach(1);
            $content='Missing file';
            $markdown = 'Missing file';

            $markdownFile = __DIR__ . '/articles/programs/program_info_' . $program['code'] . '.md';
            if (file_exists($markdownFile)) {
                $markdown = file_get_contents($markdownFile);
                $content = $Parsedown->text($markdown);
            } else {
                echo 'Missing file ' . $markdownFile . PHP_EOL;
            }
            factory(BrandedDocument::class)->create([
                'key' => 'program_info',
                'type' => BrandedDocument::TYPES['article'],
                'content' => $content,
                'markdown' => $markdown,
                'program_id' => $program->id
            ]);

            $markdownFile = __DIR__ . '/articles/programs/registration_welcome' . $program['code'] . '.md';
            if (file_exists($markdownFile)) {
                $markdown = file_get_contents($markdownFile);
                $content = $Parsedown->text($markdown);
            } else {
                echo 'Missing file ' . $markdownFile . PHP_EOL;
            }
            factory(BrandedDocument::class)->create([
                'key' => 'registration_welcome',
                'type' => BrandedDocument::TYPES['article'],
                'content' => $content,
                'markdown' => $markdown,
                'program_id' => $program->id
            ]);


        }
    }
}
