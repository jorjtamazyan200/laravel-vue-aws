<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Program;

class ProgramPhotosSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *§
     * @return void
     */
    public function run()
    {

        $programs = Program::all();

        $programs->each(function ($program) {
            $this->copySamplePhotos($program);
        });

    }


    public static function copySamplePhotos(Program $program)
    {
        if (in_array(Config::get('app.env', false), ['local'])) {
            return;
        }


        $src = __DIR__ . '/sampledata/';
        $root = __DIR__ . '/../../storage/app/public/uploads/';

        $types = ['cover' => 'cover.jpg', 'detail_photo' => 'detail_photo.jpg', 'logo' => 'logo.png'];

        foreach ($types as $type => $file) {
            $path = $src . strtolower($program->code) . '/' . $file;
            echo '.';
            if (file_exists($path)) {
                $image = Image::make($path);
                $program->updateImageAttachment($type, $image->stream(), $image->mime);
            }
        }

        echo PHP_EOL;


    }

}
