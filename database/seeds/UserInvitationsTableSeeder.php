<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;

class UserInvitationsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /**
         * @var $invites Illuminate\Database\Eloquent\Collection
         * @var $invite UserInvitation
         * @var $invitedUser User
         *
         */


        $invitedUser = User::where('email','user@volunteer-vision.com')->firstOrFail();

        $invites = factory(UserInvitation::class, 5)->create(['inviting_user_id' => $invitedUser->id]);

        $invite = $invites->get(1);
        $invite->invited_user_id = User::inRandomOrder()->first()->id;
        $invite->state = UserInvitation::STATES['ACCEPTED'];
        $invite->save();

        $invite = $invites->get(2);
        $invite->state = UserInvitation::STATES['CLICKED'];
        $invite->save();



    }

}