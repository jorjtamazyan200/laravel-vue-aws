<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\OrganizationBooking;

class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\Faker\Generator $generator)
    {
        // Create a known Organization


        $citiesMentee = ['Lima','A'];
        $citiesMentor = ['New York','Municue'];
/*
 * SOS Standorte:
Jos
Benin
Arequipa
Lima
Cuzco
Irbid
Amman
Aqaba
Bogotá
Cali

Mentoren:
New York City
Birmingham
London
Washington
Munich
Berlin
Pune
Chicago
San Francisco
Madrid */

        // Create some more random Organizations
        factory(Organization::class, 10)->create()->each(function ($organization) use ($generator, $citiesMentee, $citiesMentor) {

            if ($organization->type == Organization::TYPES['socialservice']) {
                $name = 'Mentees: '.$generator->randomElement($citiesMentee);
            } else {
                $name = 'Mentors: - '.$generator->randomElement($citiesMentor);
            }

            // Create a Group within the Organizaton
            factory(Group::class)->create([
                'name' => $name,
                'organization_id' => $organization->id,
            ]);

        });
    }
}
