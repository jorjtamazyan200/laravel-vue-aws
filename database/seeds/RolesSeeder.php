<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $descriptions = [];

        $descriptions[Role::ROLES['user']] = 'General role which gives access to the platform.';
        $descriptions[Role::ROLES['supervisor']] = 'Supervisor	Has access to the supervisor screen.';
        $descriptions[Role::ROLES['manager']] = 'Manager	Has access to the reporting screens.';
        $descriptions[Role::ROLES['coordinator']] = 'Coordinator	Has access to the administration system.';
        $descriptions[Role::ROLES['ambassador']] = 'Special users who spread the word of digital volunteering';
        $descriptions[Role::ROLES['admin']] = 'Technical role to give technical support.';
        $descriptions[Role::ROLES['superadmin']] = 'Technical role to give technical support.';
        $descriptions[Role::ROLES['partnerapi']] = 'Partner API (e.g. kiron).';


        $roles = array_keys(Role::ROLES);
        foreach ($roles as $role) {
            factory(Role::class)->create([
                'name' => $role,
                'display_name' => ucfirst(strtolower($role)),
                'description' => (isset($descriptions[$role]) ? $descriptions[$role] : '')
            ]);

        }

    }
}
