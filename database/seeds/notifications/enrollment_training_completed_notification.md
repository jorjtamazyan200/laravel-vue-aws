{{$salutation}}

Congrats for completing the training!

We’re now working hard to find the best match for you and will get back to you as soon as possible. 

Please keep an eye out for notification of your match. Once matched, you will need to confirm your match and set up your first appointment. 

Kind regards,
Your Volunteer Vision Team