{{salutation}}

a few days ago, we informed you about your match. Please confirm whether you would like to accept your match. Please do so as soon as possible as the mentee is waiting for your reply.

**[Confirm your match here]({{data.responseLink}})** 

Thank you!

If you have any questions don't hesitate to get in touch.

Warm wishes,

Your Volunteer Vision Team