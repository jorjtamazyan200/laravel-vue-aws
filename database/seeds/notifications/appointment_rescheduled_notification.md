{{$salutation}}

Your mentor has rescheduled your next mentoring session.

**Your meeting will take place on {{data.date}} in the virtual conference room of our Online Mentoring Platform.**

If you have any questions about this, please don't hesitate to contact us.

Warm wishes,
Your Volunteer Vision Team