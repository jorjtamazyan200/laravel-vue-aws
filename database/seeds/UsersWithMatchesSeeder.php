<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class MatchTableSeeder
 *
 * Here we generate users + matches randomly but realistic;
 * This affects only users who have already an match.
 * For Users
 *
 */
class UsersWithMatchesSeeder extends Seeder
{

    /***
     * Todo: diese user müssen auch alle
     * an eine Training teilgenommen haben (genauso wie auch viele von denen Ohne Match)
     *
     */
    /**
     * @var \Faker\Generator
     */
    private $generator;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\Faker\Generator $generator)
    {

        // Disable Model lifecycle events during seeding
        Match::flushEventListeners();


        $this->generator = $generator;
        $this->preLoadData();


        $mentor = $user = factory(User::class)->create([
            'email' => 'mentor@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
            'first_name' => 'Max',
            'last_name' => 'Mentor',
            'primary_role' => 'mentor'
        ]);

        $mentee = $user = factory(User::class)->create([
            'email' => 'mentee@volunteer-vision.com',
            'first_name' => 'Jasmin',
            'last_name' => 'Mentee',
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
            'primary_role' => 'mentee'

        ]);
        $this->generateRealisticMatchForUsers($mentor, $mentee, 1, Match::STATES['ACTIVE']);


        if (Config::get('app.env', false) === 'production') {
            return;
        }

        $amount = 20;
        if (in_array(App::Environment(), ['local'])) {
            $amount = 5;
        }
        echo sprintf("[UsersWithMatchSeeder] seeding %i users", $amount) . PHP_EOL;


        $client = User::where('email', 'client@volunteer-vision.com')->firstOrFail();
        $userB = $this->generateUser(User::ROLES['mentee'], 1);
        $userC = $this->generateUser(User::ROLES['mentee'], 1);

        $this->generateRealisticMatchForUsers($client, $userB);
        $this->generateRealisticMatchForUsers($client, $userC);


        for ($i = 0; $i < $amount; $i++) {
            echo '.';
            $userA = $this->generateUser(User::ROLES['mentor'], 1);
            $userB = $this->generateUser(User::ROLES['mentee'], 1);
            $this->generateRealisticMatchForUsers($userA, $userB);
        }

        $tenant = Tenant::where('name', '=', 'VolunteeringMatters')->firstOrFail();

        for ($i = 0; $i < $amount; $i++) {
            echo '.';
            $userA = $this->generateUser(User::ROLES['mentor'], $tenant->id);
            $userB = $this->generateUser(User::ROLES['mentee'], $tenant->id);
            $this->generateRealisticMatchForUsers($userA, $userB);
        }

        echo PHP_EOL;


        factory(Availability::class)->create(['owner_id' => $mentee->id, 'owner_type' => 'User'], 5);


    }


    private $socialServices;
    private $companies;
    private $programs;

    private function preLoadData()
    {
        $this->socialServices = Organization::where('type', '=',
            Organization::TYPES['socialservice'])->with('groups')->get()->toArray();
        $this->companies = Organization::where('type', '=',
            Organization::TYPES['company'])->with('groups')->get()->toArray();
        $this->programs = Program::all()->toArray();
        $this->pastWebinars = Webinar::where('starts_at', '<', Carbon::now())->get()->toArray();


    }

    public function generateUser($role, $tenant_id = null): User
    {
        $clist = $role === User::ROLES['mentor'] ? $this->companies : $this->socialServices;
        $email = $role === User::ROLES['mentor'] ? $this->generator->companyEmail : $this->generator->email;

        $organizaton = $this->generator->randomElement($clist);


        /** @var User $user */
        $user = factory(User::class)->create([
            'email' => $this->generator->companyEmail,
            'password' => bcrypt('start123'),
            'organization_id' => $organizaton['id'],
            'primary_role' => User::ROLES['mentor'],
            'tenant_id' => $tenant_id,
            'brand_id' => 1,
        ]);


        factory(Availability::class)->create(['owner_id' => $user->id, 'owner_type' => 'User'], 5);


        if (isset($organizaton['groups']) && isset($organizaton['groups'][0])) {
            $user->groups()->attach($organizaton['groups'][0]['id']);
        } else {
            echo 'organization ' . $organizaton['name'] . ' ' . $organizaton['id'] . ' has no groups! ' . PHP_EOL;

        }


        UsersTableSeeder::copySampleAvatar($user->id);
        return $user;


    }

    public
    function generateRealisticMatchForUsers(
        User $userA,
        User $userB,
        $programId = null,
        $state = null
    ) {
        Match::flushEventListeners();

        if ($state === null) {

            $state = $this->generator->randomElement(array_keys(Match::STATES));
        }

        if ($programId == null) {
            $programId = $this->generator->randomElement($this->programs)['id'];
        }

        $webinarsA = $this->generator->randomElement($this->pastWebinars)['id'];
        $webinarsB = $this->generator->randomElement($this->pastWebinars)['id'];

        factory(Availability::class)->create([
            'owner_id' => $userA->id, 'owner_type' => User::class], 10);
        factory(Availability::class)->create([
            'owner_id' => $userB->id, 'owner_type' => User::class], 10);


        /**
         * @var $enrollmentA Enrollment
         * @var $enrollmentB Enrollment
         */
        $enrollmentA = factory(Enrollment::class)->create([
            'user_id' => $userA->id,
            'program_id' => $programId,
            'role' => $userA->primary_role,
            'state' => Enrollment::STATES['ACTIVE'],
        ]);
        $enrollmentB = factory(Enrollment::class)->create([
            'user_id' => $userB->id,
            'program_id' => $programId,
            'role' => $userB->primary_role,
            'state' => Enrollment::STATES['ACTIVE'],
        ]);
        $enrollmentB->webinars()->attach($webinarsA, ['attended' => true]);
        $enrollmentA->webinars()->attach($webinarsB, ['attended' => true]);


        $match = factory(Match::class)->create(
            [
                'program_id' => $programId,
                'state' => $state
            ]
        );

        factory(Participation::class)->create([
            'enrollment_id' => $enrollmentA->id,
            'match_id' => $match->id,

        ]);
        factory(Participation::class)->create([
            'enrollment_id' => $enrollmentB->id,
            'match_id' => $match->id,
        ]);
        $this->generateRealisticAppointments($match, $userA, $userB);
        return $match;
    }

    private function generateRealisticAppointments(Match $match, User $userA, User $userB)
    {

        $appointments = null;


        switch ($match->state) {
            case Match::STATES['REJECTED']:
            case Match::STATES['UNCONFIRMED']:
                break;
            case Match::STATES['PAUSED']:
            case Match::STATES['CANCELED']:
                $appointments = factory(Appointment::class, 8)->create(['match_id' => $match->id]);

                break;
            case Match::STATES['NEW']:
                $this->createFutureAppointment($match);
                break;
            case Match::STATES['ACTIVE']:
                $appointments = factory(Appointment::class, rand(5, 12))->create(['match_id' => $match->id]);
                $this->createFutureAppointment($match);
                break;
            case Match::STATES['DONE']:
                $amount = rand(5, 12);
                for ($i = 0; $i < $amount; $i++) {
                    $state = $this->generator->randomElement([
                            Appointment::STATES['SUCCESS'],
                            Appointment::STATES['SUCCESS'],
                            Appointment::STATES['SUCCESS'],
                            Appointment::STATES['NOSHOW']
                        ]
                    );
                    $time = Carbon::now()->subHours(rand(40, 54 * 24));
                    $duration = ($state === Appointment::STATES['SUCCESS'] ? rand(50, 72) : 0);
                    $appointments = factory(Appointment::class)->create(
                        [
                            'match_id' => $match->id,
                            'state' => $state,
                            'planned_start' => $time,
                            'actual_duration_minutes' => $duration
                        ]
                    );
                }


                break;

        }
        if ($appointments) {
            $appointments->each(function ($appointment) use ($userA, $userB) {
                $this->generateRealisticFeedbacksForAppointment($appointment, $userA, $userB);
            });
        }

    }

    private function createFutureAppointment(Match $match)
    {
        $time = Carbon::now()->addHours(rand(0, 24 * 10));

        return factory(Appointment::class)->create([
            'match_id' => $match->id,
            'planned_start' => $time,
            'state' => Appointment::STATES['PLANNED']
        ]);


    }

    /**
     * @param Appointment $appointment
     * @param User $userA
     * @param User $userB
     */
    private function generateRealisticFeedbacksForAppointment(Appointment $appointment, User $userA, User $userB)
    {
        if ($appointment->planned_start && $appointment->planned_start->isPast()) {
            factory(UserFeedback::class)->create(['user_id' => $userA->id, 'appointment_id' => $appointment->id], 3);
        }


    }
}

/**
 *
 * $user2 = $this->generateUserWithEnrollment('example1@volunteer-vision.com',
 * 'mentee'); // user has an unconfirmed match
 * $user3 = $this->generateUserWithEnrollment('example2@volunteer-vision.com', 'mentee');
 * $user4 = $this->generateUserWithEnrollment('example3@volunteer-vision.com', 'mentee');
 * $user5 = $this->generateUserWithEnrollment('example4@volunteer-vision.com', 'mentee');
 *
 *
 * // incomplete, has no match
 * factory(Participation::class)->create([
 * 'enrollment_id' => $primaryUser['enrollment']->id,
 * 'match_id' => null,
 * 'match_confirmed_at' => null
 * ]);
 *
 * // incomplete, has no match
 * factory(Participation::class)->create([
 * 'enrollment_id' => $enrollment2->id,
 * 'match_id' => null,
 * 'match_confirmed_at' => null
 * ]);
 *
 * // incomplete, has no match
 * factory(Participation::class)->create([
 * 'enrollment_id' => $primaryUser['enrollment']->id,
 * 'match_id' => null,
 * 'match_confirmed_at' => null
 * ]);
 *
 * // ----------------------------------------------------------------
 * // Participation 2 : matched, but not confirmed
 * // ----------------------------------------------------------------
 * $match = factory(Match::class)->create(
 * [
 * 'program_id' => 1,
 * 'state' => Match::STATES['UNCONFIRMED']
 *
 * ]
 *
 * );
 *
 * factory(Participation::class)->create([
 * 'enrollment_id' => $enrollment2->id,
 * 'match_id' => $match->id,
 *
 * ]);
 * factory(Participation::class)->create([
 * 'enrollment_id' => $user2['enrollment']->id,
 * 'match_id' => $match->id,
 *
 * ]);
 *
 * // ----------------------------------------------------------------
 * // Participation 3 : matched, active, has no appointments
 * // ----------------------------------------------------------------
 * $match = factory(Match::class)->create(
 * [
 * 'program_id' => 1,
 * 'state' => Match::STATES['ACTIVE']
 * ]
 *
 * );
 *
 * factory(Participation::class)->create([
 * 'enrollment_id' => $primaryUser['enrollment']->id,
 * 'match_id' => $match->id,
 *
 * ]);
 * factory(Participation::class)->create([
 * 'enrollment_id' => $user3['enrollment']->id,
 * 'match_id' => $match->id,
 * ]);
 *
 *
 * // ----------------------------------------------------------------
 * // Participation 4 : matched, active, has appointments and feedbacks
 * // ----------------------------------------------------------------
 * $match = factory(Match::class)->create(
 * [
 * 'program_id' => 1,
 * 'state' => Match::STATES['ACTIVE']
 * ]
 * );
 * factory(Participation::class)->create([
 * 'enrollment_id' => $primaryUser['enrollment']->id,
 * 'match_id' => $match->id,
 *
 * ]);
 * factory(Participation::class)->create([
 * 'enrollment_id' => $user4['enrollment']->id,
 * 'match_id' => $match->id,
 * ]);
 * factory(Appointment::class, 8)->create(['match_id' => $match->id]);
 *
 *
 * // ----------------------------------------------------------------
 * // Participation 5 : matched and done;
 * // ----------------------------------------------------------------
 * $match = factory(Match::class)->create(
 * [
 * 'program_id' => 1,
 * 'state' => Match::STATES['DONE']
 * ]
 * );
 *
 * factory(Participation::class)->create([
 * 'enrollment_id' => $primaryUser['enrollment']->id,
 * 'match_id' => $match->id,
 *
 * ]);
 * factory(Participation::class)->create([
 * 'enrollment_id' => $user5['enrollment']->id,
 * 'match_id' => $match->id,
 * ]);
 * factory(Appointment::class, 12)->create(['match_id' => $match->id]);
 *   $primaryUser['enrollment'] = factory(Enrollment::class)->create([
 * 'user_id' => $primaryUser['user']->id,
 * 'program_id' => 1,
 * 'role' => 'mentor',
 * 'state' => Enrollment::STATES['ACTIVE'],
 *
 * ]);
 *
 * $enrollment2 = factory(Enrollment::class)->create([
 * 'user_id' => $primaryUser['user']->id,
 * 'program_id' => 2,
 * 'role' => 'mentor',
 * 'state' => Enrollment::STATES['ACTIVE'],
 * ]);
 *
 * $enrollment3 = factory(Enrollment::class)->create([
 * 'user_id' => $primaryUser['user']->id,
 * 'program_id' => 2,
 * 'role' => 'mentor',
 * 'state' => Enrollment::STATES['ACTIVE'],
 * ]);
 *
 *
 * $enrollment4 = factory(Enrollment::class)->create([
 * 'user_id' => $primaryUser['user']->id,
 * 'program_id' => 2,
 * 'role' => 'mentor',
 * 'state' => Enrollment::STATES['TRAINING'],
 * ]);
 *
 *
 *
 */