<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\User;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\Exercise;
use Modules\TrainingCenter\Domain\Models\Lecturer;
use Modules\TrainingCenter\Domain\Models\Like;
use Modules\TrainingCenter\Domain\Models\Note;
use Modules\TrainingCenter\Domain\Models\Section;
use Modules\TrainingCenter\Domain\Models\Video;

class TrainingCenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = factory(Course::class, 5)->create();

        $lecturers = factory(Lecturer::class, 3)->create();

        $sections = factory(Section::class, 5)->create();

        $videos = factory(Video::class, 10)->create([
            'tc_section_id' => $sections[rand(0, 4)]->id
        ]);

        $exercises = factory(Exercise::class, 10)->create([
            'tc_section_id' => $sections[rand(0, 4)]->id
        ]);

        $notes = factory(Note::class, 10)->create([
            'tc_course_id' => $courses[rand(0, 4)]->id,
            'user_id' => User::inRandomOrder()->first()->id
        ]);

        $courseLike = new Like();
        $courseLike->likeable_id = $courses[0]->id;
        $courseLike->likeable_type = Course::class;
        $courseLike->user_id = User::inRandomOrder()->first()->id;
        $courseLike->save();


    }
}
