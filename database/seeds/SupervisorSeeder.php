<?php

use Illuminate\Database\Seeder;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;

class SupervisorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = Tenant::first();

        // Create a known user
        $user = factory(User::class)->create([
            'first_name' => 'Supervisor',
            'last_name' => 'Supervisor',
            'primary_role'=> Role::ROLES['supervisor'],
            'email' => 'supervisor@volunteer-vision.com',
            'password' => bcrypt('start123'),
            'organization_id' => 1,
            'brand_id' => 1,
            'tenant_id' => $tenant->id,
        ]);



        $groups = Group::take(3)->get();


        $groups->each(function($group) use ($user) {
            $user->groups()->attach($group->id, ['role'=>'supervisor']);
        });

    }
}
