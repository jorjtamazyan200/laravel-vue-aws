## §1 Parties to the contract

The use of the online service provided by Volunteer-Vision GmbH, Amalienstraße 87, 80799 München (hereinafter called “Volunteer Vision”) is based on the following terms of use (of service). Those terms refer to the service provided at https://portal.volunteer-vision.com/.

The use of the service provided by Volunteer Vision is based on these General Terms and Conditions. Any deviating agreements require to be in written form.

## §2 Scope of service

Volunteer Vision provides a Software-as-a-service platform for online mentoring.

Part of the service is the provision of learning materials and the option to video chat. The service is to be used only within the frame of the mentoring program of Volunteer Vision.

## §3 Registration

Using the Volunteer Vision online platform requires the registration and creation of a personal account. The user must truthfully and thoroughly submit all data requested in the registration form to Volunteer Vision. The user must be at least 18 years old and must have unlimited legal capacity at the time of registration. After the registration, the user`s account will be activated. The activation of the account forms an agreement for the use of services in accordance with the General Terms and Conditions.

There is no legal claim to use of the services of Volunteer Visions. Volunteer Vision is entitled to deny registration to any user without giving any reasons.

## §4 Duty to provide information

There are no extrajudicial proceedings concerning a complaint or a legal remedy against Volunteer Vision.

For further information about Volunteer Vision, the services provided, and the procedure refer to www.volunteer-vision.com.

For any technical steps regarding the conclusion of the agreement refer to §3 of the General Terms and Conditions.

The user can, at any time during registration, delete or rectify their entries. German and English are the only applicable languages on the platform.

## §5 Rights of use

The user’s only rights to the online offer are those specified in these General Terms and Conditions.

All contents, information, pictures, videos and databases published on the online platform of Volunteer Vision are generally protected by copyright and are usually property of or licensed by Volunteer Vision.

All contents on the online platform are to be used for personal purposes only. Commercial use and reproduction are not allowed. The sharing contents without Volunteer Vision`s explicit consent is prohibited.

## §6 User data

No personal data is collected, processed, transferred or used at the time of registration without the user`s consent. By entering data, the user agrees to the recording of their data. All personal data collected is only processed or used as far as necessary for executing the agreement and performing the service provided by Volunteer Vision. The statistical evaluation of anonymized data is part of the performance. It is guaranteed that it will not be possible to refer data back to any individual person.

The German Federal Data Protection Act (Bundesdatenschutzgesetz) protects the user`s right of accessing, rectifying and deleting personal data. To assert this right against Volunteer Vision, refer to the postal address mentioned above for mail service, or to info@volunteer-vision.com for email service.

All personal data is collected and processed solely on servers in Germany. Volunteer Vision is under the obligation to protect the privacy of all customers and assures that all personal data is collected, processed and used in accordance with the German Federal Data Protection Act (Bundesdatenschutzgesetz) and the German Telemedia Act (Telemediengesetz) and only for performing contractually defined purposes. All employees of Volunteer Vision are obligated to act in accordance with this standard.

## §7 Liability

Volunteer Vision is only liable for damages, particularly due to default, non-performance, defective performance or wrongful acts, in cases of violation of material contractual obligations which could notably be trusted to be performed. Apart from that, Volunteer Vision is not liable unless there are mandatory legal provisions. The exclusion of liability does not apply in cases of willful misconduct and gross negligence.

Volunteer Vision is only liable for losses which are reasonably foreseeable. Liability is excluded for indirect losses, particularly consequential losses caused by a defect, unforeseeable or untypical losses, as well as losses of profit. The same applies to any consequences of industrial conflicts, incidental losses and cases of force majeure.

The limitations of liability specified above apply to all contractual and non-contractual claims.

## §8 Users obligations

The user shall use the services provided in an appropriate manner only. Particularly the username and password shall be maintained a secret, shall not be transferred or made available to any third party, and a third party`s knowledge of it shall not be tolerated. The user shall take all necessary measures to ensure confidentiality and shall inform the company (Volunteer Vision), should any data be misused or lost or if any suspicion of misuse arises.

The user is obligated to accurately, thoroughly and truthfully present all data necessary to provide the services.

## §9 Termination / Blocking of access account

Should there be any suspicion of misuse or material breach of contract, Volunteer Vision reserves the right to look into that matter and take the appropriate actions and, should the suspicion prove to be justified, block the user`s access account. If the suspicion can be dispelled, the user`s access account will be unblocked. Otherwise, Volunteer Vision owns the extraordinary right to terminate the agreement.

## §10 Modifications

Volunteer Vision is entitled to modify the General Terms and Conditions for all users with effect for the future at any time.

Registered users will be informed of any intended modifications by email to the last known address. Any modification becomes effective, should the user not object within two weeks after the email has been sent. The date of sending the objection is decisive for the compliance with the two-week deadline.

Should the user object within the two-week deadline, Volunteer Vision has the right to extraordinarily terminate the contractual relationship without prior notice. The user will have no resulting claims against Volunteer Vision. If the user`s objection is effective and the contractual relationship continues, the previous General Terms and Conditions shall remain valid.

## §11 Severability clause

If any provision, or portion thereof, of these General Terms and Conditions, including this provision, is or becomes invalid, this shall not affect the validity of any other provision. In place of the invalid or missing provision, the respective legal regulations shall apply.