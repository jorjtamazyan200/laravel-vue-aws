# <span class="accented">JOIN</span> the eAbility Program


eAbility is a 1:1 mentoring program by employees with or without disabilities for young professionals with disabilities to (re-) enter the job market. 

The program consists of six, one-hour sessions in English or German where mentors and mentees get to know each other, reflect on the mentee’s previous experience, design a personal career plan, and prepare a job application. eAbility is perfect for mentors who are highly interested in diversity and equality matters, have 2+ years of work experience, ideally have previous HR and recruiting experience, and are fluent in English or German.