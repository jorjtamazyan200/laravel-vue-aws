<?php

use Illuminate\Database\Seeder;
use Modules\Admin\Domain\Models\Supportanswer;

class SupportAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a known user
        $answers = factory(Supportanswer::class, 20)->create([]);

        foreach ($answers as $answer) {

            $relation = new \Modules\Admin\Domain\Models\SupportanswerRelation();
            $relation->fill([
                'answer' => 'yes',
                'links_to' => 2,
                'class' => 'solution',
                'supportanswer_id' => $answer->id
            ]);
            $relation->save();


            $relation = new \Modules\Admin\Domain\Models\SupportanswerRelation();
            $relation->fill([
                'answer' => 'no',
                'class' => 'solution',
                'links_to' => 1,
                'supportanswer_id' => $answer->id
            ]);
            $relation->save();

        }


    }


}
