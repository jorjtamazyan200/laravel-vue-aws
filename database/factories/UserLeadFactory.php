<?php

use Faker\Generator as Faker;
use Illuminate\Database\Events\QueryExecuted;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserLead;

$factory->define(\Modules\Core\Domain\Models\UserLead::class, function (Faker $faker, $overrides) {
    static $password;

    $gender = $faker->randomElement(['male', 'female']);
    $first_name = $faker->firstName($gender);
    $last_name = $faker->unique()->lastName;

    return [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'email' => strtolower($first_name) . '.' . strtolower($last_name) . '@' . $faker->safeEmailDomain,
        'language' => $faker->randomElement(['en','de','es']),
        'state' => UserLead::STATES['NEW'],
        'registration_code_id' => \Modules\Core\Domain\Models\RegistrationCode::query()->inRandomOrder()->firstOrFail()->id,
    ];
});
