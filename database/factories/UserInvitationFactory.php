<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;

$factory->define(UserInvitation::class, function (Faker $faker) {
    return [
        'uid' => $faker->uuid(),
        'inviting_user_id' => User::inRandomOrder()->first()->id,
        'invited_user_email' => $faker->unique()->email,
        'invitation_sent_at' => new DateTime('NOW'),
        'state' => UserInvitation::STATES['PENDING'],
    ];
});
