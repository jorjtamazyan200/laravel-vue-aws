<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\User;

$factory->define(\Modules\TrainingCenter\Domain\Models\Note::class, function (Faker $faker) {
    return [
        'text' => $faker->paragraph,
        'tc_course_id' => factory(\Modules\TrainingCenter\Domain\Models\Course::class)->create()->id,
        'user_id' => User::inRandomOrder()->first()->id
    ];
});
