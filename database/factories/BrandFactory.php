<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Brand;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'name' => $faker->company(),
        'frontend_url' => 'https://app.staging.volunteer-vision.com',
        'code' => 'XXXX' . rand(0, 200),
        'tenant_id' => 1
    ];
});
