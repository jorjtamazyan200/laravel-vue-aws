<?php

use Faker\Generator as Faker;
use Modules\Community\Domain\Models\Post;
use Modules\Core\Domain\Models\User;
use Modules\Community\Domain\Models\PostComment;

$factory->define(PostComment::class, function (Faker $faker, $overrides) {
    $postId = isset($overrides['post_id']) ? $overrides['post_id'] : Post::inRandomOrder()->first()->id;
    $userId = isset($overrides['user_id']) ? $overrides['user_id'] : User::inRandomOrder()->first()->id;
    $parentCommentId = isset($overrides['parent_id']) ? $overrides['parent_id'] : null;

    return [
        'user_id' => $userId,
        'post_id' => $postId,
        'body' => 'Comment #' . $faker->randomNumber(),
        'parent_id' => $parentCommentId,
    ];
});
