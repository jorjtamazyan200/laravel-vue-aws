<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Models\Program;

$factory->define(Webinar::class, function (Faker $faker) {
    $body = $faker->paragraphs;
    $when = new \Carbon\Carbon($faker->dateTimeBetween('-1 year', $endDate = '+1 year')->format(DATE_ISO8601));
    if ($when->isFuture()) {
        $state = Webinar::STATES['PLANNED'];
    } else {
        $state = Webinar::STATES['SUCCESS'];
    }

    return [
        'title' => '',
        'description' => '',
        'login_link' => $faker->url,
        'phone_dial_in' => $faker->phoneNumber,
        'starts_at' => $when,
        'program_id' => Program::inRandomOrder()->first()->id,
        'duration_minutes' => $faker->randomElement([30, 45, 60, 95, 120]),
        'seats' => rand(2, 4) * 5,
        'state' => $state,
        'last_state_change_at' => new DateTime('NOW'),
        'tenant_id' => 1
    ];
});
