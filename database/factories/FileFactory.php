<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

$factory->define(File::class, function (Faker $faker) {
    $mimeTypes = ['application/pdf', 'image/png', 'image/jpeg', 'video/vimeo'];


    $lectionNumber = null;
    $roles = array_keys(User::ROLES);
    $placements = array_keys(File::PLACEMENTS);
    $placement = $faker->randomElement($placements);
    $programId = $faker->randomElement(ProgramsTableSeeder::PROGRAM_IDS_TO_SEED);
    $categories = File::CATEGORIES;

    switch ($placement){
        case FILE::PLACEMENTS['supervisor']:
            $roles = [Role::ROLES['supervisor']];
            break;
        case FILE::PLACEMENTS['embassador']:
            $roles = [Role::ROLES['mentor']];
            break;
        case FILE::PLACEMENTS['preparation']:
            $roles = [Role::ROLES['mentor'], Role::ROLES['mentor']];
            $lectionNumber = $num = $faker->numberBetween(0, 10);
            break;

        case FILE::PLACEMENTS['reporting']:
            $roles = [Role::ROLES['manager']];
            $lectionNumber = $num = $faker->numberBetween(0, 10);
            break;

    }


    return [
        'title' => $faker->sentence(4),
        'source' => $faker->url(),
        'target_audience' => $faker->randomElement($roles),

        'program_id' => $programId,
        'filesize' => $faker->numberBetween(1024*512, 1024*1024*10),
        'language' => $faker->randomElement(['de','en','es']),
        'order' => $faker->numberBetween(0, 100),
        'placement' => $faker->randomElement($placements),
        'lection_number' => $lectionNumber,
        'mime' => $faker->randomElement($mimeTypes),
        'category' => $faker->randomElement($categories)
    ];
});
