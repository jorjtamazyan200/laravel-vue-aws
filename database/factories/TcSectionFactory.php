<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\User;

$factory->define(\Modules\TrainingCenter\Domain\Models\Section::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->paragraph,
    ];
});
