<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;

$factory->define(Enrollment::class, function (Faker $faker) {

    return [
        'user_id' => User::inRandomOrder()->first()->id,
        'program_id' => Program::inRandomOrder()->first()->id,
        'role' => $faker->randomElement(['mentee', 'mentor']),
        'state' => Enrollment::STATES['NEW'],
        'last_state_change_at' => new DateTime('NOW'),
    ];
});
