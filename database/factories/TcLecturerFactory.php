<?php

use Faker\Generator as Faker;

$factory->define(\Modules\TrainingCenter\Domain\Models\Lecturer::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
        'email' => $faker->safeEmail,
        'avatar' => $faker->imageUrl(),
        'linkedin_link' => $faker->url,
    ];
});
