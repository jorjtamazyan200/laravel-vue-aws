<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;

$factory->define(Participation::class, function (Faker $faker) {
    return [
        'enrollment_id' => Enrollment::inRandomOrder()->first()->id,
    ];
});
