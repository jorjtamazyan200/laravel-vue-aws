<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\UserFeedback;

$factory->define(UserFeedback::class, function (Faker $faker) {


    $code = $faker->randomElement(array_keys(UserFeedback::CODES));
    $time = new \Carbon\Carbon();
    $time = $time->subHours(rand(1, 30 * 24));


    $data = [
        'created_at' => $time,
        'question_code' => $code,
    ];

    if (in_array($code, array_keys(UserFeedback::SCALAR_CODES))) {
        $data['response_scalar'] = $faker->numberBetween(4, 5);
    } else {
        $data['response_text'] = $faker->text(200);
        if ($code == UserFeedback::CODES['GENERAL_COMMENT']){
            $data['is_highlighted'] = true;
        }

    }

    return $data;

});
