<?php

use Faker\Generator as Faker;

$factory->define(\Modules\TrainingCenter\Domain\Models\Course::class, function (Faker $faker) {

    return [
        'title' => $faker->title,
        'description' => $faker->sentence,
        'thumbnail' => $faker->word,
        'views_count' => 1
    ];
});
