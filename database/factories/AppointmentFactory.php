<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

$factory->define(Appointment::class, function (Faker $faker, $overrides) {


    $time = Carbon::now()->addDays($faker->numberBetween(-20, 0));
    $state = null;
    $state = $faker->randomElement([
        Appointment::STATES['HANGUP'],
        Appointment::STATES['SUCCESS'],
        Appointment::STATES['SUCCESS'],
        Appointment::STATES['SUCCESS'],
        Appointment::STATES['SUCCESS'],
        Appointment::STATES['SUCCESS'],
        Appointment::STATES['SUCCESS'],
        Appointment::STATES['CANCELED']
    ]);



    $match_id = isset($overrides['match_id']) ?
        $overrides['match_id'] :
        Match::inRandomOrder()->first()->id;



    return [
        'planned_start' => $time->toDateTimeString(),
        'planned_duration_minutes' => ($state === Appointment::STATES['SUCCESS'] ? rand(40, 60) : 0),
        'match_id' => $match_id,
        'state' => $state,
        'last_state_change_at' => new DateTime('NOW'),
        'tenant_id' => 1
    ];
});
