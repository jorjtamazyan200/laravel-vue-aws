<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\BrandedDocument;

$factory->define(BrandedDocument::class, function (Faker $faker) {
    return [
        'key' => $faker->slug,
        'type' => $faker->randomElement(['article', 'sms', 'email']),
        'subject' => $faker->word,
        'content' => $faker->paragraph,
        'language' => 'en'
    ];
});
