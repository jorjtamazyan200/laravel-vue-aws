<?php

use App\Services\MultiTenant\Facades\MultiTenant;
use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\RegistrationCode;

$factory->define(RegistrationCode::class, function (Faker $faker) {
    $name = $faker->words(3);

    MultiTenant::setTenantId(1);

    return [
        'code' => join('-', $name),
        'name' => join(' ', $name),
        'primary_role' => $faker->randomElement(['mentee', 'mentor']),
        'brand_id' => Brand::inRandomOrder()->first()->id,
        'group_id' => Group::inRandomOrder()->first()->id,
//        'organization_id' => Organization::inRandomOrder()->first()->id,
        'tenant_id' => 1,
        'users_count_limit' => rand(4, 10) * 10,
        'users_count_planned' => rand(4, 10) * 10,
    ];
});
