<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Matching\Domain\Models\Match;

$factory->define(\Modules\Partnerapi\Domain\Models\OrganizationSaml::class, function (Faker $faker) {


    $registrationCode = RegistrationCode::inRandomOrder()->first();
    return [
        'registration_code_id' => $registrationCode->id,
        'login_redirect_uri' => 'http://localhost/auth',
        'entity_uri' =>  'http://localhost/metadata',
        'logout_redirect_uri' => 'http://localhost/logout',
        'slug' => 'sample',
        'email_pattern' => '@sample.sso',
        'certificate' => '',
        'certificate_fingerprint' => ''
    ];
});
