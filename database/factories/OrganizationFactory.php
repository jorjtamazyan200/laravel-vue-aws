<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Organization;

$factory->define(Organization::class, function (Faker $faker) {
    return [
        'name' => $faker->company(),
        'type' => $faker->randomElement(array_keys(Organization::TYPES)),
        'state' => Organization::STATES['ACTIVE'],
        'last_state_change_at' => new DateTime('NOW'),
        'tenant_id' => 1
    ];
});
