<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\User;

$factory->define(\Modules\TrainingCenter\Domain\Models\Exercise::class, function (Faker $faker) {
    return [
        'path' => 'path/to/exercise',
        'tc_section_id' => factory(\Modules\TrainingCenter\Domain\Models\Section::class)->create()->id,
    ];
});
