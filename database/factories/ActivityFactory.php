<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Activity;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

$factory->define(\Modules\Core\Domain\Models\Activity::class, function (Faker $faker) {

    return [
        'user_id' => 1,
        'variables'=> ['hello'=>'test', 'user'=> ['displayName' => 'test']],
        'name' => $faker->randomElement(array_keys(Activity::ACTIVITIES))
    ];
});
