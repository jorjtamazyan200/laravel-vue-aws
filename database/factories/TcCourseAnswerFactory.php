<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\User;

$factory->define(\Modules\TrainingCenter\Domain\Models\CourseAnswer::class, function (Faker $faker) {

    return [
        'text' => $faker->paragraph,
        'tc_course_question_id' => factory(\Modules\TrainingCenter\Domain\Models\CourseQuestion::class)->create()->id,
        'user_id' => User::inRandomOrder()->first()->id
    ];
});
