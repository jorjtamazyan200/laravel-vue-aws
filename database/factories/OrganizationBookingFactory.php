<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Program;
use Modules\Matching\Domain\Models\Match;

$factory->define(\Modules\Core\Domain\Models\OrganizationBooking::class, function (Faker $faker) {

    return [
        'organization_id'=> null, // required
        'program_id' => Program::inRandomOrder()->first()->id,
        'order_number' => 'order' . rand(10,20),
        'licences' => rand(1,20) * 5,
        'order_date'=> Carbon::now()

    ];
});
