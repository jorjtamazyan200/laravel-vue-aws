<?php

use Faker\Generator as Faker;
use Modules\Core\Domain\Models\User;

$factory->define(\Modules\TrainingCenter\Domain\Models\Video::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->paragraph,
        'order' => 0,
        'video_id' => 5,
        'video_type' => 'vimeo', // this can be vimeo or youtube?
        'duration' => '01:00:00',
//        'path' => 'path/to/video'
    ];
});
