<?php

use Faker\Generator as Faker;
use Illuminate\Database\Events\QueryExecuted;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\User;

$factory->define(User::class, function (Faker $faker, $overrides) {
    static $password;

    $gender = $faker->randomElement(['male', 'female']);
    $first_name = $faker->firstName($gender);
    $last_name = $faker->unique()->lastName;

    $serviceLevel = $faker->randomElement(array_map(function ($b) {
        return $b;
    }, USER::SERVICELEVELS));
    $points = $faker->numberBetween(0, 250);

    $tenant_id = isset($overrides['tenant_id']) ? $overrides['tenant_id'] : 1;

    $organization_id = isset($overrides['organization_id']) ?
        $overrides['organization_id'] :
        Organization::inRandomOrder()->first()->id;




    return [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'email' => strtolower($first_name) . '.' . strtolower($last_name) . '@' . $faker->safeEmailDomain,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'organization_id' => $organization_id,
        'brand_id' => 1, // Brand::inRandomOrder()->first()->id,
        'color_code' => $faker->safeHexColor,
        'birthday' => $faker->date,
        'phone_number' => $faker->phoneNumber,
        'servicelevel' => $serviceLevel,
        'whatsapp_number' => $faker->phoneNumber,
        'gender' => $gender,
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'lat' =>$faker->latitude,
        'lng' =>$faker->longitude,

        'postcode' => $faker->postcode,
        'level' => ceil($points / 100),
        'points' => $points,
        'country' => $faker->countryCode,
        'country_of_origin' => $faker->countryCode,
        'state' => User::STATES['REGISTERED'],
        'last_state_change_at' => new DateTime('NOW'),
        'count_for_reporting' => true,
        'tenant_id' => $tenant_id
    ];
});
