<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Modules\Core\Domain\Models\Program;
use Modules\Matching\Domain\Models\Match;

$factory->define(Match::class, function (Faker $faker) {
    return [
        'program_id' => Program::inRandomOrder()->first()->id,
        'state' => Match::STATES['NEW'],
        'external_room_id' => null,
        'licence_activated'=> $faker->dateTime,
        'last_state_change_at' => new DateTime('NOW'),
    ];
});
