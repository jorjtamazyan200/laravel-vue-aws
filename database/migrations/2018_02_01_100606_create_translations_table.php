<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id')->nullable();
            $table->timestamps();
            $table->string('locale', 2);
            $table->string('scope');
            $table->jsonb('json_value');
            $table->unique(['locale', 'scope']);
        });

        $fallbackLang = 'en';
        $storedProcedure = " 
    CREATE OR REPLACE FUNCTION language_json(searchscope text, searchlang text) returns jsonb
language plpgsql
as $$
DECLARE
  fallback jsonb;
  locallang jsonb;

BEGIN
  SELECT json_value INTO fallback FROM translations WHERE scope = searchscope AND lower(locale) = '" . $fallbackLang . "';
  
  SELECT json_value INTO locallang FROM translations WHERE scope = searchscope AND lower(locale) =  lower(searchlang);
  IF NOT FOUND THEN
      RETURN fallback;
  END IF;
  
  
  RETURN fallback || locallang ;

END;
$$; ";
        DB::unprepared($storedProcedure);

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('translations');
        DB::unprepared('DROP FUNCTION IF EXISTS language_json(text, text)');

    }
}
