<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToUserlaunchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        // composer require whichbrowser/parser
        Schema::table('user_launches', function (Blueprint $table) {
            $table->string('client_type')->nullable();
            $table->string('client_os')->nullable();
            $table->string('client_version')->nullable();
            $table->string('client_browser')->nullable();
            $table->string('client_engine')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_launches', function (Blueprint $table) {
            $table->dropColumn('client_type');
            $table->dropColumn('client_os');
            $table->dropColumn('client_version');
            $table->dropColumn('client_browser');
            $table->dropColumn('client_engine');
        });

    }
}
