<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegistrationCodeReferenceToUserLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_leads', function (Blueprint $table) {
            $table->string('state')->default('NEW')->change();
            $table->string('code')->nullable()->change();
            $table->string('language')->nullable();
            $table->integer('registration_code_id')->unsigned()->nullable();
            $table->foreign('registration_code_id')->references('id')->on('registration_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_leads', function (Blueprint $table) {
            $table->dropColumn('registration_code_id');
            $table->dropColumn('language');
        });
    }
}
