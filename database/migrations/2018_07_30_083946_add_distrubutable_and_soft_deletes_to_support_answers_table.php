<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistrubutableAndSoftDeletesToSupportAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supportanswers', function (Blueprint $table) {
            $table->boolean('distribute_to_tenants')->default(false);
            $table->integer('distributor_source_id')->unsigned()->nullable();
            $table->softDeletes();
        });
        Schema::table('supportanswer_relations', function (Blueprint $table) {
            $table->boolean('distribute_to_tenants')->default(false);
            $table->integer('distributor_source_id')->unsigned()->nullable();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supportanswers', function (Blueprint $table) {
            $table->dropColumn('distribute_to_tenants');
            $table->dropColumn('distributor_source_id');
            $table->dropSoftDeletes();
        });
        Schema::table('supportanswer_relations', function (Blueprint $table) {
            $table->dropColumn('distribute_to_tenants');
            $table->dropColumn('distributor_source_id');
            $table->dropSoftDeletes();
        });
    }
}
