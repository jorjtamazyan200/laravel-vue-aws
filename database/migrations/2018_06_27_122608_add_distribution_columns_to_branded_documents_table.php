<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistributionColumnsToBrandedDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branded_documents', function (Blueprint $table) {
            $table->boolean('distribute_to_tenants')->default(false);
            $table->integer('distributor_source_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branded_documents', function (Blueprint $table) {
            $table->dropColumn('distribute_to_tenants');
            $table->dropColumn('distributor_source_id');
        });
    }
}
