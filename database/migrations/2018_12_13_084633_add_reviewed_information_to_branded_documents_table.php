<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewedInformationToBrandedDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branded_documents', function (Blueprint $table) {
            $table->integer('reviewer_id')->nullable();
            $table->dateTime('reviewed_at')->nullable();
            $table->foreign('reviewer_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branded_documents', function (Blueprint $table) {
            $table->dropColumn('reviewer_id');
            $table->dropColumn('reviewed_at');
            $table->foreign('reviewer_id')->references('id')->on('users');
        });
    }
}
