<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMangementInformationColumnsToGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->dateTime('last_report')->nullable();
            $table->boolean('has_availability')->default(false);
            $table->string('scheduling_comment', 1024)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->dropColumn('last_report');
            $table->dropColumn('has_availability');
            $table->dropColumn('scheduling_comment');

        });
    }
}
