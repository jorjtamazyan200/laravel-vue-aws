<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueConstraintToEnrollmentWebinar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('enrollment_webinar', function (Blueprint $table) {
            $table->unique(['enrollment_id','webinar_id']);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollment_webinar', function (Blueprint $table) {
            $table->dropUnique(['enrollment_id','webinar_id']);
        });

    }
}
