<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFileDetailsToFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->string('mime')->nullable();
            $table->integer('filesize', false, true)->nullable();
            $table->string('language', 2)->default('en');
            $table->string('cover')->nullable();

            $table->string('placement')->nullable();

            $table->integer('order')->default(100);
            $table->integer('lection_number')->nullable();

            $table->integer('program_id')->nullable()->change();
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands');

            $table->dropColumn('target_audience');
        });
        Schema::table('files', function (Blueprint $table) {
            $table->string('target_audience');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('mime');
            $table->dropColumn('language');
            $table->dropColumn('filesize');
            $table->dropColumn('placement');
            $table->dropColumn('cover');
            $table->dropColumn('order');

            $table->dropColumn('lection_number');
            $table->dropColumn('brand_id');

            $table->enum('target_audience', [
                'csr',
                'supervisor',
                'mentor',
                'mentee'
            ])->change();
        });
    }
}
