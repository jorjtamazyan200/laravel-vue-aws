<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchingLogicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matching_scores', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('mentor_participation_id')->nullable();
            $table->foreign('mentor_participation_id')->references('id')->on('participations');

            $table->integer('mentee_participation_id')->nullable();
            $table->foreign('mentee_participation_id')->references('id')->on('participations');

            $table->integer('program_id')->nullable();
            $table->foreign('program_id')->references('id')->on('programs');

            $table->float('algorithm_value');
            $table->integer('time_overlap');
            $table->integer('timezone_difference');
            $table->integer('distance')->nullable();

            $table->string('notes')->nullable();
            $table->string('algorithm');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matching_scores');
    }
}
