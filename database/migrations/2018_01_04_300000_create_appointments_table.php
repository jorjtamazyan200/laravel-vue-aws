<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('match_id')->unsigned();
            $table->foreign('match_id')->references('id')->on('matches');

            $table->dateTime('planned_start');
            $table->integer('planned_duration_minutes')->default(60);
            $table->dateTime('actual_start')->nullable();
            $table->integer('actual_duration_minutes')->nullable();

            $table->dateTime('last_notification_at')->nullable();

            $table->string('state')->default('NEW');
            $table->dateTime('last_state_change_at')->default(Carbon::now());

            $table->integer('noshow_reportee_id')->unsigned()->nullable();
            $table->foreign('noshow_reportee_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
