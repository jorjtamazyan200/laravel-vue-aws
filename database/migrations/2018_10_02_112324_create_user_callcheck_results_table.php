<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCallcheckResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_callcheck_results', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('success')->nullable();
            $table->float('download_speed')->nullable();
            $table->float('upload_speed')->nullable();
            $table->float('mos')->nullable();
            $table->string('useragent')->nullable();
            $table->json('meta')->nullable();

            $table->integer('user_id')->unsigned()->nullable();

            $table->foreign('user_id')
                ->references('id')->on('users');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_callcheck_results');
    }
}
