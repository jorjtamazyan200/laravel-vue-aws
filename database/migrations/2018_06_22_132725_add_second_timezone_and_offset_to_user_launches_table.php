<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSecondTimezoneAndOffsetToUserLaunchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_launches', function (Blueprint $table) {
            $table->string('timezone2')->nullable();
            $table->integer('offset')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_launches', function (Blueprint $table) {
            $table->dropColumn('timezone2');
            $table->dropColumn('offset');
        });
    }
}
