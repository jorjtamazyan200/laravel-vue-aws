<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTcCourseFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tc_course_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('rating');
            $table->text('text')->nullable();
            $table->unsignedInteger('tc_course_id');
            $table->foreign('tc_course_id')->on('tc_courses')->references('id')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tc_course_feedbacks');
    }
}
