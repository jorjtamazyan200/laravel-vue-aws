<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTcCourseProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tc_course_program', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('program_id');
            $table->foreign('program_id')->on('programs')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('tc_course_id');
            $table->foreign('tc_course_id')->on('tc_courses')->references('id')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tc_course_program');
    }
}
