<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTcQuizQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tc_quiz_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->enum('type', ['single_choice', 'multiple_choice'])->default('single_choice');
            $table->unsignedBigInteger('tc_section_id');
            $table->foreign('tc_section_id')->on('tc_sections')->references('id')->onDleete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tc_quiz_questions');
    }
}
