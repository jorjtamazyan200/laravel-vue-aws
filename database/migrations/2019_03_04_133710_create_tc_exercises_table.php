<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTcExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tc_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path')->nullable();
            $table->unsignedBigInteger('tc_section_id');
            $table->foreign('tc_section_id')->on('tc_sections')->references('id')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tc_exercises');
    }
}
