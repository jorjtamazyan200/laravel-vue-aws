<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorCountersInRegistrationCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registration_codes', function (Blueprint $table) {

            // similar to users_count_limit
            $table->renameColumn('planned_users_count','users_count_planned');
            $table->string('internal_comment', 1024)->change();
            $table->string('external_comment', 1024)->change();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registration_codes', function (Blueprint $table) {
            $table->renameColumn('users_count_planned','planned_users_count');
            $table->string('internal_comment', 255)->change();
            $table->string('external_comment', 255)->change();
        });
    }
}
