<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupToRegistrationCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('registration_codes', function (Blueprint $table) {
            $table->integer('group_id')->nullable()->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->dropColumn('organization_id');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('registration_codes', function (Blueprint $table) {
            $table->dropColumn('group_id');
            $table->integer('organization_id')->nullable()->unsigned();
            $table->foreign('organization_id')->references('id')->on('organizations');
        });
    }
}
