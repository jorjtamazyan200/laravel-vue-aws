<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTcQuizAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tc_quiz_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->enum('is_correct', [0, 1])->default(0)->comment('0: incorrect, 1: correct');
            $table->unsignedBigInteger('tc_quiz_question_id');
            $table->foreign('tc_quiz_question_id')->on('tc_quiz_questions')->references('id')->onDleete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tc_quiz_answers');
    }
}
