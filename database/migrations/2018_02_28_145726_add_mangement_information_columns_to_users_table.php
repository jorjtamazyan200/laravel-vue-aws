<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMangementInformationColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $servicelevels = ['noservice', 'normal', 'premium', 'priority']; // @todo: where to place this as enum?

        Schema::table('users', function (Blueprint $table) use ($servicelevels) {
            $table->string('color_code')->nullable();

            $table->enum('servicelevel', $servicelevels)->default('normal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('color_code');
            $table->dropColumn('servicelevel');
        });
    }
}
