<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMangementInformationColumnsToWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webinars', function (Blueprint $table) {
            $table->integer('referent_id')->unsigned()->nullable();
            $table->foreign('referent_id')->references('id')->on('users');
            $table->string('login_link')->nullable();
            $table->string('phone_dial_in')->nullable();
            $table->string('invitation_note')->nullable();
            $table->integer('seats')->default(0);
            $table->integer('organization_id')->unsigned()->nullable();
            $table->foreign('organization_id')->references('id')->on('organizations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webinars', function (Blueprint $table) {
            $table->dropColumn('referent_id');
            $table->dropColumn('login_link');
            $table->dropColumn('phone_dial_in');
            $table->dropColumn('invitation_note');
            $table->dropColumn('seats');
            $table->dropColumn('organization_id');
        });
    }
}
