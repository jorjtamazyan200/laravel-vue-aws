<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDailyReportsToGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->dateTime('report_weekly_sent_at')->nullable();
            $table->dateTime('report_daily_sent_at')->nullable();
            $table->dropColumn('last_report');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->dropColumn('report_weekly_sent_at');
            $table->dropColumn('report_daily_sent_at');
            $table->dateTime('last_report')->nullable();
        });
    }
}
