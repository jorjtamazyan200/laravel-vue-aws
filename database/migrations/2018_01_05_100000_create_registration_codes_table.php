<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('code')->unique();
            $table->string('name')->nullable();
            $table->enum('primary_role', [
                'mentee', 'mentor', 'supervisor', 'manager', 'admin'
            ])->default('mentee');
            $table->string('internal_comment')->nullable();
            $table->string('external_comment')->nullable();
            $table->jsonb('settings')->nullable();
            $table->integer('users_count_limit')->nullable();
            $table->integer('planned_users_count')->nullable();

            $table->integer('auto_enroll_in')->unsigned()->nullable();
            $table->foreign('auto_enroll_in')->references('id')->on('programs');

            $table->integer('brand_id')->unsigned();
            $table->foreign('brand_id')->references('id')->on('brands');

            $table->integer('organization_id')->unsigned();
            $table->foreign('organization_id')->references('id')->on('organizations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_codes');
    }
}
