<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemindAgainToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dateTime('last_manual_interaction_at')->nullable();
            $table->dateTime('remind_again_at')->nullable();
            $table->string('remind_comment')->nullable();
        });
        Schema::table('matches', function (Blueprint $table) {
            $table->dateTime('remind_again_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_manual_interaction_at');
            $table->dropColumn('remind_comment');
            $table->dropColumn('remind_again_at');
        });
        Schema::table('matches', function (Blueprint $table) {
            $table->dropColumn('remind_again_at');
        });
    }
}
