<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportAnswersRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('supportanswer_relation');
        Schema::dropIfExists('supportanswer_relations');

        Schema::create('supportanswer_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('answer');
            $table->string('class');
            $table->integer('supportanswer_id')->unsigned();
            $table->integer('links_to')->unsigned();
            $table->timestamps();

            $table->foreign('supportanswer_id')
                ->references('id')->on('supportanswers');
            $table->foreign('links_to')
                ->references('id')->on('supportanswers');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supportanswer_relations');
    }
}
