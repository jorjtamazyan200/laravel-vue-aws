<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastReminderToMultipleTables extends Migration
{

    protected $tables = ['matches', 'enrollments', 'user_leads'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        foreach ($this->tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->integer('last_reminder_type')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('last_reminder_type');
            });
        }
    }
}
