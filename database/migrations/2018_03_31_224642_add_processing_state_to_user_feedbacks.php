<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessingStateToUserFeedbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_feedbacks', function (Blueprint $table) {
            $table->string('processing_state')->default(\Modules\Core\Domain\Models\UserFeedback::STATES['NEW']);
            $table->integer('appointment_id')->nullable()->unsigned();
            $table->foreign('appointment_id')->references('id')->on('appointments');
            $table->boolean('is_highlighted')->default(false);
            $table->dropColumn('match_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_feedbacks', function (Blueprint $table) {
            $table->dropColumn('processing_state');
            $table->dropColumn('appointment_id');
            $table->dropColumn('is_highlighted');

            $table->integer('match_id')->nullable()->unsigned();
            $table->foreign('match_id')->references('id')->on('matches');



        });
    }
}
