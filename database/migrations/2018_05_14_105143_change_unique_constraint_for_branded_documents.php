<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUniqueConstraintForBrandedDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $oldIndizies =
            [
                'branded_documents_heavy_unique',
                'branded_documents_key_type_language_brand_id_unique',
                'branded_documents_key_type_language_brand_id_unique',
                'branded_documents_key_type_language_unique'
            ];

        $baseFields = ['key', 'type', 'language'];
        $optionalFields = ['brand_id', 'program_id', 'audience'];


//        Schema::table('branded_documents', function (Blueprint $table) {
//            $table->dropUnique(['key','type','language']);
//        });
//
//        Schema::table('branded_documents', function (Blueprint $table) {
//            $table->unique(['key','type','language','program_id','brand_id','audience']);
//        });
        foreach ($oldIndizies as $index) {
            $sql = 'DROP INDEX IF EXISTS ' . $index;
            DB::statement($sql);
        }

        $sql = [];

        $sql[] = 'CREATE UNIQUE INDEX branded_documents_heavy_unique ON branded_documents (key, type, language, brand_id, program_id)
            WHERE brand_id IS NOT NULL AND program_id IS NOT NULL;';
        // @todo:
//
//
//        foreach ($optionalFields as $f1) {
//            foreach ($optionalFields as $f2) {
//                foreach ($optionalFields as $f3) {
//                $sql[] = sprintf('%s NULL %s NULL %s NOT NULL', $f1, $f2, $f3, 'A');
//
////                        $sql[] = sprintf('CREATE UNIQUE INDEX branded_documents_key_type_language%s_%s ON branded_documents ' .
////                            ' WHERE %s IS NOT NULL AND %s IS NOT NULL AND %s IS NULL', $f1, $f2, $f1, $f2, 'A');
//
//                }
//            }
//        }
//        print_r($sql);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

//        Schema::table('branded_documents', function (Blueprint $table) {
//            $table->dropUnique(['key','type','language','audience']);
//        });
//        Schema::table('branded_documents', function (Blueprint $table) {
//            $table->unique(['key','type','language']);
//        });


        $first_patial_index = 'CREATE UNIQUE INDEX IF NOT EXISTS branded_documents_heavy_unique ON branded_documents
    (key, type, language, brand_id, program_id)
            WHERE brand_id IS NOT NULL AND program_id IS NOT NULL;';

        // key, type, language, brand_id
        $second_patial_index = 'CREATE UNIQUE INDEX IF NOT EXISTS branded_documents_key_type_language_brand_id_unique ON branded_documents
    (key, type, language, brand_id)
            WHERE brand_id IS NOT NULL AND program_id IS NULL;';

        // key, type, language, program_id
        $third_patial_index = 'CREATE UNIQUE INDEX IF NOT EXISTS branded_documents_key_type_language_program_id_unique ON branded_documents
    (key, type, language, program_id)
            WHERE program_id IS NOT NULL AND brand_id IS NULL;';

        // key, type, language
        $fourth_patial_index = 'CREATE UNIQUE INDEX IF NOT EXISTS branded_documents_key_type_language_unique ON branded_documents
    (key, type, language)
            WHERE program_id IS NULL AND brand_id IS NULL;';

        DB::statement($first_patial_index);
        DB::statement($second_patial_index);
        DB::statement($third_patial_index);
        DB::statement($fourth_patial_index);


    }
}
