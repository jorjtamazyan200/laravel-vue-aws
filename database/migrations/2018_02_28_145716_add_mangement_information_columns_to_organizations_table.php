<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMangementInformationColumnsToOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $servicelevels = ['noservice', 'normal', 'premium', 'priority'];

        Schema::table('organizations', function (Blueprint $table) use ($servicelevels) {
            $table->integer('keyaccount_id')->unsigned()->nullable();
            $table->foreign('keyaccount_id')->references('id')->on('users');
            $table->enum('servicelevel', $servicelevels)->default('normal');
            $table->boolean('technical_webinar_check')->default(false);
            $table->boolean('technical_guest_wifi_check')->default(false);
            $table->boolean('technical_company_network_check')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->dropColumn('servicelevel');
            $table->dropColumn('keyaccount_id');
            $table->dropColumn('technical_webinar_check');
            $table->dropColumn('technical_guest_wifi_check');
            $table->dropColumn('technical_company_network_check');
        });
    }
}
