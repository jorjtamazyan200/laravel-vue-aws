<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebinarStateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinar_state_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('webinar_id');
            $table->foreign('webinar_id')->references('id')->on('webinars');
            $table->integer('actor_id')->nullable();
            $table->foreign('actor_id')->references('id')->on('users');

            $table->string('transition');
            $table->string('from');
            $table->string('to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinar_state_logs');
    }
}
