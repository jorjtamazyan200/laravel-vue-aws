<?php

namespace Modules\Scheduling\Http\Requests;

use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class CreateAppointmentRequest extends AbstractAppointmentRequest
{
    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        $match = Match::findOrFail(request()->id);

        return $this->user()->can('create', [Appointment::class, $match]);
    }
}
