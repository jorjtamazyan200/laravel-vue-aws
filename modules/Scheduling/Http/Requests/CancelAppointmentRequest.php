<?php

namespace Modules\Scheduling\Http\Requests;

class CancelAppointmentRequest extends UpdateAppointmentRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'comment' => 'required|string',
        ];
    }
}
