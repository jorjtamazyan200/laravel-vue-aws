<?php

namespace Modules\Scheduling\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class AbstractAppointmentRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'planned_start' => 'required|date|after:now',
            'planned_duration_minutes' => 'sometimes|integer|in:15,30,45,60,75,90,105,120',
        ];
    }
}
