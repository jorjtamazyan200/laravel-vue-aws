<?php

namespace Modules\Scheduling\Http\Requests;

class FinishAppointmentRequest extends UpdateAppointmentRequest
{

    /**
     * {@inheritDoc}
     */
    public function rules(): array
    {
        return [
            'type' => 'required|string|in:online,offline',
        ];
    }
}
