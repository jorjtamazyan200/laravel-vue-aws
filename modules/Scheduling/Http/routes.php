<?php

/*
|--------------------------------------------------------------------------
| Module routes
|--------------------------------------------------------------------------
|
| Here's the place to register API routes for the current module. These routes
| are loaded by the RouteServiceProvider within a group which is assigned
| the "api" middleware group and a version prefix, eg. `/api/v1`.
|
 */

/**
 * Private Scope
 */
Route::group(['middleware' => ['auth']], function () {
    // Add an appointment
    Route::post('/matches/{id}/appointments', 'Actions\CreateAppointmentAction');
    // Reschedule an appointment
    Route::post('/appointments/{id}/reschedule', 'Actions\RescheduleAppointmentAction');
    // Cancel an appointment
    Route::post('/appointments/{id}/cancel', 'Actions\CancelAppointmentAction');
    // Inform others you were all alone :(
    Route::post('/appointments/{id}/noshow', 'Actions\SendAppointmentNoShowAction');
    // Inform others you were all alone :(
    Route::post('/matches/{id}/noshow', 'Actions\SendMatchNoShowAction');
    //finish appointment action
    Route::post('/appointments/{id}/finish', 'Actions\FinishAppointmentAction');


    /**
     * Routes for Group Supervisors
     */
    Route::get('/matches/{id}/appointments', 'Actions\ListAppointmentsForMatch');
});

Route::get('/appointment/{id}/{action}/{userid}/{secret}', 'Actions\ShowConfirmAppointmentAction')
    ->where('id', '[0-9]+')
    ->name('email.appointment.response');

