<?php

namespace Modules\Scheduling\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class AppointmentHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'plannedStart' => $this->planned_start->toIso8601String(),
            'plannedDurationMinutes' => $this->planned_duration_minutes,
            'actualStart' => $this->actual_start ? $this->actual_start->toIso8601String() : null,
            'state' => $this->state,
            'actualDurationMinutes' => $this->actual_duration_minutes,
        ];
    }
}
