<?php

namespace Modules\Scheduling\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Requests\CreateAppointmentRequest;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class CreateAppointmentAction extends Action
{
    protected $appointmentService;
    protected $responder;

    public function __construct(AppointmentService $appointmentService, ResourceResponder $responder)
    {
        $this->appointmentService = $appointmentService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, CreateAppointmentRequest $request)
    {
        $data = $request->all();
        $data['match_id'] = $id;

        // better use:
        // $this->appointmentService->planFutureAppointmentForMatch();
        /** @var Appointment $appointment */
        $appointment = $this->appointmentService->create($data);

        //statemachine
        $appointment->transition('plan');

        return $this->responder->send($appointment, AppointmentHttpResource::class);
    }
}
