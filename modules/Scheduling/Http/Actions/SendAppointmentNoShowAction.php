<?php

namespace Modules\Scheduling\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Notifications\Appointment\AppointmentNoShowMissedYouNotification;
use App\Notifications\Appointment\AppointmentNoShowSenderNotification;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Requests\EmptyAppointmentRequest;

class SendAppointmentNoShowAction extends Action
{
    protected $appointmentService;

    public function __construct(AppointmentService $appointmentService)
    {
        $this->appointmentService = $appointmentService;
    }

    public function __invoke(int $id, EmptyAppointmentRequest $request)
    {
        /** @var Appointment $appointment */
        $appointment = $this->appointmentService->get($id);

        //statemachine
        $appointment->transition('report_noshow');

        /** @var User $sender */
        $sender = Auth::user();
        $enrollments = $appointment->enrollments;

        foreach ($enrollments as $enrollment) {
            /** @var User $user */
            $user = $enrollment->user;
            if ($user->id == Auth::id()) {
                $appointment->update(['noshow_reportee_id' => $user->id]);
                $sender->notify(new AppointmentNoShowSenderNotification($sender->brand, $appointment, $enrollment));
            } else {
                $user->notify(new AppointmentNoShowMissedYouNotification($user->brand, $appointment, $enrollment));
            }
        }


        return response('', 200);
    }
}
