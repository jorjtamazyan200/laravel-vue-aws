<?php

namespace Modules\Scheduling\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\MultiTenant\Facades\MultiTenant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AddFeedbackRequest;
use Modules\Core\Http\Requests\RegisterUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class ShowConfirmAppointmentAction extends Action
{

    protected $responder;
    protected $appointmentService;
    protected $userService;


    public function __construct(
        ResourceResponder $responder,
        AppointmentService $appointmentService,
        UserService $userService

    )
    {
        $this->appointmentService = $appointmentService;
        $this->userService = $userService;


    }

    public function __invoke(Request $request)
    {

        $secret = $request->secret;
        $id = $request->id;
        $action = $request->action;
        $userid = $request->userid;

        /** @var User $user */
        /** @var Appointment $app */
        $app = $this->appointmentService->get($id);
        $user = $this->userService->get($userid);
        $url = $user->brand->frontend_url;

        App::setLocale($user->language);

        if ($user->emailSecret('RESPONSE_APPOINTMENT') !== $secret) {
            throw new AccessDeniedException("Secret invalid");
        }

        $threeMinutesAgo = Carbon::now()->subMinutes(3);
        if ($threeMinutesAgo->lessThan($app->last_state_change_at)) {
            return view('responses.toofast', ['link' => $url]);
        }


        if ($app->transitionAllowed($action)) {
            $app->transition($action);
        }


        return view('responses.appointmentConfirmed', ['link' => $url]);


    }

}
