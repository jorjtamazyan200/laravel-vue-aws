<?php

namespace Modules\Scheduling\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

class FinishAppointmentTest extends EndpointTest
{
    public function test_it_finishes_an_appointment()
    {
        //normal finish appointment - online
        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'state' => Appointment::STATES['ONLINE'],
        ]);
        $match = Match::find(1);
        $match->state = Match::STATES['ACTIVE'];
        $match->save();
        $this->doFinishRequest($this->user, $appointment, 'online');

        //offline finish appointment - offline
        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'state' => Appointment::STATES['PLANNED'],
        ]);

        $this->doFinishRequest($this->user, $appointment, 'offline');
    }


    public function test_that_supervisors_can_also_cancel_appointments()
    {


        //normal finish appointment - online
        $supervisor = factory(User::class)->create();
        $supervisor->groups()->attach(1, [
            'role' => 'supervisor',
        ]);
        $match = Match::find(1);

        $match->state = Match::STATES['ACTIVE'];
        $match->save();


        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'planned_start' => Carbon::tomorrow(),
            'state' => Appointment::STATES['ONLINE'],
        ]);

        $this->doFinishRequest($supervisor, $appointment, 'online');

        //offline finish appointment - offline
        $appointment = factory(Appointment::class)->create([
            'match_id' => 1,
            'planned_start' => Carbon::tomorrow(),
            'state' => Appointment::STATES['NOATTENDANCE'],
        ]);

        $this->doFinishRequest($supervisor, $appointment, 'offline');
    }

    public function test_that_supervisors_of_other_group_cant_cancel_appointments()
    {


        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();

        $supervisor = factory(User::class)->create();
        $group = factory(Group::class)->create();
        $supervisor->groups()->attach($group->id, [
            'role' => 'supervisor',
        ]);
        $match = factory(Match::class)->create();

        $appointment = factory(Appointment::class)->create([
            'match_id' => $match->id,
            'planned_start' => Carbon::tomorrow(),
            'state' => Appointment::STATES['ONLINE'],
        ]);

        $response = $this->actingAs($supervisor)
            ->json('POST', "/api/v2/appointments/$appointment->id/finish");

        $response->assertStatus(403);
        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id,
            'state' => Appointment::STATES['ONLINE']
        ]);
    }

    protected function doFinishRequest($user, $appointment, String $type)
    {
        //using a mock for the notifications
        Notification::fake();

        $response = $this->actingAs($user)
            ->json('POST', "/api/v2/appointments/$appointment->id/finish", [
                'type' => $type
            ]);

        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id,
            'state' => Appointment::STATES['SUCCESS'],
        ]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

}
