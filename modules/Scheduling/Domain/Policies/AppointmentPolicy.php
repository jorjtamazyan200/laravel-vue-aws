<?php

namespace Modules\Scheduling\Domain\Policies;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\GroupService;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

class AppointmentPolicy
{
    protected $appointmentService;
    protected $enrollmentService;
    protected $groupService;

    public function __construct(
        AppointmentService $appointmentService,
        EnrollmentService $enrollmentService,
        GroupService $groupService
    ) {
        $this->appointmentService = $appointmentService;
        $this->enrollmentService = $enrollmentService;
        $this->groupService = $groupService;
    }

    /**
     * A User can create an Appointment for a Match if he is matched in it via his Enrollment.
     *
     * @param User $user
     * @param Match $match
     * @return boolean
     */
    public function create(User $user, Match $match) : bool
    {
        $enrollment = $this->enrollmentService->getForUserAndMatch($user->id, $match->id);

        return $user->id === $enrollment->user_id;
    }

    /**
     * A User can update an Appointment for a Match if he is matched in it via his Enrollment.
     *
     * @param User $user
     * @param Appointment $appointment
     * @return boolean
     */
    public function update(User $user, Appointment $appointment) : bool
    {
        return $this->change($user, $appointment);
    }

    /**
     * A User can change an Appointment for a Match if he is matched in it via his Enrollment.
     *
     * @param User $user
     * @param Appointment $appointment
     * @return boolean
     */
    public function change(User $user, Appointment $appointment) : bool
    {
        return $this->belongsToUser($user, $appointment) ||
            $this->isGroupSupvervisor($user, $appointment);
    }

    /**
     * Returns true if the $user is a Group supervisor for one of the Users that are
     * part of the Appointment.
     *
     * @param User $user
     * @param Appointment $appointment
     * @return boolean
     */
    protected function isGroupSupvervisor(User $user, Appointment $appointment) : bool
    {
        // Now we have to check all groups of users that are connected to the $appointment.
        $userIds = $appointment->match->enrollments->pluck('user_id');
        $userGroups = $this->groupService->listForUsers($userIds->toArray());

        foreach ($userGroups as $group) {
            if ($user->can('supervise', $group)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if the Appointment belongs to a User.
     *
     * @param User $user
     * @param Appointment $appointment
     * @return boolean
     */
    protected function belongsToUser(User $user, Appointment $appointment) : bool
    {
        $match = $appointment->match;
        try {
            $enrollment = $this->enrollmentService->getForUserAndMatch($user->id, $match->id);
        } catch (ModelNotFoundException $e) {
            return false;
        }

        return $user->id === $enrollment->user_id;
    }
}
