<?php

namespace Modules\Scheduling\Domain\Services;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;

class AppointmentService extends EloquentBreadService
{


    /**
     * @param $matchId
     * @param $date
     */
    public function planFutureAppointmentForMatch($matchId, $date)
    {
        $futureAppointments = $this->model->newQuery()
            ->where('match_id', '=', $matchId)
            ->where('planned_start', '>', Carbon::now())
            ->get();

        if ($futureAppointments->count() > 0) {
            $appointment = $futureAppointments->first();
        } else {
            $appointment = new Appointment();
            $appointment->match_id = $matchId;
        }

        $appointment->planned_start = $date;
        $appointment->state = Appointment::STATES['NEW'];
        $appointment->save();
        $appointment->transition('plan');

        return $appointment;

    }

    /**
     * List all Appointments for a Group.
     *
     * @param Group $group
     * @return Collection
     */
    public function listForGroup(Group $group): Collection
    {
        return $this->model->newQuery()
            ->where('planned_start', '>', Carbon::today())
            ->whereHas('match.participations.enrollment.user.groups', function ($query) use ($group) {
                $query->where('group_id', $group->id);
            })
            ->get();
    }

    /**
     * @param int $currentUserId
     * @param Appointment $appointment
     * @param string $comment
     * @throws \App\Exceptions\Client\ClientException
     */
    public function cancelAppointent(int $currentUserId, Appointment $appointment, string $commentText)
    {

        $appointment->comment = $commentText;
        $appointment->save();

        //statemachine transition
        $appointment->transition('cancel');
    }

    /**
     * @param int $currentUserId
     * @param Appointment $appointment
     * @param string $comment
     * @throws \App\Exceptions\Client\ClientException
     */
    public function confirmAppointment(Appointment $appointment)
    {
        if ($appointment->transitionAllowed('confirm')){
            // statemachine transition
            $appointment->transition('confirm');
        }

    }

    /**
     * List all Appointments for all users in a group including first name and lastname
     *
     *
     *
     * @param Group $group
     * @param int $beforeDays get appointments within this time..
     * @return Collection of STdObjects
     */
    public function listWithUserForSupervisorOfGroup(Group $group, $beforeDays = 14): Collection
    {

        $result = $this->model->newQuery()
            ->join('matches', 'appointments.match_id', '=', 'matches.id')
            ->join('participations', 'participations.match_id', '=', 'matches.id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->join('group_user', 'group_user.user_id', '=', 'users.id')
            ->where('appointments.state', '<>', Appointment::STATES['CANCELED'])
            ->where('group_user.group_id', $group->id)
            ->where('planned_start', '>', Carbon::now())
            ->where('planned_start', '<', Carbon::now()->addDays($beforeDays))
            ->select([
                'appointments.id',
                'appointments.planned_start as plannedStart',
                'appointments.state',
                'users.id as userId',
                'users.first_name as firstName',
                'users.last_name as lastName'
            ])
            ->get();

        $appointments = $result->map(function ($appointment) {
            $app = $appointment->toArray();
            $app['plannedStart'] = Carbon::createFromFormat('Y-m-d H:i:s',
                $appointment->plannedStart)->toIso8601String();
            return (object)$app;
        });


        return $appointments;

    }

    public function getOnlineAppointmentsWithoutConferenceInformation()
    {
        return $this->model->newQuery()
            ->where('state', '=', Appointment::STATES['ONLINE'])
            ->where('last_state_change_at', '<', Carbon::now()->subMinutes(90))
            ->get();
    }

    /**
     * @param $status String|array (multiple)
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getPastAppointmentsWithStatus($status)
    {
        if (is_string($status)){
            $status = [$status];
        }

        return $this->model->newQuery()
            ->whereIn('state', $status)
            ->where('planned_start', '<', Carbon::now()->subMinutes(90))
            ->get();
    }

    /**
     * List all Appointments for a user within a specific group.
     * Please note: for now we are only looking for appointment according to the specific user.
     *
     *
     * @param Group $group
     * @return Collection
     */
    public function listAppointmentsForUser(User $user): Collection
    {
        return $this->model->newQuery()
            ->select(['appointments.*'])
            ->join('matches', 'appointments.match_id', '=', 'matches.id')
            ->join('participations', 'participations.match_id', '=', 'matches.id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->where('enrollments.user_id', $user->id)
            ->get();;
    }


    /**
     * Find Appointments to send SMS reminder
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAppointmentsToRemind()
    {
        $now = Carbon::now();
        $time = Carbon::now()->addHours(3);

        return $this->model->newQuery()
            ->whereNull('last_notification_at')
            ->whereIn('state', [Appointment::STATES['PLANNED'], Appointment::STATES['CONFIRMED']])
            ->whereBetween('planned_start', [$now, $time])
            ->get();
    }

    public function getCurrentAppointmentForMatch($matchId)
    {

        return $this->model->newQuery()
            ->where('match_id', '=', $matchId)

            ->where('planned_start', '>', Carbon::now()->subHours(3))
            ->where('planned_start', '<', Carbon::now()->addHours(5))

            ->orderBy('planned_start', 'DESC')
            ->first();
    }

    public function finishAppointment(Appointment $appointment, string $type)
    {

        if (!in_array($type, ['online', 'offline', 'timeout'])) {
            throw new ClientException(422, 'invalid type: ' . $type);
        }
        // @todo: add some information what to do on timeout;


        $transition = ($type === 'online' || $type === 'timeout' ? 'finish' : 'finish_offline');
        $appointment->transition($transition);
    }


}
