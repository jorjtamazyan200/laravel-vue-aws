<?php

namespace Modules\Scheduling\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;

class AppointmentStateLog extends Model
{
    use LogsState;

    public $table = 'appointment_state_logs';

    protected $fillable = ['appointment_id', 'actor_id', 'transition', 'from', 'to'];

    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
}
