<?php

namespace Modules\TrainingCenter;


use App\Infrastructure\Providers\ModuleServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\CourseAnswer;
use Modules\TrainingCenter\Domain\Models\CourseFeedback;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;
use Modules\TrainingCenter\Domain\Models\Exercise;
use Modules\TrainingCenter\Domain\Models\Lecturer;
use Modules\TrainingCenter\Domain\Models\Like;
use Modules\TrainingCenter\Domain\Models\Note;
use Modules\TrainingCenter\Domain\Models\QuizAnswer;
use Modules\TrainingCenter\Domain\Models\QuizQuestion;
use Modules\TrainingCenter\Domain\Models\Section;
use Modules\TrainingCenter\Domain\Models\Transcript;
use Modules\TrainingCenter\Domain\Models\Video;
use Modules\TrainingCenter\Domain\Services\CourseQuestionService;
use Modules\TrainingCenter\Domain\Services\AnswerService;
use Modules\TrainingCenter\Domain\Services\CourseService;
use Modules\TrainingCenter\Domain\Services\ExerciseService;
use Modules\TrainingCenter\Domain\Services\FeedbackService;
use Modules\TrainingCenter\Domain\Services\LecturerService;
use Modules\TrainingCenter\Domain\Services\LikeService;
use Modules\TrainingCenter\Domain\Services\NoteService;
use Modules\TrainingCenter\Domain\Services\QuizAnswerService;
use Modules\TrainingCenter\Domain\Services\QuizQuestionService;
use Modules\TrainingCenter\Domain\Services\SectionService;
use Modules\TrainingCenter\Domain\Services\TranscriptService;
use Modules\TrainingCenter\Domain\Services\VideoService;

class TrainingCenterModuleProvider extends ModuleServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(CourseService::class)->needs(Model::class)->give(Course::class);
        $this->app->when(SectionService::class)->needs(Model::class)->give(Section::class);
        $this->app->when(VideoService::class)->needs(Model::class)->give(Video::class);
        $this->app->when(LecturerService::class)->needs(Model::class)->give(Lecturer::class);
        $this->app->when(ExerciseService::class)->needs(Model::class)->give(Exercise::class);
        $this->app->when(QuizQuestionService::class)->needs(Model::class)->give(QuizQuestion::class);
        $this->app->when(QuizAnswerService::class)->needs(Model::class)->give(QuizAnswer::class);
        $this->app->when(TranscriptService::class)->needs(Model::class)->give(Transcript::class);
        $this->app->when(AnswerService::class)->needs(Model::class)->give(CourseAnswer::class);
        $this->app->when(NoteService::class)->needs(Model::class)->give(Note::class);
        $this->app->when(CourseQuestionService::class)->needs(Model::class)->give(CourseQuestion::class);
        $this->app->when(CourseService::class)->needs(Model::class)->give(Course::class);
        $this->app->when(LikeService::class)->needs(Model::class)->give(Like::class);
        $this->app->when(FeedbackService::class)->needs(Model::class)->give(CourseFeedback::class);
    }
}
