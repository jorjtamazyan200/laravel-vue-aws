<?php

namespace Modules\TrainingCenter\Http\Resources;


use App\Infrastructure\Http\HttpResource;

/**
 * Class SeenVideoHttpResource
 *
 * @package Modules\TrainingCenter\Http\Resources
 */
class SeenVideoHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
       return [
           'id' => $this->id,
           'duration' => $this->duration,
           'user' => new UserHttpResource($this->whenLoaded('user')),

           'createdAt' => !is_null($this->created_at) ? $this->created_at->toIso8601String() : null,
           'updatedAt' => !is_null($this->updated_at) ? $this->updated_at->toIso8601String() : null,
       ];
    }
}
