<?php

namespace Modules\TrainingCenter\Http\Resources;

use App\Infrastructure\Http\HttpResource;

/**
 * Class PostCommentsListHttpResource
 *
 * @package Modules\Community\Http\Resources
 */
class CourseQuestionHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'course' => new CourseQuestionHttpResource($this->whenLoaded('course')),
            'answers' => CourseAnswerHttpResource::collection($this->whenLoaded('answers')),
            'itsOwnership' =>  auth()->id() == $this->user_id,
            'likes_count' => $this->whenLoaded('likes', $this->likes()->count()),
            'dislikes_count' => $this->whenLoaded('likes', $this->dislikes()->count()),

            'createdAt' => !is_null($this->created_at) ? $this->created_at->toIso8601String() : null,
            'updatedAt' => !is_null($this->updated_at) ? $this->updated_at->toIso8601String() : null,
        ];
    }
}
