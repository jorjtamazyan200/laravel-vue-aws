<?php

namespace Modules\TrainingCenter\Http\Resources;

use App\Infrastructure\Http\HttpResource;

/**
 * Class PostCommentsListHttpResource
 *
 * @package Modules\Community\Http\Resources
 */
class TranscriptHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'video' => new VideoHttpResource($this->whenLoaded('video')),

            'createdAt' => !is_null($this->created_at) ? $this->created_at->toIso8601String() : null,
            'updatedAt' => !is_null($this->updated_at) ? $this->updated_at->toIso8601String() : null,
        ];
    }
}
