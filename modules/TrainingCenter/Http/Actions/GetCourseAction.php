<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Services\CourseService;
use Modules\TrainingCenter\Http\Resources\CourseHttpResource;

class GetCourseAction extends Action
{
    /**
     * @var CourseService
     */
    protected $courseService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * GetCourseAction constructor.
     * @param CourseService $courseService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(CourseService $courseService, ResourceResponder $resourceResponder)
    {
        $this->courseService = $courseService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * Get quiz with relations by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke($id)
    {
        $userId = auth()->id();
        $course = $this->courseService->getCourse($id);
        $this->courseService->incrementViewsCount($course);
        $this->courseService->getCourseStatusAndDuration($course, $userId);

        return $this->resourceResponder->send($course, CourseHttpResource::class);
    }
}
