<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Services\NoteService;
use Modules\TrainingCenter\Http\Requests\CreateNoteRequest;
use Modules\TrainingCenter\Http\Resources\NoteHttpResource;

class CreateNoteAction extends Action
{
    /**
     * @var NoteService
     */
    protected $noteService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * CreateNoteAction constructor.
     * @param NoteService $noteService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(NoteService $noteService, ResourceResponder $resourceResponder)
    {
        $this->noteService = $noteService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * Create course note
     *
     * @param Course $course
     * @param CreateNoteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Course $course, CreateNoteRequest $request)
    {
        $userId = auth()->id();
        $input = $request->validated();
        $note = $this->noteService->createNote($course, $input, $userId);

        return $this->resourceResponder->send($note, NoteHttpResource::class);
    }
}
