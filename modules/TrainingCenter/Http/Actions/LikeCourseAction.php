<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\DB;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\Like;
use Modules\TrainingCenter\Domain\Services\LikeService;
use Modules\TrainingCenter\Http\Resources\CourseHttpResource;

class LikeCourseAction extends Action
{
    /**
     * @var LikeService
     */
    protected $likeService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * LikeAnswerAction constructor.
     * @param LikeService $likeService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(LikeService $likeService, ResourceResponder $resourceResponder)
    {
        $this->likeService = $likeService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param Course $course
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Course $course)
    {
        $userId = auth()->id();
        $course = $this->likeService->likeOrDislikeCourse($course, $userId);
        $course->load('likes');
        $course->load('dislikes');

        return $this->resourceResponder->send($course, CourseHttpResource::class);
    }
}
