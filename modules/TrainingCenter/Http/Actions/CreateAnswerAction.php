<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Models\CourseAnswer;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;
use Modules\TrainingCenter\Domain\Services\AnswerService;
use Modules\TrainingCenter\Http\Requests\CreateAnswerRequest;
use Modules\TrainingCenter\Http\Resources\CourseAnswerHttpResource;

class CreateAnswerAction extends Action
{
    /**
     * @var AnswerService
     */
    protected $answerService;

    /**
     * @var $resourceResponder
     */
    private $resourceResponder;

    /**
     * CreateAnswerAction constructor.
     * @param AnswerService $answerService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(AnswerService $answerService, ResourceResponder $resourceResponder)
    {
        $this->answerService = $answerService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * Create course question answer
     *
     * @param CourseQuestion $question
     * @param CreateAnswerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(CourseQuestion $question, CreateAnswerRequest $request)
    {
        $userId = auth()->id();
        $input = $request->validated();
        $answer = $this->answerService->createAnswer($question, $input, $userId);

        return $this->resourceResponder->send($answer, CourseAnswerHttpResource::class);
    }
}
