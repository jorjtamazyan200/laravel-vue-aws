<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Services\NoteService;

class DeleteNoteAction
{
    /**
     * @var NoteService
     */
    protected $noteService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * EditNoteAction constructor.
     * @param NoteService $noteService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(NoteService $noteService, ResourceResponder $resourceResponder)
    {
        $this->noteService = $noteService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke($id)
    {
        $userId = auth()->id();
        $this->noteService->deleteNote($id, $userId);

        return response()->json('ok', 200);
    }
}
