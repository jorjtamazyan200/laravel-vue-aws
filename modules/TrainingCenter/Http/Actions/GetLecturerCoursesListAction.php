<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Modules\Core\Domain\Models\User;
use Modules\TrainingCenter\Domain\Models\Lecturer;
use Modules\TrainingCenter\Domain\Services\CourseService;
use Modules\TrainingCenter\Http\Resources\CourseHttpResource;
use Modules\TrainingCenter\Http\Resources\LecturerHttpResource;

class GetLecturerCoursesListAction extends Action
{
    /**
     * @var CourseService
     */
    protected $courseService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * GetCoursesListAction constructor.
     * @param CourseService $courseService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(CourseService $courseService, ResourceResponder $resourceResponder)
    {
        $this->courseService = $courseService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * Get user courses list with relations
     *
     * @param Request $request
     * @param Lecturer $lecturer
     * @return mixed
     */
    public function __invoke(Request $request, Lecturer $lecturer)
    {
        $lecturer = $this->courseService->getLecturerCourseList($lecturer);
        $courses = $lecturer->courses;

        return response()->json([
            'lecturer' => new LecturerHttpResource($lecturer),
            'courses' => CourseHttpResource::collection($courses)
        ]);
    }
}
