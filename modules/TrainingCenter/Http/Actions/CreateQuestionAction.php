<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;
use Modules\TrainingCenter\Domain\Services\CourseQuestionService;
use Modules\TrainingCenter\Http\Requests\CreateQuestionRequest;
use Modules\TrainingCenter\Http\Resources\CourseQuestionHttpResource;

class CreateQuestionAction extends Action
{
    /**
     * @var CourseQuestionService
     */
    protected $courseQuestionService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * CreateQuestionAction constructor.
     * @param CourseQuestionService $courseQuestionService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(CourseQuestionService $courseQuestionService, ResourceResponder $resourceResponder)
    {
        $this->courseQuestionService = $courseQuestionService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * Create course question
     *
     * @param Course $course
     * @param CreateQuestionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Course $course, CreateQuestionRequest $request)
    {
        $userId = auth()->id();
        $input = $request->validated();
        $question = $this->courseQuestionService->createCourseQuestion($course, $input, $userId);

        return $this->resourceResponder->send($question, CourseQuestionHttpResource::class);
    }
}
