<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Services\CourseQuestionService;

class DeleteQuestionAction extends Action
{
    /**
     * @var CourseQuestionService
     */
    protected $courseQuestionService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * CreateQuestionAction constructor.
     * @param CourseQuestionService $courseQuestionService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(CourseQuestionService $courseQuestionService, ResourceResponder $resourceResponder)
    {
        $this->courseQuestionService = $courseQuestionService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke($id)
    {
        $userId = auth()->id();
        $this->courseQuestionService->deleteQuestion($id, $userId);

        return response()->json('ok');
    }
}
