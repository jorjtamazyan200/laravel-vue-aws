<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Services\CourseQuestionService;
use Modules\TrainingCenter\Http\Requests\UpdateQuestionRequest;

class EditQuestionAction extends Action
{
    /**
     * @var CourseQuestionService
     */
    protected $questionService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * EditQuestionAction constructor.
     * @param CourseQuestionService $questionService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(CourseQuestionService $questionService, ResourceResponder $resourceResponder)
    {
        $this->questionService = $questionService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param int $id
     * @param UpdateQuestionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(int $id, UpdateQuestionRequest $request)
    {
        $userId = auth()->id();
        $input = $request->validated();
        $this->questionService->updateQuestion($id, $input, $userId);

        return response()->json('ok', 200);
    }
}
