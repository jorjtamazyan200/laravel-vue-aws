<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Services\NoteService;
use Modules\TrainingCenter\Http\Requests\UpdateNoteRequest;

class EditNoteAction
{
    /**
     * @var NoteService
     */
    protected $noteService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * EditNoteAction constructor.
     * @param NoteService $noteService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(NoteService $noteService, ResourceResponder $resourceResponder)
    {
        $this->noteService = $noteService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param $id
     * @param UpdateNoteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke($id, UpdateNoteRequest $request)
    {
        $input = $request->validated();
        $userId = auth()->id();
        $this->noteService->updateNote($id, $input, $userId);

        return response()->json('ok', 200);
    }
}
