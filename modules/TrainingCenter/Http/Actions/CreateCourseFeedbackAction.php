<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Services\FeedbackService;
use Modules\TrainingCenter\Http\Requests\CreateFeedbackRequest;
use Modules\TrainingCenter\Http\Resources\CourseFeedbackHttpResource;

class CreateCourseFeedbackAction extends Action
{
    /**
     * @var FeedbackService
     */
    protected $feedbackService;

    /**
     * @var $resourceResponder
     */
    private $resourceResponder;

    /**
     * CreateAnswerAction constructor.
     * @param FeedbackService $feedbackService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(FeedbackService $feedbackService, ResourceResponder $resourceResponder)
    {
        $this->feedbackService = $feedbackService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * Create course question answer
     *
     * @param Course $course
     * @param CreateFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Course $course, CreateFeedbackRequest $request)
    {
        $userId = auth()->id();
        $input = $request->validated();
        $feedback = $this->feedbackService->leaveFeedback($course, $input, $userId);

        return $this->resourceResponder->send($feedback, CourseFeedbackHttpResource::class);
    }
}
