<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\DB;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;
use Modules\TrainingCenter\Domain\Models\Like;
use Modules\TrainingCenter\Domain\Services\LikeService;
use Modules\TrainingCenter\Http\Resources\CourseQuestionHttpResource;

class DislikeQuestionAction extends Action
{
    /**
     * @var LikeService
     */
    protected $likeService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * LikeAnswerAction constructor.
     * @param LikeService $likeService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(LikeService $likeService, ResourceResponder $resourceResponder)
    {
        $this->likeService = $likeService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * Like for question
     *
     * @param CourseQuestion $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(CourseQuestion $question)
    {
        $userId = auth()->id();
        $question = $this->likeService->likeOrDislikeQuestion($question, $userId, false);
        $question->load('likes');
        $question->load('dislikes');

        return $this->resourceResponder->send($question, CourseQuestionHttpResource::class);
    }
}
