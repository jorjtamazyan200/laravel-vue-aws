<?php

namespace Modules\TrainingCenter\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Services\CourseService;
use Modules\TrainingCenter\Http\Resources\CourseHttpResource;

class GetCoursesListAction extends Action
{
    /**
     * @var CourseService
     */
    protected $courseService;

    /**
     * @var $resourceResponder
     */
    private $resourceResponder;

    /**
     * GetCoursesListAction constructor.
     * @param CourseService $courseService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(CourseService $courseService, ResourceResponder $resourceResponder)
    {
        $this->courseService = $courseService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * Get courses list with relations
     *
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $courses = $this->courseService->getCourseList();

        return $this->resourceResponder->send($courses, CourseHttpResource::class);
    }
}
