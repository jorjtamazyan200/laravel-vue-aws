<?php

namespace Modules\TrainingCenter\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\DB;
use Modules\TrainingCenter\Domain\Models\CourseAnswer;
use Modules\TrainingCenter\Domain\Models\Like;
use Modules\TrainingCenter\Domain\Services\LikeService;
use Modules\TrainingCenter\Http\Resources\CourseAnswerHttpResource;

class LikeAnswerAction extends Action
{
    /**
     * @var LikeService
     */
    protected $likeService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * LikeAnswerAction constructor.
     * @param LikeService $likeService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(LikeService $likeService, ResourceResponder $resourceResponder)
    {
        $this->likeService = $likeService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param CourseAnswer $answer
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(CourseAnswer $answer)
    {
        $userId = auth()->id();
        $answer = $this->likeService->likeOrDislikeAnswer($answer, $userId);
        $answer->load('likes');
        $answer->load('dislikes');

        return $this->resourceResponder->send($answer, CourseAnswerHttpResource::class);
    }
}
