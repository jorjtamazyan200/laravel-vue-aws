<?php
Route::group([
    'prefix' => '/training-center',
    'as' => 'trainingCenter.',
    'middleware' => ['throttle:60,1', 'auth']
], function () {
    Route::group(['prefix' => 'questions', 'as' => 'questions.'], function () {
        Route::post('/{question}/answers', 'Actions\CreateAnswerAction')->name('createAnswer');
        Route::post('/{question}/like', 'Actions\LikeQuestionAction')->name('likeQuestion');
        Route::post('/{question}/dislike', 'Actions\DislikeQuestionAction')->name('dislikeQuestion');
        Route::put('/{courseId}', 'Actions\EditQuestionAction')->name('editQuestion');
        Route::delete('/{courseId}', 'Actions\DeleteQuestionAction')->name('deleteQuestion');
    });

    Route::group(['prefix' => 'notes', 'as' => 'notes.'], function () {
        Route::put('/{id}', 'Actions\EditNoteAction')->name('editNote');
        Route::delete('/{id}', 'Actions\DeleteNoteAction')->name('deleteNote');
    });

    Route::get('/lecturers/{lecturer}', 'Actions\GetLecturerCoursesListAction')->name('getLecturerCourseList');
    Route::post('/answers/{answer}/like', 'Actions\LikeAnswerAction')->name('likeAnswer');
    Route::post('/answers/{answer}/dislike', 'Actions\DislikeAnswerAction')->name('dislikeAnswer');

    Route::group(['prefix' => '/courses', 'as' => 'courses.'], function () {
        Route::get('/', 'Actions\GetCoursesListAction')->name('getCourseList');
        Route::get('/{courseId}', 'Actions\GetCourseAction')->name('getCourse');
        Route::post('/{course}/feedback', 'Actions\CreateCourseFeedbackAction')->name('leaveCourseFeedback');
        Route::post('/{course}/like', 'Actions\LikeCourseAction')->name('likeCourse');
        Route::post('/{course}/dislike', 'Actions\DislikeCourseAction')->name('dislikeCourse');
        Route::post('/{course}/notes', 'Actions\CreateNoteAction')->name('createNote');
        Route::post('/{course}/questions', 'Actions\CreateQuestionAction')->name('createQuestion');
    });

});

