<?php

namespace Modules\TrainingCenter\Http\Requests;


use App\Infrastructure\Http\DeserializedFormRequest;

class CreateFeedbackRequest extends DeserializedFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'rating' => 'required|integer|min:0|max:5',
            'text' => 'nullable|string'
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
