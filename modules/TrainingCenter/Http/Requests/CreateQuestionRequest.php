<?php

namespace Modules\TrainingCenter\Http\Requests;


use App\Infrastructure\Http\DeserializedFormRequest;

class CreateQuestionRequest extends DeserializedFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'text' => 'required',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
