<?php

namespace Modules\TrainingCenter\Domain\Models;


use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

class CourseFeedback extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_course_feedbacks';

    /**
     * @var array
     */
    protected $fillable = [
        'rating',
        'text',
        'tc_course_id',
        'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
