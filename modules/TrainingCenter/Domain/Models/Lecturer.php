<?php

namespace Modules\TrainingCenter\Domain\Models;


use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\Traits\HasImageAttachments;

class Lecturer extends Model
{
    use HasImageAttachments;

    /**
     * @var string
     */
    protected $table = 'tc_lecturers';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'email',
        'avatar',
        'linkedin_link'
    ];

    /**
     * @var array
     */
    protected $image_attachments = [
        'avatar' => [
            'path' => 'uploads/lecturer/avatar',
            'styles' => [
                'small' => '100',
                'medium' => '500',
                'large' => '1000',
            ],
            'visibility' => 'public',
        ],
    ];

    /**
     * Accessor providing a `cover` attribute which contains an array of image urls for every defined dimension.
     *
     * @return array
     */
    public function getAvatarAttribute()
    {
        return $this->getImageAttachmentPaths('avatar');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'tc_course_lecturer', 'tc_lecturer_id', 'tc_course_id');
    }
}
