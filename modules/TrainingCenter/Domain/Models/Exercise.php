<?php

namespace Modules\TrainingCenter\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Exercise extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_exercises';

    /**
     * @var array
     */
    protected $fillable = [
        'path',
        'tc_section_id'
    ];

    /**
     * @var string
     */
    public $filePath = 'uploads/exercise/file/';

    public function getPathAttribute()
    {
        return Storage::disk('public')->url($this->filePath . $this->attributes['path']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Section::class);
    }
}
