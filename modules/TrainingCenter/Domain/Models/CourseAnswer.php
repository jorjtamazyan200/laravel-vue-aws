<?php

namespace Modules\TrainingCenter\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

class CourseAnswer extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_course_answers';

    /**
     * @var array
     */
    protected $fillable = [
        'text',
        'tc_course_question_id',
        'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(CourseQuestion::class, 'tc_course_question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable')->where('action', Like::$actions['like']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function dislikes()
    {
        return $this->morphMany(Like::class, 'likeable')->where('action', Like::$actions['dislike']);
    }
}
