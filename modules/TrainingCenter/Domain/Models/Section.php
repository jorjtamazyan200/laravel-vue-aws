<?php

namespace Modules\TrainingCenter\Domain\Models;


use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_sections';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function videos()
    {
        return $this->hasMany(Video::class, 'tc_section_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function exercises()
    {
        return $this->hasOne(Exercise::class, 'tc_section_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quizQuestions()
    {
        return $this->hasMany(QuizQuestion::class, 'tc_section_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'tc_course_section', 'tc_section_id', 'tc_course_id');
    }

}
