<?php

namespace Modules\TrainingCenter\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Transcript extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_transcripts';

    /**
     * @var array
     */
    protected $fillable = [
        'text',
        'tc_video_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function video()
    {
        return $this->belongsTo(Video::class);
    }
}
