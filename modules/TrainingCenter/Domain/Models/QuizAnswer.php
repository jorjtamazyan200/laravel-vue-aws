<?php

namespace Modules\TrainingCenter\Domain\Models;


use Illuminate\Database\Eloquent\Model;

class QuizAnswer extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_quiz_answers';

    /**
     * @var array
     */
    protected $fillable = [
        'text',
        'is_correct',
        'tc_quiz_question_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(QuizQuestion::class);
    }
}
