<?php

namespace Modules\TrainingCenter\Domain\Models;


use Illuminate\Database\Eloquent\Model;

class QuizQuestion extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_quiz_questions';

    /**
     * @var array
     */
    protected $fillable = [
        'text',
        'type',
        'tc_section_id',
    ];

    public static $types = [
        'single_choice' => 'single_choice',
        'multiple_choice' => 'multiple_choice'
    ];

    /**
     * @var array
     */
    protected $appends = ['quizCorrectAnswersCount'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(QuizAnswer::class, 'tc_quiz_question_id');
    }

    /**
     * @return mixed
     */
    public function getQuizCorrectAnswersCountAttribute()
    {
        return self::answers()->where('is_correct', 1)->count();
    }
}
