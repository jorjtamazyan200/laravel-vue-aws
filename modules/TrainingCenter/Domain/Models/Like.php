<?php

namespace Modules\TrainingCenter\Domain\Models;


use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

class Like extends Model
{
    /**
     * Action type
     */
    public static $actions = [
        'like' => 'like',
        'dislike' => 'dislike',
    ];

    /**
     * @var string
     */
    protected $table = 'tc_likes';

    /**
     * @var array
     */
    protected $fillable = [
        'likeable_id',
        'likeable_type',
        'user_id',
        'action'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
