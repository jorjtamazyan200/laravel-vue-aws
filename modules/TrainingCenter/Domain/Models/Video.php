<?php

namespace Modules\TrainingCenter\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_videos';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'order',
        'duration',
        'video_id',
        'video_type',
        'tc_section_id'
    ];

    /**
     * @var array
     */
    public static $videoTypes = [
        'vimeo' => 'vimeo'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Section::class, 'tc_section_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transcript()
    {
        return $this->hasOne(Transcript::class, 'tc_video_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function seenVideos()
    {
        return $this->hasMany(SeenVideo::class, 'tc_video_id');
    }

}
