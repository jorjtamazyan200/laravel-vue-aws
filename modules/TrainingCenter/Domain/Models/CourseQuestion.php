<?php

namespace Modules\TrainingCenter\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Domain\Models\User;

class CourseQuestion extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'tc_course_questions';

    /**
     * @var array
     */
    protected $fillable = [
        'text',
        'tc_course_id',
        'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(CourseAnswer::class, 'tc_course_question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable')->where('action', Like::$actions['like']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function dislikes()
    {
        return $this->morphMany(Like::class, 'likeable')->where('action', Like::$actions['dislike']);
    }
}
