<?php

namespace Modules\TrainingCenter\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

class Note extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_notes';

    /**
     * @var array
     */
    protected $fillable = [
        'text',
        'tc_course_id',
        'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
