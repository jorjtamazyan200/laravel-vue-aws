<?php

namespace Modules\TrainingCenter\Domain\Models;


use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

class SeenVideo extends Model
{
    /**
     * @var string
     */
    protected $table = 'tc_seen_videos';

    /**
     * @var array
     */
    protected $fillable = [
        'duration',
        'tc_video_id',
        'user_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function video()
    {
        return $this->belongsTo(Video::class, 'tc_viideo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
