<?php

namespace Modules\TrainingCenter\Domain\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Traits\HasImageAttachments;

class Course extends Model
{
    use SoftDeletes, HasImageAttachments;

    /**
     * @var string
     */
    protected $table = 'tc_courses';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'thumbnail',
        'views_count'
    ];

    const STATES = [
        'NEW' => 'NEW',
        'IN_PROGRESS' => 'IN_PROGRESS',
        'FINISHED' => 'FINISHED'
    ];

    /**
     * @var array
     */
    protected $image_attachments = [
        'thumbnail' => [
            'path' => 'uploads/course/thumbnail',
            'styles' => [
                'small' => '100',
                'medium' => '500',
            ],
            'visibility' => 'public',
        ],
    ];

    /**
     * Accessor providing a `cover` attribute which contains an array of image urls for every defined dimension.
     *
     * @return array
     */
    public function getThumbnailAttribute()
    {
        return $this->getImageAttachmentPaths('thumbnail');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function lecturers()
    {
        return $this->belongsToMany(Lecturer::class, 'tc_course_lecturer',  'tc_course_id', 'tc_lecturer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function programs()
    {
        return $this->belongsToMany(Program::class, 'tc_course_program',  'tc_course_id', 'program_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sections()
    {
        return $this->belongsToMany(Section::class, 'tc_course_section', 'tc_course_id', 'tc_section_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(CourseQuestion::class, 'tc_course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable')->where('action', Like::$actions['like']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function dislikes()
    {
        return $this->morphMany(Like::class, 'likeable')->where('action', Like::$actions['dislike']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany(Note::class, 'tc_course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedbacks()
    {
        return $this->hasMany(CourseFeedback::class, 'tc_course_id');
    }
}
