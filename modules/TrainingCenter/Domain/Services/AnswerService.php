<?php

namespace Modules\TrainingCenter\Domain\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;

class AnswerService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * AnswerService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param CourseQuestion $question
     * @param array $input
     * @param int $userId
     * @return Model
     */
    public function createAnswer(CourseQuestion $question, array $input, int $userId)
    {
        $this->model->text = $input['text'];
        $this->model->user_id = $userId;
        $question->answers()->save($this->model);

        return $this->model;
    }
}
