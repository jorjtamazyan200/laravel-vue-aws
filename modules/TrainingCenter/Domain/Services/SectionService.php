<?php

namespace Modules\TrainingCenter\Domain\Services;


use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\Section;

class SectionService extends EloquentBreadService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * SectionService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param int $sectionId
     * @param int $courseId
     * @return mixed
     */
    public function assignCourse(int $sectionId, int $courseId)
    {
        $section = Section::findOrFail($sectionId);
        $courseId = Course::findOrFail($courseId)->id;
        DB::table('tc_course_section')->updateOrInsert(
            [
                'tc_course_id' => $courseId,
                'tc_section_id' => $sectionId,
            ],
            [
                'tc_course_id' => $courseId,
                'tc_section_id' => $sectionId,
            ]
        );

        return $section;
    }

    public function detachCourseFromSection(int $sectionId, int $courseId)
    {
        $section = Section::findOrFail($sectionId);
        $courseId = Course::findOrFail($courseId)->id;
        $section->courses()->detach($courseId);

        return $section;
    }
}
