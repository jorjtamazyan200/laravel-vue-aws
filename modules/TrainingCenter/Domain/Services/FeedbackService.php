<?php

namespace Modules\TrainingCenter\Domain\Services;


use Illuminate\Database\Eloquent\Model;
use Modules\TrainingCenter\Domain\Models\Course;

class FeedbackService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * AnswerService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param Course $course
     * @param array $input
     * @param int $userId
     * @return Model
     */
    public function leaveFeedback(Course $course, array $input, int $userId)
    {
        $this->model->rating = $input['rating'];
        $this->model->text = isset($input['text']) ? $input['text'] : null;
        $this->model->user_id = $userId;
        $course->feedbacks()->save($this->model);

        return $this->model;
    }
}
