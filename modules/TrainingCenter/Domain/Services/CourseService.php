<?php

namespace Modules\TrainingCenter\Domain\Services;


use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Program;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\Lecturer;
use Modules\TrainingCenter\Domain\Models\Like;

class CourseService extends EloquentBreadService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * CourseService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * Get single course
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|Model
     */
    public function getCourse(int $id)
    {
        $course = $this->model->with([
            'notes',
            'likes',
            'sections.quizQuestions',
            'sections.quizQuestions.answers',
            'sections.exercises',
            'sections.videos' => function ($q) {
                return $q->orderBy('order')->with('transcript', 'seenVideos');
            },
            'questions',
            'questions.user',
            'questions.answers',
            'questions.answers.user',
        ])->findOrFail($id);

        return $course;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getCourseForAdmin(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param Course $course
     * @param int $userId
     * @return Course
     */
    public function getCourseStatusAndDuration(Course $course, int $userId)
    {
        $seenVideosCount = 0;
        $completedVideosCount = 0;
        $totalVideosCount = 0;
        $startTime = '00:00:00';
        $totalDuration = Carbon::createFromFormat('H:i:s', $startTime);
        $seenDuration = Carbon::createFromFormat('H:i:s', $startTime);

        foreach ($course->sections as $section) {
            foreach ($section->videos as $video) {
                $totalVideosCount++;
                $time = explode(':', $video->duration);
                $totalDuration->addHour($time[0])->addMinute($time[1])->addSecond($time[2]);

                $seenVideo = $video->seenVideos->where('user_id', $userId)->first();
                if (!is_null($seenVideo)) {
                    $seenVideosCount++;
                    $timeSeen = explode(':', $seenVideo->duration);
                    $seenDuration->addHour($timeSeen[0])->addMinute($timeSeen[1])->addSecond($timeSeen[2]);
                }

                $isCompleted = !is_null($seenVideo) ? $seenVideo->duration >= $video->duration ?? true : false;
                if ($isCompleted) {
                    $completedVideosCount++;
                }
            }
        }

        if ($seenVideosCount === $totalVideosCount) {
            $course->status = Course::STATES['FINISHED'];
        } elseif ($seenVideosCount > 0) {
            $course->status = Course::STATES['IN_PROGRESS'];
        } else {
            $course->status = Course::STATES['NEW'];
        }

        $course->seenVideosCount = $seenVideosCount;
        $course->completedVideosCount = $completedVideosCount;
        $course->totalVideosCount = $totalVideosCount;
        $course->totalDuration = $totalDuration->toTimeString();
        $course->seenDuration = $seenDuration->toTimeString();

        $likes = DB::table('tc_likes')
            ->where('user_id', $userId)
            ->where('likeable_id', $course->id)
            ->where('likeable_type', get_class($course))
            ->get();
        $course->isLiked = !is_null($likes->where('action', Like::$actions['like'])->first());
        $course->isDisliked = !is_null($likes->where('action', Like::$actions['dislike'])->first());

        return $course;
    }

    /**
     * Increment views_count
     *
     * @param Model $course
     */
    public function incrementViewsCount(Model &$course)
    {
        $course->views_count += 1;
        $course->save();
    }

    /**
     * Get all courses
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function getCourseList()
    {
        return $this->model->with('likes')->get();
    }

    /**
     * Get lecturer's courses and archived courses
     *
     * @param Lecturer $lecturer
     * @return mixed
     */
    public function getLecturerCourseList(Lecturer $lecturer)
    {
        return $lecturer
            ->load([
                'courses' => function ($q) {
                    return $q->with('likes')->withCount('likes');
                }
            ]);
    }

    /**
     * Archive course
     *
     * @param int $id
     */
    public function archiveCourse(int $id)
    {
        $this->model->findOrFail($id)->delete();
    }

    /**
     * Assign lecturer with course
     *
     * @param int $courseId
     * @param int $lecturerId
     * @return mixed
     */
    public function assignLecturer(int $courseId, int $lecturerId)
    {
        $course = $this->model->findOrFail($courseId);
        $lecturerId = Lecturer::findOrFail($lecturerId)->id;
        Db::table('tc_course_lecturer')->updateOrInsert(
            [
                'tc_course_id' => $courseId,
                'tc_lecturer_id' => $lecturerId
            ],
            [
                'tc_course_id' => $courseId,
                'tc_lecturer_id' => $lecturerId
            ]
        );

        return $course;
    }

    /**
     * @param int $courseId
     * @param int $lecturerId
     * @return mixed
     */
    public function deleteLecturerFromCourse(int $courseId, int $lecturerId)
    {
        $course = $this->model->findOrFail($courseId);
        $lecturerId = Lecturer::findOrFail($lecturerId)->id;
        $course->programs()->detach($lecturerId);

        return $course;
    }

    /**
     * @param int $courseId
     * @param int $programId
     * @return mixed
     */
    public function assignProgram(int $courseId, int $programId)
    {
        $program = $this->model->findOrFail($courseId);
        $programId = Program::findOrFail($programId)->id;
        Db::table('tc_course_program')->updateOrInsert(
            [
                'tc_course_id' => $courseId,
                'program_id' => $programId
            ],
            [
                'tc_course_id' => $courseId,
                'program_id' => $programId
            ]
        );

        return $program;
    }

    /**
     * @param int $courseId
     * @param int $programId
     * @return mixed
     */
    public function deleteProgramFromCourse(int $courseId, int $programId)
    {
        $course = $this->model->findOrFail($courseId);
        $programId = Program::findOrFail($programId)->id;
        $course->programs()->detach($programId);

        return $course;
    }

}
