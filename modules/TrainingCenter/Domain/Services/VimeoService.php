<?php

namespace Modules\TrainingCenter\Domain\Services;



use SimpleXMLElement;

class VimeoService
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var SimpleXMLElement
     */
    protected $data;

    /**
     * @var string
     */
    protected $videoId;

    /**
     * Set video id
     *
     * @param string $url
     */
    public function setVideoUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * Parse id and xml data from url
     *
     * @return null
     */
    public function parseDataFromUrl()
    {
        if (is_null($this->url)) {
            return null;
        }

        $videoId = (int)substr(parse_url($this->url, PHP_URL_PATH), 1);
        $this->setVideoId($videoId);

        $jsonUrl = 'http://vimeo.com/api/v2/video/' . $videoId . '.xml';

        $ch = curl_init($jsonUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        $data = simplexml_load_string($data);
        $this->setData($data);
    }

    /**
     * Set xml data
     *
     * @param SimpleXMLElement $data
     */
    protected function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Set video id
     *
     * @param $videoId
     * @return mixed
     */
    protected function setVideoId($videoId)
    {
        return $this->videoId = $videoId;
    }

    /**
     * Get video id
     *
     * @return string
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Get video duration
     *
     * @return int|null
     */
    public function getVideoDuration()
    {
        if (!isset($this->data->video->duration)) {
            return null;
        }

        return (int) $this->data->video->duration;
    }
}
