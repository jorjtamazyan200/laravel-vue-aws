<?php

namespace Modules\TrainingCenter\Domain\Services;


use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Database\Eloquent\Model;

class TranscriptService extends EloquentBreadService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * AnswerService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
}
