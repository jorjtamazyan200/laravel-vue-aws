<?php

namespace Modules\TrainingCenter\Domain\Services;


use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ExerciseService extends EloquentBreadService
{
    /**
     * @var Model
     */
    protected $model;


    /**
     * AnswerService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param int $id
     * @param string $fileName
     * @param string $fileContent
     * @param string $fileType
     * @return \App\Infrastructure\Domain\Illuminate\Contracts\Support\Arrayable|\Illuminate\Contracts\Support\Arrayable
     */
    public function uploadFileAndSaveExercise(int $id, string $fileName, string $fileContent, string $fileType)
    {
        $exercise = $this->get($id);

        if (!is_null($exercise->path)) {
            $this->removeFile($this->model->filePath . $fileName);
        }

        $extension = $this->extractFileExtension($fileType);
        $fileName = $fileName . '.' . $extension;
        $this->uploadFile($this->model->filePath . $fileName, $fileContent);

        $exercise->path = $fileName;
        $exercise->save();

        return $exercise;
    }

    /**
     * @param string $path
     */
    protected function removeFile(string $path)
    {
        Storage::disk('public')->delete($path);
    }

    /**
     * @param string $path
     * @param string $content
     * @param string $visibility
     */
    protected function uploadFile(string $path, string $content, $visibility = 'public')
    {
        Storage::disk('public')->put($path, $content, $visibility);
    }

    /**
     * Extracts image extension from Mime-Type
     *
     * @param string $type
     * @return string
     */
    protected function extractFileExtension(string $type)
    {
        return (explode('/', $type))[1];
    }

}
