<?php

namespace Modules\TrainingCenter\Domain\Services;


use App\Exceptions\Client\InvalidVimeoUrlException;
use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Modules\TrainingCenter\Domain\Models\Video;

class VideoService extends EloquentBreadService
{
    /**
     * @var Model
     */
    protected $model;

    protected $vimeoService;

    /**
     * AnswerService constructor.
     * @param Model $model
     * @param VimeoService $vimeoService
     */
    public function __construct(Model $model, VimeoService $vimeoService)
    {
        parent::__construct($model);
        $this->model = $model;

        $this->vimeoService = $vimeoService;
    }

    /**
     * @param array $attributes
     * @return Arrayable
     * @throws InvalidVimeoUrlException
     */
    public function create(array $attributes): Arrayable
    {
        $this->setVideoAttributes($attributes);
        return parent::create($attributes);
    }

    /**
     * @param array $attributes
     * @throws InvalidVimeoUrlException
     */
    protected function setVideoAttributes(array &$attributes)
    {
        switch ($attributes['video_type']) {
            case Video::$videoTypes['vimeo']:
                $this->vimeoService->setVideoUrl($attributes['url']);
                try {
                    $this->vimeoService->parseDataFromUrl();
                } catch (\Exception $e) {
                    throw new InvalidVimeoUrlException();
                }
                $attributes['video_id'] = $this->vimeoService->getVideoId();
                $attributes['duration'] = gmdate("H:i:s", $this->vimeoService->getVideoDuration());
                break;
            default:
                break;
        }
    }
}
