<?php

namespace Modules\TrainingCenter\Domain\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\CourseAnswer;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;
use Modules\TrainingCenter\Domain\Models\Like;

class LikeService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * LikeService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param Course $course
     * @param integer $userId
     * @param bool $isLike
     * @return \Illuminate\Database\Eloquent\Collection|Model
     */
    public function likeOrDislikeCourse(Course $course, int $userId, $isLike = true)
    {
        $this->model->updateOrCreate(
            [
                'likeable_id' => $course->id,
                'likeable_type' => Course::class,
                'user_id' => $userId
            ],
            [
                'action' => $isLike ? Like::$actions['like'] : Like::$actions['dislike']
            ]
        );

        return $course;
    }

    /**
     * @param CourseQuestion $question
     * @param int $userId
     * @param bool $isLike
     * @return \Illuminate\Database\Eloquent\Collection|Model
     */
    public function likeOrDislikeQuestion(CourseQuestion $question, int $userId, $isLike = true)
    {
        $this->model->updateOrCreate(
            [
                'likeable_id' => $question->id,
                'likeable_type' => CourseQuestion::class,
                'user_id' => $userId
            ],
            [
                'action' => $isLike ? Like::$actions['like'] : Like::$actions['dislike']
            ]
        );

        return $question;
    }

    /**
     * @param CourseAnswer $answer
     * @param int $userId
     * @param bool $isLike
     * @return \Illuminate\Database\Eloquent\Collection|Model
     */
    public function likeOrDislikeAnswer(CourseAnswer $answer, int $userId, $isLike = true)
    {
        $this->model->updateOrCreate(
            [
                'likeable_id' => $answer->id,
                'likeable_type' => CourseAnswer::class,
                'user_id' => $userId
            ],
            [
                'action' => $isLike ? Like::$actions['like'] : Like::$actions['dislike']
            ]
        );

        return $answer;
    }

}
