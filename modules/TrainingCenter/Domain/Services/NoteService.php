<?php

namespace Modules\TrainingCenter\Domain\Services;


use Illuminate\Database\Eloquent\Model;
use Modules\TrainingCenter\Domain\Models\Course;

class NoteService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * NoteService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param Course $course
     * @param array $input
     * @param int $userId
     * @return Model
     */
    public function createNote(Course $course, array $input, int $userId)
    {
        $this->model->text = $input['text'];
        $this->model->user_id = $userId;
        $course->notes()->save($this->model);

        return $this->model;
    }

    /**
     * @param int $id
     * @param array $input
     * @param int $userId
     */
    public function updateNote(int $id, array $input, int $userId) {
        $note = $this->model->where('user_id', $userId)->where('id', $id)->firstOrFail();
        $note->text = $input['text'];
        $note->save();
    }

    /**
     * @param int $id
     * @param int $userId
     */
    public function deleteNote(int $id, int $userId)
    {
        $this->model->where('user_id', $userId)->where('id', $id)->firstOrFail()->delete();
    }
}
