<?php

namespace Modules\TrainingCenter\Domain\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;

class CourseQuestionService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * CourseQuestionService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param Course $course
     * @param array $input
     * @param int $userId
     * @return Model
     */
    public function createCourseQuestion(Course $course, array $input, int $userId)
    {
        $this->model->text = $input['text'];
        $this->model->user_id = $userId;
        $course->questions()->save($this->model);

        return $this->model;
    }

    /**
     * @param $id
     * @param array $input
     * @param int $userId
     */
    public function updateQuestion($id, array $input, int $userId)
    {
        $question = $this->model->where('id', $id)->where('user_id', $userId)->firstOrFail();
        $question->text = $input['text'];
        $question->save();
    }

    /**
     * @param int $id
     * @param int $userId
     */
    public function deleteQuestion(int $id, int $userId)
    {
        $this->model->findOrFail($id)->where('user_id', $userId)->delete();
    }
}
