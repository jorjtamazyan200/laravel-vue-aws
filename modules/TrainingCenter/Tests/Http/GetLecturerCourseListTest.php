<?php

namespace Modules\TrainingCenter\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\Lecturer;

/**
 * Class GetLecturerCourseListTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class GetLecturerCourseListTest extends EndpointTest
{
    public function test_it_returns_error_when_lecturer_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->getJson(route('trainingCenter.getLecturerCourseList', 0));

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_lists_lecturer_courses()
    {
        $lecturer = factory(Lecturer::class)->create();
        $course = factory(Course::class)->make();
        $archivedCourse = factory(Course::class)->make();
        $lecturer->courses()->save($course);
        $lecturer->courses()->save($archivedCourse);
        $lecturer->courses[1]->delete();
        $response = $this->actingAs($this->user)->getJson(route('trainingCenter.getLecturerCourseList', $lecturer->id));

        self::assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'courses' => [
                [
                    'id',
                    'title',
                    'description',
                    'thumbnail',
                    'views_count',
                    'likes_count',
                    'dislikes_count',
                ]
            ],

            'lecturer' => [
                'name',
                'description',
                'avatar',
                'email',
                'linkedin_link',
            ]
        ]);
    }
}
