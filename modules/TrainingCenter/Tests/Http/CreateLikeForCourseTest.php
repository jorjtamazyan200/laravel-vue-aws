<?php

namespace Modules\TrainingCenter\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\Like;

/**
 * Class CreateLikeForCourseTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class CreateLikeForCourseTest extends EndpointTest
{
    public function test_it_returns_error_when_course_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.likeCourse', 0));
        $response->assertStatus(404);
    }

    public function test_it_returns_when_like_course_without_authentication()
    {
        $course = factory(Course::class)->create();
        $response = $this->postJson(route('trainingCenter.courses.likeCourse', $course->id));
        $response->assertStatus(401);
    }

    public function test_it_creates_like_for_course()
    {
        $course = factory(Course::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.likeCourse', $course->id));
        $response->assertStatus(200);

        $this->assertDatabaseHas('tc_likes', [
            'likeable_id' => $course->id,
            'likeable_type' => get_class($course),
            'user_id' => $this->user->id,
            'action' => Like::$actions['like']
        ]);
    }

    public function test_it_returns_when_dislike_course_without_authentication()
    {
        $course = factory(Course::class)->create();
        $response = $this->postJson(route('trainingCenter.courses.dislikeCourse', $course->id));
        $response->assertStatus(401);
    }

    public function test_it_creates_dislike_for_course()
    {
        $course = factory(Course::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.dislikeCourse', $course->id));
        $response->assertStatus(200);

        $this->assertDatabaseHas('tc_likes', [
            'likeable_id' => $course->id,
            'likeable_type' => get_class($course),
            'user_id' => $this->user->id,
            'action' => Like::$actions['dislike']
        ]);
    }
}
