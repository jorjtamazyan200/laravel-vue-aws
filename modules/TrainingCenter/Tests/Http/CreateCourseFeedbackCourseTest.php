<?php

namespace Modules\TrainingCenter\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\Course;

class CreateCourseFeedbackCourseTest extends EndpointTest
{
    public function test_it_returns_error_when_course_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.leaveCourseFeedback', 0),[
            'text' => 'Test feedback',
            'rating' => 5,
        ]);
        $response->assertStatus(404);
    }

    public function test_it_returns_error_when_user_isnt_authorized()
    {
        $course = factory(Course::class)->create();
        $response = $this->postJson(route('trainingCenter.courses.leaveCourseFeedback', $course->id),[
            'text' => 'Test feedback',
            'rating' => 5,
        ]);
        $response->assertStatus(401);
    }

    public function test_it_leaves_feedback()
    {
        $course = factory(Course::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.leaveCourseFeedback', $course->id),[
            'text' => 'Test feedback',
            'rating' => 5,
            'user_id' => $this->user->id
        ]);
        $response->assertStatus(201);

        $this->assertDatabaseHas('tc_course_feedbacks', [
            'tc_course_id' => $course->id,
            'text' => 'Test feedback',
            'rating' => 5,
            'user_id' => $this->user->id
        ]);
    }
}
