<?php

namespace Modules\TrainingCenter\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;

class DeleteCourseQuestionTest extends EndpointTest
{
    public function test_it_returns_error_when_question_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->deleteJson(route('trainingCenter.questions.deleteQuestion', 0));
        $response->assertStatus(404);
    }

    public function it_returns_error_when_user_is_not_authorized()
    {
        $question = factory(CourseQuestion::class)->create();
        $response = $this->deleteJson(route('trainingCenter.questions.deleteQuestion', $question->id));
        $response->assertStatus(422);
    }

    public function test_it_deletes_question()
    {
        $question = factory(CourseQuestion::class)->create([
            'user_id' => $this->user->id,
            'text' => 'test question'
        ]);
        $response = $this->actingAs($this->user)->deleteJson(route('trainingCenter.questions.deleteQuestion', $question->id));
        $response->assertStatus(200);

        $this->assertSoftDeleted((new CourseQuestion)->getTable(), [
            'text' => 'test question',
            'id' => $question->id
        ]);
    }
}
