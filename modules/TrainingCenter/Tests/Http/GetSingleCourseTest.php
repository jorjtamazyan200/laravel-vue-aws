<?php

namespace Modules\TrainingCenter\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\Course;

/**
 * Class GetSingleCourseTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class GetSingleCourseTest extends EndpointTest
{
    public function test_it_returns_error_when_course_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->getJson(route('trainingCenter.courses.getCourse', 0));

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_get_single_course()
    {
        $course = factory(Course::class)->create();
        $response = $this->actingAs($this->user)->getJson(route('trainingCenter.courses.getCourse', $course->id));

        self::assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'description',
                'thumbnail',
                'views_count',
                'notes',
//                'likes',
                'sections',
                'questions',
            ]
        ]);
    }
}
