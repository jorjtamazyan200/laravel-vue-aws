<?php

namespace Modules\TrainingCenter\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\CourseAnswer;
use Modules\TrainingCenter\Domain\Models\Like;

/**
 * Class CreateLikeForAnswerTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class CreateLikeForAnswerTest extends EndpointTest
{
    public function test_it_returns_error_when_answer_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.likeAnswer', 0));
        $response->assertStatus(404);
    }

    public function test_it_returns_error_when_like_answer_without_authentication()
    {
        $answer = factory(CourseAnswer::class)->create();
        $response = $this->postJson(route('trainingCenter.likeAnswer', $answer->id));
        $response->assertStatus(401);
    }

    public function test_it_creates_like_for_answer()
    {
        $answer = factory(CourseAnswer::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.likeAnswer', $answer->id));
        $response->assertStatus(200);

        $this->assertDatabaseHas('tc_likes', [
            'likeable_id' => $answer->id,
            'likeable_type' => get_class($answer),
            'user_id' => $this->user->id,
            'action' => Like::$actions['like']
        ]);
    }

    public function test_it_returns_error_when_dislike_answer_without_authentication()
    {
        $answer = factory(CourseAnswer::class)->create();
        $response = $this->postJson(route('trainingCenter.dislikeAnswer', $answer->id));
        $response->assertStatus(401);
    }

    public function test_it_creates_dislike_for_answer()
    {
        $answer = factory(CourseAnswer::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.dislikeAnswer', $answer->id));
        $response->assertStatus(200);

        $this->assertDatabaseHas('tc_likes', [
            'likeable_id' => $answer->id,
            'likeable_type' => get_class($answer),
            'user_id' => $this->user->id,
            'action' => Like::$actions['dislike']
        ]);
    }
}
