<?php

namespace Modules\TrainingCenter\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\Coursenote;
use Modules\TrainingCenter\Domain\Models\Note;

class UpdateNoteTest extends EndpointTest
{
    public function test_it_returns_error_when_note_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->putJson(route('trainingCenter.notes.editNote', 0),
            ['text' => 'test note']
        );
        $response->assertStatus(404);
    }

    public function it_returns_error_when_user_is_not_authorized()
    {
        $note = factory(Note::class)->create();
        $response = $this->putJson(route('trainingCenter.notes.editNote', $note->id),
            ['text' => 'test note']
        );
        $response->assertStatus(422);
    }

    public function test_it_updates_note()
    {
        $note = factory(Note::class)->create([
            'user_id' => $this->user->id
        ]);
        $response = $this->actingAs($this->user)->putJson(route('trainingCenter.notes.editNote', $note->id),
            ['text' => 'test note']
        );
        $response->assertStatus(200);

        $this->assertDatabaseHas((new Note)->getTable(), [
            'text' => 'test note',
            'id' => $note->id
        ]);
    }
}
