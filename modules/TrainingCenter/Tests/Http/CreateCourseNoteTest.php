<?php

namespace Modules\TrainingCenter\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\Course;

/**
 * Class CreateNoteCourseTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class CreateCourseNoteTest extends EndpointTest
{
    public function test_it_returns_error_when_course_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.createNote', 0),
            ['text' => 'Test note']);
        $response->assertStatus(404);
    }

    public function test_it_returns_when_creates_note_without_authentication()
    {
        $course = factory(Course::class)->create();
        $response = $this->postJson(route('trainingCenter.courses.createNote', $course->id), [
            'text' => 'Test note'
        ]);
        $response->assertStatus(401);
    }

    public function test_it_creates_note()
    {
        $course = factory(Course::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.createNote', $course->id), [
            'text' => 'Test note'
        ]);
        $response->assertStatus(201);
    }
}
