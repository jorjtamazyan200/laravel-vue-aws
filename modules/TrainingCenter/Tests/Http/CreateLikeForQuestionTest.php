<?php

namespace Modules\TrainingCenter\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;
use Modules\TrainingCenter\Domain\Models\Like;

/**
 * Class CreateLikeForQuestionTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class CreateLikeForQuestionTest extends EndpointTest
{
    public function test_it_returns_error_when_question_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.questions.likeQuestion', 0));
        $response->assertStatus(404);
    }

    public function test_it_returns_error_when_like_question_without_authentication()
    {
        $question = factory(CourseQuestion::class)->create();
        $response = $this->postJson(route('trainingCenter.questions.likeQuestion', $question->id));
        $response->assertStatus(401);
    }

    public function test_it_creates_like_for_question()
    {
        $question = factory(CourseQuestion::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.questions.likeQuestion', $question->id));
        $response->assertStatus(200);

        $this->assertDatabaseHas('tc_likes', [
            'likeable_id' => $question->id,
            'likeable_type' => get_class($question),
            'user_id' => $this->user->id,
            'action' => Like::$actions['like']
        ]);
    }

    public function test_it_returns_error_when_dislike_question_without_authentication()
    {
        $question = factory(CourseQuestion::class)->create();
        $response = $this->postJson(route('trainingCenter.questions.dislikeQuestion', $question->id));
        $response->assertStatus(401);
    }

    public function test_it_creates_dislike_for_question()
    {
        $question = factory(CourseQuestion::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.questions.dislikeQuestion', $question->id));
        $response->assertStatus(200);

        $this->assertDatabaseHas('tc_likes', [
            'likeable_id' => $question->id,
            'likeable_type' => get_class($question),
            'user_id' => $this->user->id,
            'action' => Like::$actions['dislike']
        ]);
    }
}
