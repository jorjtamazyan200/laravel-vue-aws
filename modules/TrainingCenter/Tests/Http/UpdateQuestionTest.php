<?php

namespace Modules\TrainingCenter\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;

class UpdateQuestionTest extends EndpointTest
{
    public function test_it_returns_error_when_question_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->putJson(route('trainingCenter.questions.editQuestion', 0),
            ['text' => 'test question']
        );
        $response->assertStatus(404);
    }

    public function it_returns_error_when_user_is_not_authorized()
    {
        $question = factory(CourseQuestion::class)->create();
        $response = $this->putJson(route('trainingCenter.questions.editQuestion', $question->id),
            ['text' => 'test question']
        );
        $response->assertStatus(422);
    }

    public function test_it_updates_question()
    {
        $question = factory(CourseQuestion::class)->create([
            'user_id' => $this->user->id
        ]);
        $response = $this->actingAs($this->user)->putJson(route('trainingCenter.questions.editQuestion', $question->id),
            ['text' => 'test question']
        );
        $response->assertStatus(200);

        $this->assertDatabaseHas((new CourseQuestion)->getTable(), [
            'text' => 'test question',
            'id' => $question->id
        ]);
    }
}
