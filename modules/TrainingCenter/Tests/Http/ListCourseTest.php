<?php

namespace Modules\TrainingCenter\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Group;

/**
 * Class ListCourseTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class ListCourseTest extends EndpointTest
{
    public function test_it_lists_course_list()
    {
        $response = $this->actingAs($this->user)->getJson(route('trainingCenter.courses.getCourseList'));

        self::assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' =>
                [
                    [
                        'id',
                        'title',
                        'description',
                        'thumbnail',
                        'views_count',
                        'likes_count',
                        'dislikes_count',
//                        'likes'
                    ]
                ]
        ]);
    }
}
