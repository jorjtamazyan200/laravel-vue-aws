<?php

namespace Modules\TrainingCenter\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\Coursenote;
use Modules\TrainingCenter\Domain\Models\Note;

class DeleteNoteTest extends EndpointTest
{
    public function test_it_returns_error_when_note_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->deleteJson(route('trainingCenter.notes.deleteNote', 0));
        $response->assertStatus(404);
    }

    public function it_returns_error_when_user_is_not_authorized()
    {
        $note = factory(Note::class)->create();
        $response = $this->deleteJson(route('trainingCenter.notes.deleteNote', $note->id));
        $response->assertStatus(422);
    }

    public function test_it_deletes_note()
    {
        $note = factory(Note::class)->create([
            'user_id' => $this->user->id
        ]);
        $response = $this->actingAs($this->user)->deleteJson(route('trainingCenter.notes.deleteNote', $note->id));
        $response->assertStatus(200);

        $this->assertDatabaseMissing((new Note)->getTable(), [
            'text' => 'test note',
            'id' => $note->id
        ]);
    }
}
