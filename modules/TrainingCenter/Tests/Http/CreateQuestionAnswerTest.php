<?php

namespace Modules\TrainingCenter\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\CourseAnswer;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;

/**
 * Class CreateQuestionAnswerTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class CreateQuestionAnswerTest extends EndpointTest
{
    public function test_it_returns_error_when_answer_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.questions.createAnswer', 0),
            ['text' => 'Test answer']);
        $response->assertStatus(404);
    }

    public function test_it_returns_when_creates_answer_without_authentication()
    {
        $question = factory(CourseQuestion::class)->create();
        $response = $this->postJson(route('trainingCenter.questions.createAnswer', $question->id), [
            'text' => 'Test answer'
        ]);
        $response->assertStatus(401);
    }

    public function test_it_creates_answer()
    {
        $question = factory(CourseQuestion::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.questions.createAnswer', $question->id), [
            'text' => 'Test answer'
        ]);
        $response->assertStatus(201);
        $this->assertDatabaseHas((new CourseAnswer())->getTable(), [
            'text' => 'Test answer'
        ]);
    }
}
