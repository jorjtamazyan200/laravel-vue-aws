<?php

namespace Modules\TrainingCenter\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\TrainingCenter\Domain\Models\Course;
use Modules\TrainingCenter\Domain\Models\CourseQuestion;

/**
 * Class CreateNoteCourseTest
 * @package Modules\TrainingCenter\Tests\Http
 */
class CreateCourseQuestionTest extends EndpointTest
{
    public function test_it_returns_error_when_course_doesnt_exist()
    {
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.createQuestion', 0),
            ['text' => 'Test question']);
        $response->assertStatus(404);
    }

    public function test_it_returns_when_creates_question_without_authentication()
    {
        $course = factory(Course::class)->create();
        $response = $this->postJson(route('trainingCenter.courses.createQuestion', $course->id), [
            'text' => 'Test question'
        ]);
        $response->assertStatus(401);
    }

    public function test_it_creates_question()
    {
        $course = factory(Course::class)->create();
        $response = $this->actingAs($this->user)->postJson(route('trainingCenter.courses.createQuestion', $course->id), [
            'text' => 'Test question'
        ]);
        $response->assertStatus(201);
        $this->assertDatabaseHas((new CourseQuestion)->getTable(), [
            'text' => 'Test question'
        ]);
    }
}
