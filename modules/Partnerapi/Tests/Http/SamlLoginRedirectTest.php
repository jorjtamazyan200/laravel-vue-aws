<?php

namespace Modules\Partnerapi\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Admin\Http\Resources\OrganizationInteractionHttpResource;
use Modules\Partnerapi\Domain\Models\OrganizationSaml;
use Modules\Partnerapi\Domain\Models\PartnerAccess;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class BasicSamlTest extends EndpointTest
{

    public function test_it_redirects_you_to_saml_server()
    {
        $this->markTestSkipped('in console test its not possible to test the redirect');

        /** @var OrganizationSaml $organiationSaml */
        $organiationSaml = OrganizationSaml::query()->firstOrFail();
        $route = route('saml.login', ['slug' => $organiationSaml->slug]);
        $response = $this->getJson($route);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


    }

}
