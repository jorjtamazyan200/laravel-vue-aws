<?php

namespace Modules\Partnerapi\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Partnerapi\Domain\Models\PartnerAccess;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class HandleOauthUserTest extends EndpointTest
{

    public function test_it_allows_simple_oauth()
    {

        $partnerAccess = new PartnerAccess();
        $partnerAccess->secret = 'sample';
        $partnerAccess->api_base = 'http://test-api.kiron.ngo/api/sso/%s';
        $partnerAccess->api_base = 'http://extern.fakir.it/vv/kiron.sample.json';
        $partnerAccess->organization_id = 1;
        $partnerAccess->registration_code_id = 1;
        $partnerAccess->save();

        // CustardappleService
        $route = route('partnerapi.oauth', ['id' => $partnerAccess->id, 'token' =>'aasdsdas']);

        $response = $this->getJson($route);
        $this->assertEquals(302, $response->getStatusCode(), $response->getContent());
    }

}
