<?php

namespace Modules\Partnerapi\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use GuzzleHttp\Psr7\Request;
use Modules\Admin\Http\Resources\OrganizationInteractionHttpResource;
use Modules\Partnerapi\Domain\Models\OrganizationSaml;
use Modules\Partnerapi\Domain\Models\PartnerAccess;

/**
 *
 * @docs https://www.samltool.com/generic_sso_res.php
 */
class SamlAfterLoginFujitsuTest extends EndpointTest
{

    public function test_it_processes_an_acs_rquest()
    {
        // fujitsu testing credentials
        //
        /** @var OrganizationSaml $organiationSaml */
        $organiationSaml = factory(OrganizationSaml::class)->create([
            'slug' => 'FUJ',
            'organization_id' => 1,
            'certificate' => 'MIIHTDCCBjSgAwIBAgIQcRtmBEYxS8cAAAAAUOwa/zANBgkqhkiG9w0BAQsFADCBujELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMxOTA3BgNVBAsTMChjKSAyMDEyIEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXplZCB1c2Ugb25seTEuMCwGA1UEAxMlRW50cnVzdCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSAtIEwxSzAeFw0xOTAxMTQwOTA1NTJaFw0yMTA0MTMwOTM1NTFaMHAxCzAJBgNVBAYTAkdCMRMwEQYDVQQHEwpNYW5jaGVzdGVyMR0wGwYDVQQKExRGdWppdHN1IFNlcnZpY2VzIEx0ZDEtMCsGA1UEAxMkc2lnbmluZy5hZGZzMS1kZXYuZ2xvYmFsLmZ1aml0c3UuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmjWOsyXb4SudXnSgXnG4NyQnXKLurpqNMrBEbPNOz+h7V7v02pBgn0hz6rM2OZdu5ggLehnockUHBmbNAiRpKtZI189a9gg9sTEfIvh/To9qsAVkaxVRp6hxqn8nwqjni2mq96VXrO8z90X5oD/J/x9onjdE5HcFzn56iBt9GWkVT4gkEZY15TVW6Bz70IN04o/ckBAeHnGblaI9V/5XbvIow4frziDrvgNOtFrknTCQ+PXILG6V1GwsUODSx7STZkg8unu1qeaxSDT2sqV+e/dLCQ+82SVTZSWmdDdbr/jOLeeBusIrc9wR0cndbWejqJ4HvvZ0SUZDEa6KmRLk/wIDAQABo4IDlTCCA5EwLwYDVR0RBCgwJoIkc2lnbmluZy5hZGZzMS1kZXYuZ2xvYmFsLmZ1aml0c3UuY29tMIIB9gYKKwYBBAHWeQIEAgSCAeYEggHiAeAAdgBVgdTCFpA2AUrqC5tXPFPwwOQ4eHAlCBcvo6odBxPTDAAAAWhLt6YEAAAEAwBHMEUCIFeBDh5fDGasJNqBl7llJryGlSNWADPKRrMNEZgTxd8aAiEAyeDs69tgDsmb9OgXOVLbsHP7se+0tCaDj0Q9119i31QAdQCHdb/nWXz4jEOZX73zbv9WjUdWNv9KtWDBtOr/XqCDDwAAAWhLt6YXAAAEAwBGMEQCID0aq8B6+UksV99zPBuiXZm8umZA8wzRUUlGpHD49qrzAiB5cAcYmMyyl/dSh2+tQkpBazasNmcX2IRFB00oBUrfiwB3AFYUBpov18Ls0/XhvUSyPsdGdrm8mRFcwO+UmFXWidDdAAABaEu3picAAAQDAEgwRgIhANYWOyHGjvHAPDvUAC57p7+5arODOeZox1vrU31/EI1vAiEA2jR8t6DD6BygkgmhyWpMv6Ma38qnVeYIyPsBD2awjbkAdgC72d+8H4pxtZOUI5eqkntHOFeVCqtS6BqQlmQ2jh7RhQAAAWhLt6YRAAAEAwBHMEUCIQDjhEJtauw/PX9si9UYwc30unvhgaba1+b/pOCikp4vYAIgJq65/aEmNm/TH3JjEJ/tmMMjPmG9WfZqHsc/0A1iDWswDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAzBgNVHR8ELDAqMCigJqAkhiJodHRwOi8vY3JsLmVudHJ1c3QubmV0L2xldmVsMWsuY3JsMEsGA1UdIAREMEIwNgYKYIZIAYb6bAoBBTAoMCYGCCsGAQUFBwIBFhpodHRwOi8vd3d3LmVudHJ1c3QubmV0L3JwYTAIBgZngQwBAgIwaAYIKwYBBQUHAQEEXDBaMCMGCCsGAQUFBzABhhdodHRwOi8vb2NzcC5lbnRydXN0Lm5ldDAzBggrBgEFBQcwAoYnaHR0cDovL2FpYS5lbnRydXN0Lm5ldC9sMWstY2hhaW4yNTYuY2VyMB8GA1UdIwQYMBaAFIKicHTdvFM/z3vU981/p2DGCky/MB0GA1UdDgQWBBSbAVeZCZdqL5XaSlIOpr+4DG3X1DAJBgNVHRMEAjAAMA0GCSqGSIb3DQEBCwUAA4IBAQA9qf5AyhBkaptoMfy1s0XHtN6zdrdKCWOpM5HFx/tE4TZiXZtpTvp1VtIHu8zVNwufFgmXoNUGu82Zs5ePPfDKXsRNWwD0ivfdFPtlF2vc49mgxRnditQ6xcS74F3xmLWPWWjs1W6oNbVp8VvrP1v4wCUFJwTvJPHlFzTDuM5oIteQObu6GAHHKHuKcxM9Obtrpn3sP0KERZx3HsrO4pQG1u37QeVfXzOL+F0IbO+hmBN2sTjCrlbKs+taMUYurN1C22w/rKNXCpTICdgMbch5JWhMIs1Ba7Lfin7SgnDn/t2tfclFUialtPZmnqmq6OhqeRXTSyGu2iwPce20n3uI'
        ]);


        $xmlData = file_get_contents(__DIR__ . '/mock/fujitsu.response.xml');
        $base64 = base64_encode($xmlData);


        $slug = $organiationSaml->slug;
        $url = route('saml.acs', ['slug' => $slug]);

        $headers = [
            'Accept' => 'application/json'
        ];

        $response = $this->json('POST', $url, [
            'SAMLResponse' => $base64
        ],
            $headers);

        $this->assertEquals(302, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_processes_an_acs_rquest2()
    {
        // fujitsu testing credentials
        //
        /** @var OrganizationSaml $organiationSaml */
        $organiationSaml = factory(OrganizationSaml::class)->create([
            'slug' => 'FUJ',
            'organization_id' => 1,
            'certificate' => 'MIIHTDCCBjSgAwIBAgIQcRtmBEYxS8cAAAAAUOwa/zANBgkqhkiG9w0BAQsFADCBujELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMxOTA3BgNVBAsTMChjKSAyMDEyIEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXplZCB1c2Ugb25seTEuMCwGA1UEAxMlRW50cnVzdCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSAtIEwxSzAeFw0xOTAxMTQwOTA1NTJaFw0yMTA0MTMwOTM1NTFaMHAxCzAJBgNVBAYTAkdCMRMwEQYDVQQHEwpNYW5jaGVzdGVyMR0wGwYDVQQKExRGdWppdHN1IFNlcnZpY2VzIEx0ZDEtMCsGA1UEAxMkc2lnbmluZy5hZGZzMS1kZXYuZ2xvYmFsLmZ1aml0c3UuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmjWOsyXb4SudXnSgXnG4NyQnXKLurpqNMrBEbPNOz+h7V7v02pBgn0hz6rM2OZdu5ggLehnockUHBmbNAiRpKtZI189a9gg9sTEfIvh/To9qsAVkaxVRp6hxqn8nwqjni2mq96VXrO8z90X5oD/J/x9onjdE5HcFzn56iBt9GWkVT4gkEZY15TVW6Bz70IN04o/ckBAeHnGblaI9V/5XbvIow4frziDrvgNOtFrknTCQ+PXILG6V1GwsUODSx7STZkg8unu1qeaxSDT2sqV+e/dLCQ+82SVTZSWmdDdbr/jOLeeBusIrc9wR0cndbWejqJ4HvvZ0SUZDEa6KmRLk/wIDAQABo4IDlTCCA5EwLwYDVR0RBCgwJoIkc2lnbmluZy5hZGZzMS1kZXYuZ2xvYmFsLmZ1aml0c3UuY29tMIIB9gYKKwYBBAHWeQIEAgSCAeYEggHiAeAAdgBVgdTCFpA2AUrqC5tXPFPwwOQ4eHAlCBcvo6odBxPTDAAAAWhLt6YEAAAEAwBHMEUCIFeBDh5fDGasJNqBl7llJryGlSNWADPKRrMNEZgTxd8aAiEAyeDs69tgDsmb9OgXOVLbsHP7se+0tCaDj0Q9119i31QAdQCHdb/nWXz4jEOZX73zbv9WjUdWNv9KtWDBtOr/XqCDDwAAAWhLt6YXAAAEAwBGMEQCID0aq8B6+UksV99zPBuiXZm8umZA8wzRUUlGpHD49qrzAiB5cAcYmMyyl/dSh2+tQkpBazasNmcX2IRFB00oBUrfiwB3AFYUBpov18Ls0/XhvUSyPsdGdrm8mRFcwO+UmFXWidDdAAABaEu3picAAAQDAEgwRgIhANYWOyHGjvHAPDvUAC57p7+5arODOeZox1vrU31/EI1vAiEA2jR8t6DD6BygkgmhyWpMv6Ma38qnVeYIyPsBD2awjbkAdgC72d+8H4pxtZOUI5eqkntHOFeVCqtS6BqQlmQ2jh7RhQAAAWhLt6YRAAAEAwBHMEUCIQDjhEJtauw/PX9si9UYwc30unvhgaba1+b/pOCikp4vYAIgJq65/aEmNm/TH3JjEJ/tmMMjPmG9WfZqHsc/0A1iDWswDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAzBgNVHR8ELDAqMCigJqAkhiJodHRwOi8vY3JsLmVudHJ1c3QubmV0L2xldmVsMWsuY3JsMEsGA1UdIAREMEIwNgYKYIZIAYb6bAoBBTAoMCYGCCsGAQUFBwIBFhpodHRwOi8vd3d3LmVudHJ1c3QubmV0L3JwYTAIBgZngQwBAgIwaAYIKwYBBQUHAQEEXDBaMCMGCCsGAQUFBzABhhdodHRwOi8vb2NzcC5lbnRydXN0Lm5ldDAzBggrBgEFBQcwAoYnaHR0cDovL2FpYS5lbnRydXN0Lm5ldC9sMWstY2hhaW4yNTYuY2VyMB8GA1UdIwQYMBaAFIKicHTdvFM/z3vU981/p2DGCky/MB0GA1UdDgQWBBSbAVeZCZdqL5XaSlIOpr+4DG3X1DAJBgNVHRMEAjAAMA0GCSqGSIb3DQEBCwUAA4IBAQA9qf5AyhBkaptoMfy1s0XHtN6zdrdKCWOpM5HFx/tE4TZiXZtpTvp1VtIHu8zVNwufFgmXoNUGu82Zs5ePPfDKXsRNWwD0ivfdFPtlF2vc49mgxRnditQ6xcS74F3xmLWPWWjs1W6oNbVp8VvrP1v4wCUFJwTvJPHlFzTDuM5oIteQObu6GAHHKHuKcxM9Obtrpn3sP0KERZx3HsrO4pQG1u37QeVfXzOL+F0IbO+hmBN2sTjCrlbKs+taMUYurN1C22w/rKNXCpTICdgMbch5JWhMIs1Ba7Lfin7SgnDn/t2tfclFUialtPZmnqmq6OhqeRXTSyGu2iwPce20n3uI'
        ]);


        $xmlData = file_get_contents(__DIR__ . '/mock/fujitsu.response2.xml');
        $base64 = base64_encode($xmlData);
        $slug = $organiationSaml->slug;
        $url = route('saml.acs', ['slug' => $slug]);

        $headers = [
            'Accept' => 'application/json'
        ];

        $response = $this->json('POST', $url, [
            'SAMLResponse' => $base64
        ],
            $headers);

        $this->assertEquals(302, $response->getStatusCode(), $response->getContent());
    }


}
