<?php

namespace Modules\Partnerapi\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Admin\Http\Resources\OrganizationInteractionHttpResource;
use Modules\Partnerapi\Domain\Models\OrganizationSaml;
use Modules\Partnerapi\Domain\Models\PartnerAccess;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class SlugtoSamlTest extends EndpointTest
{

    public function test_to_receive_correct_metadata()
    {

        /** @var OrganizationSaml $organiationSaml */
        $organiationSaml = factory(OrganizationSaml::class)->create([
            'slug' => 'UNIGUQESLUG',
            'organization_id' => 1,
            'certificate' => 'MIICajCCAdOgAwIBAgIBADANBgkqhkiG9w0BAQ0FADBSMQswCQYDVQQGEwJ1czETMBEGA1UECAwKQ2FsaWZvcm5pYTEVMBMGA1UECgwMT25lbG9naW4gSW5jMRcwFQYDVQQDDA5zcC5leGFtcGxlLmNvbTAeFw0xNDA3MTcxNDEyNTZaFw0xNTA3MTcxNDEyNTZaMFIxCzAJBgNVBAYTAnVzMRMwEQYDVQQIDApDYWxpZm9ybmlhMRUwEwYDVQQKDAxPbmVsb2dpbiBJbmMxFzAVBgNVBAMMDnNwLmV4YW1wbGUuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDZx+ON4IUoIWxgukTb1tOiX3bMYzYQiwWPUNMp+Fq82xoNogso2bykZG0yiJm5o8zv/sd6pGouayMgkx/2FSOdc36T0jGbCHuRSbtia0PEzNIRtmViMrt3AeoWBidRXmZsxCNLwgIV6dn2WpuE5Az0bHgpZnQxTKFek0BMKU/d8wIDAQABo1AwTjAdBgNVHQ4EFgQUGHxYqZYyX7cTxKVODVgZwSTdCnwwHwYDVR0jBBgwFoAUGHxYqZYyX7cTxKVODVgZwSTdCnwwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQ0FAAOBgQByFOl+hMFICbd3DJfnp2Rgd/dqttsZG/tyhILWvErbio/DEe98mXpowhTkC04ENprOyXi7ZbUqiicF89uAGyt1oqgTUCD1VsLahqIcmrzgumNyTwLGWo17WDAa1/usDhetWAMhgzF/Cnf5ek0nK00m0YZGyc4LzgD0CROMASTWNg=='
        ]);

        $route = route('saml.metadata', ['slug' => $organiationSaml->slug]);
        $response = $this->getJson($route);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertTrue(strpos($response->getContent(), '/saml/UNIGUQESLUG/metadata') !== false, 'Meta data url was not found!');

    }

    public function test_it_does_reject_invalid_slug()
    {


        $route = route('saml.metadata', ['slug' => "INVALID"]);
        $response = $this->getJson($route);

        $this->assertEquals(422, $response->getStatusCode(), $response->getContent());


    }

}
