<?php

namespace Modules\Partnerapi\Domain\Services;

use Aacotroneo\Saml2\Saml2User;
use GuzzleHttp\Client;

use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Partnerapi\Domain\Models\OrganizationSaml;

use Illuminate\Support\Facades\Log;

class SamlService extends AbstractSSOService
{

    private $token = null;
    private $user = null;
    private $baseUrl = null;

    private $enabled = false;


    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $userService;

    /**response
     * Construct an instance of the Service.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient, UserService $userService)
    {
        $this->httpClient = $httpClient;
        $this->userService = $userService;


    }

    public function findBySlug($slug): OrganizationSaml
    {
        return OrganizationSaml::query()->where('slug', $slug)->firstOrFail();
    }

    public function getSettingsBySlug($slug)
    {
        $organizationSaml = $this->findBySlug($slug);
        return $this->toSettings($organizationSaml);
    }

    /**
     * Made for single sign on redirect;
     * @param $email
     * @return OrganizationSaml|null
     */
    public function findSamlRedirectByEmail($email)
    {
        if (empty($email)) {
            return null;
        }
        $samlOrgas = OrganizationSaml::query()->whereNotNull('email_pattern')->get(); // where emai
        $foundOrga = null;

        /** @var OrganizationSaml $samlOrga */
        foreach ($samlOrgas as $samlOrga) {
            if (empty($samlOrga->email_pattern)) {
                continue;
            }
            try {
                $match = preg_match($samlOrga->email_pattern, $email);
                if ($match !== 0 && $match !== false) {
                    $foundOrga = $samlOrga;
                    break;
                }
            } catch (\Exception $e) {
            }
        }
        return $foundOrga;

    }

    /**
     * @param OrganizationSaml $organizationSaml
     * @param Saml2User $saml2User
     * @return User
     * @throws \Exception
     */
    public
    function findOrCreate(OrganizationSaml $organizationSaml, Saml2User $saml2User): User
    {

        /** @var RegistrationCode $registrationCode */
        $registrationCode = $organizationSaml->registrationCode;

        /** @var Organization $organization */
        $organization = $organizationSaml->organization;

        $existingUser = User::query()
            ->where('organization_id', $organization->id)
            ->where('external_user_id', $saml2User->getUserId())
            ->first();

        if ($existingUser) {
            return $existingUser;
        }

        // can be solved in an or query but I want to give external_user_id priority;
        $existingUser = User::query()
            ->where('organization_id', $organization->id)
            ->where('email', $saml2User->getUserId())
            ->first();

        if ($existingUser) {
            return $existingUser;
        }




        $userData = $this->saml2UserToLocalUserdata($organizationSaml, $saml2User);
        $user = $this->userService->createAndInviteUser($userData, $registrationCode, false);

        return $user;

    }

    /**
     * @param Saml2User $user
     * @param array $attributeList
     * @return null|string
     */
    private
    function tryAttributes(saml2User $user, array $attributeList)
    {
        foreach ($attributeList as $attr) {
            $result = $user->getAttribute($attr);
            if (!empty($result)) {
                if (is_array($result)) {
                    $result = implode(' ', $result);
                }
                return $result;
            }
        }
        return null;
    }

    private
    function saml2UserToLocalUserdata(OrganizationSaml $organizationSaml, Saml2User $saml2User)
    {


        $attributes = $saml2User->getAttributes();
        $attributes_json = json_encode($attributes);
        Log::error('[saml][debug] converting user - all attributes' . $attributes_json);


        $userData = [
            'id' => $saml2User->getUserId(),
            'organization_id' => $organizationSaml->organization_id,
            'email' => $this->tryAttributes($saml2User, ['emailaddress', 'User.email']),
            'external_user_id' => $saml2User->getUserId(),
            'external_attributes' => $attributes,
            'first_name' => $this->tryAttributes($saml2User, ['givenname', 'User.FirstName']),
            'last_name' => $this->tryAttributes($saml2User, ['surname', 'User.LastName']),
        ];
        if (empty($userData['email'])) {
            $userData['email'] = $saml2User->getUserId();
        }

        return $userData;

    }

    public
    function toSettings(OrganizationSaml $organizationSaml)
    {
        return [
            // Identifier of the IdP entity  (must be a URI)
            'entityId' => $organizationSaml->entity_uri,
            // SSO endpoint info of the IdP. (Authentication Request protocol)
            'singleSignOnService' => [
                // URL Target of the IdP where the SP will send the Authentication Request Message,
                // using HTTP-Redirect binding.
                'url' => $organizationSaml->login_redirect_uri
            ],
            // SLO endpoint info of the IdP.
            'singleLogoutService' => [
                // URL Location of the IdP where the SP will send the SLO Request,
                // using HTTP-Redirect binding.
                'url' => $organizationSaml->logout_redirect_uri
            ],
            // Public x509 certificate of the IdP
            'x509cert' => $organizationSaml->certificate,
//            'certFingerprint' => $organizationSaml->certificate_fingerprint,
            /*
             *  Instead of use the whole x509cert you can use a fingerprint
             *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it)
             */
            // 'certFingerprint' => '',
        ];

    }


}
