<?php

namespace Modules\Partnerapi\Domain\Services;

use App\Exceptions\Client\NotAuthorizedException;
use App\Exceptions\Server\ServerException;
use Brick\PhoneNumber\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberParseException;
use GuzzleHttp\Exception\ClientException;
use Laravel\Passport\Client as OauthClient;

use GuzzleHttp\Client;


use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;

use Modules\Core\Domain\Services\UserService;
use Modules\Partnerapi\Domain\Models\PartnerAccess;

class PartnerOrganizationAccessService extends AbstractSSOService
{

    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $partnerOrganizationAccessService;

    protected $userService;

    /**
     * Construct an instance of the Service.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient, UserService $userService)
    {
        $this->httpClient = $httpClient;

        $this->userService = $userService;

    }

    /**
     * @param $client_id
     * @param string $token
     * @return User
     * @throws NotAuthorizedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws ServerException
     */
    public function getUserDetails($client_id, string $token)
    {

        /** @var PartnerAccess $client */
        $partnerAccess = PartnerAccess::query()->find($client_id);

        if (!$partnerAccess) {
            throw new NotAuthorizedException("Partner with id {$client_id} not found. Please check the configuration");
        }
        $client = $partnerAccess;


        /** @var RegistrationCode $registrationCode */
        $registrationCode = $partnerAccess->registrationCode;

        $partnerData = $this->getPartnerInformation($client, $token);

        $userArray = $this->mapPartnerInformation($partnerData);

        /** @var User $user */

        $user = $this->findOrCreateUser($userArray, $client->organization, $registrationCode);


        return $user;

    }


    /**
     * @param OauthClient $client
     * @throws NotAuthorizedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws ServerException
     */
    private function getPartnerInformation(PartnerAccess $client, $token)
    {

        $url = sprintf($client->api_base, $token, $client->secret);
        $options = ['headers' => ['Accept', 'application/json']];


        try {
            $response = $this->httpClient->request('GET', $url, $options);
        } catch (ClientException $e) {
            $status = $e->getResponse()->getStatusCode();

            if ($status === 403) {
                throw new NotAuthorizedException('Access denied. Invalid token. Please try it again. (Responsecode 403)');
            }

            throw new ServerException('Error connecting to the partner API' . $e->getMessage());

        } catch (\Exception $e) {
            report($e);
            throw new ServerException('Error connecting to the partner API' . $e->getMessage());
        }
        $body = $response->getBody();
        $statusCode = $response->getStatusCode();


        if ($statusCode !== 200) {
            throw new NotAuthorizedException("Partner API rejected your login (${$statusCode}) {$body}");
        }

        return json_decode($response->getBody()->getContents(), true)['data'];

    }

    private function phoneNumberToUser(&$user, $phone)
    {
        if (!$phone && empty($phone)) {
            return;
        }

        try {
            $number = PhoneNumber::parse($phone);
        } catch (PhoneNumberParseException $e) {
            report($e);
            return;
        }

        $user['phone_number_prefix'] = $number->getCountryCode();
        $user['phone_number'] = $number->getNationalNumber();
    }

    /**
     * @param $partnerData
     * @return array uniform user object
     */
    public function mapPartnerInformation($partnerData): array
    {
        $user = [];

        if (is_object($partnerData)) {
            $partnerData = (array)$partnerData;
        }
        $allowedFields = ['first_name', 'last_name', 'email', 'phone'];

        foreach ($allowedFields as $field) {
            if (!empty($partnerData[$field])) {
                $user[$field] = $partnerData[$field];
            }
        }
        $user['state'] = User::STATES['REGISTERED'];

        if (isset($partnerData['status'])) {
            if (in_array($partnerData['status'], ['inactive'])) {
                $user['state'] = User::STATES['QUIT'];
            }
        }

        if (isset($partnerData['phone'])) {

            $this->phoneNumberToUser($user, $partnerData['phone']);
        }


        if (isset($partnerData['country']) &&
            isset($partnerData['country']['code2'])) {
            $user['country'] = $partnerData['country']['code2'];
            $user['country_of_origin'] = $partnerData['country']['code2'];
        }

        if (empty($user['email'])) {
            throw new ServerException('Partner data invalid, could not find the email address');
        }

        return $user;
    }

}
