<?php

namespace Modules\Partnerapi\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\RegistrationCode;

class OrganizationSaml extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    protected $table = 'organization_saml';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'registration_code_id',
        'organization_id',
        'login_redirect_uri',
        'entity_uri',
        'logout_redirect_uri',
        'slug',
        'email_pattern',
        'certificate',
        'certificate_fingerprint'
    ];

    /**
     * The attributes that should be hidden.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];


    /**
     * A User can be member of a Brand.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function registrationCode()
    {
        return $this->belongsTo(RegistrationCode::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }



}

