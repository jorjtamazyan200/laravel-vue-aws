<?php

namespace Modules\Partnerapi\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class HandleOauthRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'token' => 'required|string',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        // Authorization handled via Scope Middleware.
        return true;
    }
}
