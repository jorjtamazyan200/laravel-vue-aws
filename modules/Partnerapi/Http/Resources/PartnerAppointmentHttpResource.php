<?php

namespace Modules\Partnerapi\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class PartnerAppointmentHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'actualDurationMinutes' => $this->actual_duration_minutes,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at,
            'actualStart' => ($this->actual_start ? $this->actual_start->toIso8601String() : null),
            'plannedStart' => ($this->planned_start ? $this->planned_start->toIso8601String() : null)
        ];
    }
}
