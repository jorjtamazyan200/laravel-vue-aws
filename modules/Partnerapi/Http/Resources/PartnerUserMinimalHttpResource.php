<?php

namespace Modules\Partnerapi\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class PartnerUserMinimalHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'displayName' => $this->first_name . ' ' . $this->last_name,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,
            'email' => $this->email,
            'gender' => $this->gender,
            'primaryRole' => $this->primary_role,
            'qualifiedPhoneNumber' => $this->qualifiedPhoneNumber,
            'qualifiedWhatsappNumber' => $this->QualifiedWhatsappNumber,
            'language' => $this->language,
        ];
    }
}
