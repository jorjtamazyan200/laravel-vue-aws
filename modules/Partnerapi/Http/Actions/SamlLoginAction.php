<?php

namespace Modules\Partnerapi\Http\Actions;

use Aacotroneo\Saml2\Saml2Auth;
use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;


use Illuminate\Http\Request;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Partnerapi\Domain\Services\SamlService;


/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class SamlLoginAction extends Action
{


    private $samlService;
    private $saml2Auth;


    /**
     * @var
     */
    private $responder;


    public function __construct(
        SamlService $samlService,
        Saml2Auth $saml2Auth,
        ResourceResponder $responder
    )
    {
        $this->samlService = $samlService;
        $this->responder = $responder;
        $this->saml2Auth = $saml2Auth;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @docs: Aacotroneo\Saml2\Http\Controllers\Saml2Controller
     * @throws ClientException
     */
    public function __invoke(Request $request, $slug)
    {

        $organizationSaml = $this->samlService->findBySlug($slug);;
        $callbackUrl = route('saml.acs', ['slug' => $organizationSaml->slug]);
        $request->saml2Auth->login($callbackUrl);

    }
}
