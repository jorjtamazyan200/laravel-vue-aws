<?php

namespace Modules\Partnerapi\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Client\NotAuthorizedException;
use App\Exceptions\Server\ServerException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;

use Illuminate\Http\Request;

use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Partnerapi\Domain\Services\PartnerOrganizationAccessService;
use Modules\Partnerapi\Http\Requests\HandleOauthRequest;



/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class HandleKironOauthUserAction extends Action
{

    protected $userService;
    protected $matchService;
    protected $responder;

    protected $partnerOrganizationAccessService;


    public function __construct(
        UserService $userService,
        ResourceResponder $responder,
        PartnerOrganizationAccessService $partnerOrganizationAccessService
    )
    {
        $this->userService = $userService;
        $this->responder = $responder;
        $this->partnerOrganizationAccessService = $partnerOrganizationAccessService;
    }

    /**
     *
     * @param HandleOauthRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(HandleOauthRequest $request)
    {


        $platform = $request->platform;
        try {

            $partnerId = $this->getPartnerIdFromPlatform($platform);

            /** @var User $user */
            $user = $this->partnerOrganizationAccessService->getUserDetails($partnerId, $request->token);
            $link = $this->userService->getLoginLink($user);

        } catch (NotAuthorizedException $e) {
            return view('errors.partnerapi', ['message' => 'Your request was denied. Please close down everything and open it again.', 'technicalMessage' => $e->getMessage()]);
        } catch (ServerException $e) {
            return view('errors.partnerapi', ['message' => 'There was an technical error, please contact the IT department.', 'technicalMessage' => $e->getMessage()]);
        } catch (\Exception $e) {
            return view('errors.partnerapi', ['message' => 'There was an unknown error, please contact the IT department.', 'technicalMessage' => get_class($e) . ':' . $e->getMessage()]);

        }

        return redirect($link);


    }

    private function getPartnerIdFromPlatform($platform)
    {
        $platform = strtolower($platform);

        switch ($platform) {
            case 'stg':
                return 3;
            case 'uat':
                return 2;
            default:
                return 1;
        }


    }

    /**
     * @param $refer
     * @return int
     * @throws ClientException
     * @Deprecated
     *
     * Was the initial proposal, but now they added the paraemters;
     */
    private function getPartnerIdFromRefer($refer)
    {

        if (strstr($refer, 'campus.kiron.ngo') !== false) {
            return 1;
        }
        if (strstr($refer, 'uat-campus.kiron.ngo') !== false) {
            return 2;
        }
        if (strstr($refer, 'stg-campus.kiron.ngo') !== false) {
            return 3;
        }
        if (strstr($refer, 'localhost') !== false) {
            return 3;
        }

        throw new ClientException('Could not find the corresponding campus. Detected campus refer: ' . $refer . '. Please talk to the support');


    }


}
