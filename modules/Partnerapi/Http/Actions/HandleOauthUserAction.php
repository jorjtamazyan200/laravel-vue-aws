<?php

namespace Modules\Partnerapi\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;

use Illuminate\Http\Request;

use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Partnerapi\Domain\Services\PartnerOrganizationAccessService;
use Modules\Partnerapi\Http\Requests\HandleOauthRequest;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class HandleOauthUserAction extends Action
{

    protected $userService;
    protected $matchService;
    protected $responder;

    protected $partnerOrganizationAccessService;


    public function __construct(
        UserService $userService,
        ResourceResponder $responder,
        PartnerOrganizationAccessService $partnerOrganizationAccessService
    )
    {
        $this->userService = $userService;
        $this->responder = $responder;
        $this->partnerOrganizationAccessService = $partnerOrganizationAccessService;


    }

    /**
     *
     * @param $id
     * @param HandleOauthRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \App\Exceptions\Server\ServerException
     */
    public function __invoke($id, HandleOauthRequest $request)
    {

        /** @var User $user */
        $user = $this->partnerOrganizationAccessService->getUserDetails($id, $request->token);

        $link = $this->userService->getLoginLink($user);
        return redirect($link);


    }


}
