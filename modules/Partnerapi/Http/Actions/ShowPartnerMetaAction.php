<?php

namespace Modules\Partnerapi\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;

use App\Services\WaboboxService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Log;


use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class ShowPartnerMetaAction extends Action
{

    public function __invoke(Request $request)
    {

        Log::info('[wabobox] got incoming message' . json_encode($request->all()));
        $user = Auth::user();

        if (!$user->hasRole(Role::ROLES['partnerapi'])){
            throw new NotAuthorizedException();
        }

        $data = [
            'userStates' => array_keys(User::STATES),
            'matchStates' => array_keys(Match::STATES),
            'enrollmentStates' => array_keys(Enrollment::STATES),
            'appointmentStates' => array_keys(Appointment::STATES),
        ];

        return response()->json(['data' => $data]);
    }

}