<?php

namespace Modules\Matching\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class AvailabilityHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $tz = $request->query('timezone')? $request->query('timezone') : $this->timezone;

        return $this->getInTz($tz);
    }
}
