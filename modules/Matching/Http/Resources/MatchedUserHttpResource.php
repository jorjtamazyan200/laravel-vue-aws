<?php

namespace Modules\Matching\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\Role;

class MatchedUserHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $enrollment = $this->enrollment;


        $user = [
            'id' => $this->id,
            'avatar' => $this->avatar,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'email' => $this->email,
            'gender' => $this->gender,
            'primaryRole' => $this->primary_role,
            'generalComment' => $this->general_comment,
            'availabilities' => AvailabilityHttpResource::collection($this->whenLoaded('availabilities')),
        ];

        if ($user['primaryRole'] === Role::ROLES['mentee']){
            $user['phone'] = $this->getQualifiedPhoneNumber();
            $user['whatsapp'] = $this->getQualifiedWhatsappNumber();
        }
        return $user;

    }
}
