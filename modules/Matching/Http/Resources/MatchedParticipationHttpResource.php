<?php

namespace Modules\Matching\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Admin\Http\Resources\EnrollmentAdminHttpResource;
use Modules\Admin\Http\Resources\GroupMinimalHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class MatchedParticipationHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->enrollment->user;

        return [
            'id' => $this->id,
            'user' => new MatchedUserHttpResource($user),
            'groups'=> $this->enrollment->user ? GroupMinimalHttpResource::collection($this->enrollment->user->groups) : null,
            'enrollment' => new EnrollmentAdminHttpResource($this->whenLoaded('enrollment')),// this isrequired to unmatch!
            'matchConfirmedAt' => $this->match_confirmed_at ? $this->match_confirmed_at->toIso8601String() : null,
            'createdAt' => $this->created_at->toIso8601String()
        ];
    }
}
