<?php

namespace Modules\Matching\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Http\Resources\EnrollmentMinimalHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class MatchingScoreHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'mentorParticipation' =>
                new MatchedParticipationHttpResource($this->whenLoaded('mentorParticipation')),
            'menteeParticipation' =>
                new MatchedParticipationHttpResource($this->whenLoaded('menteeParticipation')),

            'program' =>
                new ProgramHttpResource($this->whenLoaded('program')),
            'notes' => $this->notes,
            'algorithmValue' => $this->algorithm_value,
            'distance' => $this->distance,
            'finalValue' => $this->final_value,
            'timeOverlap' =>  $this->time_overlap,

//            'mentee' => MatchedParticipationResource::collection($this->participations)
//            'mentee' => MatchedParticipationResource::collection($this->participations)
        ];
    }
}
