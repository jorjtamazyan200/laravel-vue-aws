<?php

namespace Modules\Matching\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\AppointmentRequest;
use Modules\Admin\Http\Resources\AppointmentListHttpResource;
use Modules\Admin\Http\Resources\AppointmentHttpResource;
use Modules\Matching\Http\Resources\MatchingScoreHttpResource;

class MatchingScoreController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {

        return MatchingScoreHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return  '';
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return ''; // AppointmentRequest::class;
    }
}
