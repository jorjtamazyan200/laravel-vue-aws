<?php

/*
|--------------------------------------------------------------------------
| Module routes
|--------------------------------------------------------------------------
|
| Here's the place to register API routes for the current module. These routes
| are loaded by the RouteServiceProvider within a group which is assigned
| the "api" middleware group and a version prefix, eg. `/api/v1`.
|
 */

/**
 * Private Scope
 */
Route::group(['middleware' => ['auth']], function () {
    /**
     * User Profile (/users/me)
     */
    Route::group(['prefix' => '/users/me'], function () {
        // Retrieve availabilities
        Route::get('/availabilities', 'Actions\ShowMyAvailabilitiesAction');
        // Mass-update availabilities
        Route::put('/availabilities', 'Actions\UpdateMyAvailabilitiesAction');
        Route::post('/availabilities', 'Actions\UpdateMyAvailabilitiesAction');
    });


    // @todo: oppenent availability missing?

    Route::get('/admin/users/{id}/availabilities', 'Actions\ShowAvailabilitiesOfUserAction');
    Route::put('/admin/users/{id}/availabilities', 'Actions\UpdateUsersAvailabilitiesAction');

    // Mass-update availabilities
    Route::put('/availabilities', 'Actions\UpdateMyAvailabilitiesAction');

    // Confirm and cancel matches
    Route::post('/matches/{id}/confirm', 'Actions\ConfirmMatchAction');
    Route::post('/matches/{id}/call', 'Actions\CallMatchAction');
    Route::get('/matches/{id}/conference', 'Actions\CallMatchAction');
    // Confirm, reject and cancel matches

    Route::post('/matches/{id}/cancel', 'Actions\CancelMatchAction');
    Route::post('/matches/{id}/reject', 'Actions\RejectMatchAction');
    Route::post('/matches/{id}/end', 'Actions\EndMatchAction');

    Route::get('/matches/{id}', 'Actions\ShowMatchAction');
    Route::get('/matches/{id}/availabilities', 'Actions\ShowMatchAvailabilitiesAction');


    Route::get('/matches/{id}/lections/{lection_id}/files', 'Actions\ListFilesForMatchLection');
    Route::get('/matches/{id}/files', 'Actions\ListFilesForMatch');


    // Feedback
    Route::post('/matches/{id}/feedback', 'Actions\AddFeedbackAction');
    Route::post('/matches/{id}/leavemessage', 'Actions\AddLeaveAMessageAction');

    Route::get('/matches/{id}/certificate', 'Actions\RedirectToMatchCertificateAction');


    /**
     * Admin Scope
     */
    Route::group(['middleware' => ['scope:admin']], function () {
        Route::get('/admin/matchingscores', 'Controllers\MatchingScoreController@index');

        Route::post('/admin/matchingscores/calculate', 'Actions\CreateMatchScoresAction');


        // List possible matching Participations
//        Route::get('/participations/{id}/match-suggest    ions', 'Actions\ListMatchSuggestionsAction');
        // Create a new match with given Participations
//        Route::post('/programs/{id}/matches', 'Actions\CreateMatchAction');
    });
});

Route::get('/matches/{id}/reply/{action}/{userid}/{secret}', 'Actions\MatchEmailResponseAction')
    ->where('id', '[0-9]+')
    ->where('userid', '[0-9]+')
    ->name('match.email.response');



Route::get('/participations/{id}/{hash}/certificate', 'Actions\ShowMatchCertificateAction')->name('certificate_pdf');
Route::get('/participations/{id}/{hash}/certificate/html', 'Actions\ShowMatchCertificateAction');