<?php

namespace Modules\Matching\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\FileService;
use Modules\Core\Http\Resources\FileHttpResource;
use Modules\Core\Http\Resources\ParticipationHttpResource;
use Modules\Matching\Domain\MatchingLogic\Suggester;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class ListFilesForMatchLection extends Action
{
    protected $suggester;
    /**
     * @var FileService
     */
    protected $fileService;
    /**
     * @var MatchService
     */
    protected $matchService;
    protected $responder;

    public function __construct(MatchService $matchService, FileService $fileService, ResourceResponder $responder)
    {
        $this->responder = $responder;
        $this->fileService = $fileService;
        $this->matchService = $matchService;
    }

    public function __invoke(int $id, int $lection_id, Request $request)
    {
        $user = Auth::user();

        /**
         * @var $match Match
         * @var $enrollments Collection
         * @var $enrollment Enrollment
         * @var $myEnrollment Enrollment
         * @var $user User
         */

        $match = $this->matchService->get($id);
        $enrollments = $match->enrollments()->get();

        $myEnrollment = $enrollments->filter(function ($enrollment) use ($user) {
            return $enrollment->user_id == $user->id;
        })->first();

        if ($myEnrollment  == null) {
            throw new NotAuthorizedException('NOT_ENROLLED_IN_THIS_MATCH');
        }


        $files = $this->fileService->listForProgramAndPlacement(
            File::PLACEMENTS['preparation'],
            $match->program_id,
            $myEnrollment->role,
            $lection_id
        );

        return $this->responder->send($files, FileHttpResource::class);
    }
}
