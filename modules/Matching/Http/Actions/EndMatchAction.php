<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;

/**
 * Class RejectMatchAction
 * @package Modules\Matching\Http\Actions
 * @todo: add tests
 */
class EndMatchAction extends Action
{
    protected $participationService;
    protected $matchService;
    protected $responder;

    public function __construct(
        ParticipationService $participationService,
        MatchService $matchService,
        ResourceResponder $responder
    ) {
        $this->participationService = $participationService;
        $this->matchService = $matchService;
        $this->responder = $responder;
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws NotAuthorizedException
     */
    public function __invoke($id, Request $request)
    {
        $user = Auth::user();
        /** @var Match $match */
        $match = $this->matchService->get($id);


        // @todo: check authorizatio
//        if ($user->cannot('cancel', [$match, $participation])) {
//            throw new NotAuthorizedException;
//        }


        if ($match->transitionAllowed('finish')){
            $match->transition('finish');
        }





        return response('ok', 200);
    }
}
