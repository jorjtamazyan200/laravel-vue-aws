<?php

namespace Modules\Matching\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Server\ServerException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;
use Illuminate\Support\Facades\App;

class MatchEmailResponseAction extends Action
{

    protected $matchService;
    protected $responder;
    protected $userService;


    public function __construct(
        MatchService $matchService,
        UserService $userService,
        ResourceResponder $responder
    )
    {

        $this->matchService = $matchService;
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {

        /** @var Match $match */

        $secret = $request->secret;
        $id = $request->id;
        $action = $request->action;
        $userid = $request->userid;
        $requestAction = $action . '_match';

        /** @var User $user */
        $match = $this->matchService->get($id);
        $user = $this->userService->get($userid);


        App::setLocale($user->language);

        if ($user->emailSecret($requestAction) !== $secret) {
            throw new NotAuthorizedException("Secret invalid");
        }

        if (!$this->matchService->isPartOfMatch($match, $user)) {
            throw new NotAuthorizedException("Not allowed to access this mentorship!");
        }

        $url = $user->brand->frontend_url;

        $threeMinutesAgo = Carbon::now()->subMinutes(3);
        if ($threeMinutesAgo->lessThan($match->last_state_change_at)){
            return view('responses.toofast', ['link' => $url]);
        }

        if (!$match->transitionAllowed($action)) {
            return view('responses.invalidlink', ['link' => $url]);
        }

        $match->transition($action);

        return view('responses.generalResponse', ['link' => $url]);
    }
}
