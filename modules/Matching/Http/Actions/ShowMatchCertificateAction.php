<?php

namespace Modules\Matching\Http\Actions;


use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
//use Barryvdh\Snappy\PdfWrapper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;

use PDF;


class ShowMatchCertificateAction extends Action
{
    protected $matchService;
    protected $participationService;
    protected $responder;


    public function __construct(
        MatchService $matchService,
        ParticipationService $participationService,
        ConferenceService $conferenceService,
        ResourceResponder $responder
    )
    {
        $this->matchService = $matchService;
        $this->participationService = $participationService;
        $this->responder = $responder;
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     * @throws ClientException
     */
    public function __invoke($id, $hash, Request $request)
    {

        $requestUri = $request->getUri();

        /** @var Participation $participation */
        $participation = $this->participationService->get($id);
        $user = $participation->enrollment->user;

        /** @var Match $match */
        $match = $participation->match;

        if ($hash != $match->getHash()){
            throw new NotAuthorizedException();
        }

        if (!$match || !$match->certificateAllowed()) {
            return new Response(
                'This match does not have a certificate yet',
                200);
        }

        App::setLocale($user->language);


        $parameters = $this->getParameters($participation);

        if (strstr($requestUri, 'html') !== false) {
            return $this->render($user, 'html', $parameters);
        }

        return $this->render($user, 'pdf', $parameters);

    }

    private function getParameters(Participation $participation)
    {


        $user = $participation->enrollment->user;
        $match = $participation->match;
        $document = $this->getTextForParticipation($participation, ['user' => $user, 'match' => $match]);

        if (empty($match->match_done_at)) {
            $match->match_done_at = Carbon::now();
        }

        return [
            'participation' => $participation,
            'enrollment' => $participation->enrollment,
            'match' => $participation->match,
            'user' => $user,
            'brand' => $user->brand,
            'text' => $document,
        ];
    }

    private function getTextForParticipation(Participation $participation, $parameters): string
    {
        $user = $participation->enrollment->user;

        $searchBy = new SearchObject(
            'certificate_text',
            'article',
            $user->language,
            $user->brand,
            $participation->enrollment->program,
            $participation->enrollment->role
        );

        /** @var BrandedDocument $brandedDocument */
        $brandedDocument = ContentChooser::findDocument($searchBy);
        $text = $brandedDocument->parseContent($parameters);

        return $text;

    }

    private function render(User $user, $type, $parameters)
    {
        $view = 'pdf.certificate';

        Carbon::setLocale($user->language);


        if ($type === 'html') {
            return view($view, $parameters);
        }

        /** @var PdfWrapper $pdf */
        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView($view, $parameters);
        $output = $pdf->output();

        return new Response(
            $output,
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="file.pdf"',
                'Content-Length' => strlen($output)

            )
        );
    }
}
