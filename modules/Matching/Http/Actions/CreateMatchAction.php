<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\CreateMatchRequest;
use Modules\Matching\Http\Resources\MatchHttpResource;

/**
 * Class CreateMatchAction
 * @package Modules\Matching\Http\Actions
 * @deprecated see match participation
 */
class CreateMatchAction extends Action
{
    /**
     * @var MatchService
     */
    protected $matchService;
    protected $responder;

    public function __construct(MatchService $matchService, ResourceResponder $responder)
    {
        $this->matchService = $matchService;
        $this->responder = $responder;
    }

    /**
     * @param $id
     * @param CreateMatchRequest $request
     * @return mixed
     * @throws \App\Exceptions\Client\ClientException
     * @throws \Exception
     * @deprecated See Match Participation action;
     *
     */
    public function __invoke($id, CreateMatchRequest $request)
    {
        /** @var Match $match */
        $match = $this->matchService->createForParticipations(
            $id,
            $request->input('participation_ids')
        );

        $match->transition('request');

        return $this->responder->send($match, MatchHttpResource::class);
    }
}
