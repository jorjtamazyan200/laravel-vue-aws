<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Services\AvailabilityService;
use Modules\Matching\Http\Requests\UpdateAvailabilitiesRequest;
use Modules\Matching\Http\Resources\AvailabilityHttpResource;

class UpdateUsersAvailabilitiesAction extends Action
{
    /** @var \Modules\Matching\Domain\Services\AvailabilityService **/
    protected $availabilityService;
    protected $responder;

    public function __construct(AvailabilityService $availabilityService, ResourceResponder $responder)
    {
        $this->availabilityService = $availabilityService;
        $this->responder = $responder;
    }

    public function __invoke($id, UpdateAvailabilitiesRequest $request)
    {
        $timezone = $request->input('timezone');
        $data = $request->input('data');

        /** @var User $user * */
        $user = User::findOrFail($id);

        $availabilities = $this->availabilityService->setFor($user, $data, $timezone);

        return $this->responder->send($availabilities, AvailabilityHttpResource::class);
    }
}
