<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\External\Domain\Services\Conference3Service;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;

class ShowMatchAction extends Action
{
    protected $participationService;
    protected $matchService;
    protected $responder;
    protected $conferenceService;
    protected $conference3Service;


    public function __construct(
        ParticipationService $participationService,
        MatchService $matchService,
        ConferenceService $conferenceService,
        Conference3Service $conference3Service,
        ResourceResponder $responder
    )
    {
        $this->participationService = $participationService;
        $this->matchService = $matchService;
        $this->conferenceService = $conferenceService;
        $this->conference3Service = $conference3Service;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var Match $match */
        $match = $this->matchService->get($id);


        $match->conferenceRoomUri = $this->matchService->getClassroomLink($match, $user);

        // find if currentUser Mentor or Mentee
        $match->currentEnrollment = $match->enrollments()->where(['user_id' => $user->id])->first();


        return $this->responder->send($match, MatchHttpResource::class);
    }
}
