<?php

namespace Modules\Matching\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;

use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class AddFeedbackAction extends Action
{
    protected $matchService;
    protected $userFeedbackService;


    public function __construct(
        MatchService $matchService,
        UserFeedbackService $userFeedbackService

    ) {
        $this->matchService = $matchService;
        $this->userFeedbackService = $userFeedbackService;
    }

    /**
     * @todo: check if user has right to post feedback.
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke($id, AddFeedbackRequest $request)
    {
        $user = Auth::user();

        /** @var Match $match */
        $match = $this->matchService->get($id);

        UserFeedback::query()->truncate();



        /** @var Appointment $appointment */
        $appointment = $match->getCurrentAppointment();

        $code = $request->input('question_code');

        if (!isset(UserFeedback::CODES[$code])) {
            throw new ClientException('INVALID_QUESTION_CODE:' . $code, 401);
        }

        $this->userFeedbackService->create(
            [
                'user_id' => Auth::id(),
                'question_code' => $code,
                'response_scalar' => $request->input('response_scalar'),
                'response_text' => $request->get('response_text'),
                'appointment_id' => $appointment->id
            ]
        );

        return response()->json(['message' => 'ok']);
    }
}
