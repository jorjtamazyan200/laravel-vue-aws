<?php

namespace Modules\Matching\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\FileService;
use Modules\Core\Http\Resources\FileHttpResource;
use Modules\Core\Http\Resources\ParticipationHttpResource;
use Modules\Matching\Domain\MatchingLogic\Suggester;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class ListFilesForMatch extends Action
{
    protected $suggester;
    /**
     * @var FileService
     */
    protected $fileService;
    /**
     * @var MatchService
     */
    protected $matchService;
    protected $responder;

    public function __construct(MatchService $matchService, FileService $fileService, ResourceResponder $responder)
    {
        $this->responder = $responder;
        $this->fileService = $fileService;
        $this->matchService = $matchService;
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     * @throws \App\Exceptions\Client\ClientException
     */
    public function __invoke(int $id, Request $request)
    {
        $user = Auth::user();

        /**
         * @var $match Match
         * @var $enrollments Collection
         * @var $enrollment Enrollment
         * @var $myEnrollment Enrollment
         * @var $user User
         */

        $match = $this->matchService->get($id);

        /** @var Participation $participation */
        $participation = $match->getParticipationOfUser($user);
        $role = $participation->enrollment->role;


        $files = $this->fileService->listForProgramAndPlacement(
            File::PLACEMENTS['preparation'],
            $match->program_id,
            $role
        );

        return $this->responder->send($files, FileHttpResource::class);
    }
}
