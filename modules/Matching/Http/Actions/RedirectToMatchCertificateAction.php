<?php

namespace Modules\Matching\Http\Actions;


use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
//use Barryvdh\Snappy\PdfWrapper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;

use PDF;


class RedirectToMatchCertificateAction extends Action
{
    protected $participationService;
    protected $matchService;
    protected $responder;


    public function __construct(
        ParticipationService $participationService,
        MatchService $matchService
    )
    {
        $this->matchService = $matchService;
        $this->participationService = $participationService;
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     * @throws ClientException
     */
    public function __invoke($id, Request $request)
    {


        /** @var Match $match */
        $match = $this->matchService->get($id);
        /** @var User $user */
        $user = Auth::user();

        $myParticipation = $match->participations->first(function ($participation) use ($user) {
            return $participation->enrollment->user_id = $user->id;
        });
        if (!$myParticipation) {
            throw new NotAuthorizedException();
        }

        if ($request->acceptsJson()) {
            $route = route('certificate_pdf', ['id' => $myParticipation->id, 'hash' => $match->getHash()]);
            $response = [
                'data' => ['link' => $route]
            ];
            return response()->json($response);
        }


        return redirect()->route('certificate_pdf', ['id' => $myParticipation->id, 'hash' => $match->getHash()]);

    }


}
