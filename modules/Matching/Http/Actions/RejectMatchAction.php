<?php

namespace Modules\Matching\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;

/**
 * Class RejectMatchAction
 * @package Modules\Matching\Http\Actions
 * @todo: add tests
 */
class RejectMatchAction extends Action
{
    protected $participationService;
    protected $matchService;
    protected $responder;

    public function __construct(
        ParticipationService $participationService,
        MatchService $matchService,
        ResourceResponder $responder
    )
    {
        $this->participationService = $participationService;
        $this->matchService = $matchService;
        $this->responder = $responder;
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws NotAuthorizedException
     */
    public function __invoke($id, Request $request)
    {
        $user = Auth::user();
        /** @var Match $match */
        $match = $this->matchService->get($id);
        $comment = $request->comment;
        $action = $request->action;
        $timeShift = $request->timeShift ? $request->timeShift : 7;

        /** @var Participation $participation */
        $participation = $this->participationService->getForUserAndMatch($user->id, $match->id);

        if ($user->cannot('cancel', [$match, $participation])) {
            throw new NotAuthorizedException;
        }


        if ($action === 'quit') {
            $participation->enrollment->transition('quit');
        }

        if ($action === 'participate') {
            $this->participationService->createNewParticipationIfAllowed($participation->enrollment, $timeShift);
        }

        $match->user_comment = $comment;
        $match->save();
        $match->transition('reject');


        return response('ok', 200);
    }
}
