<?php

namespace Modules\Matching\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class AddLeaveAMessageRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'message' => 'required|string',
            'attachContactInformation' => 'boolean'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        // Authentication is done via the Scope Middleware
        return true;
    }
}
