<?php

namespace Modules\Matching\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class RejectMatchRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'comment' => 'required|string',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        // Authentication is done via the Scope Middleware
        return true;
    }
}
