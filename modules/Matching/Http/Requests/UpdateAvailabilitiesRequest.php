<?php

namespace Modules\Matching\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class UpdateAvailabilitiesRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'data' => 'array',
            'data.*.day_of_week' => 'required|integer|between:0,6',
            'data.*.start_minute_of_day' => 'required|integer', //, 'regex:/^([01]\d|2[0-3]):([0-5]\d)$/'
            'data.*.duration_minutes' => 'required|integer|min:30',
            'timezone' => 'required|string'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    protected function deserialize(array $input) : array
    {
        $deserialized = $this->keysToSnakeCase($input);

        if (array_key_exists('data', $deserialized) && is_array($deserialized['data'])) {
            $deserialized['data'] = array_map(function ($item) {
                return $this->keysToSnakeCase($item);
            }, $deserialized['data']);
        }

        return $deserialized;
    }
}
