<?php

namespace Modules\Matching;

use App\Infrastructure\Contracts\BreadService;
use App\Infrastructure\Providers\ModuleServiceProvider;
use Illuminate\Database\Eloquent\Model;


use Modules\Matching\Domain\Events\MatchCreated;
use Modules\Matching\Domain\Listeners\CreateConferenceRoomOnMatchCreation;
use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Models\MatchingScore;
use Modules\Matching\Domain\Services\AvailabilityService;
use Modules\Matching\Domain\Services\MatchingScoreService;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Controllers\MatchingScoreController;

class MatchingModuleProvider extends ModuleServiceProvider
{
    /**
     * A mapping of events to their handlers.
     *
     * @var array
     */
    protected $listen = [
        MatchCreated::class => [
            CreateConferenceRoomOnMatchCreation::class
        ]
    ];

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(AvailabilityService::class)->needs(Model::class)->give(Availability::class);
        $this->app->when(MatchService::class)->needs(Model::class)->give(Match::class);


        $this->app->when(MatchingScoreService::class)->needs(Model::class)->give(MatchingScore::class);
        $this->app->when(MatchingScoreController::class)->needs(BreadService::class)->give(MatchingScoreService::class);

    }
}
