<?php

namespace Modules\Matching\Tests\Unit;

use App\Infrastructure\AbstractTests\LaravelTest;
use Illuminate\Support\Facades\App;
use Mockery;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Matching\Domain\Models\Match;

class MatchTest extends LaravelTest
{
    public function test_it_creates_a_conference_room_on_match_creation_and_adds_room_id_to_match()
    {
        // Mock the ConferenceService because we don't want to test that
        $conferenceServiceMock = Mockery::mock(ConferenceService::class);
        $conferenceServiceMock->shouldReceive('createRoomForMatch')
            ->once()
            ->andReturn('someRoomId');
        App::instance(ConferenceService::class, $conferenceServiceMock);

        // Create a Match
        $match = Match::create(['program_id' => 1,]);

        $conferenceServiceMock->createRoomForMatch($match);

        // Make sure the external roomId is added to the Match in database
        $this->assertDatabaseHas('matches', [
            'id' => $match->id,
//            'external_room_id' => 'someRoomId' //@todo: aber ich finde der Match Zieptunkt sowieso zu fru¨¨ um einen Raum anzulegne..
        ]);
    }
}
