<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;

class RejectMatchTest extends EndpointTest
{
    private function sampleSetup()
    {
        $match = factory(Match::class)->create(
            ['state' => Match::STATES['UNCONFIRMED']]
        );

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['ACTIVE'],
            'role' => 'mentor',
        ]);
        $participation = factory(Participation::class)->create([
            'match_id' => $match->id,
            'enrollment_id' => $enrollment->id,
        ]);
        $enrollmentMentee = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentee',
        ]);
        $participation = factory(Participation::class)->create([
            'match_id' => $match->id,
            'enrollment_id' => $enrollmentMentee->id,
        ]);
        return [$match, $enrollment];
    }

    public function test_it_rejects_a_match()
    {

        Notification::fake();

        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();
        list($match, $enrollment) = $this->sampleSetup();
        $response = $this->actingAs($this->user)->postJson("/api/v2/matches/$match->id/reject");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('matches', [
            'id' => $match->id,
            'state' => Match::STATES['REJECTED'],
        ]);
    }

    public function test_user_can_quit_on_reject_match()
    {
        Notification::fake();


        // Disable model events, as we don't wan
        //t to call the ConferenceService during this test
        Match::flushEventListeners();

        list($match, $enrollment) = $this->sampleSetup();

        $response = $this->actingAs($this->user)->postJson("/api/v2/matches/$match->id/reject",
            ['comment' => 'Hello'
                , 'action' => 'quit']);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $this->assertDatabaseHas('matches', [
            'id' => $match->id,
            'state' => Match::STATES['REJECTED'],
        ]);
        $this->assertDatabaseHas('enrollments', [
            'id' => $enrollment->id,
            'state' => Enrollment::STATES['QUIT'],
        ]);
    }

    public function test_user_can_participate_again()
    {
        Notification::fake();



        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();

        list($match, $enrollment) = $this->sampleSetup();

        $response = $this->actingAs($this->user)->postJson("/api/v2/matches/$match->id/reject",
            ['comment' => 'Hello'
                , 'action' => 'participate', 'timeShift' => 10]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('matches', [
            'id' => $match->id,
            'state' => Match::STATES['REJECTED'],
        ]);

        $this->assertDatabaseHas('enrollments', [
            'id' => $enrollment->id,
            'state' => Enrollment::STATES['ACTIVE'],
        ]);

        $this->assertDatabaseHas('participations', [
            'enrollment_id' => $enrollment->id,
            'match_id' => null
        ]);


    }
}
