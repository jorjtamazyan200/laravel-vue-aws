<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/cancel` endpoint.
 */
class CancelMatchTest extends EndpointTest
{
    public function test_it_cancels_a_match()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();
        $match = factory(Match::class)->create(
            ['state' => Match::STATES['ACTIVE']]
        );

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentee',
        ]);
        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);

        $response = $this->actingAs($this->user)->post("/api/v2/matches/$match->id/cancel");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('matches', [
            'id' => $match->id,
            'state' => Match::STATES['CANCELED'],
        ]);
    }

    public function test_it_cancels_a_mentorship_with_comment()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();
        $match = factory(Match::class)->create(
            ['state' => Match::STATES['ACTIVE']]
        );

        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'role' => 'mentee',
        ]);
        $participation = factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id,
            'match_confirmed_at' => Carbon::now(),
        ]);

        $response = $this->actingAs($this->user)->post("/api/v2/matches/$match->id/cancel");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('matches', [
            'id' => $match->id,
            'state' => Match::STATES['CANCELED'],
        ]);
    }
}
