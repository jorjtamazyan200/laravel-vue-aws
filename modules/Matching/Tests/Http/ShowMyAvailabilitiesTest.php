<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Matching\Domain\Models\Availability;

/**
 * Test the functionality of the `/api/v1/users/me/availabilities` endpoint.
 */
class ShowMyAvailabilitiesTest extends EndpointTest
{
    public function test_it_shows_my_availabilities()
    {
        $minuteOfDay = 12*60;

        factory(Availability::class)->create([
            'owner_id' => $this->user->id,
            'day_of_week' => 1,
            'start_minute_of_day' => $minuteOfDay,
            'duration_minutes' => 60,
        ]);


        $tz = 'Europe/Berlin';

        $this->actingAs($this->user);
        $response = $this->call(
            'get',
            '/api/v2/users/me/availabilities',
            ['timezone' => $tz]
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertExactJson([
            'data' => [
                [
                    'dayOfWeek' => 1,
                    'startMinuteOfDay' => $minuteOfDay,
                    'durationMinutes' => 60,
                    'timezone' => $tz
                ]
            ]
        ]);
    }
    public function test_it_shows_my_availabilities_in_other_tz()
    {
        $winerTime = Carbon::create(2017, 02, 01, 12);
        Carbon::setTestNow($winerTime);


        $minuteOfDay = 12*60;
        $tz = 'America/New_York';
        $offset = -360;

        factory(Availability::class)->create([
            'owner_id' => $this->user->id,
            'day_of_week' => 1,
            'start_minute_of_day' => $minuteOfDay,
            'duration_minutes' => 60,
        ]);



        $this->actingAs($this->user);
        $response = $this->call(
            'get',
            '/api/v2/users/me/availabilities',
            ['timezone' => $tz]
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertExactJson([
            'data' => [
                [
                    'dayOfWeek' => 1,
                    'startMinuteOfDay' => $minuteOfDay + $offset,
                    'timezone' => $tz,
                    'durationMinutes' => 60,
                ]
            ]
        ]);
    }
}
