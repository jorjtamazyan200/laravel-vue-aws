<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/programs/{id}/matches` endpoint.
 */
class CreateFeedbackTest extends EndpointTest
{
    public function test_it_creates_a_new_match()
    {
        Notification::fake();

        /** @var User $mentor */
        $mentor = User::where('email', '=', 'mentor@volunteer-vision.com')->firstOrFail();


        /** @var Match $match */
        $match = $mentor->enrollments->first()->participations()->first()->match;

        $appointment = $match->getCurrentAppointment();

        $response = $this->actingAs($mentor)->json(
            'POST',
            '/api/v2/matches/' . $match->id . '/feedback',
            [
                'responseScalar' => 3,
                'questionCode' => UserFeedback::CODES['MENTORSHIP_HAPPINESS']
            ]
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        // Make sure the match has been created.
        $this->assertDatabaseHas(
            'user_feedbacks',
            [
                'user_id' => $mentor->id,
                'appointment_id' => $appointment->id,
                'response_scalar' => 3,
                'question_code' => UserFeedback::CODES['MENTORSHIP_HAPPINESS']
            ]
        );
    }
}
