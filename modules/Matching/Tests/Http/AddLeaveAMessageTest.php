<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/programs/{id}/matches` endpoint.
 */
class AddLeaveAMessageTest extends EndpointTest
{
    public function test_it_sends_a_message()
    {
        Notification::fake();

        // Disable model events, as we don't want to call the ConferenceService during this test

        $response = $this->actingAs($this->user)->json(
            'POST',
            '/api/v2/matches/1/leavemessage',
            [
                'message' => 'hello'
            ]
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
    public function test_it_sends_a_message_with_contact_details()
    {
        Notification::fake();

        // Disable model events, as we don't want to call the ConferenceService during this test

        $response = $this->actingAs($this->user)->json(
            'POST',
            '/api/v2/matches/1/leavemessage',
            [
                'message' => 'hello',
                'attachContactInformation' => true
            ]
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
