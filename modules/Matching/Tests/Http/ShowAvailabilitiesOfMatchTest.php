<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Availability;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/users/me/availabilities` endpoint.
 */
class ShowAvailabilitiesOfMatchTest extends EndpointTest
{
    public function test_it_shows_availabilities_of_match()
    {


        /** @var User $mentor */
        $mentor = User::where('email', '=', 'mentor@volunteer-vision.com')->firstOrFail();

        $mentee = User::where('email', '=', 'mentee@volunteer-vision.com')->firstOrFail();


        factory(Availability::class, 3)->create(['owner_id' => $mentee->id]);

        /** @var Match $match */
        $match = $mentor->enrollments->first()->participations()->first()->match;




        $tz = 'Europe/Berlin';

        $response = $this
            ->actingAs($mentor)
            ->json(
                'get',
                '/api/v2/matches/' . $match->id . '/availabilities',
                ['timezone' => $tz]
            );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            "data"=>[
                [
                    'dayOfWeek',
                    'timezone',
                ]
            ]

        ]);

//        $response->assertExactJson([
//            'data' => [
//                [
//                    'dayOfWeek' => 1,
//                    'startMinuteOfDay' => $minuteOfDay,
//                    'durationMinutes' => 60,
//                    'timezone' => $tz
//                ]
//            ]
//        ]);
    }
}
