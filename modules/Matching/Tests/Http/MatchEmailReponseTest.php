<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/programs/{id}/matches` endpoint.
 */
class MatchEmailReponseTest extends EndpointTest
{
    public function test_too_fast_response()
    {
        Notification::fake();
        $user = $this->user;

        $match = Match::find(1);
        $match->state = Match::STATES['ACTIVE'];
        $match->last_state_change_at = Carbon::now();
        $match->save();

        $secret = $user->emailSecret('PAUSE_MATCH');
        $route = route('match.email.response',
            [
                'userid' => $user->id,
                'id' => 1,
                'action' => 'pause',
                'secret' => $secret,
            ]
        );

        $response = $this->getJson($route);

        $this->assertContains(' not ready yet',$response->getContent());
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $match = Match::find(1);
        $this->assertEquals($match->state, Match::STATES['ACTIVE']);

    }

    public function test_user_appointment_response()
    {
        Notification::fake();
        $user = $this->user;

        $match = Match::find(1);
        $match->state = Match::STATES['ACTIVE'];
        $match->last_state_change_at = Carbon::now()->subMinutes(10);
        $match->save();

        $secret = $user->emailSecret('PAUSE_MATCH');
        $route = route('match.email.response',
            [
                'userid' => $user->id,
                'id' => 1,
                'action' => 'pause',
                'secret' => $secret,
            ]
        );

        $response = $this->getJson($route);


        $this->assertContains('Great',$response->getContent());
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $match = Match::find(1);
        $this->assertEquals($match->state, Match::STATES['PAUSED']);

    }

    public function test_it_does_not_allow_on_wroong_secert()
    {
        Notification::fake();
        $user = $this->user;

        $match = Match::find(1);
        $match->state = Match::STATES['ACTIVE'];
        $match->save();

        $secret = $user->emailSecret('PAUSE_XXXXX_MATCH');
        $route = route('match.email.response',
            [
                'userid' => $user->id,
                'id' => 1,
                'action' => 'pause',
                'secret' => $secret,
            ]
        );


        $response = $this->getJson($route);

        $this->assertEquals(403, $response->getStatusCode(), $response->getContent());

    }
}
