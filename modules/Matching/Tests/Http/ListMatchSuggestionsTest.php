<?php

namespace Modules\Matching\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Participation;

/**
 * Test the functionality of the `/api/v1/participations/{id}/match-suggestions` endpoint.
 */
class ListMatchSuggestionsTest extends EndpointTest
{
    private $defaultTz = 'Europe/Berlin';

    public function test_it_lists_match_suggestions()
    {
        $this->markTestSkipped('..');

        $participation = Participation::find(1);
        $otherParticipation = Participation::find(2);

        $participation->enrollment->update(['role' => 'mentor', 'program_id' => 1]);
        $otherParticipation->enrollment->update(['role' => 'mentee', 'program_id' => 1]);

        $otherParticipation->enrollment->user
            ->update(['timezone' => $participation->enrollment->user->timezone]);

        $participation->enrollment->user->availabilities()->create([
            'day_of_week' => 2,
            'start_minute_of_day' => 720,
            'duration_minutes' => 120,
            'timezone' => $this->defaultTz
        ]);
        $otherParticipation->enrollment->user->availabilities()->create([
            'day_of_week' => 2,
            'start_minute_of_day' => 720,
            'duration_minutes' => 120,
            'timezone' => $this->defaultTz
        ]);

        $response = $this->actingAs($this->admin)->get('/api/v2/participations/1/match-suggestions');
//

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'matchingScore',
                ],
            ],
        ]);
    }
}
