<?php

namespace Modules\Matching\Listeners\Match;

use App\Notifications\Match\MatchCancellationInfoNotification;
use App\Notifications\Match\MatchConfirmedNotification;
use App\Notifications\Match\MatchFinishedNotification;
use App\Notifications\Match\MatchRejectedNotification;
use App\Notifications\Match\MatchRequestNotification;
use App\Notifications\Match\MatchSuggestionNotification;
use App\Services\ChatbotService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Models\MatchStateLog;
use Modules\Matching\Domain\Services\MatchService;
use SM\Event\TransitionEvent;

class DefaultMatchStateHandler
{

    /**
     * Requests if the mentoee still wants to partiicpate;
     *
     * @param TransitionEvent $event
     */
    public function request(TransitionEvent $event): void
    {
        /**
         * @var Match $match
         * @var Enrollment $enrollment
         * @var User $user
         * */
        $match = $event->getStateMachine()->getObject();

        /** @var Enrollment $enrollment */
        $enrollment = $match->getMenteeParticipation()->enrollment;

        if ($enrollment->hasApprovedRequestWithinDays(21)) {
            $match->transition('suggest');
            return;
        }

        $mentee = $enrollment->user;

        $match->id;
        $enrollment->id;


        $chatbotService = app(ChatbotService::class);
        $chatbotService->setUserState($mentee, ChatbotService::STATES['match_request'], ['id' => $match->id]);
        $mentee->notify(new MatchRequestNotification($mentee->brand, $match, $enrollment));


        Log::debug('Handling "suggest" transition', ['event' => $event]);

    }

    public function mentee_reject_auto(TransitionEvent $event): void
    {

        /** @var MatchService $matchService */
        $matchService = app(MatchService::class);

        /**
         * @var Match $match
         * @var Enrollment $enrollment
         * @var User $user
         * */
        $match = $event->getStateMachine()->getObject();

        $match->rejector_id = $match->getMenteeParticipation()->enrollment->user_id;
        $match->save();

        $count = $matchService->countRejectedMentorships($match->getMenteeParticipation()->enrollment->user);
        $hadMultipleRejections = $count >= 3;


        $enrollments = $match->enrollments;
        foreach ($enrollments as $enrollment) {
            if ($enrollment->isMentee()) {
                if ($hadMultipleRejections) {
                    $enrollment->transition('exclude');
                } else {
                    $this->makeEnrollmentsAvailableAgain($enrollment, 14);
                }

            }
            if ($enrollment->isMentor()) {
                $this->makeEnrollmentsAvailableAgain($enrollment);

            }
        }
    }

    public function mentee_reject(TransitionEvent $event): void
    {


        /**
         * @var Match $match
         * @var Enrollment $enrollment
         * @var User $user
         * */
        $match = $event->getStateMachine()->getObject();
        $enrollments = $match->enrollments;


        foreach ($enrollments as $enrollment) {
            if ($enrollment->isMentee()) {
                $enrollment->transition('quit');
            }
            if ($enrollment->isMentor()) {
                $this->makeEnrollmentsAvailableAgain($enrollment);

            }
        }
    }


    public function mentee_approve(TransitionEvent $event): void
    {
        /** @var Match $match */
        $match = $event->getStateMachine()->getObject();

        $enrollment = $match->getMenteeParticipation()->enrollment;
        $enrollment->last_request_accepted_at = Carbon::now();
        $enrollment->save();

        $match->transition('suggest');
    }

    /**
     * Handle the `suggest` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function suggest(TransitionEvent $event): void
    {
        /**
         * @var Match $match
         * @var Enrollment $enrollment
         * @var User $user
         * */
        $match = $event->getStateMachine()->getObject();
        $enrollments = $match->enrollments;
        /** @var ActivityService $activityService */
        $activityService = app(ActivityService::class);


        foreach ($enrollments as $enrollment) {
            if ($enrollment->isMentor()) {
                $user = $enrollment->user;
                $user->notify(new MatchSuggestionNotification($user->brand, $match, $enrollment));
                $activityService->logActivityForUser($user, Activity::ACTIVITIES['MATCH_OFFERED']);
            }
        }

        Log::debug('Handling "suggest" transition', ['event' => $event]);
    }

    /**
     * Handle the `confirm` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function confirm(TransitionEvent $event): void
    {

        /**
         * @var Match $match
         * @var Enrollment $enrollment
         * @var User $user
         * */

        /** @var Match $match */
        $match = $event->getStateMachine()->getObject();
        $enrollments = $match->enrollments;
        /** @var ActivityService $activityService */
        $activityService = app(ActivityService::class);


        $enrollment = $match->getMentorParticipation()->enrollment;
        $enrollment->last_request_accepted_at = Carbon::now();
        $enrollment->save();


        foreach ($enrollments as $enrollment) {
            $user = $enrollment->user;

            if ($enrollment->isMentee()) {
                $user->notify(new MatchConfirmedNotification($user->brand, $match, $enrollment));
                $activityService->logActivityForUser($user, Activity::ACTIVITIES['MATCH_CONFIRMED_MENTEE']);
            } else {
                $activityService->logActivityForUser($user, Activity::ACTIVITIES['MATCH_CONFIRMED_MENTOR']);
            }
        }

        Log::debug('Handling "confirm" transition', ['event' => $event]);
    }

    /**
     * @param TransitionEvent $event
     * @throws \App\Exceptions\Server\ServerException
     */
    public function autoreject(TransitionEvent $event): void
    {

        /** @var Match $match */
        $match = $event->getStateMachine()->getObject();

        $mentorEnrollment = $match->getMentorParticipation()->enrollment;
        $menteeEnrollment = $match->getMenteeParticipation()->enrollment;

        /** @var User $mentor */
        $mentor = $mentorEnrollment->user;
        $mentor->notify(new MatchCancellationInfoNotification($mentor->brand, $match, $mentorEnrollment));


        $match->rejector_id = $mentor->id;
        $match->save();

        $this->reject($event);
    }

    /**
     * Mentor rejects  (or autorejects) mentorship.
     *
     * @param TransitionEvent $event
     * @return void
     * @throws \App\Exceptions\Server\ServerException
     */
    public function reject(TransitionEvent $event): void
    {
        /** @var Match $match */
        $match = $event->getStateMachine()->getObject();

        /** @var ParticipationService $participationService */
        $participationService = app(ParticipationService::class);


        $mentorEnrollment = $match->getMentorParticipation()->enrollment;
        $mentor = $mentorEnrollment->user;


        $hadMultipleRejections = $participationService->hadToManyRejections($mentorEnrollment);

        if (!$hadMultipleRejections) {
            $participationService->createNewParticipationIfAllowed($mentorEnrollment);
        }

        $match->rejector_id = $mentor->id;
        $match->save();


        if ($hadMultipleRejections) {
            if ($mentorEnrollment->transitionAllowed('exclude')) {
                $mentorEnrollment->transition('exclude');
            }
        }

        $this->makeEnrollmentsAvailableAgain($match->getMenteeParticipation()->enrollment);
        /** @var CoordinatorTodoService $coordinatorTodoService */
        $coordinatorTodoService = app(CoordinatorTodoService::class);

        // if you remove this, please add 'MATCH_AUTO_REJECTED'
        $todo = new CoordinatorTodo(
            [
                'customer_id' => $mentor->id,
                'description' => ($hadMultipleRejections ? 'Mentor was exlcuded for too many rejections.' : '') .
                    'Please have a short look if there is anything to do here:' . $match->user_comment,
                'meta' => ['match_id' => $match->id, 'enrollment_id' => $mentorEnrollment->id],
                'type' => CoordinatorTodo::TYPES['MATCH_REJECTED'],
                'duedate' => Carbon::now()
            ]);

        $coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);

        Log::debug('Handling "reject" transition', ['event' => $event]);
    }


    /**
     * Handle the `activate` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function activate(TransitionEvent $event): void
    {
        /** @var Match $match */
        $match = $event->getStateMachine()->getObject();
        $match->activateLicence('ACTIVATION');
        $match->save();

        Log::debug('Handling "activate" transition', ['event' => $event]);
    }

    /**
     * Handle the `pause` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function pause(TransitionEvent $event): void
    {
        Log::debug('Handling "pause" transition', ['event' => $event]);
    }

    /**
     * Handle the `resume` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function resume(TransitionEvent $event): void
    {
        Log::debug('Handling "resume" transition', ['event' => $event]);
    }

    /**
     * Handle the `finish` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function finish(TransitionEvent $event): void
    {
        /** @var Match $match */
        $match = $event->getStateMachine()->getObject();
        $match->match_done_at = Carbon::now();
        $match->save();

        $enrollments = $match->enrollments;

        foreach ($enrollments as $e) {
            /** @var User $user */
            $user = $e->user;
            $user->notify(new MatchFinishedNotification($user->brand, $match, $e));
        }

        Log::debug('Handling "finish" transition', ['event' => $event]);
    }

    /**
     * Handle the `cancel` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function cancel(TransitionEvent $event): void
    {

        // @TODO: Decide which action is triggered here
        /** @var Match $match */
        $match = $event->getStateMachine()->getObject();

        Log::debug('Handling "cancel" transition', ['event' => $event]);
    }

    private function makeEnrollmentsAvailableAgain($enrollments, $inDays = 0)
    {
        if (!is_array($enrollments)) {
            $enrollments = [$enrollments];
        }
        /** @var ParticipationService $participationService */
        $participationService = app(ParticipationService::class);

        /** @var Enrollment $enrollment */
        foreach ($enrollments as $enrollment) {
            $participationService->createFromEnrollment($enrollment, $inDays);
        }
    }
}
