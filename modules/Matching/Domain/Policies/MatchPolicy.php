<?php

namespace Modules\Matching\Domain\Policies;

use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\ParticipationService;

class MatchPolicy
{
    protected $groupService;
    protected $participationService;

    public function __construct(GroupService $groupService, ParticipationService $participationService)
    {
        $this->groupService = $groupService;
        $this->participationService = $participationService;
    }

    /**
     * A User can cancel a Match if he is matched in it via his Participation.
     *
     * @param User $user
     * @param Match $match
     * @param Participation $participation
     * @return boolean
     */
    public function cancel(User $user, Match $match, Participation $participation) : bool
    {
        return $user->id === $participation->enrollment->user_id &&
            $participation->match_id === $match->id;
    }

    /**
     * User can confirm a Match if he is a mentor in its Participation.
     *
     * @param User $user
     * @param Match $match
     * @param Participation $participation
     * @return bool
     */
    public function confirm(User $user, Match $match, Participation $participation) : bool
    {
        $enrollment = $participation->enrollment;

        return $user->id === $enrollment->user_id &&
            $match->id === $participation->match_id &&
            $enrollment->isMentor();
    }

    public function showComment(User $authUser, Comment $comment, Match $match) : bool
    {
        if ($comment->type === 'SUPERVISOR_COMMENT') {
            return $this->isSupervisor($authUser, $match);
        }

        return $this->connectedToMatch($authUser, $match);
    }

    public function addComment(User $authUser, Match $match) : bool
    {
        return $this->connectedToMatch($authUser, $match) ||
            $this->isSupervisor($authUser, $match);
    }

    public function listComments(User $authUser, Match $match) : bool
    {
        return $this->connectedToMatch($authUser, $match) ||
            $this->isSupervisor($authUser, $match);
    }

    public function listSupervisorComments(User $authUser, Match $match) : bool
    {
        return $this->isSupervisor($authUser, $match);
    }

    protected function isSupervisor(User $authUser, Match $match) : bool
    {
        $userIds = $match->enrollments->pluck('user_id');

        foreach ($userIds as $userId) {
            if ($this->groupService->isSupervisorOf($authUser->id, $userId)) {
                return true;
            }
        }

        return false;
    }

    protected function connectedToMatch(User $authUser, Match $match) : bool
    {
        try {
            $participation = $this->participationService->getForUserAndMatch($authUser->id, $match->id);
            if ($participation) {
                return true;
            }
        } catch (\Exception $e) {
            //
        }

        return false;
    }
}
