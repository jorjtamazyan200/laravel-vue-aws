<?php

namespace Modules\Matching\Domain\Models\Traits;

trait HasAvailability
{
    /**
     * A User can have many Availabilities.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function availabilities()
    {
        return $this->morphMany('Modules\Matching\Domain\Models\Availability', 'owner');
    }
}
