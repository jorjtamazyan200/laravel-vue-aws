<?php

namespace Modules\Matching\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\External\Domain\Services\Conference3Service;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Models\MatchStateLog;

class MatchService extends EloquentBreadService
{
    protected $participationService;

    /**
     * The constructor must be declared to require a service via dependency injection.
     */
    public function __construct(ParticipationService $participationService, Model $model)
    {
        $this->participationService = $participationService;

        parent::__construct($model);
    }

    /**
     * Create a new match for given participations.
     *
     * @param int $programId
     * @param array $participationIds
     * @return Arrayable
     * @throws \Exception
     */
    public function createForParticipations(int $programId, array $participationIds): Arrayable
    {
        try {
            DB::beginTransaction();
            $match = $this->create([
                'program_id' => $programId,
            ]);
            foreach ($participationIds as $id) {
                $this->connectParticipationToMatch($id, $match->id);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return $match;
    }

    /**
     * Retrieve a Match by its `external_room_id` attribute.
     *
     * @param mixed $roomId
     * @return Arrayable
     */
    public function getByExternalRoomId($roomId): Arrayable
    {
        return $this->model->newQuery()->where('external_room_id', $roomId)->firstOrFail();
    }

    /**
     * Update the participations and connect them to the new Match.
     *
     * @param int $participationId
     * @param int $matchId
     * @return void
     */
    protected function connectParticipationToMatch(int $participationId, int $matchId)
    {
        $participation = $this->participationService->get($participationId);
        $participation->match_id = $matchId;

        if ($participation->enrollment->isMentee()) {
            $participation->match_confirmed_at = Carbon::now();
        }

        $participation->save();
    }


    public function findNewMatchesForSupervisorInGroup(Group $group, $days)
    {

        // @todo: status should be at least 'confirmed'

        return $this->model->newQuery()
            ->select(['users.first_name', 'users.last_name', 'matches.created_at', 'matches.state'])
            ->join('participations', 'participations.match_id', '=', 'matches.id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->join('group_user', 'group_user.user_id', '=', 'users.id')
            ->where('group_user.group_id', '=', $group->id)
            ->where('matches.created_at', '>', Carbon::now()->subDays($days))
            ->get();
    }

    public function existsMatchOfUsers(User $user1, User $user2)
    {
        $result = $this->model->newQuery()
            ->select('matches.id')
            ->join('participations', 'participations.match_id', '=', 'matches.id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->whereIn('enrollments.user_id', [$user1->id, $user2->id])
            ->groupBy('matches.id')
            ->havingRaw('count(*) = 2')
            ->first();

        if ($result == null) {
            return $result;
        }
        return $result->id;
    }

    public function findMatchesWithoutPlannedAppointments($state, $hoursSinceLastAppointment = false): Builder
    {

        $time = Carbon::now()->subDays(3);

        $query = $this->model->newQuery()
            ->select('matches.*')
            // @todo replace NOW() with $time;
            ->leftJoin('appointments', function ($leftJoin) use ($time) {
                $leftJoin->on('appointments.match_id', '=', 'matches.id')
                    ->where('appointments.planned_start', '>', $time);

                // @todo $minAppointmentsTime
            })
            ->whereNull('appointments.id')
            ->where('matches.state', $state);


        return $query;


    }

    public function getProgramReport(Program $program, $organization = null)
    {

        $query = DB::table('matches')
            ->select(DB::raw('count(distinct matches) as total'), 'matches.state')
            ->join('participations', 'matches.id', '=', 'participations.match_id')
            ->join('enrollments', 'enrollments.id', '=', 'participations.enrollment_id')
            ->join('users', 'users.id', '=', 'enrollments.user_id')
            ->where('users.count_for_reporting', true)
            ->where('enrollments.program_id', $program->id)
            ->groupBy(['matches.state']);

        if ($organization) {
            $query->where('users.organization_id', $organization->id);
        }


        return $query;

//        SELECT COUNT(*), primary_role, enrollments.state FROM enrollments
// JOIN users u on enrollments.user_id = u.id
//GROUP BY primary_role, enrollments.state


    }


    public function findMatchOfUsers(User $user1, User $user2)
    {
        $id = $this->existsMatchOfUsers($user1, $user2);
        if ($id === null) {
            return null;
        }
        return $this->get($id);
    }

    public function countRejectedMentorships(User $user): int
    {

        $count = Match::query()->where('rejector_id', $user->id)->count();
        return $count;
    }

    /**
     *
     * @todo: check this method;
     *
     * @param Match $match
     */
    public function activateMatchIfHasMultipleRejections(Match $match)
    {
        $participation = $match->getMentorParticipation();
        /** @var Enrollment $enrollment */
        $enrollment = $participation->enrollment;
        $user = $enrollment->user;

        $rejectionsWithoutLicenceCount = 0;

        foreach ($enrollment->participations as $participation) {
            if (!$participation->match)
                continue;

            if ($participation->match->state !== Match::STATES['REJECTED'])
                continue;

            if ($participation->match->licence_activated)
                continue;

            $rejectionsWithoutLicenceCount++;
        }

        return false;
    }

    public function isPartOfMatch(Match $match, User $user)
    {
        /** @var Participation $participation */
        foreach ($match->participations as $participation) {
            /** @var Enrollment $enrollment */
            $enrollment = $participation->enrollment;
            if ($user->id === $enrollment->user_id) {
                return true;
            }

        }
        return false;

    }

    /**
     * @todo: probabyl not working
     * @param User $user
     * @param int $days
     * @return bool
     */
//    public function hasApprovedRequestWithinDays(User $user, $days = 21): bool
//    {
//
//
//
////        return MatchStateLog::query()
////            ->where('actor_id', $user->id)// @todo: actor maybe does not work;
////            ->where('transition', 'mentee_approve')
////            ->where('created_at', '>', Carbon::now()->subDays($days))
////            ->exists();
//    }

    public function getClassroomLink(Match $match, User $user)
    {

        if ($match->classroom_version === 3) {
            /** @var Conference3Service $conference3Service */
            $conference3Service = app(Conference3Service::class);
            return $conference3Service->getExternalRoomLink($match, $user);
        }
        // old version;

        if (in_array($match->state, [Match::STATES['CONFIRMED'], Match::STATES['ACTIVE']])
            && empty($match->external_room_id)) {
            /** @var Conference3Service $conference3Service */
            $conferenceService = app(ConferenceService::class);
            // adds the room id to the match internally
            $conferenceService->createRoomForMatch($match);
            $match->save();
        }

        if (empty($match->external_room_id)) {
            return null;
        }
        return ConferenceService::getLinkForRoom($match->external_room_id, $user->id);

    }

    public function getAmountOfTransitionsOfUser(User $user, $transitionName): int
    {
        return MatchStateLog::query()
            ->where('actor_id', $user->id)
            ->where('transition', $transitionName)
            ->count();
    }

    /**
     * @param User $user
     * @param Participation $participation
     * @param Match $match
     * @param $comment
     * @throws \App\Exceptions\Client\ClientException
     * @throws \App\Exceptions\Server\ServerException
     */
    public function userCancelsMatch(User $user, Participation $participation, Match $match, $comment)
    {
        if ($comment) {
            // Mentor todo
            $match->user_comment = $comment;
            $match->save();
            $this->createCoordinatorTodo($user, $match, CoordinatorTodo::TYPES['MATCH_CANCELED']);
        }
        $match->transition('cancel');
    }

    /**
     * @param $user
     * @param $match
     * @param $type
     * @throws \App\Exceptions\Server\ServerException
     */
    private function createCoordinatorTodo($user, $match, $type)
    {
        /** @var CoordinatorTodoService $coordinatorTodoService */
        $coordinatorTodoService = app(CoordinatorTodoService::class);
        $data = [
            'meta' => ['match_id' => $match->id],
            'type' => $type,
            'customer_id' => $user->id,
        ];
        $todo = new CoordinatorTodo($data);
        $coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);
    }
}
