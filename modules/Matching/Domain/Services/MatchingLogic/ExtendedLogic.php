<?php

namespace Modules\Matching\Domain\Services\MatchingLogic;

use Modules\Core\Domain\Models\Participation;

/**
 * Default matching logic that only respects a participation's role to match mentors with mentees.
 */
class ExtendedLogic implements MatchingLogic
{
    /** {@inheritDoc} */
    public function calculateScore(Participation $reference, Participation $candidate) : float
    {
        // Match mentors to mentees
        if ($reference->enrollment->role != $candidate->enrollment->role) {
            return 1;
        }

        return -1;
    }
}
