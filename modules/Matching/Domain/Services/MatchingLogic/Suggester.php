<?php

namespace Modules\Matching\Domain\Services\MatchingLogic;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Matching\Domain\Models\MatchingScore;
use Modules\Matching\Domain\Services\AvailabilityService;

class Suggester
{
    /**
     * The ParticipationService for interacting with Participations.
     *
     * @var Modules\Core\Domain\Services\ParticipationService
     */
    protected $participationService;


    protected $availabilityTimeService;


    /**
     * The matching logic to fall back to when no other logic applies.
     *
     * @var string
     */
    protected $defaultLogic = DefaultLogic::class;

    /**
     * Construct an instance of the Suggest class.
     */
    public function __construct(
        ParticipationService $participationService,
        AvailabilityService\AvailabilityTimeService $availabilityTimeService
    )
    {
        $this->participationService = $participationService;
        $this->availabilityTimeService = $availabilityTimeService;
    }


    /**
     * Suggest matchable Participations for given participationId.
     *
     * @param int $participationId Id of the Participation for which we're looking for matches
     * @return Collection A collection of Participations that match according to the selected logic
     */
    public function suggest(int $participationId): Collection
    {
        $participation = $this->participationService->get($participationId);

        $otherParticipations = $this->participationService->findMatchableParticipationsForParticiption($participation);

        return $this->filterCandidates($otherParticipations, $participation);
    }

    /**
     * Calculate a score for each pair and remove those whose score is insufficient.
     *
     * @deprecated
     * @param Collection $candidates
     * @return Collection
     * @deprecated
     */
    private function filterCandidates(Collection $candidates, Participation $participation): Collection
    {
        $logic = $this->getLogicForProgram($participation->enrollment->program);

        return [];
        //Info :used in real time calculation
        // @deprected;

        // Calculate score for each pair
//        $scored = $candidates->map(function ($candidate) use ($participation, $logic) {
//
//            if (!$candidate->enrollment->user) {
//                return null;
//            }
//
//            /** @var Participation $candidate */
//            $candidate->matching_score = $logic->calculateScore($participation, $candidate);
//
//            $candidate->availibility_overlap =
//                $this
//                    ->availabilityTimeService
//                    ->getOverlappingMinutes($participation->enrollment->user, $candidate->enrollment->user);
//
//
//            $candidate->timezone_difference =
//                $this
//                    ->availabilityTimeService
//                    ->getTimezoneDifference(
//                        $participation->enrollment->user->timezone,
//                        $candidate->enrollment->user->timezone
//                    );
//
//            $candidate->minutes_available =
//                $this->availabilityTimeService
//                    ->getMinutesAvailabilitesTotal($candidate->enrollment->user);
//
//            return $candidate;
//        });

//        // Only keep candidates with a positive score
//        $filtered = $scored->filter(function ($candidate) {
//            return $candidate->matching_score > 0;
//        })->values();
//
//        $filtered = $filtered->sortBy(function ($candidate) {
//            return $candidate->matching_score;
//        });
//
//        return $filtered;
    }

    /**
     * @param Participation $participation
     * @param Participation $candiate
     * @param MatchingLogic $logic
     *  replaces the one above;
     * @return MatchingScore
     *
     */
    public function processOne(Participation $mentorParticipation, Participation $menteeParticipation, MatchingLogic $logic)
    {


        $userMentor = $mentorParticipation->enrollment->user;
        $userMentee = $menteeParticipation->enrollment->user;


        $score = new MatchingScore();
        $score->mentor_participation_id = $mentorParticipation->id;
        $score->mentee_participation_id = $menteeParticipation->id;
        $score->program_id = $mentorParticipation->enrollment->program_id;


        $logic->calculateScore($score, $mentorParticipation, $menteeParticipation);

        $score->time_overlap =
            $this
                ->availabilityTimeService
                ->getOverlappingMinutes($userMentor, $userMentee);

        $score->distance = (int)(self::vincentyGreatCircleDistance($userMentor->lat, $userMentor->lng, $userMentee->lat, $userMentee->lng));

        $score->algorithm = get_class($logic);
        $score->timezone_difference =
            $this
                ->availabilityTimeService
                ->getTimezoneDifference(
                    $userMentor->timezone,
                    $userMentee->timezonex
                );


        $logic->calculateFinalScore($score);

//        $candidate->minutes_available =
//            $this->availabilityTimeService
//                ->getMinutesAvailabilitesTotal($candidate->enrollment->user);
        $score->save();

        return $score;
    }


    /**
     * @todo: move it to a helper function;
     *
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public static function vincentyGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        if (!$latitudeFrom || !$longitudeFrom || !$latitudeTo || !$longitudeTo){
            return null;
        }
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }

    /**
     * Determine which matching logic to use for given Program.
     *
     * @param Program $program
     * @return MatchingLogic
     */
    public function getLogicForProgram(Program $program): MatchingLogic
    {
        $class = $this->defaultLogic;

        // @todo: add cache;
        $overrideClass = __NAMESPACE__ . '\\' . $program->matching_logic;
        if (class_exists($overrideClass)) {
            $class = $overrideClass;
        }

        return App::make($class);
    }
}
