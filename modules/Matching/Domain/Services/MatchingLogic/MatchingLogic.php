<?php

namespace Modules\Matching\Domain\Services\MatchingLogic;

use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Models\MatchingScore;

interface MatchingLogic
{
    /**
     * Calculate a "match score" for given pair of Participations.
     *
     * Zero or negative means "no match". Any positive number means "match".
     * The higher the number, the closer the match.
     *
     * @param Participation $reference
     * @param Participation $candidate
     * @return int
     */
    public function calculateScore(MatchingScore $matchingScore, Participation $reference, Participation $candidate) : float;

    public function calculateFinalScore(MatchingScore $matchingScore);
}
