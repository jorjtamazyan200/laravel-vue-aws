<?php

namespace Modules\Matching\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Matching\Domain\Models\MatchingScore;
use Modules\Matching\Domain\Services\MatchingLogic\Suggester;

class MatchingService
{
    protected $participationService;
    protected $suggester;

    /**
     * The constructor must be declared to require a service via dependency injection.
     */
    public function __construct(ParticipationService $participationService, Suggester $suggester)
    {

        $this->participationService = $participationService;
        $this->suggester = $suggester;

    }

    public function generateMissingCombinations()
    {

        $processed = 0;
        $allPrograms = Program::all();

        foreach ($allPrograms as $program) {
            $combinations = $this->getNoneExistingCombinations($program);

            foreach ($combinations as $comb) {
                $processed++;
                $this->processCombination($program, $comb);
            }
        }
        return $processed;
    }


    public function removeParticipationFromMatchingPool(Participation $participation)
    {

        MatchingScore::query()
            ->orWhere('mentor_participation_id', $participation->id)
            ->orWhere('mentee_participation_id', $participation->id)
            ->delete();

    }

    private function processCombination(Program $program, $combination)
    {

        /**
         * [
         * 'mentorId' => $mentorId,
         * 'menteeId' => $menteeId
         * ] *
         */

        $logic = $this->suggester->getLogicForProgram($program);

        /** @var Participation $menteeParticipation */
        $menteeParticipation = $this->participationService->get($combination['menteeParticipationId']);

        /** @var Participation $mentorParticipation */
        $mentorParticipation = $this->participationService->get($combination['mentorParticipationId']);

        $this->suggester->processOne($mentorParticipation, $menteeParticipation, $logic);


    }

    private function getNoneExistingCombinations(Program $program)
    {
        $participationsWithRole = $this->participationService->findMatchableParticipationForProgram($program);
        $mentees = [];
        $mentors = [];

        foreach ($participationsWithRole as $p) {
            if ($p->role == 'mentor') {
                array_push($mentors, $p->id);
            }
            if ($p->role == 'mentee') {
                array_push($mentees, $p->id);
            }
        }

        $neededCombinations = $this->filterNeededCombinations($mentors, $mentees, $program);
        $openCount = count($neededCombinations);
        if ($openCount > 0) {
            echo 'Total combinations in queue of program ' . $program->title . ' : ' . $openCount . PHP_EOL;
        }


        $neededCombinations = array_slice($neededCombinations, rand(0, $openCount - 50), 50);


        return $neededCombinations;

    }

    private function filterNeededCombinations($allMentors, $allMentees, $program): array
    {
        // List of mentorParticipationId: int; menteeParticipationId: int
        $databaseCombinations = MatchingScore::query()
            ->select(array('mentor_participation_id', 'mentee_participation_id'))
            ->where('program_id', $program->id)
            ->get();

        $neededCombinations = [];

        foreach ($allMentors as $mentorId) {
            foreach ($allMentees as $menteeId) {

                // one combination
                $found = false;
                foreach ($databaseCombinations as $row) {
                    if (
                        $row['mentor_participation_id'] === $mentorId &&
                        $row['mentee_participation_id'] === $menteeId
                    ) {
                        $found = true;
                        break;
                    }
                }
                if ($found) {
                    continue;
                }
                $neededCombinations[] = [
                    'mentorParticipationId' => $mentorId,
                    'menteeParticipationId' => $menteeId
                ];


            }
        }
        return $neededCombinations;

    }

}
