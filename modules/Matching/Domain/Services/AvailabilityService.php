<?php

namespace Modules\Matching\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Interfaces\HasAvailabilityInterface;

class AvailabilityService extends EloquentBreadService
{
    /**
     * Return all availabilities for a user|group.
     *
     * @param integer $userId
     * @return Collection
     *
     */
    public function listForUser(User $user): Collection
    {
        // @todo:check if group has an availaibliy.
        if ($user->availabilities->count() === 0) {
            // serach in regisrtraetion code;
        }

        return $user->availabilities;
    }

    /**
     * @param HasAvailabilityInterface $entity
     * @return Collection
     */
    public function listFor(HasAvailabilityInterface $entity): Collection
    {
        //@todo: does not work, because it uses FQN instead of 'User'
        return $entity->availabilities()->getEager();
    }

    /**
     * Set availabilities for a User.
     *
     * @param HasAvailabilityInterface $userId
     * @param array $data
     * @return Collection
     */
    public function setFor(HasAvailabilityInterface $entity, array $data, string $timezone): Collection
    {
        if ($timezone != null) {
            $data = array_map(function ($row) use ($timezone) {
                $row['timezone'] = $timezone;
                return $row;
            }, $data);
        }


        DB::beginTransaction();

        $entity->availabilities()->delete();

        // Delete all old availabilities
//        $this->model->newQuery()
//            ->where('user_id', $userId)
//            ->delete();

        // Create the new ones...
        $entity->availabilities()->createMany($data);

        DB::commit();

        return $entity->availabilities;
    }
}
