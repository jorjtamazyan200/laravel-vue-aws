<?php



Route::group(['prefix' => '/distribution'], function () {
    Route::group(['prefix' => '/distributable-content/{type}', 'middleware' => ['auth','throttle:500,1']], function () {
        Route::get('/', 'Actions\GetDistributableContentAction');
        Route::post('/', 'Actions\UpdateDistributableContentAction');
    });
});

