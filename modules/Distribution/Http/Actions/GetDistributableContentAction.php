<?php

namespace Modules\Distribution\Http\Actions;

use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Distribution\Domain\Services\DistributionService;
use Modules\Distribution\Http\Requests\GetDistributableContentRequest;
use Modules\Distribution\Http\Resources\DistributionHttpResource;

class GetDistributableContentAction extends Action
{
    protected $distService;
    protected $responder;

    public function __construct(DistributionService $distService, ResourceResponder $responder)
    {
        $this->distService = $distService;
        $this->responder = $responder;
    }

    /**
     * @param GetDistributableContentRequest $request
     * @param string $type
     * @return mixed
     * @throws NotFoundException
     */
    public function __invoke(GetDistributableContentRequest $request, string $type)
    {
        $model = $this->distService->resolveModel($type);

        if (is_null($model)) {
            throw new NotFoundException("Bad type for distributable content: $type");
        }

        $records = $model::getDistributableRecords();

        return $this->responder->send($records, DistributionHttpResource::class);
    }
}
