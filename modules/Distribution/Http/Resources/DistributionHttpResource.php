<?php

namespace Modules\Distribution\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class DistributionHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->allAttributesToArray();
    }
}
