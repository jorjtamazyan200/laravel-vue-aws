<?php

namespace Modules\Distribution;

use App\Infrastructure\Providers\ModuleServiceProvider;

class DistributionModuleProvider extends ModuleServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
