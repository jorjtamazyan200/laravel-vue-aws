<?php

namespace Modules\Distribution\Domain\Services;

use Illuminate\Support\Facades\App;
use Modules\Admin\Domain\Models\Supportanswer;
use Modules\Admin\Domain\Models\SupportanswerRelation;
use Modules\Distribution\Domain\Contracts\Distributable;

class DistributionService
{
    /**
     * All distributable models by their identifier.
     *
     * @var array
     */
    protected $models = [
        'branded-documents' => \Modules\Core\Domain\Models\BrandedDocument::class,
        'translations' => \Modules\Core\Domain\Models\Translation::class,
        'files' => \Modules\Core\Domain\Models\File::class,
        'supportanswers' => Supportanswer::class,
        'supportanswer-relations' => SupportanswerRelation::class,
    ];

    /**
     * Get a model by its identifier.
     *
     * @param  string $identifier
     * @return Modules\Distribution\Domain\Contracts\Distributable|null
     */
    public function resolveModel(string $identifier): ?Distributable
    {
        if (!array_key_exists($identifier, $this->models)) {
            return null;
        }

        return App::make($this->models[$identifier]);
    }
}
