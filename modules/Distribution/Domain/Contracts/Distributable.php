<?php

namespace Modules\Distribution\Domain\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface Distributable
{
    /**
     * Return all attributes of the model.
     *
     * @return array
     */
    public function allAttributesToArray(): array;

    /**
     * Retrieve all records that are distributable.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public static function getDistributableRecords(): Collection;
}
