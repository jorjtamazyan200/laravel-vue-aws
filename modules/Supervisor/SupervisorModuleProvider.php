<?php

namespace Modules\Supervisor;

use App\Infrastructure\Providers\ModuleServiceProvider;
use Illuminate\Database\Eloquent\Model;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\ProfileField;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\Terms;
use Modules\Core\Domain\Models\Translation;
use Modules\Core\Domain\Models\User;

use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Models\UserInvitation;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\BrandService;
use Modules\Core\Domain\Services\ActivityService;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\FileService;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\ProfileFieldService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\TranslationService;
use Modules\Core\Domain\Services\TermsService;

use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserInvitationService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Domain\Services\WebinarService;

class SupervisorModuleProvider extends ModuleServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
