<?php

namespace Modules\Supervisor\Test\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class ResetPasswordForUserTest extends EndpointTest
{
    /** @test */
    public function test_i_can_reset_password_for_a_user()
    {
        $user = User::where('email', 'supervisor@volunteer-vision.com')->firstOrFail();


        $response = $this->actingAs($user)->postJson('/api/v2/supervisor/users/1/resetpassword');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
