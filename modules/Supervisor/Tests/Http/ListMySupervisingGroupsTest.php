<?php

namespace Modules\Supervisor\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class ListMySupervisingGroupsTest extends EndpointTest
{
    /** @test */
    public function test_it_lists_all_my_groups()
    {
        $user = User::where('email', 'supervisor@volunteer-vision.com')->firstOrFail();

        $response = $this->actingAs($user)->getJson('/api/v2/supervisor/groups');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        //@todo: cound data > 0;


        $response->assertJsonStructure([
            'data' => [
                [
                    'id'
                ]
            ]
        ]);
    }
}
