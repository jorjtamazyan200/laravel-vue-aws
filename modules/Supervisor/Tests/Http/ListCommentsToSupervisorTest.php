<?php

namespace Modules\Supervisor\Test\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Faker\Factory;
use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class ListCommentsToSupervisorTest extends EndpointTest
{
    /** @test */
    public function test_i_can_read_the_comments_of_a_user()
    {
        $user1 = User::where('email', 'supervisor@volunteer-vision.com')->firstOrFail();
        $user2 = User::where('email', 'client@volunteer-vision.com')->firstOrFail();



        $user1->groups()->attach(1, [
            'role' => 'supervisor',
        ]);
        $user2->groups()->attach(1);

        $values = [
            'type' => Comment::TYPES['SUPERVISOR_COMMENT'],
            'commentable_id' => $user2->id,
            'commentable_type' => 'users',
            'body' => 'hello',
            'author_id' => 1
        ];


        $comment = new Comment();
        $comment->fill($values);
        $comment->author()->associate($user1);
        $comment->commentable()->associate($user2);
        $comment->save();



        $response = $this->actingAs($user1)->getJson('/api/v2/supervisor/users/' . $user2->id . '/comments');


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $response->assertJsonStructure([
            'data' => [[
                'id',
                'body',
                'type',
                'pinToTop'
            ]]
        ]);
    }
}
