<?php

namespace Modules\Supervisor\Test\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class ShowAvailabilityToSupervisorTest extends EndpointTest
{
    /** @test */
    public function test_i_can_see_availability_of_a_user()
    {

        $mail = 'mentee@volunteer-vision.com';
        $user = User::where('email', '=', $mail)->firstOrFail();
        $response = $this->actingAs($this->user)->getJson('/api/v2/supervisor/users/' . $user->id . '/availability');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $response->assertJsonStructure([
            'data' => [
                [
                    'dayOfWeek',
                    'durationMinutes',
                    'startMinuteOfDay',
                    'timezone'
                ]
            ]
        ]);
    }

    public function test_i_can_see_availability_of_a_user_in_other_tz()
    {

        $mail = 'mentee@volunteer-vision.com';
        $newYorkTimezone = 'America/New_York';

        $user = User::where('email', '=', $mail)->firstOrFail();
        $response = $this->actingAs($this->user)->getJson('/api/v2/supervisor/users/' . $user->id . '/availability?timezone=' . $newYorkTimezone);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $content = json_decode($response->getContent());

        $this->assertEquals($content->data[0]->timezone, $newYorkTimezone);

    }
}
