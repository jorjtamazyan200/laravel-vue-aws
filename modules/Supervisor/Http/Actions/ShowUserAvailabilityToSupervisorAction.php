<?php

namespace Modules\Supervisor\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\QueryParams;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Matching\Domain\Services\AvailabilityService;
use Modules\Matching\Http\Resources\AvailabilityHttpResource;
use Modules\Supervisor\Http\Resources\UserSupervisorHttpResource;

class ShowUserAvailabilityToSupervisorAction extends Action
{
    protected $userService;
    protected $availabilityService;
    protected $responder;

    public function __construct(UserService $userService, AvailabilityService $availabilityService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->availabilityService = $availabilityService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {
        // @todo: check if user is allowed to to so.

        /**
         * @var $me User
         */
        $user = $this->userService->get($id);


        $availabilities = $this->availabilityService->listForUser($user);

        return $this->responder->send($availabilities, AvailabilityHttpResource::class);
    }
}
