<?php

namespace Modules\Supervisor\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\GroupService;

use Modules\Supervisor\Http\Resources\GroupSupervisorHttpResource;

class ListMySupervisingGroupsAction extends Action
{
    protected $groupService;
    protected $responder;

    public function __construct(GroupService $groupService, ResourceResponder $responder)
    {
        $this->groupService = $groupService;
        $this->responder = $responder;
    }


    public function __invoke(Request $request)
    {
        $enrollments = $this->groupService->listSupervisingForUser(Auth::id());

        return $this->responder->send($enrollments, GroupSupervisorHttpResource::class);
    }
}
