<?php

namespace Modules\Supervisor\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Models\Traits\Commentable;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Supervisor\Http\Resources\ParticipationSupervisorHttpResource;

class ListCommentsForSupervisorAction extends Action
{
    protected $responder;

    protected $userService;

    public function __construct(ResourceResponder $responder, UserService $userService)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }


    public function __invoke($id, Request $request)
    {
        $authUser = Auth::user();
        $user = $this->userService->get($id);

        if (!$authUser->can('listSupervisorComments', $user)) {
            throw new NotAuthorizedException('NOT_ALLOWED_TO_LIST_COMMENTS');
        }


        /** @var Collection $comments */
        $comments = $user->comments()->where('type', Comment::TYPES['SUPERVISOR_COMMENT'])->get();

        $comments->load('author');

        return $this->responder->send($comments, CommentHttpResource::class);
    }
}
