<?php

namespace Modules\Supervisor\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\QueryParams;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;
use Modules\Supervisor\Http\Resources\UserSupervisorHttpResource;

class oldListAppointmentsForUserToSupervisorAction extends Action
{
    protected $appointmentService;
    protected $responder;

    public function __construct(AppointmentService $appointmentService, ResourceResponder $responder)
    {
        $this->appointmentService = $appointmentService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {
        $user = User::find($id);
        /**
         * @var $me User
         */
        $appointments = $this->appointmentService->listAppointmentsForUser($user);

        return $this->responder->send($appointments, AppointmentHttpResource::class);
    }
}
