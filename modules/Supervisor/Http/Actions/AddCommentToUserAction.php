<?php

namespace Modules\Supervisor\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Modules\Core\Domain\Models\Comment;
use Modules\Core\Domain\Models\Traits\Commentable;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Supervisor\Http\Resources\ParticipationSupervisorHttpResource;

class AddCommentToUserAction extends Action
{
    protected $responder;
    protected $userService;

    public function __construct(ResourceResponder $responder, UserService $userService)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }


    public function __invoke($id, Request $request)
    {
        $user = Auth::user();
        $commentUser = $this->userService->get($id);

        if ($user->cannot('addComment', $commentUser)) {
            throw new NotAuthorizedException;
        }

        $comment = new Comment($request->all());

        $comment->type = Comment::TYPES['SUPERVISOR_COMMENT'];
        $comment->author_id = $user->id;
        $comment->commentable()->associate($commentUser);
        $comment->save();
        $comment->refresh(); // Retreive default value for pin_to_top instead of null

        return $this->responder->send($comment, CommentHttpResource::class);
    }
}
