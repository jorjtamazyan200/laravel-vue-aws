<?php

namespace Modules\Supervisor\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Services\GroupService;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class ListAppointmentsForGroupAction extends Action
{
    protected $appointmentService;
    protected $groupService;
    protected $responder;

    public function __construct(
        AppointmentService $appointmentService,
        GroupService $groupService,
        ResourceResponder $responder
    ) {
        $this->appointmentService = $appointmentService;
        $this->groupService = $groupService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        $user = Auth::user();
        /**
         * @var $group Group
         */
        $group = $this->groupService->get($id);

        if ($user->cannot('viewAppointments', $group)) {
            throw new NotAuthorizedException();
        }

        $appointments = $this->appointmentService->listWithUserForSupervisorOfGroup($group);

        return response()->json(['data' => $appointments]);
    }
}
