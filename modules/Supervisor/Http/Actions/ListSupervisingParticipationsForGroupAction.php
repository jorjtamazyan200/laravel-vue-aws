<?php

namespace Modules\Supervisor\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Supervisor\Http\Resources\ParticipationSupervisorHttpResource;

/**
 * Class ListSupervisingParticipationsForGroupAction
 * @package Modules\Supervisor\Http\Actions
 * @depracted
 */
class ListSupervisingParticipationsForGroupAction extends Action
{
    protected $groupService;
    protected $participationService;
    protected $responder;

    public function __construct(
        ParticipationService $participationService,
        GroupService $groupService,
        ResourceResponder $responder
    ) {
        $this->groupService = $groupService;
        $this->participationService = $participationService;
        $this->responder = $responder;
    }


    public function __invoke($id, Request $request)
    {
        $participations = $this->participationService->listParticipationsForGroup((int)$id, ['match', 'enrollment','enrollment.user']);
        return $this->responder->send($participations, ParticipationSupervisorHttpResource::class);
    }
}
