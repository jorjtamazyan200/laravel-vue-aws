<?php

namespace Modules\Supervisor\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Scheduling\Http\Resources\AppointmentHttpResource;

class MatchSupervisorHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'completedLection' => $this->lections_completed,
            'plannedLections' => 10,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at,
            'appointments' => AppointmentHttpResource::collection($this->appointments),
        ];
    }
}
