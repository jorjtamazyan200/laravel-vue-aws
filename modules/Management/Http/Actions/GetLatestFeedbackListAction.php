<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Modules\Admin\Http\Resources\UserFeedbackHttpResource;
use Modules\Management\Domain\Services\UserFeedbackService;

class GetLatestFeedbackListAction
{
    /**
     * @var UserFeedbackService
     */
    protected $userFeedbackService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * ManagementDashboardAction constructor.
     * @param UserFeedbackService $userFeedbackService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(UserFeedbackService $userFeedbackService, ResourceResponder $resourceResponder)
    {
        $this->userFeedbackService = $userFeedbackService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $organizationId = auth()->user()->organization_id;
        $limit = $request->get('limit', 3);

        $newestUsers = $this->userFeedbackService->getLatestHighlightedFeedbacks($organizationId, $limit);

        return $this->resourceResponder->send($newestUsers, UserFeedbackHttpResource::class);
    }
}
