<?php

namespace Modules\Management\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Management\Domain\Services\UserManagementService;

class ProgressAction extends Action
{
    /**
     * @var UserManagementService
     */
    protected $userService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * ManagementDashboardAction constructor.
     * @param UserManagementService $userService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(UserManagementService $userService, ResourceResponder $resourceResponder)
    {
        $this->userService = $userService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function __invoke()
    {
        $organizationId = auth()->user()->organization_id;
        $data = $this->userService->getprogress($organizationId);

        return response()->json($data);
    }
}
