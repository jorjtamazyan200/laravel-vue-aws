<?php

namespace Modules\Management\Http\Resources;


use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Core\Http\Resources\FileHttpResource;

class FileGroupHttpResource extends ResourceCollection
{
    public $collects = FileHttpResource::class;

    public function toArray($request)
    {
        return $this->collection->groupBy('category');
    }
}
