<?php

namespace Modules\Management\Http\Resources;


use App\Infrastructure\Http\HttpResource;

class EnrollmentManagementHttpResource extends HttpResource
{
    public function toArray($request)
    {
        return [
            'program' => $this->program->code,
            'role' => $this->role,
            'status' => (count($this->participations) && $this->participations()->first()->match) ?
                $this->participations()->first()->match->state : $this->state,
            'completed' => $this->whenLoaded('participations', function () {
                return $this->participations->reduce(function ($sum, $participation) {
                    return $sum + ($participation->match ? $participation->match->lections_completed : 0);
                }, 0);
            }),
            'activated' => $this->whenLoaded('participations', function () {
                $licenseActivated = 0;

                foreach ($this->participations as $participation) {
                    if ($participation->match && $participation->match->licence_activated) {
                        $licenseActivated ++;
                    }
                }

                return $licenseActivated > 0;
            })
        ];
    }
}
