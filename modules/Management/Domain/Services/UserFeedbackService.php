<?php

namespace Modules\Management\Domain\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class UserFeedbackService
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var float
     */
    protected $happinessRating = 4.5;

    /**
     * UserService constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param $organizationId
     * @return mixed
     */
    protected function userFeedbacksQuery($organizationId) {
        return $this->model
            ->join('users', 'user_feedbacks.user_id', '=', 'users.id')
            ->where('users.organization_id', '=', $organizationId);
    }

    /**
     * @param $organizationId
     * @return mixed
     */
    public function getUserFeedbacks($organizationId)
    {
        return $this->userFeedbacksQuery($organizationId)->get();
    }

    /**
     * @param $organizationId
     * @return float
     */
    public function getHappinessFeedbacksPercent($organizationId)
    {
        $feedbacksCount = $this->userFeedbacksQuery($organizationId)
            ->groupBy('user_id')
            ->selectRaw('user_id')
            ->get()
            ->count();
        $happinessFeedbacksCount = $this->userFeedbacksQuery($organizationId)
            ->groupBy('user_id')
            ->havingRaw('AVG(response_scalar) > ?', [$this->happinessRating])
            ->selectRaw('user_id')
            ->get()
            ->count();

        return round($happinessFeedbacksCount * 100 / $feedbacksCount);
    }

    /**
     * @param $organizationId
     * @param int $limit
     * @return mixed
     */
    public function getLatestHighlightedFeedbacks($organizationId, $limit = 3)
    {
        return $this->model
            ->with('user')
            ->whereHas('user', function($q) use($organizationId) {
                return $q->where('users.organization_id', '=', $organizationId);
            })
            ->where('is_highlighted', '=', true)
            ->inRandomOrder()
            ->take($limit)
            ->whereNotNull('response_text')
            ->get();
    }
}
