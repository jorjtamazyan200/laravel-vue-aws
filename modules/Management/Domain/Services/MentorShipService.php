<?php

namespace Modules\Management\Domain\Services;


use Modules\Core\Domain\Models\Webinar;

class MentorShipService
{
    /**
     * MentorShipService constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param int $organizationId
     * @return mixed
     */
    public function getMentoringHours(int $organizationId)
    {
        return Webinar::query()
            ->where('webinars.state', Webinar::STATES['SUCCESS'])
            ->where('users.organization_id', $organizationId)
            ->join('programs', 'programs.id', '=', 'webinars.program_id')
            ->join('users', 'users.id', '=', 'programs.manager_id')
            ->sum('webinars.duration_minutes');
    }
}
