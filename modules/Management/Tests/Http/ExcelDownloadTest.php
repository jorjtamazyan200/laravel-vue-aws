<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Core\Domain\Models\Role;
use Modules\Management\Exports\UsersExport;

class ExcelDownloadTest extends EndpointTest
{
    /**
     *
     */
    public function test_it_downloads_excel()
    {
        Excel::fake();
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);


        $this->actingAs($this->user)->get(route('management.exportUsers'));
        Excel::assertDownloaded('users.xlsx', function (UsersExport $export) {
            return array_key_exists('first_name', $export->array()[0]);
        });
    }
}
