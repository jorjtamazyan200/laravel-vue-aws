<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Role;

class GetLatestFeedbacksTest extends EndpointTest
{
    public function test_it_lists_latest_feedbacks()
    {

        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);

        $response = $this
            ->actingAs($this->user)
            ->getJson(route('management.getLatestFeedbacks'));


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' =>
                [
                    [
                        'id',
                        'questionCode',
                        'responseScalar',
                        'responseText',
                        'createdAt',
                        'user',
                        'processingState',
                        'isHighlighted',
                    ]
                ]
        ]);
    }
}
