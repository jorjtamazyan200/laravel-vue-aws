<?php

namespace Modules\Management\Tests\Http;


use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Role;

class GetProgressTest extends EndpointTest
{
    public function test_it_get_progress()
    {
        $date = Carbon::parse(now());
        $quarterYearIndex = $date->quarter . $date->year;
        $managerRole = Role::whereName(Role::ROLES['manager'])->first();
        $this->user->roles()->attach($managerRole);



        $response = $this->actingAs($this->user)->getJson(route('management.progress'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            $quarterYearIndex => [
                'quarter',
                'users_count_progress',
                'users_percent_progress',
                'appointments_count_progress',
                'appointments_percent_progress',
                'activated_licenses_count_progress',
                'activated_licenses_percent_progress',
            ]
        ]);
    }
}
