<?php

namespace Modules\Community\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

/**
 * Class CreatePostRequest
 *
 * @package Modules\Community\Http\Requests
 */
class CreatePostRequest extends DeserializedFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'group_id' => 'required|integer',
            'body' => 'required|string|min:1',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
