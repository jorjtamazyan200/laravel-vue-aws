<?php

namespace Modules\Community\Http\Actions;

use Illuminate\Support\Facades\Request;
use Modules\Community\Domain\Services\ChatroomService;
use Modules\Community\Http\Resources\ChatroomHttpResource;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Requests\CommentRequest;
use App\Infrastructure\Http\ResourceResponder;
use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class ShowChatroomAction extends Action
{
    /**
     * @var ChatroomService
     */
    private $chatroomService;

    /**
     * @var ResourceResponder
     */
    private $responder;

    /**
     * ShowChatroomAction constructor.
     * @param ResourceResponder $responder
     * @param ChatroomService $chatroomService
     */
    public function __construct(ResourceResponder $responder, ChatroomService $chatroomService)
    {
        $this->responder = $responder;
        $this->chatroomService = $chatroomService;
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     * @throws \App\Exceptions\Client\ClientException
     */
    public function __invoke($id, Request $request)
    {
        $myId = Auth::id();

        $users = [$id, $myId];

        $chatroom = $this->chatroomService->findChatRoomForUsers($users);

        return $this->responder->send($chatroom, ChatroomHttpResource::class);
    }
}
