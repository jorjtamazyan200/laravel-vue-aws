<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Models\PostComment;
use Modules\Community\Http\Requests\UpdatePostCommentRequest;
use Modules\Community\Http\Resources\PostCommentHttpResource;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UpdatePostCommentAction
 *
 * @package Modules\Community\Http\Actions
 */
class UpdatePostCommentAction extends Action
{
    /** @var ResourceResponder */
    private $resourceResponder;

    /**
     * UpdatePostCommentAction constructor.
     *
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(ResourceResponder $resourceResponder)
    {
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param int $id Post database id
     * @param int $commentId
     * @param UpdatePostCommentRequest $request
     *
     * @return mixed
     */
    public function __invoke(int $id, int $commentId, UpdatePostCommentRequest $request)
    {
        $post = Post::find($id);
        if (is_null($post)) {
            throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $id));
        }

        $comment = PostComment::find($commentId);
        if (is_null($comment)) {
            throw new NotFoundHttpException(sprintf('Comment with id %d could not be found', $commentId));
        }

        if ($comment->user_id !== Auth::id()) {
            throw new AccessDeniedHttpException('User can edit only his own comments.');
        }

        $commentBody = $request->get('body');
        $comment->body = $commentBody;
        $comment->save();

        return $this->resourceResponder->send($comment, PostCommentHttpResource::class);
    }
}
