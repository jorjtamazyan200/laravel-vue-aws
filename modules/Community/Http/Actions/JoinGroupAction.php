<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Services\GroupService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class JoinGroupAction
 *
 * @package Modules\Community\Http\Actions
 */
class JoinGroupAction extends Action
{
    /** @var GroupService */
    private $groupService;

    /**
     * JoinGroupAction constructor.
     *
     * @param GroupService $groupService
     */
    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    /**
     * @param int $id Group id
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(int $id)
    {
        $user = Auth::user();
        $group = Group::where('id', $id)->first();
        if (is_null($group)) {
            throw new NotFoundHttpException(sprintf('Group with id %d could not be found', $id));
        }

        if (Group::PRIVACY_PUBLIC !== $group->privacy) {
            throw new AccessDeniedHttpException('User can join only public groups');
        }

        if (false === $this->groupService->isMemberOf($user, $group->id)) {
            $this->groupService->attachUserToGroup($user, $group, 'member');
        }

        return response([], 201);
    }
}
