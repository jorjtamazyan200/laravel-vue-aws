<?php

namespace Modules\Community\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\Query;
use App\Infrastructure\Http\QueryParams;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\UserPublicHttpResource;
use Modules\Core\Http\Resources\UserHttpResource;

class ShowPublicProfileAction extends Action
{
    protected $userService;
    protected $responder;

    public function __construct(UserService $userService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {

        /**
         * @var $currentUser User
         * @var $user User
         */
        $currentUser = Auth::user();
        $user = $this->userService->get($id);

        // @todo: is the target user in the community?



        if ($currentUser->cannot('viewCommunityProfile', $user)) {
            throw new NotAuthorizedException();
        }

        return $this->responder->send($user, UserPublicHttpResource::class);
    }
}
