<?php

namespace Modules\Community\Http\Actions;


use App\Infrastructure\Http\ResourceResponder;
use Modules\Community\Domain\Services\PostService;
use Modules\Community\Http\Resources\PostHttpResource;
use Modules\Community\Http\Resources\PostsListHttpResource;

class GetPostAction
{
    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * @var PostService
     */
    private $postService;

    /**
     * GetPostAction constructor.
     * @param ResourceResponder $resourceResponder
     * @param PostService $postService
     */
    public function __construct(ResourceResponder $resourceResponder, PostService $postService)
    {
        $this->resourceResponder = $resourceResponder;
        $this->postService = $postService;
    }

    public function __invoke($id)
    {
        $userId = auth()->id();
        $post = $this->postService->getPost($id, $userId);

        return $this->resourceResponder->send($post, PostsListHttpResource::class);
    }
}
