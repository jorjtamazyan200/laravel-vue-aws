<?php

namespace Modules\Community\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\Query;
use App\Infrastructure\Http\QueryParams;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Resources\GroupMinimalHttpResource;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Services\PostService;
use Modules\Community\Http\Resources\CommunityGroupResoure;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Core\Tests\Unit\GroupServiceTest;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DislikePostAction
 *
 * @package Modules\Community\Http\Actions
 */
class ShowPublicGroupAction extends Action
{
    /**
     * @var GroupService
     */
    private $groupService;

    /**
     * @var ResourceResponder
     */
    private $responder;

    /**
     * ShowPublicGroupAction constructor.
     * @param ResourceResponder $responder
     * @param GroupService $groupService
     */
    public function __construct(ResourceResponder $responder, GroupService $groupService)
    {
        $this->responder = $responder;
        $this->groupService = $groupService;
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(int $id)
    {
        /** @var User $user */
        $user = Auth::user();

        $query = new QueryParams();
        $query->setIncludes(['users']);
        $group = $this->groupService->get($id, $query);

//        if (false === $this->groupService->isMemberOf($user, $id)) {
//            throw new NotAuthorizedException("you can only fetch groups you are member of");
//        }

        return $this->responder->send($group, CommunityGroupResoure::class);
    }
}
