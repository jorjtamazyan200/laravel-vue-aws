<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Services\PostService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DeletePostAction
 *
 * @package Modules\Community\Http\Actions
 */
class DeletePostAction extends Action
{
    /** @var PostService */
    private $postService;

    /**
     * DeletePostAction constructor.
     *
     * @param PostService $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @param int $postId
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(int $postId)
    {
        $post = Post::find($postId);
        if (is_null($post)) {
            throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $postId));
        }

        $loggedInUser = Auth::user();
        if ($post->user_id !== $loggedInUser->id) {
            throw new AccessDeniedHttpException('User can only delete his own posts.');
        }

        $this->postService->delete($postId);

        return response([], 204);
    }
}
