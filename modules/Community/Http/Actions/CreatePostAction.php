<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Community\Domain\Services\PostService;
use Modules\Community\Http\Requests\CreatePostRequest;
use Modules\Community\Http\Resources\PostHttpResource;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Services\GroupService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CreatePostAction
 *
 * @package Modules\Community\Http\Actions
 */
class CreatePostAction extends Action
{
    /** @var PostService */
    private $postService;

    /** @var GroupService */
    private $groupService;

    /** @var ResourceResponder */
    private $resourceResponder;

    /**
     * CreatePostAction constructor.
     *
     * @param PostService $postService
     * @param GroupService $groupService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(PostService $postService, GroupService $groupService, ResourceResponder $resourceResponder)
    {
        $this->postService = $postService;
        $this->groupService = $groupService;
        $this->resourceResponder = $resourceResponder;
    }

    /**
     * @param CreatePostRequest $request
     *
     * @return mixed
     */
    public function __invoke(CreatePostRequest $request)
    {
        $loggedInUser = Auth::user();
        $groupId = $request->get('group_id');
        $body = $request->get('body');

        $group = Group::find($groupId);
        if (is_null($group)) {
            throw new NotFoundHttpException(sprintf('Group with id %d could not be found', $groupId));
        }

        if (false === $this->groupService->isMemberOf($loggedInUser, $groupId)) {
            throw new AccessDeniedHttpException('User can create posts only in groups he is member of.');
        }

        $postData = [
            'user_id' => $loggedInUser->id,
            'group_id' => $groupId,
            'body' => $body,
        ];
        $createdPost = $this->postService->create($postData);

        return $this->resourceResponder->send($createdPost, PostHttpResource::class);
    }
}
