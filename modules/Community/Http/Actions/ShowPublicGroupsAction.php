<?php

namespace Modules\Community\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Resources\GroupMinimalHttpResource;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Services\PostService;
use Modules\Community\Http\Resources\CommunityGroupResoure;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Core\Tests\Unit\GroupServiceTest;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DislikePostAction
 *
 * @package Modules\Community\Http\Actions
 */
class ShowPublicGroupsAction extends Action
{
    /**
     * @var ResourceResponder
     */
    private $responder;

    /**
     * @var GroupService
     */
    private $groupService;

    /**
     * ShowPublicGroupsAction constructor.
     * @param ResourceResponder $responder
     * @param GroupService $groupService
     */
    public function __construct(ResourceResponder $responder, GroupService $groupService)
    {
        $this->responder = $responder;
        $this->groupService = $groupService;
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke()
    {
        $userId = auth()->id();

        $groups = $this->groupService->getPublicGroups();
        $groups = $this->groupService->addIsMemberOfToGroups($groups, $userId);

        return $this->responder->send($groups, CommunityGroupResoure::class);
    }
}
