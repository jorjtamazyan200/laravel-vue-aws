<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Community\Domain\Services\PostService;
use Modules\Community\Http\Requests\ListPostsRequest;
use Modules\Community\Http\Resources\PostsListHttpResource;

/**
 * Class ListPostsAction
 *
 * @package Modules\Community\Http\Actions
 */
class ListPostsAction extends Action
{

    /** @var ResourceResponder */
    private $resourceResponder;

    /**
     * @var PostService
     */
    private $postService;

    /**
     * ListPostCommentsAction constructor.
     *
     * @param ResourceResponder $resourceResponder
     * @param PostService $postService
     */
    public function __construct(ResourceResponder $resourceResponder, PostService $postService)
    {
        $this->resourceResponder = $resourceResponder;
        $this->postService = $postService;
    }

    /**
     * @param ListPostsRequest $request
     *
     * @return mixed
     */
    public function __invoke(ListPostsRequest $request)
    {
        $groupIds = $request->get('group_id');
        $lastPostId = $request->get('last_post_id', null);
        $limit = $request->get('limit');
        $userId = auth()->id();

        $posts = $this->postService->getPostsByGroupId($groupIds, $userId, $lastPostId, $limit);

        return $this->resourceResponder->send($posts, PostsListHttpResource::class);
    }
}
