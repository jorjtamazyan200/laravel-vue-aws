<?php

namespace Modules\Community\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Models\PostComment;
use Modules\Community\Domain\Services\PostService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DeletePostCommentAction
 *
 * @package Modules\Community\Http\Actions
 */
class DeletePostCommentAction extends Action
{
    /** @var PostService */
    private $postService;

    /**
     * DeletePostCommentAction constructor.
     *
     * @param PostService $resourceResponder
     */
    public function __construct(PostService $resourceResponder)
    {
        $this->postService = $resourceResponder;
    }

    /**
     * @param int $id Post database id
     * @param int $commentId
     *
     * @return mixed
     */
    public function __invoke(int $id, int $commentId)
    {
        $post = Post::find($id);
        if (is_null($post)) {
            throw new NotFoundHttpException(sprintf('Post with id %d could not be found', $id));
        }

        $comment = PostComment::find($commentId);
        if (is_null($comment)) {
            throw new NotFoundHttpException(sprintf('Comment with id %d could not be found', $commentId));
        }

        $loggedInUser = Auth::user();
        if (
            $comment->user_id !== $loggedInUser->id &&
            false === in_array($loggedInUser->role, ['admin', 'superadmin'])
        ) {
            throw new AccessDeniedHttpException('User can delete only his own comments.');
        }

        $this->postService->deleteComment($commentId);

        return response([], 204);
    }
}
