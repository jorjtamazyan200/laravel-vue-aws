<?php

namespace Modules\Community\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\UserPublicHttpResource;

/**
 * Class PostsListHttpResource
 *
 * @package Modules\Community\Http\Resources
 */
class PostsListHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'group_name'=>$this->group->name,
            'group_id'=>$this->group_id,
            'user_id' => $this->user_id,
            'username' => e($this->user()->first()->first_name) . ' ' . e($this->user()->first()->last_name),
            'body' => e($this->body),
            'comments_count' => $this->comments()->count(),
            'user' => new UserPublicHttpResource($this->whenLoaded('user')),
            'likes_count' => $this->likes()->count(),
            'is_liked' => $this->isLiked,
            'created_at' => $this->created_at->format('d-M-Y'),
        ];
    }
}
