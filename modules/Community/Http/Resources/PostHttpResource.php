<?php

namespace Modules\Community\Http\Resources;

use App\Infrastructure\Http\HttpResource;

/**
 * Class PostHttpResource
 *
 * @package Modules\Community\Http\Resources
 */
class PostHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'group_id' => $this->group_id,
            'body' => e($this->body),
            'created_at' => $this->created_at->toIso8601String(),
        ];
    }
}
