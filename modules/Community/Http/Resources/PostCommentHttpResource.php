<?php

namespace Modules\Community\Http\Resources;

use App\Infrastructure\Http\HttpResource;

/**
 * Class PostCommentHttpResource
 *
 * @package Modules\Community\Http\Resources
 */
class PostCommentHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'post_id' => $this->post_id,
            'body' => e($this->body),
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at->toIso8601String(),
        ];
    }
}
