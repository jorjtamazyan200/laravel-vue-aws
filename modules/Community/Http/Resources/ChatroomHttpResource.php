<?php

namespace Modules\Community\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Admin\Http\Resources\UserMinimalHttpResource;

class ChatroomHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'externalRoomId' => $this->external_room_id
        ];
    }
}
