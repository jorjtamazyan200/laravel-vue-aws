<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Community\Domain\Models\Post;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;

/**
 * Class DeletePostTest
 *
 * @package Modules\Community\Tests\Http
 */
class DeletePostTest extends EndpointTest
{
    public function test_it_returns_error_when_post_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->deleteJson('/api/v2/community/users/me/posts/0');

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Post with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_user_deletes_other_users_post()
    {
        $this->user->community_enabled = true;

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $post = factory(Post::class)->create([
            'user_id' => $otherUser->id,
            'group_id' => 1,
            'body' => 'Hello',
        ]);

        $response = $this->actingAs($this->user)->deleteJson('/api/v2/community/users/me/posts/' . $post->id);

        $response->assertJson([
            'message' => 'User can only delete his own posts.',
        ]);
    }

    public function test_it_deletes_post()
    {
        $this->user->community_enabled = true;
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => 1,
            'body' => 'Hello',
        ]);

        $response = $this->actingAs($this->user)->deleteJson('/api/v2/community/users/me/posts/' . $post->id);

        self::assertEquals(204, $response->getStatusCode(), $response->getContent());
        self::assertNull(Post::find($post->id));
    }
}
