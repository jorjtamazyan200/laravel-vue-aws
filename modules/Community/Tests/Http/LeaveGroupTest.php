<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Group;

/**
 * Class LeaveGroupTest
 *
 * @package Modules\Community\Tests\Http
 */
class LeaveGroupTest extends EndpointTest
{
    public function test_it_returns_error_when_group_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/groups/0');

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Group with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_group_is_not_public()
    {
        $this->user->community_enabled = true;
        $organization = factory(\Modules\Core\Domain\Models\Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PRIVATE
        ]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/groups/' . $group->id);

        $response->assertJson([
            'message' => 'User can leave only public groups',
        ]);
    }

    public function test_it_detaches_user_from_group()
    {
        $this->user->community_enabled = true;
        $organization = factory(\Modules\Core\Domain\Models\Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Test group',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PUBLIC
        ]);

        $group->users()->attach($this->user->id);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/groups/' . $group->id);

        self::assertEquals(204, $response->getStatusCode());
        self::assertNull($group->users()->find($this->user->id));
    }
}
