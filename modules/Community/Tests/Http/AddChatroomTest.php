<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Community\Domain\Models\Chatroom;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class AddChatroomTest extends EndpointTest
{
    /** @test */
    public function test_it_creates_a_chatroom()
    {
        $this->user->community_enabled = true;
        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();


        $response = $this->actingAs($this->user)->postJson(
            '/api/v2/community/users/' . $otherUser->id . '/chatroom',
            ['externalRoomId' => rand(0, 1000)]
        );

        $this->assertEquals(201, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                'id'
            ]
        ]);
    }

    public function test_it_creates_only_one_chatroom()
    {
        $this->user->community_enabled = true;
        $externalRoomId = rand(0, 1000);


        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();

        $response = $this->actingAs($this->user)->postJson(
            '/api/v2/community/users/' . $otherUser->id . '/chatroom',
            ['externalRoomId' => $externalRoomId]
        );
        $this->assertEquals(201, $response->getStatusCode(), $response->getContent());


        $response = $this->actingAs($this->user)->postJson(
            '/api/v2/community/users/' . $otherUser->id . '/chatroom',
            ['externalRoomId' => $externalRoomId]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                'id'
            ]
        ]);
    }

    /** @test */
    public function test_it_does_load_given_chatroom()
    {
        $this->user->community_enabled = true;
        $userid = $this->user->id;

        $user = User::where('email', 'supervisor@volunteer-vision.com')->firstOrFail();

        $chatroom = new Chatroom();
        $chatroom->external_room_id = rand(0, 100);
        $chatroom->save();
        $chatroom->users()->sync([$this->user->id, $user->id]);


        $response = $this->actingAs($this->user)->getJson('/api/v2/community/users/' . $user->id . '/chatroom');


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $response->assertJsonFragment(['externalRoomId' => $chatroom->external_room_id]);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'externalRoomId'
            ]
        ]);
    }
}
