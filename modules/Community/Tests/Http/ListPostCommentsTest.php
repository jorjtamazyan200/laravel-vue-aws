<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Models\PostComment;

/**
 * Class ListPostCommentsTest
 *
 * @package Modules\Community\Tests\Http
 */
class ListPostCommentsTest extends EndpointTest
{
    public function test_it_returns_error_when_post_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->getJson('/api/v2/community/users/me/posts/0/comments');

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Post with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_last_comment_does_not_exist()
    {
        $this->user->community_enabled = true;
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => 1,
            'body' => 'Post #1',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->getJson('/api/v2/community/users/me/posts/' . $post->id . '/comments?last_comment_id=0');

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Comment with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_parent_comment_does_not_exist()
    {
        $this->user->community_enabled = true;
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => 1,
            'body' => 'Post #1',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->getJson('/api/v2/community/users/me/posts/' . $post->id . '/comments?parent_comment_id=0');

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Comment with id 0 could not be found',
        ]);
    }

    public function test_it_returns_post_comments()
    {
        $this->user->community_enabled = true;

        /** @var Post  */
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => 1,
            'body' => 'Post #1',
        ]);

        factory(PostComment::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $post->id,
            'body' => 'Post #1',
        ]);

        $response = $this
            ->actingAs($this->user)
            ->getJson('/api/v2/community/users/me/posts/' . $post->id . '/comments?limit=1');

        self::assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'user_id',
                    'username',
                    'body',
                    'parent_id',
                    'comments_count',
                    'created_at',
                ]
            ],
        ]);
    }
}
