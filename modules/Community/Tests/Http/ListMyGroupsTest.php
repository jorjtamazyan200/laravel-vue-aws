<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Class ListMyGroupsTest
 *
 * @package Modules\Community\Tests\Http
 */
class ListMyGroupsTest extends EndpointTest
{
    public function test_it_lists_all_my_groups()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->get('/api/v2/community/users/me/groups?page=1&per_page=1');

        self::assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'description',
                    'name',
                    'users' => [],
                ]
            ]
        ]);
    }
}
