<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Community\Domain\Models\Post;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;

/**
 * Class UpdatePostTest
 *
 * @package Modules\Community\Tests\Http
 */
class UpdatePostTest extends EndpointTest
{
    public function test_it_returns_error_when_post_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->putJson('/api/v2/community/users/me/posts/0', [
            'group_id' => 1,
            'body' => 'Lorem Ipsum'
        ]);

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Post with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_user_updates_other_users_post()
    {
        $this->user->community_enabled = true;
        $organization = factory(Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Private group',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $post = factory(Post::class)->create([
            'user_id' => $otherUser->id,
            'group_id' => $group->id,
            'body' => 'Hello',
        ]);

        $response = $this->actingAs($this->user)->putJson('/api/v2/community/users/me/posts/' . $post->id, [
            'group_id' => $group->id,
            'body' => 'Lorem Ipsum'
        ]);

        $response->assertJson([
            'message' => 'User can only edit own posts.',
        ]);
    }

    public function test_it_updates_post()
    {
        $this->user->community_enabled = true;
        $organization = factory(Organization::class)->create([
            'name' => 'Test Volunteer Vision',
            'type' => 'company',
        ]);

        $group = factory(Group::class)->create([
            'name' => 'Group X',
            'organization_id' => $organization->id,
            'privacy' => Group::PRIVACY_PUBLIC,
        ]);

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => $group->id,
            'body' => 'Hello',
        ]);

        $response = $this->actingAs($this->user)->putJson('/api/v2/community/users/me/posts/' . $post->id, [
            'group_id' => $group->id,
            'body' => 'Lorem Ipsum'
        ]);

        self::assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'group_id',
                'body',
                'created_at',
            ],
        ]);

        $updatedPost = Post::find($post->id);
        self::assertEquals('Lorem Ipsum', $updatedPost->body);
    }
}
