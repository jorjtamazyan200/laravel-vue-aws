<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class ShowPublicProfileTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_public_profile()
    {


        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->getJson('/api/v2/community/users/' . $this->user->id);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                'id',
                'firstName',
                'lastName'
            ]
        ]);
    }

    /** @test */
    public function test_it_does_not_show_private_profile()
    {
        $this->user->community_enabled = true;
        $user = User::where('email', 'supervisor@volunteer-vision.com')->firstOrFail();

        $response = $this->actingAs($this->user)->getJson('/api/v2/community/users/' . $user->id);


        $this->assertEquals(403, $response->getStatusCode(), $response->getContent());
    }


}
