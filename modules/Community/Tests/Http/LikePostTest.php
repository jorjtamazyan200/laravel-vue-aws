<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Community\Domain\Models\Post;
use Modules\Community\Domain\Models\PostLike;
use Modules\Core\Domain\Models\User;

/**
 * Class LikePostTest
 *
 * @package Modules\Community\Tests\Http
 */
class LikePostTest extends EndpointTest
{
    public function test_it_returns_error_when_post_does_not_exist()
    {
        $this->user->community_enabled = true;
        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/posts/0/likes');

        self::assertEquals(404, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'message' => 'Post with id 0 could not be found',
        ]);
    }

    public function test_it_returns_error_when_owner_user_likes_post()
    {
        $this->user->community_enabled = true;

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'group_id' => 1,
            'body' => 'Hello',
        ]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/posts/' . $post->id . '/likes');

        $response->assertJson([
            'message' => 'User can not like his own post.',
        ]);
    }

    public function test_it_returns_error_when_same_user_tries_to_like_post_twice()
    {
        $this->user->community_enabled = true;

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $post = factory(Post::class)->create([
            'user_id' => $otherUser->id,
            'group_id' => 1,
            'body' => 'Hello',
        ]);

        factory(PostLike::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $post->id,
        ]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/posts/' . $post->id . '/likes');

        $response->assertJson([
            'message' => 'User can not like the same post twice.',
        ]);
    }

    public function test_it_likes_post()
    {
        $this->user->community_enabled = true;

        $otherUser = User::where('email', '=', 'community@volunteer-vision.com')->firstOrFail();
        $post = factory(Post::class)->create([
            'user_id' => $otherUser->id,
            'group_id' => 1,
            'body' => 'Hello',
        ]);

        $response = $this->actingAs($this->user)->postJson('/api/v2/community/users/me/posts/' . $post->id . '/likes');
        self::assertEquals(201, $response->getStatusCode(), $response->getContent());
        self::assertEquals(1, $post->likes()->count());
    }
}
