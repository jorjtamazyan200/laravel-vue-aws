<?php

namespace Modules\Community\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class ShowPublicGroupsTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_public_groups()
    {

        $group = Group::query()->inRandomOrder()->first();
        $this->user->community_enabled = true;

        $response = $this->actingAs($this->user)
            ->getJson('/api/v2/community/groups');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [[
                'id',
                'name'
            ]]
        ]);
    }
}
