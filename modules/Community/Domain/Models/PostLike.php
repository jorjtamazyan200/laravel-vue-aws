<?php

namespace Modules\Community\Domain\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PostLike
 *
 * @package Modules\Community\Domain\Models
 */
class PostLike extends Model
{
    /** @var string */
    protected $table = 'post_likes';

    /** @var bool */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'user_id',
        'created_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];
}
