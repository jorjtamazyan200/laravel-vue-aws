<?php

namespace Modules\ModuleBlueprint\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class ExampleHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'camelCaseField' => $this->snake_case_field,
        ];
    }
}
