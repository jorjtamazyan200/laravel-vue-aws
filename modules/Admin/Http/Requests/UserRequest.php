<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'string|min:2',
            'last_name' => 'string|min:2',
            'password' => 'string|min:6',
            'email' => 'unique:users',
            'birthday' => 'nullable|date|before:today',
//            'phone_number' => 'number',
//            'phone_number_prefix' => 'number',
//            'whatsapp_number' => 'number',
//            'whatsapp_number_prefix' => 'number',
            'city' => 'nullable|string',
            'postcode' => 'nullable|string',
            'country' => 'nullable|string',
            'country_of_origin' => 'nullable|string',
            'language' => 'string|min:2|max:2',
            'accept_sms' => 'boolean',
            'accept_mail' => 'boolean',
            'accept_push' => 'boolean',
            'is_anonymized' => 'boolean',
            'last_reminder' => 'date',
            'last_login' => 'date',
            'tenant_id' => 'exists:tenants,id',
        ];

        // https://laracasts.com/discuss/channels/requests/laravel-5-validation-request-how-to-handle-validation-on-update
        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $rules['email'] = 'unique:users,email,' . $this->id;
        }

        return $rules;
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
