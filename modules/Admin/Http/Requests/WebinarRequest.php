<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class WebinarRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'title' => 'string|min:2',
            'duration_minutes' => 'required|integer',
            'referent_id' => 'required|integer',
            'program_id' => 'required|integer',
            'seats' => 'required|integer',
            'starts_at' => 'required|date',
            'target_audience' => 'required|string',
            'login_link' => 'required|string'
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
