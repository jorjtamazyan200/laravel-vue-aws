<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Http\Request;

class AddUserToGroupRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {

        // @todo: validity check: supervisor | member
        $rules = [
            'role' => 'string|min:2',
        ];


        return $rules;
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
