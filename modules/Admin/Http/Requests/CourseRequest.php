<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class CourseRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'thumbnail' => 'image',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
