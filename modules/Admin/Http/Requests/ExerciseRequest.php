<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class ExerciseRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'tc_section_id' => 'required|exists:tc_sections,id',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
