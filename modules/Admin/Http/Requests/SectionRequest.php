<?php

namespace Modules\Admin\Http\Requests;


use App\Infrastructure\Http\DeserializedFormRequest;

class SectionRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
