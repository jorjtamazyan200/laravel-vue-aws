<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Modules\TrainingCenter\Domain\Models\QuizQuestion;

class VideoTranscriptRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'text' => 'required',
            'tc_video_id' => 'required|exists:tc_videos,id',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
