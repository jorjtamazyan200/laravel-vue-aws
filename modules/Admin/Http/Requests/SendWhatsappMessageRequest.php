<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class SendWhatsappMessageRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'message' => 'string|required',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
