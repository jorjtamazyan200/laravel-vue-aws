<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class RegistrationCodeRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'name' => 'required|string',
            'code' => 'required|string', // |exists:registration_codes,code ?
            'primary_role' => 'required|string',
            'brand_id' => 'required|integer',
            'group_id' => 'required|integer'
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
