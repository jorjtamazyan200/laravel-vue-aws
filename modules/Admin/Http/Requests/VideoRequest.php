<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Modules\TrainingCenter\Domain\Models\Video;

class VideoRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'order' => 'numeric',
            'url' => 'required',
            'video_type' => 'in:' . implode(',', Video::$videoTypes),
            'tc_section_id' => 'required|exists:tc_sections,id',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
