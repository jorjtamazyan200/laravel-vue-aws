<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Validation\Rule;
use Modules\TrainingCenter\Domain\Models\Video;

class LecturerRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        $linkedinRegex = '/^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\/(pub|in|profile)/';

        $uniqueRule = '|unique:tc_lecturers,email';
        if (!is_null(request()->id)) {
            $uniqueRule .= ',' . request()->id . ',id';
        }

        return [
            'name' => 'required',
            'description' => 'required',
            'email' => 'required|email' . $uniqueRule,
            'avatar' => 'image',
            'linkedin_link' => ['required', 'regex:' . $linkedinRegex],
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
