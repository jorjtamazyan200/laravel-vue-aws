<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class MatchRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'licenceFree' => 'boolean',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
