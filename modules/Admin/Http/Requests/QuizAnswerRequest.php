<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Modules\TrainingCenter\Domain\Models\QuizQuestion;

class QuizAnswerRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'text' => 'required',
            'is_correct' => 'required|in:0,1',
            'tc_quiz_question_id' => 'required|exists:tc_quiz_questions,id',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
