<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use App\Exceptions\Client\ValidationException;

class ExerciseFileUploadRequest extends FormRequest implements ValidatesWhenResolved
{
    /**
     * {@inheritDoc}
     */
    public function validate()
    {
        $errors = [];
        $valid_types = ['application/pdf'];
        $maxFilesize = 20000000;

        if (strlen($this->fileContent()) == 0) {
            $errors[] = 'validation.no_file';
        }

        // Example: 20 MB = 20000000
        if ($this->fileSize() > $maxFilesize) {
            $errors[] = 'validation.filesize';
        }


        if (!in_array($this->fileType(), $valid_types)) {
            $errors[] = 'validation.content_type';
        }

        if (count($errors)) {
            throw new ValidationException('File upload failed', null, null, ['file' => $errors]);
        }
    }

    public function fileName()
    {
        return $this->header('Filename', 'file_' . rand(100, 1000));
    }

    public function fileSize() {
        return strlen($this->fileContent());
    }
    /**
     * Returns the sent binary data
     *
     * @return String
     */
    public function fileContent()
    {
        return $this->getContent();
    }

    /**
     * Returns the sent Content-Type Header
     *
     * @return String
     */
    public function fileType()
    {
        return $this->header('Content-Type');
    }
}
