<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class OrganizationRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'name' => 'string|min:2',
            'keyaccount_id' => 'integer',
//            'logo' => 'string|min:2'
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
