<?php

namespace Modules\Admin\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Modules\TrainingCenter\Domain\Models\QuizQuestion;

class QuizQuestionRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'text' => 'required',
            'type' => 'required|in:' . implode(',', QuizQuestion::$types),
            'tc_section_id' => 'required|exists:tc_sections,id',
        ];
    }

    /** {@inheritDoc} */
    public function authorize()
    {
        return true;
    }
}
