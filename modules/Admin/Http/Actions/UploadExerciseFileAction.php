<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Admin\Http\Requests\ExerciseFileUploadRequest;
use Modules\TrainingCenter\Domain\Services\ExerciseService;
use Modules\TrainingCenter\Http\Resources\ExerciseHttpResource;

class UploadExerciseFileAction extends Action
{
    /**
     * @var ExerciseService
     */
    protected $exerciseService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * UploadExerciseFileAction constructor.
     * @param ExerciseService $exerciseService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(ExerciseService $exerciseService, ResourceResponder $resourceResponder)
    {
        $this->exerciseService = $exerciseService;
        $this->resourceResponder = $resourceResponder;
    }

    public function __invoke($id, ExerciseFileUploadRequest $request)
    {
        $exercise = $this->exerciseService->uploadFileAndSaveExercise($id, $request->fileName(), $request->fileContent(), $request->fileType());

        return $this->resourceResponder->send($exercise, ExerciseHttpResource::class);
    }
}
