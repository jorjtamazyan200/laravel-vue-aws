<?php

namespace Modules\Admin\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Admin\Http\Resources\SectionAdminHttpResource;
use Modules\TrainingCenter\Domain\Services\SectionService;

class AssignCourseToSectionAction extends Action
{
    /**
     * @var SectionService
     */
    protected $sectionService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * AssignCourseToSectionAction constructor.
     * @param SectionService $sectionService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(SectionService $sectionService, ResourceResponder $resourceResponder)
    {
        $this->sectionService = $sectionService;
        $this->resourceResponder = $resourceResponder;
    }

    public function __invoke(int $id, int $courseId)
    {
        $section = $this->sectionService->assignCourse($id, $courseId);
        $section->load('courses');

        return $this->resourceResponder->send($section, SectionAdminHttpResource::class);
    }
}
