<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\External\Domain\Services\Conference3Service;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

/**
 * Class AppProgramToOrganizationAction
 * @package Modules\Admin\Http\Actions
 * @todo add Test for Endpoint
 */
class ShowRoomLinkAction extends Action
{

    /**
     * @var ConferenceService
     * @deprecated
     */
    protected $conferenceService;
    protected $conference3Service;
    protected $programService;

    protected $matchService;

    public function __construct(MatchService $matchService, ConferenceService $conferenceService, Conference3Service $conference3Service, ProgramService $programService)
    {
        $this->conferenceService = $conferenceService;
        $this->conference3Service = $conference3Service;
        $this->programService = $programService;

        $this->matchService = $matchService;
    }

    public function __invoke(string $type, int $id)
    {
        /** @var User $user */
        $user = Auth::user();


        if ($type === 'match') {
            /** @var Match $match */
            $match = $this->matchService->get($id);
            $url = $this->matchService->getClassroomLink($match, $user);
        }
        if ($type === 'program') {
            /** @var Program $program */
            $program = $this->programService->get($id);
            $room = $this->conference3Service->getDemoRoomId($program);
            $url = $this->conference3Service->getExternalLinkForRoomId($room, $user, 'demo');
        }

        return response()->json(['data' => [
            'url' => $url,
            'permanentPreview' => $this->conference3Service->getPublicPreviewUrl($program)
        ]]);
    }
}
