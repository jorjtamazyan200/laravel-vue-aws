<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Http\Requests\MatchParticipationRequest;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchingService;
use Modules\Matching\Domain\Services\MatchService;

/**
 * @todo: this endpoint has no test;
 *
 * Class MatchParticipationAction
 * @package Modules\Admin\Http\Actions
 */
class MatchParticipationAction extends Action
{
    /**
     * @var MatchService $matchService
     */
    protected $matchService;
    protected $matchingService;

    protected $participationService;


    public function __construct(MatchService $matchService, MatchingService $matchingService, ParticipationService $participationService)
    {
        $this->matchService = $matchService;
        $this->matchingService = $matchingService;
        $this->participationService = $participationService;
    }


    /**
     * @param int $id
     * @param MatchParticipationRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws ClientException
     */
    public function __invoke(int $id, MatchParticipationRequest $request)
    {

// @todo: this should be placed in the matching services shoudn't it?

        $otherParticipation = $request->get('participationId');


        /** @var Participation $oneParticipation */
        $oneParticipation = $this->participationService->get($id);
        /** @var Participation $secondParticipation */
        $secondParticipation = $this->participationService->get($otherParticipation);

        /** @var Program $program */
        $program = $oneParticipation->enrollment->program;

        if ($this->hasEnrollmentNewMentorship($oneParticipation->enrollment)) {
            throw new ClientException("One participation has a new mentorship (not yet active).");
        }
        if ($this->hasEnrollmentNewMentorship($secondParticipation->enrollment)) {
            throw new ClientException("One participation has a new mentorship (not yet active).");
        }

        if ($oneParticipation->match_id != null || $secondParticipation->match_id != null) {
            throw new ClientException("One participation was already matched.");
        }

        if ($oneParticipation->enrollment->user->state != User::STATES['REGISTERED']) {
            throw new ClientException("One user is not fully registered (maybe he quit?)");
        }
        if ($secondParticipation->enrollment->user->state != User::STATES['REGISTERED']) {
            throw new ClientException("One user is not fully registered (maybe he quit?)");
        }

        if ($secondParticipation->enrollment->program->id != $oneParticipation->enrollment->program->id) {
            throw new ClientException("Looks like, they are in different programs! Something went wrong here!");
        }


        DB::beginTransaction();

        /** @var Match $match */
        $match = $this->matchService->create(
            [
                'program_id' => $program->id,
            ]
        );

        $oneParticipation->match_id = $match->id;
        $secondParticipation->match_id = $match->id;

        $oneParticipation->save();
        $secondParticipation->save();


        $match->transition('request');

        $this->matchingService->removeParticipationFromMatchingPool($oneParticipation);
        $this->matchingService->removeParticipationFromMatchingPool($secondParticipation);
        DB::commit();

        return response('ok', 200);
    }

    private function hasEnrollmentNewMentorship(Enrollment $enrollment)
    {

        $found = false;
        foreach ($enrollment->participations as $participation) {
            if ($participation->match && $participation->match->isNew()) {
                $found = true;
                break;
            }
        }

        return $found;

    }
}
