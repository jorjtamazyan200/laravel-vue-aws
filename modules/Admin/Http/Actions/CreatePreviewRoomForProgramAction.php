<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Server\ServerException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class CreatePreviewRoomForProgramAction extends Action
{
    protected $organizationService;
    protected $conferenceService;


    public function __construct(ProgramService $programService, ConferenceService $conferenceService)
    {
        $this->conferenceService = $conferenceService;
        $this->programService = $programService;
    }

    public function __invoke(int $id)
    {
        /** @var Program $program */
        $program = Program::findOrFail($id);

        $program->demo_room_id = $this->conferenceService->createRoom($program->conference_entry_url);

        if ($program->demo_room_id == '0') {
            throw new ServerException("Could not create conference room");
        }

        $program->save();

        return response('ok', 200);
    }
}
