<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;

/*
 * @todo add test
 */
class UserRemoveRoleAction extends Action
{
    /**
     * @var GroupService $groupService
     */
    protected $groupService;


    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    public function __invoke(int $id, int $role_id)
    {
        $user = User::findOrFail($id);
        // @todo: Important check if user is allowed to rmeove this role

        $user->roles()->detach($role_id);

        return response('ok', 200);
    }
}
