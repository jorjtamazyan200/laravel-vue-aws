<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\QueryParams;
use App\Infrastructure\Http\ResourceResponder;
use App\Notifications\User\UserPasswordAdminChangedNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Supervisor\Http\Resources\UserSupervisorHttpResource;

class SetDemoPasswordAction extends Action
{
    protected $userService;
    protected $responder;

    public function __construct(UserService $userService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {

        /** @var User $user */
        $user = $this->userService->get($id);

        if (Auth::user()->cannot('resetPassword', $user)) {
            throw new NotAuthorizedException();
        }
        if ($user->count_for_reporting) {
            throw new NotAuthorizedException("Only allowed for demo accounts. Please save first");
        }

        $this->userService->setNewPassword($user, "volunteer!");
        $user->save();

        return response()->json(['data' => []]);
    }
}
