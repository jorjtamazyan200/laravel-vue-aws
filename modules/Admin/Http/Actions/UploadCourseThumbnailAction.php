<?php

namespace Modules\Admin\Http\Actions;


class UploadCourseThumbnailAction extends UploadCourseImageAction
{
    protected $attachmentName = 'thumbnail';
}
