<?php

namespace Modules\Admin\Http\Actions;


class UploadLecturerAvatarAction extends UploadLecturerImageAction
{
    protected $attachmentName = 'avatar';
}
