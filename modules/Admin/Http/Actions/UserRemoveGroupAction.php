<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;

class UserRemoveGroupAction extends Action
{
    /**
     * @var GroupService $groupService
     */
    protected $groupService;


    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    public function __invoke(int $id, int $group_id)
    {
        $user = User::findOrFail($id);
        $group = Group::findOrFail($group_id);

        $this->groupService->detachUserToGroup($user, $group);

        return response('ok', 200);
    }
}
