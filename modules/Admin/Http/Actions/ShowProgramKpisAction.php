<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Reporting\Services\ReportingService;
use Illuminate\Http\Request;

class ShowProgramKpisAction extends Action
{
    protected $organizationService;
    protected $reportingService;
    protected $programService;
    protected $enrollmentService;
    protected $participationService;


    public function __construct(OrganizationService $organizationService, ReportingService $reportingService, MatchService $matchService, ProgramService $programService, EnrollmentService $enrollmentService, ParticipationService $participationService)
    {
        $this->reportingService = $reportingService;
        $this->organizationService = $organizationService;
        $this->programService = $programService;
        $this->enrollmentService = $enrollmentService;
        $this->participationService = $participationService;
        $this->matchService = $matchService;

    }

    public function __invoke($id, Request $request)
    {

        // @todo: optional filter by company?
        $program = $this->programService->get($id);

        $data = [
            'enrollmentStates' => $this->enrollmentService->getProgramReport($program)->get(),
            'matchStates' => $this->matchService->getProgramReport($program)->get(),
            'matchableParticipations' => $this->participationService->getProgramReport($program)->get(),

        ];
//        $data = [
//            'reachedPeople' => $this->reportingService->getReachedPeople($startDate, $endDate),
//            'registeredUsersByRole' => $this->reportingService->getUsersByPrimaryRole(),
//            'newRegistrationsByRole' => $this->reportingService->getUsersByPrimaryRole($startDate, $endDate),
//
//            'successfulSessionsCount' => $this->reportingService->getSuccessfulSessionsCount($startDate, $endDate),
//            'activatedLicences' => $this->reportingService->getLicencesActivated($startDate, $endDate),
//            'mentoringsWithOneSession' => $this->reportingService->getMentorshipsWithOneOrMoreSessions($startDate, $endDate),
//            'appointmentsPerMonth' => $this->reportingService->getSuccessfulAppoinmentsByMonth($startDate, $endDate)
//
//        ];

        return response()->json($data);
    }

}
