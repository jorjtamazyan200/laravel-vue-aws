<?php

namespace Modules\Admin\Http\Actions;


use App\Infrastructure\Http\ResourceResponder;
use Modules\Admin\Http\Resources\LecturerAdminHttpResource;
use Modules\Core\Http\Requests\FileUploadRequest;
use Modules\TrainingCenter\Domain\Services\LecturerService;

class UploadLecturerImageAction
{
    /**
     * @var LecturerService
     */
    protected $lecturerService;

    /**
     * @var ResourceResponder
     */
    protected $responder;

    /**
     * @var
     */
    protected $attachmentName;

    public function __construct(LecturerService $lecturerService, ResourceResponder $responder)
    {
        $this->lecturerService = $lecturerService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, FileUploadRequest $request)
    {
        $lecturer = $this->lecturerService->get($id);


        try {
            $lecturer->updateImageAttachment($this->attachmentName, $request->fileContent(), $request->fileType());
        } catch (\Exception $exception) {
            var_dump($exception);
            exit;
        }
        return $this->responder->send($lecturer, LecturerAdminHttpResource::class);
    }
}
