<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\QueryParams;
use App\Infrastructure\Http\ResourceResponder;
use App\Notifications\User\UserPasswordAdminChangedNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\GroupHttpResource;
use Modules\Supervisor\Http\Resources\UserSupervisorHttpResource;

class ApplyRegistrationCodeToUserAction extends Action
{
    protected $userService;
    protected $responder;
    protected $registrationCodeService;

    public function __construct(UserService $userService, RegistrationCodeService $registrationCodeService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->registrationCodeService = $registrationCodeService;

        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {

        /** @var User $user */
        $user = $this->userService->get($id);

        /** @var RegistrationCode $registrationCode */
        $registrationCode = $this->registrationCodeService->get($request->codeid);

        $this->registrationCodeService->applyRegistrationCode($user, $registrationCode, true);

        return response()->json(['data' => []]);
    }
}
