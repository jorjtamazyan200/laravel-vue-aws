<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Client\ValidationException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use http\Exception\InvalidArgumentException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class ProgramUpdateOrganizationAction extends Action
{
    /**
     * @var OrganizationService $organizationService
     */
    protected $organizationService;


    public function __construct(OrganizationService $programService)
    {
        $this->organizationService = $programService;
    }

    public function __invoke(int $id, int $program_id, Request $request)
    {
        $organization = Organization::findOrFail($id);
        $program = Program::findOrFail($program_id);
        $enrollmentType = $request->input('enrollmentType');

        $organization->programs()->sync([$program->id => ['enrollment_type'=> $enrollmentType]], false);

        return response('ok', 200);
    }
}
