<?php


namespace Modules\Admin\Http\Actions;


use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Admin\Http\Resources\CourseAdminHttpResource;
use Modules\TrainingCenter\Domain\Services\CourseService;

class DetachProgramFromCourseAction extends Action
{
    /**
     * @var CourseService
     */
    protected $courseService;

    /**
     * @var ResourceResponder
     */
    private $resourceResponder;

    /**
     * AssignLecturerToCourse constructor.
     * @param CourseService $courseService
     * @param ResourceResponder $resourceResponder
     */
    public function __construct(CourseService $courseService, ResourceResponder $resourceResponder)
    {
        $this->courseService = $courseService;
        $this->resourceResponder = $resourceResponder;
    }

    public function __invoke(int $id, int $lecturerId)
    {
        $course = $this->courseService->deleteProgramFromCourse($id, $lecturerId);
        $course->load('programs');

        return $this->resourceResponder->send($course, CourseAdminHttpResource::class);
    }
}
