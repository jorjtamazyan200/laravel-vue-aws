<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Admin\Http\Resources\CourseAdminHttpResource;
use Modules\Core\Http\Requests\FileUploadRequest;
use Modules\TrainingCenter\Domain\Services\CourseService;

class UploadCourseImageAction extends Action
{
    protected $courseService;
    protected $responder;
    protected $attachmentName;

    public function __construct(CourseService $courseService, ResourceResponder $responder)
    {
        $this->courseService = $courseService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, FileUploadRequest $request)
    {
        $course = $this->courseService->getCourseForAdmin($id);


        try {
            $course->updateImageAttachment($this->attachmentName, $request->fileContent(), $request->fileType());
        } catch (\Exception $exception) {
            var_dump($exception);
            exit;
        }
        return $this->responder->send($course, CourseAdminHttpResource::class);
    }
}
