<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Notifications\BrandedMailMessage;
use App\Notifications\BrandedNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use App\Services\StringHelper;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Modules\Admin\Http\Requests\MatchParticipationRequest;
use Modules\Admin\Http\Resources\NotificationHttpResource;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Illuminate\Contracts\View\Factory as ViewFactory;

/**
 * @todo: this endpoint has no test;
 *
 * Class MatchParticipationAction
 * @package Modules\Admin\Http\Actions
 */
class ShowLogOfBrandedDocumentAction extends Action
{
    /**
     * @var MatchService $matchService
     */
    protected $matchService;
    protected $viewFactory;

    protected $brandedDocumentService;
    protected  $responder;


    public function __construct(
        BrandedDocumentService $brandedDocumentService,
        ResourceResponder $resourceResponder
    )
    {
        $this->brandedDocumentService = $brandedDocumentService;
        $this->responder = $resourceResponder;
    }


    public function __invoke(int $id, Request $request)
    {

        /** @var BrandedDocument $document */
        $document = $this->brandedDocumentService->get($id);
        $logs = $this->brandedDocumentService->findLogOfBrandedDocument($document);

        return $this->responder->send($logs, NotificationHttpResource::class);
        // NotificationHttpResource


    }

}
