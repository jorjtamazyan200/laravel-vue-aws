<?php

namespace Modules\Admin\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Notifications\BrandedMailMessage;
use App\Notifications\BrandedNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use App\Services\StringHelper;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Modules\Admin\Http\Requests\MatchParticipationRequest;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Illuminate\Contracts\View\Factory as ViewFactory;

/**
 * @todo: this endpoint has no test;
 *
 * Class MatchParticipationAction
 * @package Modul   es\Admin\Http\Actions
 */
class PreviewBrandedDocumentAction extends Action
{
    /**
     * @var MatchService $matchService
     */
    protected $matchService;
    protected $viewFactory;

    protected $brandedDocumentService;


    public function __construct(
        MatchService $matchService,
        ViewFactory $view,
        BrandedDocumentService $brandedDocumentService
    )
    {
        $this->matchService = $matchService;
        $this->brandedDocumentService = $brandedDocumentService;
        $this->viewFactory = $view;
    }


    public function __invoke(int $id, Request $request)
    {

        /** @var BrandedDocument $document */
        $document = $this->brandedDocumentService->get($id);

        $defaults =
            [
                'plattformName' => Config::get('app.name'),
                'greetings' => Config::get('app.greetings'),
                'signature' => '[custom email signature]'
            ];

        if ($document->isNotification()) {
            $placeholder_data = $this->getPlaceHolderDataFromNotification($document->key);
        } else {
            $placeholder_data = $this->getPlaceHolderDataFor($document->key);
        }

        if (!$placeholder_data) {
            echo 'Did not find notificaiton placeholder data :( ';
            return response('Did not find notificaiton placeholder data :( ', 400);

//            $placeholder_data = $this->getPlaceHolderDataFor($document->key);
        }

        $placeholder_data = array_merge($placeholder_data, $defaults);

        $content = $document->parseContent($placeholder_data, true);

        if ($document->type === 'email') {
            return $this->renderEmail($content, $placeholder_data);
        }
        return response($content, 200);
    }

    private function renderEmail($content, $data)
    {

        $message = new BrandedMailMessage();
        $message->subject("demo");
        $lines = explode("\n", $content);
        foreach ($lines as $line) {
            $message->line($line);
        }
        if ($data['brand']) {
            $message->addData('brand', $data['brand']->toArray());
        }

        $markdown = new \Illuminate\Mail\Markdown(view(), config('mail.markdown'));


        return $markdown->render('vendor.notifications.email', $message->data());

    }

    private function getPlaceHolderDataFromNotification($notificationName)
    {


        // find class
        $className = StringHelper::dashesToCamelCase($notificationName);
        $notificationNamespaces = glob(app_path() . '/Notifications/*', GLOB_ONLYDIR);;
        $notificationNamespaces = array_map(function ($n) {
            return basename($n);
        }, $notificationNamespaces);

        $user = Auth::user();
        if (!$user) {
            $user = User::query()->where('email', 'mentor@volunteer-vision.com')->firstOrFail();
        }

        /** @var BrandedNotification $class */
        $class = null;

        foreach ($notificationNamespaces as $ns) {
            $fqn = 'App\\Notifications\\' . $ns . '\\' . $className;
            if (class_exists($fqn)) {
                $class = $fqn;
                break;
            }
        }
        if (!$class) {
            return null;
        }

        $data = $class::samplePlaceholderData($user);
        $data['salutation'] = $user->getSaluation();
        return $data;

        //

    }


    /**
     * @param $key
     * @return array
     *
     */
    private function getPlaceHolderDataFor($key)
    {
        /**
         * @var User $user
         * @var Enrollment $enrollment
         * @var Match $match
         * */
        $user = User::query()->where('email', '=', 'mentor@volunteer-vision.com')->firstOrFail();

        $enrollment = $user
            ->enrollments()
            ->where('state', Enrollment::STATES['ACTIVE'])
            ->first()
        ;

        $match = $enrollment->participations->first()->match;
        $appointment = $match->getCurrentAppointment();

        return [
            'user' => $user,
            'program' => Program::query()->inRandomOrder()->first(),
            'brand' => $user->brand,
            'enrollment' => $enrollment,
            'apponintment' => $appointment,
            'match' => $match,
            'webinar' => Webinar::whereNotNull('starts_at')->first(),
            'salutation' => $user->getSaluation()
        ];
    }

}
