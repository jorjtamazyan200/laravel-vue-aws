<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Modules\Admin\Http\Resources\ParticipationAdminHttpResource;
use Modules\Core\Http\Resources\ParticipationHttpResource;
use Modules\Matching\Domain\Services\MatchingLogic\Suggester;

/**
 * @todo: this endpoint has no test yet.
 *
 * Class ListMatchSuggestionsAdminAction
 * @package Modules\Admin\Http\Actions
 */
class ListMatchSuggestionsAdminAction extends Action
{
    protected $suggester;
    protected $responder;

    public function __construct(Suggester $suggester, ResourceResponder $responder)
    {
        $this->suggester = $suggester;
        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        $participations = $this->suggester->suggest($id);

        $participations->load('enrollment', 'enrollment.user');

        return $this->responder->send($participations, ParticipationAdminHttpResource::class);
    }
}
