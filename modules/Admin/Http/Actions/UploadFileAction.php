<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Requests\AdminFileUploadRequest;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Services\FileService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\FileHttpResource;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Core\Http\Requests\FileUploadRequest;

class UploadFileAction extends Action
{
    protected $fileService;
    protected $responder;

    public function __construct(FileService $fileService, ResourceResponder $responder)
    {
        $this->fileService = $fileService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, AdminFileUploadRequest $request)
    {
        /** @var File $file */
        $file = $this->fileService->get($id);

        $file->updateAttachment($request->fileName(), $request->fileContent(), $request->fileType(), $request->filesize());

        return $this->responder->send($file, FileHttpResource::class);
    }
}
