<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

/**
 * Class CancelMatchAction
 * Warning: this is the admin action;
 * there is also a user facting action having the same name!
 *
 * @package Modules\Admin\Http\Actions
 */
class CancelMatchAction extends Action
{
    /**
     * @var MatchService $matchService
     */
    protected $matchService;


    public function __construct(MatchService $matchService)
    {
        $this->matchService = $matchService;
    }

    /**
     * Unmatching a match...
     *  1. match state changes;
     *  2. create new enrollment for both parties.
     *
     */
    public function __invoke(int $id, Request $request)
    {


        /**
         * @var $match Match
         */
        $match = $this->matchService->get($id);
        $match->transition('cancel');
        $comment = $request->get('comment');

        $comment = 'Match cancelation comment of match id ' . $match->id . ':'
            . PHP_EOL . '----' . PHP_EOL .
            $comment;

        if (!empty($comment)) {
            $enrollments = $match->enrollments();

            $enrollments->each(function ($enrollment) use ($comment) {
                $user = $enrollment->user;
                $user->addComment(Auth::id(), $comment);
            });
        }


        return response('ok', 200);
    }
}
