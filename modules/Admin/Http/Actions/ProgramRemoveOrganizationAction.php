<?php

namespace Modules\Admin\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class ProgramRemoveOrganizationAction extends Action
{
    /**
     * @var OrganizationService $organizationService
     */
    protected $organizationService;


    public function __construct(OrganizationService $programService)
    {
        $this->organizationService = $programService;
    }

    public function __invoke(int $id, int $program_id)
    {
        $organization = Organization::findOrFail($id);
        $program = Program::findOrFail($program_id);

        $this->organizationService->detachProgram($organization, $program);

        return response('ok', 200);
    }
}
