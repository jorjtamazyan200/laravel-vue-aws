<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\User;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\FileHttpResource;

/**
 * Currenty not used;
 * @deprecated
 */
class FileAdminResource extends FileHttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $additional =   [
            'program' => new ProgramHttpResource($this->whenLoaded('program')),
            'distributeToTenants' =>  $this->distribute_to_tenants,
            'distributorSourceId' =>  $this->distributor_source_id
        ];

        return array_merge(
            parent::toArray($request),
            $additional
        );

    }
}
