<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class CoordinatorTodoMinimalHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'meta' => $this->meta,
            'type' => $this->type,
            'state' => $this->state,
            'duedate' => $this->duedate ? $this->duedate->toIso8601String() : $this->duedate,
            'description' => $this->description,
            'assignee_id' => $this->assignee_id,
            'lastStateChangeAt' => $this->duedate ? $this->duedate->toIso8601String() : $this->duedate,
            'customer' => new UserMinimalHttpResource($this->whenLoaded('customer')),
            'assignee' => new UserMinimalHttpResource($this->whenLoaded('assignee')),
        ];
    }
}
