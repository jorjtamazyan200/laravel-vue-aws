<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class ParticipationAdminHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'startMatchingAfter' => ($this->start_matching_after ? $this->start_matching_after->toIso8601String() : null),
            'matchConfirmedAt' => ($this->match_confirmed_at ? $this->match_confirmed_at->toIso8601String() : null),
            'match_id' => $this->match_id,
            'matchingScore' => $this->matching_score,
            'availibilityOverlap' => $this->availibility_overlap,
            'minutesAvailable' => $this->when(!is_null($this->minutes_available), $this->minutes_available),
            'timezoneDifference' => $this->when(!is_null($this->timezone_difference), $this->timezone_difference),
            'enrollment' => new EnrollmentMinimalHttpResource($this->whenLoaded('enrollment')),
            'match' => new MatchAdminListHttpResource($this->whenLoaded('match')),
        ];
    }
}
