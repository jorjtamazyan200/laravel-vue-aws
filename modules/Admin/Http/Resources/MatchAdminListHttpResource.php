<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Matching\Http\Resources\MatchedParticipationHttpResource;

class MatchAdminListHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'lectionsCompleted' => $this->lections_completed,
            'state' => $this->state,
            'licenceActivated' => $this->licence_activated ? $this->licence_activated->toIso8601String() : $this->licence_activated,
            'licenceReason' => $this->licence_reason,
            'licenceFree' => $this->licence_free,
            'program' => new ProgramHttpResource($this->whenLoaded('program')),
            'matchedParticipations' => MatchedParticipationHttpResource::collection($this->whenLoaded('participations')),
            'appointments' => AppointmentHttpResource::collection($this->whenLoaded('appointments')),
        ];
    }
}
