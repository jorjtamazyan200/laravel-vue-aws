<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class SmsLogResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'from' => $this->from,
            'to' => $this->to,
            'channel' => $this->channel,
            'direction' => $this->direction,
            'body' => $this->body,
            'user' => new UserMinimalHttpResource($this->whenLoaded('user')),
            'updatedAt' => $this->updated_at ? $this->updated_at->toIso8601String() : null,
            'reviewedAt' => $this->reviewed_at ? $this->reviewed_at->toIso8601String() : null,
            'sentAt' => $this->sent_at ? $this->sent_at->toIso8601String() : null,
            'createdAt' => $this->created_at ? $this->created_at->toIso8601String() : null,
            'whatsappStatus' => $this->whatsapp_status
        ];
    }
}
