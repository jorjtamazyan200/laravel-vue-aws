<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class RegistrationCodeHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'externalComment' => $this->external_comment,
            'internalComment' => $this->internal_comment,

            'primaryRole' => $this->primary_role,

            'usersCount' => isset($this->users_count) ? $this->users_count : null,

            'usersCountLimit' => $this->users_count_limit,
            'usersCountPlanned' => $this->users_count_planned,
            'createdAt' => $this->created_at->toIso8601String(),
            'hasAvailability' => false,
            'updatedAt' => $this->updated_at->toIso8601String(),
            'deletedAt' => ($this->deleted_at ? $this->deleted_at->toIso8601String() : null),

            'validUntil' => ($this->valid_until ? $this->valid_until->toIso8601String() : null),
            'isValid' => $this->is_valid,
            'groupId' => $this->group_id,
            'brandId' => $this->brand_id,
            'autoEnrollIn' => $this->auto_enroll_in,
            'program' => new ProgramHttpResource($this->whenLoaded('auto_enroll_in')),
            // @todo: maynbe registration codes;
            'users' => UserMinimalHttpResource::collection($this->whenLoaded('users')),
//            'group' => new GroupResource($this->whenLoaded('group')),
            'group' => new GroupHttpResource($this->whenLoaded('group')),
        ];
    }
}
