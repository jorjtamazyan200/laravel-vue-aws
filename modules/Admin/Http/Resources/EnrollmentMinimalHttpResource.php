<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\ParticipationHttpResource;

class EnrollmentMinimalHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'role' => $this->role,
            'state' => $this->state,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,

            'program' =>  new ProgramHttpResource($this->whenLoaded('program')),
            'participations' =>  ParticipationAdminHttpResource::collection($this->whenLoaded('participations')),
            'user' =>   new UserMinimalHttpResource($this->whenLoaded('user')),
        ];
    }
}
