<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class UserLaunchHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'timezone' => $this->timezone,
            'offset' => $this->offset,
            'clientType' => $this->client_type,
            'clientBrowser' => $this->client_browser,
            'clientOS' => $this->client_os,
            'clientEngine' => $this->client_engine,
            'useragent' => $this->useragent,

            'createdAt' => $this->created_at->toIso8601String(),
        ];
    }
}
