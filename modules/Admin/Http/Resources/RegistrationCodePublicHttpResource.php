<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class RegistrationCodePublicHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'primaryRole' => $this->primary_role,
            'usersCountLimit' => $this->users_count_limit,
            'usersCountPlanned' => $this->users_count_planned,
        ];
    }
}
