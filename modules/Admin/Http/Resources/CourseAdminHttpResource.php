<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\TrainingCenter\Http\Resources\CourseQuestionHttpResource;
use Modules\TrainingCenter\Http\Resources\NoteHttpResource;
use Modules\TrainingCenter\Http\Resources\SectionHttpResource;

/**
 * Class PostCommentsListHttpResource
 *
 * @package Modules\Community\Http\Resources
 */
class CourseAdminHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'thumbnail' => $this->thumbnail,
            'views_count' => $this->views_count,
            'likes_count' => $this->whenLoaded('likes', $this->likes()->count()),
            'lecturers' => $this->whenLoaded('lecturers', LecturerAdminHttpResource::collection($this->lecturers)),
            'dislikes_count' => $this->whenLoaded('likes', $this->dislikes()->count()),
            'notes' => NoteHttpResource::collection($this->whenLoaded('notes')),
            'sections' => SectionHttpResource::collection($this->whenLoaded('sections')),
            'questions' => CourseQuestionHttpResource::collection($this->whenLoaded('questions')),
            'status' => $this->whenLoaded('sections', function () {
                return $this->status;
            }),
            'seenVideosCount' => $this->whenLoaded('sections', function () {
                return $this->seenVideosCount;
            }),
            'completedVideosCount' => $this->whenLoaded('sections', function () {
                return $this->completedVideosCount;
            }),
            'totalVideosCount' => $this->whenLoaded('sections', function () {
                return $this->totalVideosCount;
            }),
            'totalDuration' => $this->whenLoaded('sections', function () {
                return $this->totalDuration;
            }),
            'seenDuration' => $this->whenLoaded('sections', function () {
                return $this->seenDuration;
            }),
            'isLiked' => $this->whenLoaded('sections', function () {
                return $this->isLiked;
            }),
            'isDisliked' => $this->whenLoaded('sections', function () {
                return $this->isDisliked;
            }),

            'createdAt' => !is_null($this->created_at) ? $this->created_at->toIso8601String() : null,
            'updatedAt' => !is_null($this->updated_at) ? $this->updated_at->toIso8601String() : null,
        ];
    }
}
