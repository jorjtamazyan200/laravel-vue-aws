<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Carbon\Carbon;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class WebinarAdminHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        //@todo: no idea why we need carbon here?
        $startsAt = $this->starts_at ? (new Carbon($this->starts_at))->toIso8601String() : null;
        return [
            'id' => $this->id,
            'referentId' => $this->referent_id,
            'organizationId' => $this->organization_id,
            'title' => $this->title,
            'description' => $this->description,
            'startsAt' =>  $startsAt,
            'durationMinutes' => $this->duration_minutes,
            'state' => $this->state,

            'enrollmentsCount' => isset($this->enrollments_count) ? $this->enrollments_count : null,
            'seats' => $this->seats,
            'loginLink' => $this->login_link,
            'phoneDialIn' => $this->phoneDialIn,
            'invitiationNote' => $this->invitation_note,
            'targetAudience' => $this->target_audience,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,

            'program' => new ProgramHttpResource($this->whenLoaded('program')),
            'referent' => new UserMinimalHttpResource($this->whenLoaded('referent')),
            'programId' => $this->when(!$this->pivot, function () {
                return $this->program_id;
            }),
            'enrollments' => WebinarEnrollmentsHttpResource::collection($this->whenLoaded('enrollments')),
            'participants' => $this->whenPivotLoaded('enrollment_webinar', function () {
                return true;
            }),
            'attended' => $this->whenPivotLoaded('enrollment_webinar', function () {
                return $this->pivot->attended;
            }),
            'updatedAt' => $this->updated_at->toIso8601String(),
            'createdAt' => $this->created_at->toIso8601String()

        ];
    }
}
