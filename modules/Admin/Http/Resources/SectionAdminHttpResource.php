<?php

namespace Modules\Admin\Http\Resources;


use App\Infrastructure\Http\HttpResource;

class SectionAdminHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'courses' => CourseAdminHttpResource::collection($this->whenLoaded('courses')),
            'videos' => VideoAdminHttpResource::collection($this->whenLoaded('videos')),

            'createdAt' => !is_null($this->created_at) ? $this->created_at->toIso8601String() : null,
            'updatedAt' => !is_null($this->updated_at) ? $this->updated_at->toIso8601String() : null,
        ];
    }
}
