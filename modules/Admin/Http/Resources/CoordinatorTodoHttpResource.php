<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Resources\StateLogHttpResource;

class CoordinatorTodoHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'meta' => $this->meta,
            'type' => $this->type,
            'state' => $this->state,
            'duedate' => $this->duedate ? $this->duedate->toIso8601String() : $this->duedate,
            'description' => $this->description,
            'assigneeId' => $this->assignee_id,
            'lastStateChangeAt' => $this->last_state_change_at ? $this->last_state_change_at->toIso8601String() : $this->last_state_change_at,
            'customer' => new UserAdminHttpResource($this->whenLoaded('customer')),
            'assignee' => new UserMinimalHttpResource($this->whenLoaded('assignee')),
            'creator' => new UserMinimalHttpResource($this->whenLoaded('creator')),
        ];
    }
}
