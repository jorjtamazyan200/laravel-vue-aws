<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class GroupMinimalHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => ($this->name ? $this->name : 'GroupName Placeholder'),
            'createdAt' => $this->created_at->toIso8601String(),
            'updatedAt' => $this->updated_at->toIso8601String(),
            // @todo: maybe registration codes;
            'users' => UserMinimalHttpResource::collection($this->whenLoaded('users')),
            'organization' => new OrganizationHttpResource($this->whenLoaded('organization')),
            'organizationId' => $this->organization_id,
            'profileFields' => new ProfileFieldResourceCollection($this->whenLoaded('profileFields')),
            'role' => (isset($this->pivot) && isset($this->pivot->role) ? $this->pivot->role : null)
        ];
    }
}
