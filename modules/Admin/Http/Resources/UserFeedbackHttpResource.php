<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class UserFeedbackHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'questionCode' => $this->question_code,
            'responseScalar' => $this->response_scalar,
            'responseText' => $this->response_text,
            'createdAt' => $this->created_at->toIso8601String(),
            'user' => new UserMinimalHttpResource($this->whenLoaded('user')),
            'processingState' => $this->processing_state,
            'isHighlighted' => $this->is_highlighted
//            'organization' => new OrganizationResource($this->whenLoaded('organization')),
//            'profileFields' => new ProfileFieldResourceCollection($this->whenLoaded('profileFields')),
        ];
    }
}
