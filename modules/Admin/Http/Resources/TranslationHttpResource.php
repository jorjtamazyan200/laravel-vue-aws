<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class TranslationHttpResource extends HttpResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'locale' => $this->locale,
            'scope' => $this->scope,
            'json_value' => json_decode($this->json_value)
        ];
    }
}
