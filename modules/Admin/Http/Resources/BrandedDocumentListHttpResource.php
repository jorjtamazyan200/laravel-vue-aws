<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class BrandedDocumentListHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'key' => $this->key,
            'type' => $this->type,
            'subject' => $this->subject,
            'internalTitle' => $this->internal_title,
            'language' => $this->language,
            'brand_id' => $this->brand_id,
            'audience' => $this->audience,
            'reviewedAt' => $this->reviewed_at ? $this->reviewed_at->toIso8601String() : $this->reviewed_at,
            'contentAvailable' => strlen($this->content) >= 100,
            'program' => new ProgramHttpResource($this->whenLoaded('program'))
        ];
    }
}
