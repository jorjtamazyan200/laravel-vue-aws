<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class AdminFileHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'target' => $this->target,
            'source' => $this->source,
            'cover' => $this->cover,
            'title' => $this->title,
            'target_audience' => $this->target_audience
        ];
    }
}
