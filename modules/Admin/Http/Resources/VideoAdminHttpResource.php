<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;

/**
 * Class PostCommentsListHttpResource
 *
 * @package Modules\Community\Http\Resources
 */
class VideoAdminHttpResource extends HttpResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'duration' => $this->duration,
            'order' => $this->order,
            'video_id' => $this->video_id,
            'video_type' => $this->video_type,
            'isCompleted' => $this->whenLoaded('seenVideos', function () {
                $seenVideo = $this->seenVideos->where('user_id', auth()->id())->first();

                return !is_null($seenVideo) ? $seenVideo->duration >= $this->duration ?? true : false;
            }),
            'section' => new SectionAdminHttpResource($this->whenLoaded('section')),
//            'transcript' => new TranscriptHttpResource($this->whenLoaded('transcript')),
//            'seenVideo' => new SeenVideoHttpResource($this->whenLoaded('seenVideos', function () {
//                return $this->seenVideos->where('user_id', auth()->id())->first();
//            })),
            'createdAt' => !is_null($this->created_at) ? $this->created_at->toIso8601String() : null,
            'updatedAt' => !is_null($this->updated_at) ? $this->updated_at->toIso8601String() : null,
        ];
    }
}
