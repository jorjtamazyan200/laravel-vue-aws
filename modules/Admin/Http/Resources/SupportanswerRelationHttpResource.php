<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\Activity;

class SupportanswerRelationHttpResource extends HttpResource
{
    /**
     * Unset the default "data" wrapper, so that we can return
     * a value without casting it to an array.
     *
     * @var string
     */
    public static $wrap = '';

    /**
     * Transform the resource.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'answer' => $this->answer,
            'linksTo' => $this->links_to,
            'class' => $this->class
        ];
    }
}
