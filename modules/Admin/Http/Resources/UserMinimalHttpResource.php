<?php

namespace Modules\Admin\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class UserMinimalHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'displayName' => $this->first_name . ' ' . $this->last_name,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'email' => $this->email,
            'servicelevel' => $this->servicelevel,
            'state' => $this->state,
            'colorCode' => $this->color_code,
            'primaryRole' => $this->primary_role,
            'avatar' => $this->avatar,
            'lastInteraction' => $this->last_interaction,
            'createdAt' => $this->created_at->toIso8601String(),
            'countForReporting' => $this->count_for_reporting,
            'generalComment' => $this->general_comment,

            'brand' => new BrandHttpResource($this->whenLoaded('brand')),
            'enrollments' => EnrollmentMinimalHttpResource::collection($this->whenLoaded('enrollments')),
            'organization' => new OrganizationListHttpResource($this->whenLoaded('organization')),
            'profileFields' => new ProfileFieldResourceCollection($this->whenLoaded('profileFields')),
            'roles' => ($this->whenLoaded('roles')),
            'role' => ($this->pivot && $this->pivot->role ? $this->pivot->role : null)

        ];
    }
}
