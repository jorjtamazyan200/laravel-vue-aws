<?php

namespace Modules\Admin\Http\Controllers;

use ArrayAccess;
use App\Infrastructure\Contracts\BreadService;
use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\ParticipationRequest;
use Modules\Admin\Http\Resources\ParticipationAdminHttpResource;
use Modules\Core\Domain\Models\Participation;
use Modules\Matching\Domain\Services\MatchingService;

class ParticipationController extends BreadServiceController
{

    protected $matchingService;
    public function __construct(BreadService $service, MatchingService $matchingService)
    {
        parent::__construct($service);
        $this->matchingService = $matchingService;
    }


    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return ParticipationAdminHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return ParticipationRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return ParticipationRequest::class;
    }

    /**
     * Handle a DELETE request to the /{id} endpoint.
     *
     * @param  mixed $id Unique ID of the record to delete
     * @return \ArrayAccess
     */
    public function destroy($id): ArrayAccess
    {

        /** @var Participation $participation */
        $participation = $this->service->get($id);
        $this->matchingService->removeParticipationFromMatchingPool($participation);

        $response =  parent::destroy($id);


        return $response;
    }



}
