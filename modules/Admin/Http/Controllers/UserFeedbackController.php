<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\BrandedDocumentRequest;
use Modules\Admin\Http\Requests\UserFeedbackRequest;
use Modules\Admin\Http\Resources\BrandedDocumentHttpResource;
use Modules\Admin\Http\Resources\UserFeedbackHttpResource;

class UserFeedbackController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return UserFeedbackHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return UserFeedbackRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return UserFeedbackRequest::class;
    }
}
