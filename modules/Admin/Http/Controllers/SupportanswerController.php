<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use App\Infrastructure\Http\Query;
use Illuminate\Http\Request;
use Modules\Admin\Domain\Models\Supportanswer;
use Modules\Admin\Domain\Models\SupportanswerRelation;
use Modules\Admin\Http\Requests\SupportanswerRequest;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Http\Resources\SupportanswerHttpResource;
use Modules\Admin\Http\Resources\UserAdminHttpResource;
use Modules\Admin\Http\Resources\UserMinimalHttpResource;
use Modules\Admin\Http\Resources\UserResource;
use Modules\Core\Http\Requests\FileUploadRequest;

class SupportanswerController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return SupportanswerHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return SupportanswerRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return SupportanswerRequest::class;
    }


    /**
     * Handle a GET request to the /{id} endpoint.
     *
     * @param  mixed $id Unique ID of the record to retrieve
     * @return \ArrayAccess
     */
    public function show($id, Request $request): \ArrayAccess
    {
        $query = new Query($request);

        /** @var Supportanswer $answer */
        $answer = $this->service->get($id, $query);
        $answer->views = ($answer->views || 0)+1;
        $answer->save();

        return $this->makeResource($answer);
    }

    public function linkSupportAnswer(Request $request) {
        $relation = new SupportanswerRelation();
        $relation->fill($request->all());
        $relation->supportanswer_id = $request->id;
        $relation->save();

        return response('', 200);
    }
    public function unlinkSupportAnswer(Request $request) {

        /** @var SupportanswerRelation $relation */
        $relation = SupportanswerRelation::find($request->linkid);
        $relation->delete();
        return response('', 200);

    }


}