<?php


namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\UserleadRequest;
use Modules\Admin\Http\Requests\WebinarRequest;

use Modules\Admin\Http\Resources\UserleadResource;
use Modules\Admin\Http\Resources\WebinarAdminHttpResource;
use Modules\Core\Http\Resources\WebinarHttpResource;

class UserleadController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return UserleadResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return UserleadRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return UserleadRequest::class;
    }
}
