<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\LecturerRequest;
use Modules\Admin\Http\Requests\VideoRequest;
use Modules\Admin\Http\Resources\LecturerAdminHttpResource;

class LecturerController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return LecturerAdminHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return LecturerRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return LecturerRequest::class;
    }
}
