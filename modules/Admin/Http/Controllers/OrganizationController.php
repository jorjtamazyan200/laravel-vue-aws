<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\OrganizationRequest;
use Modules\Admin\Http\Resources\OrganizationListHttpResource;
use Modules\Admin\Http\Resources\OrganizationHttpResource;

class OrganizationController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        if ($view === 'list') {
            return OrganizationListHttpResource::class;
        }

        return OrganizationHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return OrganizationRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return OrganizationRequest::class;
    }
}
