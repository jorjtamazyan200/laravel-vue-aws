<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use App\Infrastructure\Http\Query;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Domain\Models\Supportanswer;
use Modules\Admin\Domain\Models\SupportanswerRelation;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Admin\Http\Requests\CoordinatorTodoRequest;
use Modules\Admin\Http\Requests\SupportanswerRequest;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Http\Resources\CoordinatorTodoMinimalHttpResource;
use Modules\Admin\Http\Resources\CoordinatorTodoHttpResource;
use Modules\Admin\Http\Resources\SupportanswerHttpResource;
use Modules\Admin\Http\Resources\UserAdminHttpResource;
use Modules\Admin\Http\Resources\UserMinimalHttpResource;
use Modules\Admin\Http\Resources\UserResource;
use Modules\Core\Http\Requests\FileUploadRequest;
use ArrayAccess;

class CoordinatorTodoController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {

        if ($view === 'list') {
            return CoordinatorTodoMinimalHttpResource::class;
        }
        return CoordinatorTodoHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return CoordinatorTodoRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return CoordinatorTodoRequest::class;
    }


    /**
     * Handle a POST request to the / endpoint.
     *
     * @return \ArrayAccess
     */
    public function store(): ArrayAccess
    {
        $userid = Auth::id();
        if ($userid) {
            request()->merge(['creator_id' => $userid]);
        }

        return parent::store();

    }


}