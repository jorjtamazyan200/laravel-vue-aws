<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\ProgramRequest;
use Modules\Admin\Http\Resources\ProgramHttpResource;

class ProgramController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return ProgramHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return ProgramRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return ProgramRequest::class;
    }
}
