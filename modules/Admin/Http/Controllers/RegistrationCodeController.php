<?php


namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\GroupRequest;
use Modules\Admin\Http\Requests\RegistrationCodeRequest;
use Modules\Admin\Http\Resources\GroupHttpResource;
use Modules\Admin\Http\Resources\RegistrationCodeHttpResource;

class RegistrationCodeController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return RegistrationCodeHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return RegistrationCodeRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return RegistrationCodeRequest::class;
    }
}
