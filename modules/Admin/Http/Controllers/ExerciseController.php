<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\ExerciseRequest;
use Modules\TrainingCenter\Http\Resources\ExerciseHttpResource;

class ExerciseController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return ExerciseHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return ExerciseRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return ExerciseRequest::class;
    }
}
