<?php


namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\WebinarRequest;

use Modules\Admin\Http\Resources\WebinarAdminHttpResource;
use Modules\Core\Http\Resources\WebinarHttpResource;

class WebinarController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return WebinarAdminHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return WebinarRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return WebinarRequest::class;
    }
}
