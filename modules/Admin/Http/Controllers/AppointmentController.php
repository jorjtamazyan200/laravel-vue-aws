<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\AppointmentRequest;
use Modules\Admin\Http\Resources\AppointmentListHttpResource;
use Modules\Admin\Http\Resources\AppointmentHttpResource;

class AppointmentController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        if ($view =='list') {
            return AppointmentListHttpResource::class;
        }
        return AppointmentHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass() : string
    {
        return AppointmentRequest::class;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass() : string
    {
        return AppointmentRequest::class;
    }
}
