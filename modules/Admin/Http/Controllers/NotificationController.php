<?php

namespace Modules\Admin\Http\Controllers;

use App\Infrastructure\Http\BreadServiceController;
use Modules\Admin\Http\Requests\ProgramRequest;
use Modules\Admin\Http\Resources\NotificationHttpResource;
use Modules\Admin\Http\Resources\ProgramHttpResource;

class NotificationController extends BreadServiceController
{
    /** {@inheritdoc} */
    protected function getResourceClass(string $view): string
    {
        return NotificationHttpResource::class;
    }

    /** {@inheritdoc} */
    protected function getStoreRequestClass(): string
    {
        return null;
    }

    /** {@inheritdoc} */
    protected function getUpdateRequestClass(): string
    {
        return null;
    }
}
