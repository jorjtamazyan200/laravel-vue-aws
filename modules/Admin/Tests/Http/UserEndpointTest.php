<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Tenant;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v2/admin/users` endpoints.
 */
class UserEndpointTest extends EndpointTest
{
    public function test_it_lists_all_users()
    {
        // List all Users via API
        $response = $this->actingAs($this->admin)
            ->get('/api/v2/admin/users');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        // According to UsersTableSeeder we have 25 Users
        $this->assertCount(25, $response->json()['data']);

        // Don't check every single record, just the structure of each
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'firstName',
                    'lastName',
                ]
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
        ]);
    }

    public function test_it_shows_a_single_user()
    {

        // Retrieve a single User via API
        $response = $this->actingAs($this->admin)
            ->getJson('/api/v2/admin/users/1')
            ;
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        // Assert response payload
        $response->assertJson([
            'data' => [
                'id' => $this->user->id,
                'firstName' => 'Default',
                'lastName' => 'User',
            ]
        ]);
    }

    public function test_it_creates_a_user()
    {
        $tenant = Tenant::first();

        // Create a new User via API
        $response = $this->actingAs($this->admin)
            ->postJson('/api/v2/admin/users', [
                'firstName' => 'I am',
                'lastName' => 'a test user',
                'email' => 'tester@example.com',
                'password' => 'secret',
                'tenant_id' => $tenant->id,
                'brand_id' => 1
            ])
            ;
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        // Extract ID from response
        $id = $response->json()['data']['id'];

        // Test the response payload
        $response->assertJson([
            'data' => [
                'id' => $id,
                'firstName' => 'I am',
                'lastName' => 'a test user',
            ]
        ]);

        // Ensure the entity has been created in DB
        $this->assertDatabaseHas('users', [
            'id' => $id,
            'first_name' => 'I am',
            'last_name' => 'a test user',
            'email' => 'tester@example.com',
        ]);
    }

    public function test_it_updates_a_user()
    {
        $user_id = $this->user->id;

        // Pre-assertion: old name & email in DB
        $this->assertDatabaseHas('users', [
            'id' => $user_id,
            'email' => 'user@volunteer-vision.com'
        ]);

        // Update a user via API
        $response = $this->actingAs($this->admin)
            ->putJson("/api/v2/admin/users/$user_id", [
                'firstName' => 'Please call me Jack from now on…',
                'lastName' => 'Please call me Jack from now on…',
                'email' => 'jack@example.com',
            ])
            ->assertStatus(200);

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());
        // Test the response payload
        $response->assertJson([
            'data' => [
                'id' => $user_id,
                'firstName' => 'Please call me Jack from now on…',
                'lastName' => 'Please call me Jack from now on…',
                'email' => 'jack@example.com',
            ]
        ]);

        // Check updated record in DB
        $this->assertDatabaseHas('users', [
            'id' => $user_id,
            'email' => 'jack@example.com',
        ]);
    }
    public function test_it_updates_a_user_with_id_in_values()
    {
        $user_id = $this->user->id;

        // Pre-assertion: old name & email in DB
        $this->assertDatabaseHas('users', [
            'id' => $user_id,
            'email' => 'user@volunteer-vision.com'
        ]);

        // Update a user via API
        $response = $this->actingAs($this->admin)
            ->putJson("/api/v2/admin/users/$user_id", [
                'id' => $user_id,
                'firstName' => 'Please call me Jack from now on…',
                'lastName' => 'Please call me Jack from now on…',
                'email' => 'jack@example.com',
            ]);

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());
        // Test the response payload
    }

    public function test_it_deletes_a_user()
    {

        $user_id = $this->user->id;

        // Pre-assertion old user in db
        $this->assertDatabaseHas('users', [
            'id' => $user_id,
            'first_name' => $this->user->first_name,
        ]);

        // Delete user via API
        $response = $this->actingAs($this->admin)
            ->delete("/api/v2/admin/users/$user_id");

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());
        // Test the response payload
        $response->assertJson([
            'data' => [
                'id' => $user_id
            ]
        ]);

        // Ensure the record is missing from DB
//        $this->assertSoftDeleted('users', ['id' => $user_id]);
    }
}
