<?php

namespace modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;

class ManageGroupsAndUserTest extends EndpointTest
{
    public function test_addUserToGroupTest()
    {
        $user = User::inRandomOrder()->get()->first();
        $group = Group::inRandomOrder()->get()->first();
        $role = User::ROLES['supervisor'];

        $url = '/api/v2/admin/users/' . $user->id . '/groups/' . $group->id;

        // Retrieve a single Translation via API
        $response = $this->actingAs($this->admin)
            ->post($url, ['role' => $role])
            ->assertStatus(200);

        $this->assertDatabaseHas('group_user', [
            'group_id' => $group->id,
            'user_id' => $user->id,
            'role' => $role
        ]);
    }

    public function test_removeUserFromGroup()
    {
        $user = User::inRandomOrder()->get()->first();
        $group = Group::inRandomOrder()->get()->first();

        DB::delete('DELETE FROM group_user WHERE group_id = ? AND user_id = ? ', [$group->id, $user->id]);
//
        $user->groups()->attach($group, ['role' => 'member']);

        $url = '/api/v2/admin/users/' . $user->id . '/groups/' . $group->id;

        // Retrieve a single Translation via API
        $response = $this->actingAs($this->admin)
            ->delete($url)
            ->assertStatus(200);

        $this->assertDatabaseMissing('group_user', [
            'group_id' => $group->id,
            'user_id' => $user->id
        ]);
    }
}
