<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;

class ApplyRegistrationCodeToUserTest extends EndpointTest
{
    public function test_it_receives_an_access_token_for_another_user()
    {
        $subject = factory(User::class)->create();

        $registrationCode = RegistrationCode::query()->firstOrFail();
        $route = route('admin.user.applyRegistrationCode', ['id' => $subject, 'codeid' => $registrationCode->id]);


        $response = $this->actingAs($this->admin)
            ->json('POST', $route)
            ->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'id' => $subject->id,
            'registration_code_id' => $registrationCode->id,
//            'body' => $message,
        ]);
    }

}
