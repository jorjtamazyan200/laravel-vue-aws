<?php

namespace modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

class TranslationEndpointTest extends EndpointTest
{
    public function test_it_shows_a_single_translation()
    {
        // Retrieve a single Translation via API
        $response = $this->actingAs($this->admin)
            ->get('/api/v2/admin/translations/1')
            ->assertStatus(200);

        // Assert response payload
        $response->assertJson([
            'data' => []
        ]);
    }
}
