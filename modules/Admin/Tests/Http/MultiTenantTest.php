<?php

namespace modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use App\Services\MultiTenant\Facades\MultiTenant;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\Role;

class MultiTenantTest extends EndpointTest
{
    public function test_it_sets_the_tenant_id_from_the_authenticated_user()
    {
        $this->markTestSkipped();

        $this->assertNotNull($this->admin->tenant_id);
        $this->actingAs($this->admin)
            ->json('GET', '/api/v2/admin/users')
            ->assertStatus(200);

        $this->assertEquals($this->admin->tenant_id, MultiTenant::getTenantId());
    }

    public function test_it_lets_super_admins_override_the_tenant_id_via_header()
    {
        $this->markTestSkipped();
        $overrideTenantId = -1;
        $headers = [
            'X-Tenant-Id' => $overrideTenantId,
        ];

        $superAdminRole = Role::whereName(Role::ROLES['superadmin'])->first();
        $this->admin->roles()->attach($superAdminRole);

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/v2/admin/users', [], $headers);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $this->assertEquals($overrideTenantId, MultiTenant::getTenantId());
    }

    public function test_it_lets_non_super_admins_not_override_the_tenant_id_via_header()
    {
        $this->markTestSkipped();
        $this->assertNotNull($this->admin->tenant_id);
        $overrideTenantId = -1;
        $headers = [
            'X-Tenant-Id' => $overrideTenantId,
        ];

        $this->actingAs($this->admin)
            ->json('GET', '/api/v2/admin/users', $headers)
            ->assertStatus(200);

        $this->assertEquals($this->admin->tenant_id, MultiTenant::getTenantId());
        $this->assertNotEquals($overrideTenantId, MultiTenant::getTenantId());
    }


    public function test_it_uses_my_tenant_if_i_create_an_registration_link()
    {
        $this->markTestSkipped();

//        Response::macro('caps', function ($value) {
//            return Response::make(strtoupper($value));
//        });
//
        $registrationCodeData = factory(RegistrationCode::class)->make()->toArray();
        $this->assertNotNull($this->admin->tenant_id);


        $response = $this->actingAs($this->admin)
            ->json('POST', '/api/v2/admin/registrationcodes', $registrationCodeData);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


//        $this->assertEquals($this->admin->tenant_id, MultiTenant::getTenantId());
    }


}
