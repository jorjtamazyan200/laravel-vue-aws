<?php

namespace Modules\Admin\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

class QuitUserTest extends EndpointTest
{
    public function test_it_receives_an_access_token_for_another_user()
    {
        $subject = factory(User::class)->create();

        $response = $this->actingAs($this->admin)
            ->json('POST', "/api/v2/admin/users/$subject->id/quit")
            ->assertStatus(200)
            ;
    }

}
