<?php

namespace Modules\Admin;

use App\Infrastructure\Contracts\BreadService;
use App\Infrastructure\Providers\ModuleServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Models\OrganizationInteraction;
use Modules\Admin\Domain\Models\Supportanswer;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Admin\Domain\Services\OrganizationInteractionService;
use Modules\Admin\Domain\Services\SupportanswerService;
use Modules\Admin\Http\Controllers\AppointmentController;
use Modules\Admin\Http\Controllers\BrandController;
use Modules\Admin\Http\Controllers\BrandedDocumentController;

use Modules\Admin\Http\Controllers\CoordinatorTodoController;
use Modules\Admin\Http\Controllers\CourseController;
use Modules\Admin\Http\Controllers\EnrollmentController;
use Modules\Admin\Http\Controllers\ExerciseController;
use Modules\Admin\Http\Controllers\FileController;
use Modules\Admin\Http\Controllers\GroupController;
use Modules\Admin\Http\Controllers\LecturerController;
use Modules\Admin\Http\Controllers\MatchController;
use Modules\Admin\Http\Controllers\NotificationController;
use Modules\Admin\Http\Controllers\OrganizationController;
use Modules\Admin\Http\Controllers\OrganizationInteractionController;
use Modules\Admin\Http\Controllers\ParticipationController;
use Modules\Admin\Http\Controllers\ProgramController;
use Modules\Admin\Http\Controllers\QuizAnswerController;
use Modules\Admin\Http\Controllers\QuizQuestionController;
use Modules\Admin\Http\Controllers\RegistrationCodeController;
use Modules\Admin\Http\Controllers\SectionController;
use Modules\Admin\Http\Controllers\SmslogController;
use Modules\Admin\Http\Controllers\SupportanswerController;
use Modules\Admin\Http\Controllers\TenantController;
use Modules\Admin\Http\Controllers\TranslationController;
use Modules\Admin\Http\Controllers\UserController;

use Modules\Admin\Http\Controllers\UserFeedbackController;
use Modules\Admin\Http\Controllers\UserleadController;
use Modules\Admin\Http\Controllers\VideoController;
use Modules\Admin\Http\Controllers\VideoTranscriptController;
use Modules\Admin\Http\Controllers\WebinarController;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\BrandService;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\FileService;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\NotificationsService;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\SmslogService;
use Modules\Core\Domain\Services\TenantService;
use Modules\Core\Domain\Services\TranslationService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserLeadService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Modules\TrainingCenter\Domain\Services\CourseService;
use Modules\TrainingCenter\Domain\Services\ExerciseService;
use Modules\TrainingCenter\Domain\Services\LecturerService;
use Modules\TrainingCenter\Domain\Services\QuizAnswerService;
use Modules\TrainingCenter\Domain\Services\QuizQuestionService;
use Modules\TrainingCenter\Domain\Services\SectionService;
use Modules\TrainingCenter\Domain\Services\TranscriptService;
use Modules\TrainingCenter\Domain\Services\VideoService;

class AdminModuleProvider extends ModuleServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(UserController::class)->needs(BreadService::class)->give(UserService::class);
        $this->app->when(OrganizationController::class)->needs(BreadService::class)->give(OrganizationService::class);
        $this->app->when(AppointmentController::class)->needs(BreadService::class)->give(AppointmentService::class);
        $this->app->when(BrandController::class)->needs(BreadService::class)->give(BrandService::class);
        $this->app->when(TenantController::class)->needs(BreadService::class)->give(TenantService::class);
        $this->app->when(TranslationController::class)->needs(BreadService::class)->give(TranslationService::class);
        $this->app->when(EnrollmentController::class)->needs(BreadService::class)->give(EnrollmentService::class);
        $this->app->when(ParticipationController::class)->needs(BreadService::class)->give(ParticipationService::class);
        $this->app->when(ProgramController::class)->needs(BreadService::class)->give(ProgramService::class);
        $this->app->when(CourseController::class)->needs(BreadService::class)->give(CourseService::class);
        $this->app->when(SectionController::class)->needs(BreadService::class)->give(SectionService::class);
        $this->app->when(VideoController::class)->needs(BreadService::class)->give(VideoService::class);
        $this->app->when(LecturerController::class)->needs(BreadService::class)->give(LecturerService::class);
        $this->app->when(ExerciseController::class)->needs(BreadService::class)->give(ExerciseService::class);
        $this->app->when(QuizQuestionController::class)->needs(BreadService::class)->give(QuizQuestionService::class);
        $this->app->when(QuizAnswerController::class)->needs(BreadService::class)->give(QuizAnswerService::class);
        $this->app->when(VideoTranscriptController::class)->needs(BreadService::class)->give(TranscriptService::class);
        $this->app->when(WebinarController::class)->needs(BreadService::class)->give(WebinarService::class);

        $this->app->when(UserleadController::class)->needs(BreadService::class)->give(UserLeadService::class);

        $this->app->when(BrandedDocumentController::class)->needs(BreadService::class)->give(BrandedDocumentService::class);
        $this->app->when(UserFeedbackController::class)->needs(BreadService::class)->give(UserFeedbackService::class);
        $this->app->when(GroupController::class)->needs(BreadService::class)->give(GroupService::class);
        $this->app->when(RegistrationCodeController::class)->needs(BreadService::class)->give(RegistrationCodeService::class);
        $this->app->when(MatchController::class)->needs(BreadService::class)->give(MatchService::class);
        $this->app->when(FileController::class)->needs(BreadService::class)->give(FileService::class);

        $this->app->when(NotificationController::class)->needs(BreadService::class)->give(NotificationsService::class);

        $this->app->when(SupportanswerController::class)->needs(BreadService::class)->give(SupportanswerService::class);
        $this->app->when(SupportanswerService::class)->needs(Model::class)->give(Supportanswer::class);

        $this->app->when(OrganizationInteractionController::class)->needs(BreadService::class)->give(OrganizationInteractionService::class);
        $this->app->when(OrganizationInteractionService::class)->needs(Model::class)->give(OrganizationInteraction::class);

        $this->app->when(CoordinatorTodoController::class)->needs(BreadService::class)->give(CoordinatorTodoService::class);
        $this->app->when(SmslogController::class)->needs(BreadService::class)->give(SmslogService::class);
        $this->app->when(CoordinatorTodoService::class)->needs(Model::class)->give(CoordinatorTodo::class);

    }
}
