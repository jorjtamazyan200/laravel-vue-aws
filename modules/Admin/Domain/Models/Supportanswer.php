<?php

namespace Modules\Admin\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Distribution\Domain\Contracts\Distributable;
use Modules\Distribution\Domain\Traits\HasDistributionLogic;

class Supportanswer extends Model  implements Distributable
{

    use SoftDeletes;
    use HasDistributionLogic;


    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','question', 'description', 'description_markdown', 'solution', 'type', 'views', 'likes', 'dislikes',
        'distribute_to_tenants',
        'distributor_source_id'];

    /**
     * The attributes that should be hidden.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    protected $searchable = ['title','question', 'description'];



    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function relations()
    {
        return $this->hasMany(SupportanswerRelation::class, 'supportanswer_id');
    }
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parents()
    {
        return $this->hasMany(SupportanswerRelation::class, 'links_to');
    }

}
