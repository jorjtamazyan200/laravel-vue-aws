<?php

namespace Modules\Admin\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\User;
use RobThree\Auth\TwoFactorAuth;

/**
 * Class TwoFactorAuthService
 * @docs: https://github.com/RobThree/TwoFactorAuth
 * @package Modules\Admin\Domain\Services
 */
class TwoFactorAuthService
{

    public function getQRCode(User $user)
    {
        $name = $user->getDisplayNameAttribute();
        $tfa = $this->getTFAHelper();
        $this->ensureSecret($user);
        return $tfa->getQRCodeImageAsDataUri($name, $user->auth_secret);
    }
    public function verify(User $user, $validation){


        $this->ensureSecret($user);
        return $this
            ->getTFAHelper()
            ->verifyCode($user->auth_secret,  $validation);

    }

    private function ensureSecret(User $user){
        if (empty($user->auth_secret)) {
            $tfa = $this->getTFAHelper();
            $user->auth_secret = $tfa->createSecret();
            $user->save();
        }
    }

    private function getTFAHelper() : TwoFactorAuth{
        $appName = config('app.name');
        $tfa = new TwoFactorAuth($appName);
        return $tfa;
    }
}
