<?php

namespace Modules\Admin\Domain\Services;

use App\Exceptions\Server\ServerException;
use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\User;

class CoordinatorTodoService extends EloquentBreadService
{


    /**
     * @param $todo
     * @param $days
     */
    public function createTodoIfNotExistsWithinDays(CoordinatorTodo $todo, $days)
    {

        $exists = CoordinatorTodo::query()
            ->where('customer_id', $todo->customer_id)
            ->where('type', $todo->type)
            ->where('created_at', '>', Carbon::now()->subDays($days))
            ->exists();

        if (!$todo->customer_id){
            throw new ServerException('Customer Id is required to create a todo');
        }
        $user = User::find($todo->customer_id);

        if (!$user->count_for_reporting) {
            return null;
        }

        if ($exists) {
            return null;
        }

        if (!$todo->duedate) {
            $todo->duedate = Carbon::now()->addDays(3);
        }
        return $todo->save();


    }

    public function getOpenTodosForUserCount(User $user)
    {
        return CoordinatorTodo::query()
            ->where('assignee_id', $user->id)
            ->where('state', CoordinatorTodo::STATES['OPEN'])
            ->count();
    }
    public function getOverdueTodosAssignedTo(User $user)
    {
        return CoordinatorTodo::query()
            ->where('assignee_id', $user->id)
            ->where('state', CoordinatorTodo::STATES['OPEN'])
            ->where('duedate','<',Carbon::now())
            ->count();
    }

    public function getThisWeekCompletedTodos(User $user)
    {
        $beginningOfTheWeek = Carbon::now()->startOfWeek();

        return CoordinatorTodo::query()
            ->where('assignee_id', $user->id)
            ->where('state', CoordinatorTodo::STATES['DONE'])
            ->where('last_state_change_at','>', $beginningOfTheWeek)
            ->count();
    }
}
