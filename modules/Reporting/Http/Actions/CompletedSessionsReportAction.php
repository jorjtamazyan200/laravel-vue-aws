<?php

namespace Modules\Reporting\Http\Actions;

use App\Exceptions\Client\ClientException;
use Illuminate\Database\Query\Builder;

class CompletedSessionsReportAction extends SessionReportAction
{
    protected $POSSIBLE_GROUPS = [
        'month' => 'month',
        'week' => 'week',
    ];

    protected function sessionSelect(Builder $query)
    {
        $group = $this->POSSIBLE_GROUPS['month'];

        if ($this->request->has('group')) {
            if (!array_key_exists($this->request->input('group'), $this->POSSIBLE_GROUPS)) {
                throw new ClientException("The group parameter is invalid.");
            }

            $group = $this->POSSIBLE_GROUPS[$this->request->input('group')];
        }

        $query->selectRaw("COUNT(*) AS completed_sessions,
                EXTRACT(EPOCH FROM DATE_TRUNC('$group', planned_start)) AS group")
            ->where(
                'actual_duration_minutes',
                '>=',
                config('reports.minimumSessionDurationMinutes')
            )
            ->groupBy('group');
    }
}
