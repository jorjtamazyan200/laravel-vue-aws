<?php

namespace Modules\Reporting\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\PlainHttpResource;
use App\Infrastructure\Http\ResourceResponder;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\OrganizationService;
use Modules\Reporting\Http\Requests\GenericReportRequest;

abstract class BaseReportAction extends Action
{
    protected $organizationService;

    /**
     * The Query Builder instance
     *
     * @var Builder
     */
    protected $query;
    protected $request;
    protected $responder;

    public function __construct()
    {
        # Don't resolve via typehinting, so we can easily overwrite this in the
        # specific reports.
        $this->organizationService = app(OrganizationService::class);
        $this->request = app(GenericReportRequest::class);
        $this->responder = app(ResourceResponder::class);
    }

    public function __invoke()
    {
        $this->query = $this->mainQuery();

        /** @var User $user */
        $user = Auth::user();

        $organization = null;

        if ($this->request->has('organization')) {
            $organization = $this->request->input('organization');
        }
        if (!$user->isAdmin()) {
            $organization = $user->organization_id;
        }

        if ($organization) {
            $this->scopeOrganization($organization);
        }

        if ($this->request->has('start_date')) {
            $this->scopeStartDate($this->request->input('start_date'));
        }

        if ($this->request->has('end_date')) {
            # If only a date is given, make sure that day is fully.
            $endDate = Carbon::parse($this->request->get('end_date'));
            if ($endDate->hour === 0) {
                $endDate = Carbon::parse($endDate)->hour(24);
            }
            $this->scopeEndDate($endDate->toDateTimeString());
        }

        return $this->report();
    }

    /**
     * Scope by a given organization. Override this in a report implementation as needed.
     *
     * @param integer $organizationId
     * @return void
     */
    protected function scopeOrganization(int $organizationId)
    {
        //
    }

    /**
     * Scope by a given start date. Override this in a report implementation as needed.
     *
     * @param string $startDate
     * @return void
     */
    protected function scopeStartDate(string $startDate)
    {
        //
    }

    /**
     * Scope by a given end date. Override this in a report implementation as needed.
     *
     * @param string $endDate
     * @return void
     */
    protected function scopeEndDate(string $endDate)
    {
        //
    }

    /**
     * Generate the final report.
     *
     * @return Collection
     */
    protected function report()
    {
        $data = $this->transformData($this->query->get());
        return $this->responder->send($data, PlainHttpResource::class);
    }

    /**
     * Hook to transform data after the database query.
     *
     * @param Collection $data
     * @return Arrayable
     */
    protected function transformData($data)
    {
        return $data;
    }

    /**
     * The main query for this report.
     *
     * @return void
     */
    abstract protected function mainQuery();
}
