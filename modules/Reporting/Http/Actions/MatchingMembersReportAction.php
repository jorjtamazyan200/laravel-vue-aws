<?php

namespace Modules\Reporting\Http\Actions;

use Illuminate\Support\Facades\DB;

class MatchingMembersReportAction extends BaseReportAction
{
    /** {@inheritDoc} */
    protected function mainQuery()
    {
        return DB::table('users')
            ->join('enrollments', 'users.id', '=', 'enrollments.user_id')
            ->join('participations', 'enrollments.id', '=', 'participations.enrollment_id')
            ->whereNull('participations.match_confirmed_at')
            ->selectRaw('COUNT(DISTINCT users.id) AS data');
    }

    /** {@inheritDoc} */
    protected function scopeOrganization(int $organizationId)
    {
        $this->query->where('users.organization_id', '=', $organizationId);
    }

    /** {@inheritDoc} */
    protected function scopeStartDate(string $startDate)
    {
        $this->query->where('users.created_at', '>=', $startDate);
    }

    /** {@inheritDoc} */
    protected function scopeEndDate(string $endDate)
    {
        $this->query->where('users.created_at', '<=', $endDate);
    }

    protected function report()
    {
        $data = $this->query->get();

        return $data[0]->data;
    }
}
