<?php

namespace Modules\Reporting\Http\Actions;

use Modules\Core\Domain\Models\User;

class ActiveLicencesReportAction extends BaseReportAction
{
    /**
     * {@inheritDoc}
     */
    protected function mainQuery()
    {
        return User::selectRaw('COUNT(id) as data');
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeOrganization(int $organizationId)
    {
        $this->query->where('organization_id', '=', $organizationId);
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeStartDate(string $startDate)
    {
        $this->query->where('created_at', '>=', $startDate);
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeEndDate(string $endDate)
    {
        $this->query->where('created_at', '<=', $endDate);
    }

    /**
     * {@inheritDoc}
     */
    protected function transformData($data)
    {
        return $data[0];
    }
}
