<?php

namespace Modules\Reporting\Http\Actions;

use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Core\Domain\Models\User;

class ActiveMembersReportAction extends BaseReportAction
{
    /**
     * {@inheritDoc}
     */
    protected function mainQuery()
    {
        return User::join('oauth_access_tokens', 'users.id', '=', 'oauth_access_tokens.user_id')
            ->orderBy('users.id')
            ->orderBy('oauth_access_tokens.created_at', 'DESC')
            ->selectRaw('DISTINCT on (users.id) users.*')
            ->take(15);
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeOrganization(int $organizationId)
    {
        $this->query->where('users.organization_id', '=', $organizationId);
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeStartDate(string $startDate)
    {
        $this->query->where('oauth_access_tokens.created_at', '>=', $startDate);
    }

    /**
     * {@inheritDoc}
     */
    protected function scopeEndDate(string $endDate)
    {
        $this->query->where('oauth_access_tokens.created_at', '<=', $endDate);
    }

    /**
     * {@inheritDoc}
     */
    protected function report()
    {
        $users = $this->query->get();
        return $this->responder->send($users, UserHttpResource::class);
    }
}
