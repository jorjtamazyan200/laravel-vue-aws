<?php

/*
|--------------------------------------------------------------------------
| Module routes
|--------------------------------------------------------------------------
|
| Here's the place to register API routes for the current module. These routes
| are loaded by the RouteServiceProvider within a group which is assigned
| the "api" middleware group and a version prefix, eg. `/api/v1`.
|
 */

/**
 * /reports
 */
Route::group(['prefix' => '/reports', 'middleware' => ['auth', 'scope:manager']], function () {
    Route::get('/session-hours', 'Actions\SessionHoursReportAction');
    Route::get('/session-minutes', 'Actions\SessionMinutesReportAction');
    Route::get('/completed-sessions', 'Actions\CompletedSessionsReportAction');
    Route::get('/active-members', 'Actions\ActiveMembersReportAction');
    Route::get('/active-licences', 'Actions\ActiveLicencesReportAction');
    Route::get('/licences', 'Actions\LicencesReportAction');
    Route::get('/member-states', 'Actions\MemberStatesReportAction');
    Route::get('/satisfaction', 'Actions\SatisfactionReportAction');
    Route::get('/highlighted-feedback', 'Actions\ShowHighlightedFeedbackAction');
    Route::get('/programs', 'Actions\ShowProgramsToCSRAction');
});

Route::group(['prefix' => '/bv',], function () {
    Route::get('/login', 'Actions\BvReportingAction');
    Route::get('/reporting', 'Actions\BvReportingAction')->name('reporting.bvLogin');
//    Route::get('/reporting', 'Actions\BvReportingAction')->name('reporting.bvLogin');
});
