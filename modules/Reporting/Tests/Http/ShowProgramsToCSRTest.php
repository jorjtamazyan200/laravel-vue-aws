<?php

namespace Modules\Reporting\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\Terms;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me/accept-terms` endpoint.
 */
class ShowProgramsToCSRTest extends LaravelTest
{
    public function test_it_shows_programs()
    {
        $manager = User::where('email', 'manager@volunteer-vision.com')->firstOrFail();


        $response = $this->actingAs($manager)->json('GET', '/api/v2/reports/programs');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [[
                'id',
                'title'
            ]]
        ]);
    }

    public function test_it_should_not_show_programs()
    {
        $user = User::where('email', 'user@volunteer-vision.com')->firstOrFail();

        $this->assertFalse($user->hasRole(Role::ROLES['manager']));

        $response = $this->actingAs($user)->json('GET', '/api/v2/reports/programs');

        $this->assertEquals(403, $response->getStatusCode(), $response->getContent());
    }
}
