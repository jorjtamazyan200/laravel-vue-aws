<?php

namespace Modules\Reporting\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use App\Infrastructure\AbstractTests\LaravelTest;
use Modules\Core\Domain\Models\Terms;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;

/**
 * Test the functionality of the `/api/v1/users/me/accept-terms` endpoint.
 */
class ShowHighlightedFeedbackTest extends LaravelTest
{
    public function test_it_shows_feedback()
    {

        $manager = User::where('email', 'manager@volunteer-vision.com')->firstOrFail();

        Factory(UserFeedback::class)->create(['user_id' => $manager->id, 'is_highlighted'=> true]);


        $response = $this->actingAs($manager)->json('GET', '/api/v2/reports/highlighted-feedback');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                'id',
                'questionCode',
                'responseText'
            ]
        ]);
    }
}
