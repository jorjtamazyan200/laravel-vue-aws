<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Enrollment;

/**
 * Test the functionality of the `/api/v1/enrollment/{id}` endpoint.
 */
class ShowEnrollmentTypeTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_one_of_my_enrollments()
    {
        $response = $this->actingAs($this->user)->get('/api/v2/enrollments/1/type');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'type'
            ]
        ]);
    }


    /** @test */
    public function test_it_shows_custom_enrollment_type()
    {

        // @docs: pivot table docs: https://stackoverflow.com/questions/26566675/getting-the-value-of-an-extra-pivot-table-column-laravel
        $customEntrollment = 'magicEnrollment';

        $program = $this->user->organization->programs()->first();
        $this->user->organization->programs()->sync([$program->id => ['enrollment_type' => $customEntrollment]]);
        /** @var Enrollment $enrollment */
        $enrollment = $this->user->enrollments->first();
        $enrollment->program_id = $program->id;
        $enrollment->save();


        $response = $this->actingAs($this->user)->get('/api/v2/enrollments/' . $enrollment->id . '/type');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'type'
            ]
        ]);
        $response->assertJson([
            'data'=> [
                'type' => $customEntrollment
            ]
        ]);

    }

}
