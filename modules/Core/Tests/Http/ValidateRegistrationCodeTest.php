<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/registration-codes/validate` endpoint.
 */
class ValidateRegistrationCodeTest extends EndpointTest
{
    /** @test */
    public function test_it_returns_200_if_a_registration_code_is_valid()
    {
        $response = $this->json('POST', '/api/v2/registration-codes/validate', [
            'code' => 'default'
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    /** @test */
    public function test_it_returns_an_error_if_a_registration_code_is_not_valid()
    {
        $response = $this->json('POST', '/api/v2/registration-codes/validate', [
            'code' => 'unvalid-code'
        ]);

        $response->assertStatus(422);
    }
}
