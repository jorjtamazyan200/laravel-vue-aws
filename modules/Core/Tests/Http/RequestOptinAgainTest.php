<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v2/programs` endpoint.
 */
class RequestOptinAgainTest extends EndpointTest
{
    public function test_it_shows_an_welcome_text_for_enrollment()
    {

        /** @var Enrollment $enrollment */
        $enrollment = Enrollment::inRandomOrder()->firstOrFail();

        /** @var User $user */
        $user = $enrollment->user;


        $url = route('me.requestoptin');

                /** @var Program $program */
        $program = $enrollment->program;

        $response = $this->actingAs($this->user)->postJson($url);


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

    }

}
