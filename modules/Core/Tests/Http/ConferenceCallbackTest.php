<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\Config;

/**
 * @todo: add real events (e.g. appointment online, appointment end)
 *
 * Test the functionality of the `/api/v1/conference/callback` endpoint.
 */
class ConferenceCallbackTest extends EndpointTest
{
    public function test_it_rejects_requests_with_bad_or_missing_signature()
    {
        // Bad signature
        $response = $this->json(
            'POST',
            '/api/v2/conference/events/callback',
            ['event' => 'foo'],
            ['X-VV-Signature' => 'badSignature']
        );

        $this->assertEquals(403, $response->getStatusCode(), $response->getContent());
        // Missing signature
        $response = $this->json(
            'POST',
            '/api/v2/conference/callback',
            ['event' => 'foo']
        );
        $this->assertEquals(404, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_accepts_requests_with_good_signature()
    {
        $algo = Config::get('conference.signature_algo');
        $secret = Config::get('conference.shared_secret');

        $payload = ['event' => ['type' => 'foo', 'extra' => ['data', 1, 2, 3]]];
        $signature = hash_hmac($algo, json_encode($payload), $secret);

        $response = $this->json(
            'POST',
            '/api/v2/conference/events/callback',
            $payload,
            ['X-VV-Signature' => $signature]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
