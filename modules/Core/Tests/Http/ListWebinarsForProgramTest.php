<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v2/programs/1/webinars` endpoint.
 */
class ListWebinarsForProgramTest extends EndpointTest
{
    public function test_it_lists_webinars()
    {
        $response = $this->actingAs($this->user)->get('/api/v2/programs/1/webinars');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'title',
                    'description',
                    'startsAt',
                    'programId',
                ]
            ]
        ]);
    }
}
