<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\File;

/**
 * Test the functionality of the `/api/v2/programs/1/files` endpoint.
 */
class ListFilesForProgramTest extends EndpointTest
{
    public function test_it_lists_files_for_a_program()
    {
        // Generate a known file for our User
        factory(File::class)->create([
            'title' => 'Test File',
            'source' => 'https://example.com',
            'target_audience' => 'mentor',
            'program_id' => 1,
        ]);

        $response = $this->actingAs($this->user)->getJson('/api/v2/programs/1/files');

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonFragment([
            'title' => 'Test File',
            'source' => 'https://example.com',
            'targetAudience' => 'mentor',
        ]);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'title',
                    'source',
                    'targetAudience',
                ]
            ]
        ]);
    }
}
