<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v1/users/me/profile/{code}` endpoint.
 */
class ShowMyProfileFieldTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_my_profile_field()
    {
        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/profile/LANGUAGES');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertExactJson([
            'data' => [
                'en' => 'advanced',
                'de' => 'basic',
                'ar' => 'fluent',
            ]
        ]);
    }
}
