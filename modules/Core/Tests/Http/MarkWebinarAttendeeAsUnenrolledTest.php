<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

/**
 * Test the functionality of the `/api/v2/webinars/{webinarId}/attendees/{userId}/missed` endpoint.
 */
class MarkWebinarAttendeeAsUnenrolledTest extends EndpointTest
{


    public function test_it_marks_as_unenrolled()
    {
        $webinar = factory(Webinar::class)->create([
            'program_id' => 1,
        ]);
        $webinar->enrollments()->attach(1);

        $user = User::where('email', 'admin@volunteer-vision.com')->first();

        $response = $this->actingAs($user)
            ->post("/api/v2/webinars/$webinar->id/attendees/1/unenroll");

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseMissing('enrollment_webinar', [
            'enrollment_id' => 1,
            'webinar_id' => $webinar->id,
        ]);
    }
}
