<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;

class InviteUserTest extends EndpointTest
{
    use DatabaseTransactions;

    /**
     * Test the functionality of the POST `/api/v2/invites` endpoint.
     */
    public function test_it_invites_a_user()
    {
        $response = $this->actingAs($this->user)->postJson('/api/v2/invites/', [
            'invited_user_email' => 'invited.user@test.de'
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseHas('user_invitations', [
            'inviting_user_id' => $this->user->id,
            'invited_user_email' => 'invited.user@test.de',
            'state' => UserInvitation::STATES['PENDING'],
        ]);
    }

    /**
     * Test the functionality of the GET `/api/v2/invites` endpoint.
     */
    public function test_it_lists_invitations_by_user()
    {
        $user = Factory(User::class)->create();
        $invitation_1 = Factory(UserInvitation::class)->create(['inviting_user_id' => $user->id]);
        $invitation_2 = Factory(UserInvitation::class)->create(['inviting_user_id' => $user->id]);

        $response = $this->actingAs($user)->get('/api/v2/invites/');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                [
                    'uid',
                    'inviting_user_id',
                    'invited_user_email',
                    'invitation_sent_at',
                    'invited_user_id',
                    'state',
                ]
            ]
        ]);
    }

    /**
     * Test the functionality of the GET `/api/v2/invites/accept/{uid}` endpoint.
     */
    public function test_accepting_invitation()
    {
        $invite = Factory(UserInvitation::class)->create(['inviting_user_id' => $this->user->id]);

        $response = $this->getJson('/api/v2/invites/accept/'.$invite->uid);

        $this->assertEquals(302, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('user_invitations', [
            'inviting_user_id' => $this->user->id,
            'invited_user_email' => $invite->invited_user_email,
            'uid' => $invite->uid,
            'state' => UserInvitation::STATES['CLICKED'],
//            'last_clicked_at' => notNullValue(),
        ]);
    }
}
