<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\RegistrationCode;

/**
 * Test the functionality of the `/api/v2/programs` endpoint.
 */
class ShowArticleForRegistrationCodeTest extends EndpointTest
{
    public function test_it_shows_an_article()
    {

        /** @var RegistrationCode $registratonCode */
        $registratonCode = RegistrationCode::inRandomOrder()->get()->first();

        $response = $this->getJson('/api/v2/registration-codes/' . $registratonCode->code . '/article');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'content'
            ]
        ]);
    }

    public function test_it_shows_an_terms_and_conditions_article()
    {

        /** @var RegistrationCode $registratonCode */
        $registratonCode = RegistrationCode::inRandomOrder()->get()->first();

        $response = $this->getJson('/api/v2/registration-codes/' . $registratonCode->code . '/article?article=terms');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        

        $response->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'content'
            ]
        ]);
    }
}
