<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v1/participations/{id}` endpoint.
 */
class ShowParticipationTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_one_of_my_participations()
    {
        $response = $this->actingAs($this->user)->getJson('/api/v2/participations/1');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'match',
            ]
        ]);
    }
}
