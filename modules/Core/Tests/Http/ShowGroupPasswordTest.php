<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v2/groups/{id}/password` endpoint.
 */
class ShowGroupPasswordTest extends EndpointTest
{
    public function test_it_shows_group_password_for_supervisor()
    {
        $supervisor = factory(User::class)->create();
        $supervisor->groups()->attach(1, [
            'role' => 'supervisor'
        ]);

        $response = $this->actingAs($supervisor)->get('/api/v2/groups/1/password');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    public function test_it_hides_group_password_for_member()
    {
        $response = $this->actingAs($this->user)->get('/api/v2/groups/1/password');
        $response->assertStatus(403);
    }
}
