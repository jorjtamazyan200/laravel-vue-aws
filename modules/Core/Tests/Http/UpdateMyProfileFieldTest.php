<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test updating a profile field.
 */
class UpdateMyProfileFieldTest extends EndpointTest
{
    /** @test */
    public function test_it_updates_my_profile_field()
    {
        $response = $this->actingAs($this->user)
            ->json('POST', '/api/v2/users/me/profile/personality', [
                'data' => 'humble'
            ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertExactJson([
            'data' => 'humble'
        ]);

        $this->assertDatabaseHas('profile_fields', [
            'user_id' => 1,
            'code' => 'personality',
            'value' => '"humble"',
        ]);
    }
}
