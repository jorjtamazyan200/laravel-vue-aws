<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;

/**
 * Test the functionality of the `/api/v2/programs` endpoint.
 */
class ShowEnrollmentArticleTest extends EndpointTest
{
    public function test_it_shows_an_welcome_text_for_enrollment()
    {

        $correct_article = factory(BrandedDocument::class)->create([
            'type' => 'article',
            'key' => 'welcomeexample',
            'markdown' => '@markdown',
            'audience' => Role::ROLES['mentor'],
            'language' => 'en'
        ]);

        /** @var Enrollment $enrollment */
        $enrollment = Enrollment::inRandomOrder()->firstOrFail();

        $response = $this->actingAs($this->user)->getJson('/api/v2/enrollments/' . $enrollment->id . '/articles/welcomeexample');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
//                'id' => $correct_article->id,
//                'content' => $correct_article->content
                'key' => 'welcomeexample'
            ]
        ]);
    }

}
