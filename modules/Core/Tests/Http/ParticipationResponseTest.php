<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class ParticipationResponseTest extends EndpointTest
{
    /** @test */
    public function test_it_removes_participation()
    {

        /** @var User $user */

        /** @var Participation $participation */
        $participation = Participation::query()->inRandomOrder()->firstOrFail();
        $enrollment = $participation->enrollment;
        $user = $participation->enrollment->user;
        $action = 'disable';

        $secret = $user->emailSecret($action);

        $route = route('email.response.participation', [
            'action' => $action,
            'participationid' => $participation->id,
            'userid' => $user->id,
            'secret' => $secret
        ]);

        // public url
        $response = $this->getJson($route);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $newPart = Participation::find($participation->id);
        $this->assertNull($newPart);

        $newEnrollment = Enrollment::find($enrollment->id);
        $this->assertEquals($newEnrollment->state, Enrollment::STATES['QUIT']);
    }
}
