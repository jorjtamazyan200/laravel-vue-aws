<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use App\Notifications\User\UserPasswordResetNotification;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\User;

class PasswordResetTest extends EndpointTest
{
    public function test_it_mails_a_password_reset_token()
    {
        Notification::fake();

        $response = $this->json('POST', '/api/v2/auth/password-reset/request', [
            'email' => $this->user->email,
        ]);

        $response->assertStatus(200);
        $this->assertEquals('ok', $response->content());

        Notification::assertSentTo(
            [$this->user],
            UserPasswordResetNotification::class
        );
        $this->assertDatabaseHas('password_resets', ['email' => $this->user->email]);
    }

    public function test_it_redeems_a_password_reset_token()
    {
        $broker = App::make(PasswordBroker::class);

        $this->assertDatabaseMissing('password_resets', ['email' => $this->user->email]);
        $token = $broker->createToken($this->user);
        $this->assertDatabaseHas('password_resets', ['email' => $this->user->email]);

        $this->assertFalse($this->userHasPassword($this->user->email, 'newPassword'));
        $response = $this->json('POST', '/api/v2/auth/password-reset/save', [
            'email' => $this->user->email,
            'password' => 'newPassword',
            'passwordConfirmation' => 'newPassword',
            'token' => $token,
        ]);

        $response->assertStatus(200);
        $this->assertEquals('ok', $response->content());
        $this->assertTrue($this->userHasPassword($this->user->email, 'newPassword'));
    }

    private function userHasPassword(string $email, string $password)
    {
        $hash = User::where('email', $email)->firstOrFail()->password;

        return Hash::check($password, $hash);
    }
}
