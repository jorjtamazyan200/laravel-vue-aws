<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;

/**
 * Test the functionality of the `/api/v2/groups/{id}/members` endpoint.
 */
class AddOpenFeedbackTest extends EndpointTest
{
    public function test_it_adds_a_feedback()
    {

        $user = $this->user;

        $text = 'hello' . rand(0, 1000);

        $response = $this->actingAs($user)->json('POST', '/api/v2/feedbacks', [
            'response_text' => $text,
            'response_scalar' => 5,
        ]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseHas('user_feedbacks', [
            'user_id' => $this->user->id,
            'response_text' => $text,
            'question_code' => UserFeedback::CODES['OPEN_FEEDBACK'],
            'response_scalar' => 5,
        ]);

    }

    public function test_it_allows_empty_texts()
    {

        $user = $this->user;

        $response = $this->actingAs($user)->json('POST', '/api/v2/feedbacks', [
            'response_text' => '',
            'response_scalar' => 3,
        ]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseHas('user_feedbacks', [
            'user_id' => $this->user->id,
            'question_code' => UserFeedback::CODES['OPEN_FEEDBACK'],
            'response_scalar' => 3,
        ]);

    }

    public function test_it_adds_coordinator_todo()
    {

        $user = $this->user;

        $response = $this->actingAs($user)->json('POST', '/api/v2/feedbacks', [
            'response_text' => '',
            'response_scalar' => 3,
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $this->assertDatabaseHas('coordinator_todos', [
            'customer_id' => $this->user->id,
            'type' => CoordinatorTodo::TYPES['USER_BAD_FEEDBACK']
        ]);

    }
}
