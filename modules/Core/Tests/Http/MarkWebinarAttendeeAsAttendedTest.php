<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Enrollment;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

/**
 * Test the functionality of the `/api/v2/webinars/{webinarId}/attendees/{userId}/attended` endpoint.
 */
class MarkWebinarAttendeeAsAttendedTest extends EndpointTest
{
    public function test_it_throws_unauthorized_for_normal_users()
    {
        $webinar = factory(Webinar::class)->create([
            'program_id' => 1,
            'state' => 'PLANNED', //webinar should be planned to be marked as attended
        ]);
        $webinar->enrollments()->attach(1);
        $response = $this->actingAs($this->user)
            ->postJson("/api/v2/webinars/$webinar->id/attendees/1/attended");

        $this->assertEquals($response->getStatusCode(),403, $response->getContent());
        $this->assertDatabaseHas('enrollment_webinar', [
            'enrollment_id' => 1,
            'webinar_id' => $webinar->id,
            'attended' => null,
        ]);
    }

    public function test_it_marks_as_attended()
    {
        Notification::fake();

        $webinar = factory(Webinar::class)->create([
            'program_id' => 1,
            'state' => Webinar::STATES['PLANNED'] //webinar should be planned to be marked as attended
        ]);
        $webinar->enrollments()->attach(1);

        $user = User::where('email', 'admin@volunteer-vision.com')->first();


        $enrollment = Enrollment::find(1);
        $enrollment->state = Enrollment::STATES['TRAINING'];
        $enrollment->save();

        $response = $this->actingAs($user)
            ->postJson("/api/v2/webinars/$webinar->id/attendees/$enrollment->id/attended");


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('enrollment_webinar', [
            'enrollment_id' => 1,
            'webinar_id' => $webinar->id,
            'attended' => true,
        ]);
    }
}
