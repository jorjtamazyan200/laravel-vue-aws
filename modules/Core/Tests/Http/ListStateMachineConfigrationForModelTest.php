<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

class ListStateMachineConfigrationForModelTest extends EndpointTest
{
    /** @test */
    public function test_it_lists_all_configuration_for_user()
    {
        $response = $this->actingAs($this->user)->get('/api/v2/statemachine/user');

        $response->assertJsonStructure([
            'states'=> [],
            'transitions'=> [],
        ]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
