<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/users/register` endpoint.
 */
class RegisterUserTest extends EndpointTest
{


    /** @test */
    public function test_it_does_autoenrolment()
    {
        $registrationCode =
            factory(RegistrationCode::class)->create([
                'code' => 'hello',
                'auto_enroll_in' => 1,
                'primary_role' => User::ROLES['mentor']
            ])->first();

        $response = $this->json('POST', '/api/v2/users/register', [
            'code' => 'hello',
            'firstName' => 'Peter',
            'lastName' => 'Pan',
            'email' => 'peter@123pan.com',
            'password' => 'Start123!',
            'preferredChannel' => 'email'
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $data = json_decode($response->getContent())->data;
        $userid = $data->id;

        $this->assertDatabaseHas('users', [
            'email' => 'peter@123pan.com'
        ]);
        $this->assertDatabaseHas('enrollments', [
            'user_id' => $userid,
            'program_id' => 1,
            'role' => User::ROLES['mentor']
        ]);
    }  /** @test */
    public function test_it_allows_star_as_password()
    {
        $registrationCode =
            factory(RegistrationCode::class)->create([
                'code' => 'hello',
                'auto_enroll_in' => 1,
                'primary_role' => User::ROLES['mentor']
            ])->first();

        $response = $this->json('POST', '/api/v2/users/register', [
            'code' => 'hello',
            'firstName' => 'Peter',
            'lastName' => 'Pan',
            'email' => 'peter@123pan.com',
            'password' => 'Start123*',
            'preferredChannel' => 'email'
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('users', [
            'email' => 'peter@123pan.com'
        ]);
    }

    /** @test */
    public function test_it_sets_people_on_the_waiting_list()
    {
        $registrationCode =
            factory(RegistrationCode::class)->create([
                'code' => 'hello12345',
                'users_count_limit' => 1,
                'auto_enroll_in' => 1,
                'primary_role' => User::ROLES['mentor']
            ]);

        // create another user who is already there...
        factory(User::class)->create([
            'registration_code_id' => $registrationCode->id
        ]);


        $response = $this->json('POST', '/api/v2/users/register', [
            'code' => 'hello12345',
            'firstName' => 'Peter',
            'lastName' => 'Pan',
            'email' => 'peter@123pan.com',
            'password' => 'Start123!',
            'preferredChannel' => 'email'
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $data = json_decode($response->getContent())->data;
        $userid = $data->id;

        $this->assertDatabaseHas('users', [
            'email' => 'peter@123pan.com'
        ]);
        $this->assertDatabaseHas('enrollments', [
            'user_id' => $userid,
            'program_id' => 1,
            'role' => User::ROLES['mentor'],
            'state' => Enrollment::STATES['WAITINGLIST']
        ]);
    }

    /** @test */
    public function test_it_registers_a_user_with_a_code()
    {
        $response = $this->json('POST', '/api/v2/users/register', [
            'code' => 'default',
            'firstName' => 'Peter',
            'lastName' => 'Pan',
            'email' => 'peter@pan.com',
            'password' => 'Start123!',
            'preferredChannel' => 'email'
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('users', [
            'email' => 'peter@pan.com'
        ]);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'firstName',
                'lastName',
                'email',
                'preferredChannel',
                'organization' => [
                    'id',
                    // ...
                ],
                'brand' => [
                    'id',
                    // ...
                ],
            ]
        ]);
    }

    /** @test */
    public function test_it_validates_e164_phone_number_format_and_trims_spaces()
    {
        $response = $this->json('POST', '/api/v2/users/register', [
            'phone_number' => '12 34 56',
            'phone_number_prefix' => '+12',
            'code' => 'default',
            'firstName' => 'Peter',
            'lastName' => 'Pan',
            'email' => 'peter@pan.com',
            'password' => 'Start123!',
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $this->assertDatabaseHas('users', [
            'phone_number' => '123456',
            'phone_number_prefix' => '12',
        ]);

        $response->assertJson([
            'data' => [
                'phoneNumber' => '+123456',
            ]
        ]);
    }

    /** @test */
    public function test_supervisor_register()
    {

        $registrationCode =
            factory(RegistrationCode::class)->create([
                'code' => 'supervisorcode',
                'primary_role' => User::ROLES['supervisor'],
                'group_id' => 1
            ])->first();


        $response = $this->json('POST', '/api/v2/users/register', [
            'phone_number' => '12 34 56',
            'phone_number_prefix' => '+12',
            'code' => 'supervisorcode',
            'firstName' => 'Peter',
            'lastName' => 'Pan',
            'email' => 'supervisorcode@vv.com',
            'password' => 'Start123!',
        ]);


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $data = json_decode($response->getContent());
        $userid = $data->data->id;


        $this->assertDatabaseHas('users', [
            'email' => 'supervisorcode@vv.com',
            'primary_role' => User::ROLES['supervisor']
        ]);


        $this->assertDatabaseHas('group_user', [
            'user_id' => $userid,
            'group_id' => 1,
            'role' => User::ROLES['supervisor']
        ]);


    }
}
