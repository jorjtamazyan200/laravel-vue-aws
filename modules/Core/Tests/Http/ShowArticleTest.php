<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Program;

/**
 * Test the functionality of the `/api/v2/programs` endpoint.
 */
class ShowArticleTest extends EndpointTest
{
    public function test_it_shows_an_article()
    {
        $wrong_article = factory(BrandedDocument::class)->create(['type' => 'article', 'key' => 'wrong-article']);
        $correct_article = factory(BrandedDocument::class)->create(['type' => 'article', 'key' => 'test-article', 'content' => '@markdown
This test is **nice**!
@endmarkdown
']);

        $response = $this->actingAs($this->user)->getJson('/api/v2/articles/test-article');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'id' => $correct_article->id,
                'content' => '<p>This test is <strong>nice</strong>!</p>'
            ]
        ]);
    }

    public function test_it_shows_an_article_for_a_program()
    {
        $program = Program::find(1);

        $fitting_article = factory(BrandedDocument::class)->create(['type' => 'article', 'program_id' => null, 'key' => 'test-article', 'content' => '@markdown
Hey {{ $user->first_name }}! This article is about program **{{ $program->title }}** but it is pretty lame!
@endmarkdown
']);
        $response = $this->actingAs($this->user)->getJson('/api/v2/articles/test-article?program_id=1');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'id' => $fitting_article->id,
                'content' => "<p>Hey {$this->user->first_name}! This article is about program <strong>{$program->title}</strong> but it is pretty lame!</p>"
            ]
        ]);

        $better_fitting_article = factory(BrandedDocument::class)->create(['type' => 'article', 'program_id' => $program->id, 'key' => 'test-article', 'content' => '@markdown
Hey {{ $user->first_name }}! This article is about program **{{ $program->title }}** and it contains all the fun information that fallbacks do not tell you!
@endmarkdown
']);
        $response = $this->actingAs($this->user)->getJson('/api/v2/articles/test-article?program_id=1');
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'data' => [
                'id' => $better_fitting_article->id,
                'content' => "<p>Hey {$this->user->first_name}! This article is about program <strong>{$program->title}</strong> and it contains all the fun information that fallbacks do not tell you!</p>"
            ]
        ]);
    }
}
