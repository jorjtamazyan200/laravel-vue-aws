<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\User;

/**
 * Test the functionality of the `/api/v1/groups/{id}/users` endpoint.
 */
class ListUserForGroupTest extends EndpointTest
{
    public function test_it_lists_all_users_in_a_group()
    {
        $supervisor = factory(User::class)->create();
        $supervisor->groups()->attach(1, [
            'role' => 'supervisor'
        ]);


        $response = $this->actingAs($supervisor)->getJson('/api/v2/groups/1/users');
        

        $this->assertEquals($response->getStatusCode(), 200, $response->getContent());

        // Assert that User with ID 1 is listed in the response...
        // as he is part of this group
        $response->assertJsonFragment([
            'id' => $this->user->id,
        ]);
    }
}
