<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Facades\Log;

/**
 * Test the functionality of the `/api/v1/users/me/launch` endpoint.
 */
class LaunchSessionTest extends EndpointTest
{
    public function test_it_updates_my_timezone()
    {
        $response = $this->actingAs($this->user)->json('POST', '/api/v2/users/me/launch', [
            'timezone' => 'Asia/Pyongyang',
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $this->assertDatabaseHas('users', [
            'id' => $this->user->id,
            'timezone' => 'Asia/Pyongyang',
        ]);
    }

//    public function test_it_writes_to_the_logfile()
//    {
//        config(['database.log_queries' => false]);
//
//        Log::shouldReceive('info')
//            ->once()
//            ->with('New session for User #1 with User-Agent "Symfony/3.X"');
//
//        // Execute the action that will write in the log
//        $response = $this->actingAs($this->user)->json('POST', '/api/v2/users/me/launch', [
//            'timezone' => 'Asia/Pyongyang',
//        ]);
//    }
}
