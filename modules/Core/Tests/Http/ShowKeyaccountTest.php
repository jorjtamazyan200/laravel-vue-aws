<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v1/users/me` endpoint.
 */
class ShowKeyaccountTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_me()
    {
        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/keyaccount');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'id',
                'firstName',
                'lastName',
                'email'
            ]
        ]);
    }
}
