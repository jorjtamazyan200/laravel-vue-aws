<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/users/me/participations` endpoint.
 */
class ListMyParticipationsTest extends EndpointTest
{
    /** @test */
    public function test_my_participations_is_an_array()
    {


//        $user = User::where('email', 'mentor@volunteer-vision.com')->firstOrFail();
        $user = factory(User::class)->create(['first_name' => 'Tom', 'language' => 'de', 'accept_email' => true]);

        /** @var Enrollment $enrollment */
        $match = factory(Match::class)->create([
            'state' => Match::STATES['CONFIRMED'],
            'last_reminder' => null
        ]);

        /** @var Enrollment $enrollment */
        $enrollment = factory(Enrollment::class)->create([
            'state' => Enrollment::STATES['ACTIVE'],
            'user_id' => $user->id
        ]);

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);


        $response = $this->actingAs($user)->get('/api/v2/users/me/participations');


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
//

//        $response->assertJsonStructure([
//            'data' => [
//                [
//                    'id',
//                    'match' => [
//                        'matchedParticipations' => [
//                            [
//                                'user'
//                            ]
//                        ],
//                        'appointments'
//                    ]
//                ]
//            ]
//        ]);

    }

    /** @test */
    public function test_it_lists_all_my_participations()
    {


        $response = $this->actingAs($this->user)->get('/api/v2/users/me/participations');


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'match' => [
                        'matchedParticipations' => [
                            [
                                'user'
                            ]
                        ],
                        'appointments'
                    ]
                ]
            ]
        ]);
    }

    /** @test */
    public function test_it_does_not_show_a_rejected_match()
    {

        $enrollment = $this->user->enrollments()->first();
        $match = factory(Match::class)->create(['state' => Match::STATES['REJECTED']]);
        factory(Participation::class)->create(['match_id' => $match->id, 'enrollment_id' => $enrollment->id]);

        $response = $this->actingAs($this->user)->get('/api/v2/users/me/participations');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response = json_decode($response->getContent());
        $participations = collect($response->data);

        $rejected = $participations->filter(function ($participation) {
            return $participation->match && $participation->match->state == Match::STATES['REJECTED'];
        });

//        var_dump($participations->count());

        $this->assertEquals(0, $rejected->count());
    }


}
