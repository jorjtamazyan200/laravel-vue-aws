<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test updating a profile.
 */
class UpdateMyProfileTest extends EndpointTest
{
    /** @test */
    public function test_it_mass_updates_my_own_profile_fields()
    {
        $response = $this->actingAs($this->user)
            ->json('PUT', '/api/v2/users/me/profile', [
                'PERSONALITY' => 'humble',
                'SOME_FIELD' => 'a-value',
            ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonStructure([
            'data' => [
                'PERSONALITY',
                'SOME_FIELD'
            ]
        ]);
        $response->assertJsonFragment([
            'PERSONALITY' => 'humble',
            'SOME_FIELD' => 'a-value',
        ]);

        $this->assertDatabaseHas('profile_fields', [
            'user_id' => $this->user->id,
            'code' => 'PERSONALITY',
            'value' => '"humble"',
        ]);
        $this->assertDatabaseHas('profile_fields', [
            'user_id' => $this->user->id,
            'code' => 'SOME_FIELD',
            'value' => '"a-value"',
        ]);
    }
}
