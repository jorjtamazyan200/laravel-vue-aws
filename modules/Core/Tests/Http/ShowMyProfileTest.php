<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;

/**
 * Test the functionality of the `/api/v1/users/me/profile` endpoint.
 */
class ShowMyProfileTest extends EndpointTest
{
    /** @test */
    public function test_it_shows_my_profile()
    {
        $response = $this->actingAs($this->user)->getJson('/api/v2/users/me/profile');

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJsonFragment([
            'languages' => [
                'en' => 'advanced',
                'de' => 'basic',
                'ar' => 'fluent',
            ],
            'interests' => ['Climbing', 'Reading', 'Coding'],
            'personality' => 'outgoing',
        ]);
    }
}
