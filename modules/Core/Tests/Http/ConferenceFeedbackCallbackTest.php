<?php

namespace Modules\Core\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Modules\Core\Domain\Models\UserFeedback;

/**
 * @todo: add real events (e.g. appointment online, appointment end)
 *
 * Test the functionality of the `/api/v1/conference/callback` endpoint.
 *
 * method: 'POST',
 * path: `/appointments/for-room/${this.room.id}`,
 * payload: {
 * date: date
 * }
 * })
 */
class ConferenceFeedbackCallbackTest extends EndpointTest
{

    public function test_it_creates_a_new_appointment()
    {

        $algo = Config::get('conference.signature_algo');
        $secret = Config::get('conference.shared_secret');

        $user = $this->user;

        $match = $user->enrollments->first()->participations->first()->match;
        $match->external_room_id = 12345;
        $match->save();

        $date = Carbon::now()->addDays(7);

        $payload = [
            'feedbacks' => [
                ['code' => UserFeedback::CODES['CONNECTION_QUALITY'], 'rating' => 3, 'text' => 'Hello'],
                ['code' => UserFeedback::CODES['GENERAL_COMMENT'], 'rating' => null, 'text' => 'Genral Comment'],
            ]
        ];
        //@todo: this
        $signature = hash_hmac($algo, json_encode($payload), $secret);

        $response = $this->json(
            'POST',
            '/api/v2/conference/messages/rooms/' . $match->external_room_id . '/feedbacks',
            $payload,
            ['X-VV-Signature' => $signature, "X-VV-User-Id" => $user->id]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'added' => 2
        ], $response->getContent());
    }
}
