<?php

namespace Modules\Core\Tests\Unit;


use App\Infrastructure\AbstractTests\LaravelTest;
use App\Notifications\Match\MatchNotConfirmedReminderNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Reminder\MatchNotConfirmedReminderHandler;
use Modules\Matching\Domain\Models\Match;

class MatchNotConfirmedReminderTest extends LaravelTest
{


//
    public function test_unconfirmed_email()
    {
        Notification::fake();


        $matches =  Match::query()->where('id',1)->get();

        /** @var Match $match */
        $match = $matches->first();
        $mentor = $match->getMentorParticipation()->enrollment->user;
        $match->last_state_change_at = Carbon::now()->subDays(4);
        $match->last_reminder = null;


        /** @var MatchNotConfirmedReminderHandler $handler */
        $handler = App(MatchNotConfirmedReminderHandler::class);
        $eventsFired = $handler->applyActions($matches);

        $this->assertEquals(1,$eventsFired);

        Notification::assertSentTo(
            $mentor,
            MatchNotConfirmedReminderNotification::class
        );



    }
}
