<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

/***
 * Class Role
 * Follows best practice of
 * https://github.com/Zizaco/entrust/blob/master/src/Entrust/Traits/EntrustRoleTrait.php
 *
 * @package Modules\Core\Domain\Models
 */
class Role extends Model
{
    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const ROLES =
        [
            'user' => 'user',
            'mentor' => 'mentor',
            'mentee' => 'mentee',
            'supervisor' => 'supervisor',
            'manager' => 'manager',
            'coordinator' => 'coordinator',
            'ambassador' => 'ambassador',
            'editor' => 'editor',
            'admin' => 'admin',
            'superadmin' => 'superadmin',
            'distributionservice' => 'distributionservice',
            'partnerapi' => 'partnerapi'
        ];

    protected $fillable = ['name', 'display_name', 'description'];

    /**
     * Many-to-Many relations with the user model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_role');
    }
}
