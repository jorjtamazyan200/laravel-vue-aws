<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['body', 'pin_to_top', 'type'];


    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const TYPES = [
        'SUPERVISOR_COMMENT' => 'SUPERVISOR_COMMENT',
        'TECHNICAL' => 'TECHNICAL',
        'ORGANISATIONAL' => 'ORGANISATIONAL'
    ];

    /**
     * An Comment belongs to a User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }
}
