<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordHistory extends Model
{
    protected $fillable = [
        'user_id',
        'password'
    ];

    //

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
