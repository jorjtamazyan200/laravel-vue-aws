<?php

namespace Modules\Core\Domain\Models;

use App\Domain\Models\BaseModel;
use App\Infrastructure\Traits\Statable;
use App\Services\MultiTenant\BelongsToTenant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Webinar extends BaseModel
{
    use SoftDeletes;
    use Statable;
    use BelongsToTenant;



    /*
    * State Machine Configuration
    */

    const HISTORY_MODEL = WebinarStateLog::class;
    const SM_CONFIG = 'webinar'; // the SM graph to use

    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */

    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'NEW' => 'NEW',
        'PLANNED' => 'PLANNED',
        'CANCELED' => 'CANCELED',
        'SUCCESS' => 'SUCCESS',

    ];

    /**
     * All possible state transitions that can be performed on the model.
     *
     * @var array
     */
    const TRANSITIONS = [
        'plan' => [
            'from' => ['NEW'],
            'to' => 'PLANNED'
        ],
        'reschedule' => [
            'from' => ['PLANNED', 'CANCELED'],
            'to' => 'PLANNED'
        ],
        'cancel' => [
            'from' => ['PLANNED'],
            'to' => 'CANCELED'
        ],

        'finish' => [
            'from' => ['PLANNED'],
            'to' => 'SUCCESS'
        ],
    ];

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state',
        'title',
        'description',
        'duration_minutes',
        'program_id',
        'referent_id',
        'login_link',
        'organization_id',
        'last_state_change_at',
        'invitation_note',
        'seats',
        'target_audience',
        'starts_at'
    ];

    // @todo: webinar_video_url
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'starts_at' => 'datetime',
        'last_state_change_at' => 'datetime'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * A Webinar belongs to a Program.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    /**
     * A Webinar belongs to a Program.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referent()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Many Users can enroll in a Webinar through their Enrollment to a Program.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function enrollments()
    {
        return $this->belongsToMany(Enrollment::class)->withPivot('attended');
    }

    /**
     * A User is member of exactly one Organization.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function getLoginLink() {
        return $this->login_link;
    }

    public function save(array $options = [])
    {

        if ( (!$this->state || $this->state == self::STATES['NEW']) && $this->starts_at) {
            $this->state = self::STATES['PLANNED'];
        }


        if (!$this->description) {
            $this->description = ''; // avoid nulls
        }
        if (!$this->title) {
            $this->title = ''; // avoid nulls
        }

        parent::save($options);



    }
}
