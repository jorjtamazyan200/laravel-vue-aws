<?php

namespace Modules\Core\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\User;

class UserStateLog extends Model
{
    use LogsState;

    /**
     * @var string
     */
    protected $table = 'user_state_logs';

    protected $fillable = ['user_id', 'actor_id', 'transition', 'from', 'to'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function actor()
    {
        return $this->belongsTo(User::class);
    }
}
