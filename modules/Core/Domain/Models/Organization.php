<?php

namespace Modules\Core\Domain\Models;

use App\Domain\Models\BaseModel;
use App\Infrastructure\Traits\Statable;
use App\Services\MultiTenant\BelongsToTenant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Core\Domain\Models\Traits\Commentable;

class Organization extends BaseModel
{
    use Statable;
    use Commentable;
    use BelongsToTenant;

    /*
    * State Machine Configuration
    */

    const HISTORY_MODEL = OrganizationStateLog::class;
    const SM_CONFIG = 'organization'; // the SM graph to use

    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */

    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const STATES = [
        'ONBOARDING' => 'ONBOARDING',
        'ROLLOUT' => 'ROLLOUT',
        'ACTIVE' => 'ACTIVE',
        'EXIT' => 'EXIT',
    ];

    /**
     * The different kinds of organizations.
     *
     * @var array
     */
    const TYPES = [
        'socialservice' => 'socialservice',
        'company' => 'company'
    ];

    /**
     * All possible state transitions that can be performed on the model.
     *
     * @var array
     */
    const TRANSITIONS = [
        'signed' => [
            'from' => ['ONBOARDING'],
            'to' => 'ROLLOUT'
        ],
        'activate' => [
            'from' => ['ROLLOUT','ONBOARDING'],
            'to' => 'ACTIVE'
        ],
        'end_contract' => [
            'from' => ['ACTIVE'],
            'to' => 'EXIT'
        ],
    ];


    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'state',
        'code',
        'video_provider',
        'keyaccount_id',
        'servicelevel',
        'zendesk_last_sync',
        'zendesk_id',
        'private_webinars',
        'webinar_comment',
        'login_link',
        'old_plattform_id',
        'tenant_id',
        'technical_usage_comment',
        'technical_webinar_check',
        'technical_guest_wifi_check',
        'technical_company_network_check',
        'technical_device_check', // work device allowed;
        //
        'last_customer_interaction',
        'licence_potential_estimate',
        'classification',
        'dpa_status',
        'contract_status',
        'licence_logic',
        'contract_expiration',
        'satisfaction_level',
        'churn_predictor',
        'jf_interval',
        'jf_last_at',
        //
        'whatsapp_enabled',
        'community_enabled'
    ];

    /**
     * The attributes that should be hidden.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    /**
     * used by the eloquent service to fuzzy search entities
     * @var array
     */
    public $searchable = [
        'name'
    ];

    protected $casts = [
        'zendesk_last_sync' => 'datetime',
        'jf_last_at' => 'datetime'
    ];

    public function getIsIncompleteAttribute(): bool
    {

        return is_null($this->technical_webinar_check) ||
            is_null($this->technical_guest_wifi_check) ||
            is_null($this->technical_company_network_check) ||
            is_null($this->technical_device_check) ||
            is_null($this->dpa_status) ||
            is_null($this->contract_status) ||
            is_null($this->manager_id) ||
            is_null($this->licence_logic)
            ;

    }

    public function getPlannedJfAtAttribute()
    {
        if ($this->state != self::STATES['ACTIVE']){
            return null;
        }

        if (!$this->jf_interval){
            return null;
        }
        /** @var Carbon $lastJf */
        $lastJf = $this->jf_last_at;
        if (!$lastJf) {
            return Carbon::now();
        }
        return $lastJf->addDays($this->jf_interval);
   }

        /*
        |--------------------------------------------------------------------------
        | Relations
        |--------------------------------------------------------------------------
        */

    /**
     * An Organization has many Users.
     *
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     *
     *
     * @return BelongsTo
     */
    public function keyaccount()
    {
        return $this->belongsTo(User::class);
    }

    /**
     *
     *
     * @return BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * An Organization can use many Programs.
     *
     * @return BelongsToMany
     */
    public function programs()
    {
        return $this->belongsToMany(Program::class)->withPivot('enrollment_type');
    }

    /**
     * An Organization has many groups
     *
     * @return HasMany
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
