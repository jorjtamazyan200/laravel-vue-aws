<?php

namespace Modules\Core\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Modules\Core\Domain\Models\Traits\HasImageAttachments;
use Modules\Distribution\Domain\Contracts\Distributable;
use Modules\Distribution\Domain\Traits\HasDistributionLogic;

class File extends Model implements Distributable
{
    use HasImageAttachments;
    use SoftDeletes;
    use HasDistributionLogic;

    /*
    |--------------------------------------------------------------------------
    | Config
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'program_id',
        'target_audience',
        'mime',
        'cover',
        'language',
        'source',
        'order',
        'lection_number',
        'brand_id',
        'placement',
        'target_audience',
        'distribute_to_tenants',
        'distributor_source_id'
    ];

    const CATEGORIES = [
        'general' => 'general',
        'intranet' => 'intranet',
        'video' => 'video',
        'personal_upload' => 'personal_upload'
    ];

    /**
     * used by the eloquent service to fuzzy search entities
     * @var array
     */
    public $searchable = [
        'title'
    ];
    /**
     * All possible states that this model can have.
     *
     * @var array
     */
    const PLACEMENTS = [
        'preparation' => 'preparation',
        'reporting' => 'reporting',
        'embassador' => 'embassador',
        'supervisor' => 'supervisor',
        'help' => 'help'
    ];


    protected $image_attachments = [
        'cover' => [
            'path' => 'uploads/user/files',
            'styles' => [
                'small' => '150',
                'medium' => '500'
            ]
        ]
    ];


    /**
     * Accessor providing an `avatar` attribute which contains an array of image urls for every defined dimension.
     *
     * @return array
     */
    public function getLinkAttribute()
    {
        if (empty($this->source)) {
            return null;
        }
        if ($this->mime === 'vimeo' || $this->mime === 'youtube'){
            return $this->source;
        }
        return Storage::disk('public')->url($this->source);
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * A File belongs to a Program.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
        return $this->belongsTo(Program::class);
    }
    /*
    |--------------------------------------------------------------------------
    | File Store
    |--------------------------------------------------------------------------
    */
    /**
     * - Saves a resized version of each defined style to disk
     * - Removes old files if existing
     * - Saves the filename to the database column
     *
     * @param String $attachment_name
     * @param String $content
     * @param String $type
     * @return Boolean
     */
    public function updateAttachment(
        String $attachment_name,
        String $content,
        String $type,
        int $filesize
    ) {

        $path = 'uploads/files/' . $attachment_name;
        $this->saveFile($path, $content, 'public');
        $this->source = $path;
        $this->mime = $type;
        $this->filesize = $filesize;
        $this->save();
    }

    /**
     * Removes a file from disk
     *
     * @param String $filepath The filepath including filename with extension
     * @return Boolean
     */
    protected function removeFile(
        String $filepath
    ) {
        return Storage::disk('public')->delete($filepath);
    }


    /**
     * Saves a file to disk
     *
     * @param String $path The filepath including filename with extension
     * @param String $img
     * @return Boolean
     */
    protected function saveFile(
        String $filepath,
        String $img,
        String $visibility = 'private'
    ) {
        return Storage::disk('public')->put($filepath, $img, $visibility);
    }






}
