<?php

namespace Modules\Core\Domain\Models;

use App\Infrastructure\Traits\LogsState;
use Illuminate\Database\Eloquent\Model;

class WebinarStateLog extends Model
{
    use LogsState;

    /**
     * @var string
     */
    protected $table = 'webinar_state_logs';

    protected $fillable = ['webinar_id', 'actor_id', 'transition', 'from', 'to'];

    public function webinar()
    {
        return $this->belongsTo(Webinar::class);
    }
}
