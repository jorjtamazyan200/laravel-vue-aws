<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Support\Arrayable;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;

class UserFeedbackService extends EloquentBreadService
{
    public function filterOrganization(Builder $query, $method, $clauseOperator, $value)
    {
        // if clauseOperator is idential to false,
        //     we are using a specific SQL method in its place (e.g. `in`, `between`)
        if ($clauseOperator === false) {
            call_user_func([$query, $method], 'users.organization_id', $value);
        } else {
            call_user_func([$query, $method], 'users.organization_id', $clauseOperator, $value);
        }

        // @add join tables
        return ['user' => 'user'];
    }

    /**
     * Create a new entity by an array of attributes.
     *
     * Attributes missing from the array will be
     * set to their respective default values.
     *
     * @param  array $attributes Attributes of the new entity
     */
    public function create(array $attributes): Arrayable
    {
        $model = parent::create($attributes);
        $this->createCoordinatorTodo($model);
        return $model;
    }

    private function createCoordinatorTodo($model)
    {
        if (!$model->response_scalar || $model->response_scalar > 3) {
            return;
        }

        /** @var CoordinatorTodoService $coordinatorTodoService */
        $coordinatorTodoService = app(CoordinatorTodoService::class);

        $data = [
            'meta' => ['feedback_id' => $model->id],
            'type' => CoordinatorTodo::TYPES['USER_BAD_FEEDBACK'],
            'customer_id' => $model->user_id,
        ];
        $todo = new CoordinatorTodo($data);
        $coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);


    }


}
