<?php

namespace Modules\Core\Domain\Services;

use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Domain\EloquentBreadService;
use App\Services\StringHelper;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Translation;
use Parsedown;

class BrandedDocumentService extends EloquentBreadService
{
    public function translateDocumentFromEnglish(BrandedDocument $brandedDocument)
    {
        /** @var DeeplService $deeplService */
        $deeplService = app(DeeplService::class);

        // find english version
        $enDocument = $this->findEnglishVersionFor($brandedDocument);

        if (empty($enDocument)) {
            throw new NotFoundException('Corresponding english document not found');
        }


        if ($enDocument->key != $brandedDocument->key) {
            throw new NotFoundException('Invalid document');
        }
        $translationCache = $deeplService->translate($enDocument->markdown, $enDocument->language, $brandedDocument->language);

        $brandedDocument->markdown = $translationCache->output_text;
        $brandedDocument->save();
        return $brandedDocument;
    }

    public function findLogOfBrandedDocument(BrandedDocument $brandedDocument)
    {
        $key = StringHelper::snake_to_camel($brandedDocument->key);
        return DatabaseNotification::query()->where('type', 'LIKE', '%' . $key)->get();

    }

    public function findOrCreateTranslationForDocument($targetLang, BrandedDocument $englishDocument)
    {

        /** @var DeeplService $deeplService */
        $deeplService = app(DeeplService::class);


        /** @var BrandedDocument $doc */
        $doc = BrandedDocument::query()->firstOrNew(
            [
                'key' => $englishDocument->key,
                'language' => $targetLang,
                'audience' => $englishDocument->audience,
                'program_id' => $englishDocument->program_id
            ],
            [
                'type' => $englishDocument->type,
                'internal_title' => $englishDocument->internal_title . ' (DeepL translated)',
                'distribute_to_tenants' => $englishDocument->distribute_to_tenants
            ]
        );
        if (empty($englishDocument->markdown)) {
            $englishDocument->markdown = $englishDocument->content;
        }
        $translationCache = $deeplService->translate($englishDocument->markdown, $englishDocument->language, $targetLang);

        if (!empty($englishDocument->subject)) {
            $subjectCache = $deeplService->translate($englishDocument->subject, $englishDocument->language, $targetLang);
            $doc->subject = $subjectCache->output_text;
        }


        $doc->markdown = $translationCache->output_text;
        $doc->parseMarkdown();
        $doc->save();
        return $doc;


    }

    public function findDocument($key, $lang)
    {
        return BrandedDocument::query()->where(['key' => $key])->limit(1)->firstOrFail();
    }

    public function findEnglishVersionFor(BrandedDocument $brandedDocument)
    {
        return $this->findDocumentInLanguage($brandedDocument, 'en');
    }

    public function findDocumentInLanguage(BrandedDocument $brandedDocument, $targetLang)
    {
        return BrandedDocument::query()->where(
            [
                'key' => $brandedDocument->key,
                'language' => $targetLang,
                'program_id' => $brandedDocument->program_id,
                'audience' => $brandedDocument->audience,
            ])
            ->limit(1)->first();


    }

    public function findDocumentKeysWithMissingTranslations($lang)
    {
        $allDocuments = BrandedDocument::query()->where(['language' => 'en'])->get();
        $missingList = [];
        /** @var BrandedDocument $doc */
        foreach ($allDocuments as $doc) {
            $exists = BrandedDocument::query()->where(['language' => $lang, 'key' => $doc->key, 'audience' => $doc->audience, 'program_id' => $doc->program_id])->exists();
            if (!$exists) {
                array_push($missingList, $doc);
            }
        }

        return $missingList;
    }
}
