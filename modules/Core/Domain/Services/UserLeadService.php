<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\UserLead;
use Modules\Core\Domain\Models\Webinar;

class UserLeadService extends EloquentBreadService
{
    public function getLeadsOnState(array $states)
    {
        return $this->model->query()
            ->whereIn('state', $states)
            ->get();

    }

    /**
     * @param $email
     * @throws \App\Exceptions\Client\ClientException
     */
    public function eventEmailRegistered($email){

        /** @var UserLead $lead */
        $lead = $this->model->query()
            ->where('email',strtolower($email))
            ->first()
        ;

        if (!$lead){
            return;
        }
        if (!$lead->transitionAllowed('register')){
            return;
        }
        $lead->transition('register');
    }
}
