<?php

namespace Modules\Core\Domain\Services\ContentChooser;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;

class SearchObject
{
    /**
     * The key to search for
     *
     * @var String
     */
    public $key;

    /**
     * The type to search for
     *
     * @var String
     */
    public $type;

    /**
     * The language to search for
     *
     * @var String
     */
    public $lang;

    /**
     * The role to search
     *
     * @var String
     */
    public $role;

    /**
     * Optional Brand to search for
     *
     * @var Modules\Core\Domain\Models\Brand
     */
    public $brand;

    /**
     * Optional Program to search for
     *
     * @var Modules\Core\Domain\Models\Program
     */
    public $program;

    public function __construct(
        String $key,
        String $type,
        String $lang,
        Brand $brand = null,
        Program $program = null,
        $role = null
    ) {
        $this->key = strtolower($key);
        $this->type = $type;
        $this->lang = $lang;

        if ($brand) {
            $this->brand = $brand;
        }
        if ($program) {
            $this->program = $program;
        }
        if ($role) {
            $this->role = $role;
        }
    }

    public function setRole($role){
        $this->role = $role;

    }
    public function toString()
    {
        $program = ($this->program? $this->program->title :'' );
        $brand = ($this->brand? $this->brand->name :'' );

        return " key: $this->key; type: $this->type; lang:$this->lang; role: $this->role; program: $program brand: $brand ";
    }
}
