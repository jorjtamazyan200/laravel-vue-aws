<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;

class OrganizationService extends EloquentBreadService
{

    /**
     * @param Program $program
     */
    public function attachProgram(Organization $organization, Program $program): void
    {
        $organization->programs()->attach($program);
    }

    /**
     * @param Program $program
     */
    public function detachProgram(Organization $organization, Program $program): void
    {
        $organization->programs()->detach($program);
    }
    /**
     *
     * This filter was specially made to find Partiicpations which should be mached.
     *
     * @param Builder $query
     * @param $method where
     * @param $clauseOperator something like =
     * @param $value is passed value
     * @return array
     */
    public function filterPrivatewebinar(Builder $query, $method, $clauseOperator, $value)
    {
        $programId = $value;

        $query
            ->whereHas('programs', function ($query) use ($programId) {
                $query->where('id', $programId);
            })
            ->where('private_webinars', true)
            ;

        return [];
    }

}
