<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\User;

/**
 * Class GoogleMapsService
 * @docs https://developers.google.com/maps/documentation/geocoding/intro#geocoding
 * @package Modules\Core\Domain\Services
 */
class GoogleMapsService
{

    protected $geoCodingUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
    protected $apiKey = 'AIzaSyCLjPgTw2MlMi5QK2LA5teJ-zHtSxpE7c4';

    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * Construct an instance of the Service.
     *
     * @param GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }


    public function lookUpAddress($address)
    {
        // ?address=
        $url = $this->geoCodingUrl . '?address=' . $address;

        $options = [
            'query' => [
                'address' => $address,
                'key' => $this->apiKey
            ]
        ];

        $response = $this->httpClient->request('GET', $url, $options);
        $parsedResponse = json_decode($response->getBody());
        $error = false;

        if ($parsedResponse->status !== 'OK') {
            $error = '[Google Maps] error on request: ' . $parsedResponse->status . ' address: ' . $address;
        }

        if ($error) {
            echo $error . PHP_EOL;
            Log::info($error);
            return null;
        }

        $coordinates = $parsedResponse->results[0]->geometry->location;

        return $coordinates;
    }
}
