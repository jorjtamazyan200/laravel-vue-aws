<?php

namespace Modules\Core\Domain\Services;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Domain\EloquentBreadService;
use HttpInvalidParamException;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\File;
use Modules\Core\Domain\Models\User;

class FileService extends EloquentBreadService
{
    /**
     * Get all Files that belong to a specific Program.
     *
     * @param integer $programId
     * @return Collection
     */
    public function listForProgram(int $programId): Collection
    {
        return $this->model->newQuery()
            ->where('program_id', $programId)
            ->get();
    }

    /**
     * Get all Files that belong to a specific Program and
     * are suitable for a given target audience.
     *
     * @param string $placement see Files::PLACEMENTS
     * @param integer $programId Optional
     * @param string $targetAudience Optional
     * @return Collection
     */
    public function listForProgramAndPlacement(
        string $placement,
        int $programId = null,
        string $targetAudience = null,
        $lectionNumber = null
    ): Collection {
        if ($targetAudience != null && !array_key_exists($targetAudience, User::ROLES)) {
            throw new ClientException("Audience $targetAudience is not known");
        }
        if (!array_key_exists($placement, File::PLACEMENTS)) {
            throw new ClientException("Placement $placement is not known");
        }

        $query = $this->model->newQuery()
            ->where('program_id', $programId);

        if ($targetAudience != null) {
            $query->where(function ($qb) use ($targetAudience) {
                return $qb->where('target_audience', '=', $targetAudience)
                    ->orWhereNull('target_audience');
            });
        }

        if ($lectionNumber != null) {
            $query->where('lection_number', $lectionNumber);
        }

        return $query->get();
    }
}
