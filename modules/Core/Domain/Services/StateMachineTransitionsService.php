<?php

namespace Modules\Core\Domain\Services;

use App\Exceptions\Client\ClientException;
use Illuminate\Support\Facades\Config;
use SM\SMException;

/**
 * Class StateMachineTransitionsService
 * @package Modules\Core\Domain\Services
 */
class StateMachineTransitionsService
{
    /**
     * This method returns the specific instance that the transition should be applied on
     * throws an error if the model does not exist in the state machine, or the id is wrong
     *
     * @param $model_name
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getModelInstance($model_name, $id)
    {
        $config = Config::get('state-machine');

        //checking if the passed model exists in the statemachine
        if (!array_key_exists($model_name, $config)) {
            throw new ClientException("Statemachine Configuration File Not Found for " . $model_name);
        }

        //retrieving the class of the statemachine
        $class = $config[$model_name]['class'];

        //retrieving the instance of the class with the specifid passed id
        $model_instance = $class::find($id);


        //throwing an error if no model with the passed id is found
        if (!$model_instance) {
            throw new ClientException("id passed is invalid");
        }

        return $model_instance;
    }

    /**
     * @param $model_instance
     * @param $transition
     * @throws ClientException
     */
    public function applyTransition($model_instance, $transition)
    {
        try {
            if (!$model_instance->transitionAllowed($transition)) {
                throw new ClientException("Transition is not Allowed: " . $transition);
            }
        } catch (SMException $e) {
            throw new ClientException("Transition does not exist: ". $transition . ' - ' . $e->getMessage());
        }

        $model_instance->transition($transition);
    }
}
