<?php

namespace Modules\Core\Domain\Services;

use App\Infrastructure\Domain\EloquentBreadService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\User;

class ActivityService extends EloquentBreadService
{

    /**
     * @param int $userId
     */
    public function listForUser(int $userId)
    {
        return $this->model->newQuery()
            ->where('user_id', $userId)
            ->get();
    }

    /**
     * So far interface only, should be called when a specific event happened.
     *
     * @param User $user
     * @param array $meta
     */
    public function logActivityForUser(User $user, string $activityName, array $meta = []): void
    {
        if (!in_array($activityName, array_keys(Activity::ACTIVITIES))) {
            throw new \InvalidArgumentException("$activityName unknown");
        }

        $user->points += rand(1, 8) * 50; //@todo: based on the activity, maybe add also an 1)activity log 2) store activites on a central location;
        $user->save();

        $activity = new Activity();
        $activity
            ->fill(['name' => $activityName, 'user_id' => $user->id])
            ->save();
    }

    /**
     *
     * For now it's just 100 points = 1 level, but this is going to change;
     * @param int $currentLevel
     * @return int
     */
    public static function getPointsForNextLevel(int $points): int
    {
        // double the last thounsands.

        return round($points, -3) + 1000;
    }
}
