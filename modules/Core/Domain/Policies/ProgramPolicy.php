<?php

namespace Modules\Core\Domain\Policies;

use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;

class ProgramPolicy
{
    /**
     * User can enroll to any program that is linked to their Organization.
     *
     * @param User $user
     * @param Program $program
     * @return bool
     */
    public function enroll(User $user, Program $program) : bool
    {
        return $user->organization->programs()->where('id', $program->id)->count() > 0;
    }
}
