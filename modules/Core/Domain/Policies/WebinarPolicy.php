<?php

namespace Modules\Core\Domain\Policies;

use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

class WebinarPolicy
{
    /**
     * User can enroll to any Webinar that is linked to a Program that in turn is
     * linked to their Organization.
     *
     * @param User $user
     * @param Webinar $webinar
     * @param int $enrollmentId
     * @return bool
     */
    public function enroll(User $user, Webinar $webinar, int $enrollmentId) : bool
    {
        return $user->organization->programs()->where('id', $webinar->program_id)->count() > 0 &&
            $user->enrollments()->where('id', $enrollmentId)->count() > 0;
    }
}
