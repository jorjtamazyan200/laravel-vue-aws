<?php

namespace Modules\Core\Domain\Policies;

use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;

class OrganizationPolicy
{
    public function addComment(User $authUser, Organization $user) : bool
    {
        return $authUser->isSupport();
    }

    public function listComments(User $authUser, Organization $user) : bool
    {
        return $authUser->isSupport();
    }
}
