<?php

namespace Modules\Core\Listeners\User;


use App\Notifications\User\UserRegistrationCompleteNotification;
use App\Notifications\User\UserRegistrationMobileAcceptedNotification;
use App\Notifications\User\UserRegistrationMobileConfirmationNotification;
use App\Notifications\User\WelcomeBackNotification;
use App\Services\ChatbotService;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Activity;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ActivityService;
use SM\Event\TransitionEvent;

class DefaultUserStateHandler
{

    /**
     * Handle the `complete_registration` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function completeRegistration(TransitionEvent $event): void
    {
        /** @var User $user */
        $user = $event->getStateMachine()->getObject();

        /** @var ActivityService $activityService */
        $activityService = app(ActivityService::class);
        $activityService->logActivityForUser($user, Activity::ACTIVITIES['USER_REGISTERED']);

        $user->notify(new UserRegistrationCompleteNotification($user->brand, $user));

        if ($user->getQualifiedWhatsappNumber()) {
            // confirm whatsapp number if user has whatsapp;

            $chatbotService = app(ChatbotService::class);
            $chatbotService->setUserState($user, ChatbotService::STATES['account_confirmation'], []);

            $user->notify(new UserRegistrationMobileConfirmationNotification($user->brand, $user));
        }

        /** @var Organization $organisation */
        $organisation = $user->organization;

        if ($organisation->transitionAllowed('activate')){
            $organisation->transition('activate');
        }

        Log::debug('Handling "complete_registration" transition', ['event' => $event]);
    }

    /**
     * Handle the `quit` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function quit(TransitionEvent $event): void
    {
        $user = $event->getStateMachine()->getObject();


//        $user->notify(new UserGoodByeNotification($user->brand, $user));

        Log::debug('Handling "quit" transition', ['event' => $event]);
    }

    /**
     * Handle the `return` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function return(TransitionEvent $event): void
    {
        $user = $event->getStateMachine()->getObject();

        $user->notify(new WelcomeBackNotification($user->brand, $user));

        Log::debug('Handling "return" transition', ['event' => $event]);
    }

    /**
     * Handle the `ban` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function ban(TransitionEvent $event): void
    {
        Log::debug('Handling "ban" transition', ['event' => $event]);
    }

    /**
     * Handle the `unban` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function unban(TransitionEvent $event): void
    {
        Log::debug('Handling "unban" transition', ['event' => $event]);
    }
}
