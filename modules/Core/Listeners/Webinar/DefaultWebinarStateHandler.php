<?php

namespace Modules\Core\Listeners\Webinar;

use App\Notifications\Webinar\WebinarCanceledNotification;
use App\Notifications\Webinar\WebinarFinishedNotification;
use App\Notifications\Webinar\WebinarInvitationNotification;
use App\Notifications\Webinar\WebinarRescheduledNotification;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;
use SM\Event\TransitionEvent;

class DefaultWebinarStateHandler
{

    /**
     * Handle the `plan` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function plan(TransitionEvent $event): void
    {
        // nothing;
    }

    /**
     * Handle the `reschedule` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function reschedule(TransitionEvent $event): void
    {
        /** @var Webinar $webinar */
        $webinar = $event->getStateMachine()->getObject();
        $program = $webinar->program;
        $enrollments = $webinar->enrollments;

        foreach ($enrollments as $e) {
            /** @var User $user */
            $user = $e->user;
            $user->notify(new WebinarRescheduledNotification($user->brand, $webinar));
        }

        Log::debug('Handling "reschedule" transition', ['event' => $event]);
    }

    /**
     * Handle the `cancel` transition event.
     *g
     * @param TransitionEvent $event
     * @return void
     */
    public function cancel(TransitionEvent $event): void
    {
        /** @var Webinar $webinar */
        $webinar = $event->getStateMachine()->getObject();
        $enrollments = $webinar->enrollments;

//        $enrollments->load('user');


//        foreach ($enrollments as $e) {
//            /** @var User $user */
//            $user = $e->user;
//            $user->notify(new WebinarCanceledNotification($user->brand, $webinar));
//        }

        Log::debug('Handling "cancel" transition', ['event' => $event]);
    }

    /**
     * Handle the `finish` transition event.
     *
     * @param TransitionEvent $event
     * @return void
     */
    public function finish(TransitionEvent $event): void
    {
        Log::debug('Handling "finish" transition', ['event' => $event]);
    }
}
