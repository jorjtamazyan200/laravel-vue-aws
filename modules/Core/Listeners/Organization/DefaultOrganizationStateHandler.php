<?php

namespace Modules\Core\Listeners\Organization;

use Illuminate\Support\Facades\Log;
use Symfony\Component\EventDispatcher\Event;

class DefaultOrganizationStateHandler
{

    /**
     * Handle the `perform_it_check` transition event.
     *
     * @param Event $event
     * @return void
     */
    public function performItCheck(Event $event) : void
    {
        Log::debug('Handling "perform_it_check" transition', ['event' => $event]);
    }

    /**
     * Handle the `activate` transition event.
     *
     * @param Event $event
     * @return void
     */
    public function activate(Event $event) : void
    {
        Log::debug('Handling "activate" transition', ['event' => $event]);
    }

    /**
     * Handle the `end_contract` transition event.
     *
     * @param Event $event
     * @return void
     */
    public function endContract(Event $event) : void
    {
        Log::debug('Handling "end_contract" transition', ['event' => $event]);
    }
}
