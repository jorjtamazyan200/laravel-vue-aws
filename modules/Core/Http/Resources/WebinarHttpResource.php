<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Carbon\Carbon;

class WebinarHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'id' => $this->id,
            'title' => $this->title,
            'state' => $this->state,
            'lastStateChange' => $this->last_state_change,
            'description' => $this->description,
            'startsAt' => $this->starts_at ? $this->starts_at->toIso8601String() : null,
            'durationMinutes' => $this->duration_minutes,
            'targetAudience' => $this->target_audience,
            'programId' => $this->when(!$this->pivot, function () {
                return $this->program_id;
            }),
            'attended' => $this->whenPivotLoaded('enrollment_webinar', function () {
                return $this->pivot->attended;
            }),
        ];
    }
}
