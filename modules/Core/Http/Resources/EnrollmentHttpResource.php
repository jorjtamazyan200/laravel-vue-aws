<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class EnrollmentHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'program' => new ProgramHttpResource($this->program),
            'role' => $this->role,
            'state' => $this->state,
            'lastStateChange' => $this->last_state_change,
            'participations' => ParticipationHttpResource::collection($this->participations),
            'webinars' => WebinarHttpResource::collection($this->webinars),
        ];
    }
}
