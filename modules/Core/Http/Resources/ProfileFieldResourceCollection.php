<?php

namespace Modules\Core\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProfileFieldResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection
                ->keyBy('code')
                ->map(function ($field) {
                    return $field->value;
                })
        ];
    }
}
