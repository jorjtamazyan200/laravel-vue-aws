<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Core\Domain\Models\Activity;

class ActivityHttpResource extends HttpResource
{
    /**
     * Unset the default "data" wrapper, so that we can return
     * a value without casting it to an array.
     *
     * @var string
     */
    public static $wrap = '';

    /**
     * Transform the resource.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $meta = (isset(Activity::$ACTIVITY_CONFIG[$this->name]) ?
            Activity::$ACTIVITY_CONFIG[$this->name] :
            Activity::$ACTIVITY_CONFIG['DEFAULT'])
        ;
        return [
            'variables' => $this->variables,
            'name' => $this->name,
            'createdAt' => $this->created_at->toIso8601String(),
            'meta' => $meta
        ];
    }
}
