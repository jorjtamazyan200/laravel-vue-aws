<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class GroupedUserHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'email' => $this->email,
            'gender' => $this->gender,
            'primaryRole' => $this->primary_role,
            'groupRole' => $this->pivot->role,
        ];
    }
}
