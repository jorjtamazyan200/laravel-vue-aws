<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Matching\Http\Resources\MatchHttpResource;

class ParticipationHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'enrollmentId' => $this->enrollment->id,
            'match' => new MatchHttpResource($this->match), // wtf?
            'matchConfirmedAt' => $this->match_confirmed_at ? $this->match_confirmed_at->toIso8601String() : null,
            'startMatchingAfter' => $this->start_matching_after ? $this->start_matching_after->toIso8601String() : null,
            'matchingScore' => $this->when(!is_null($this->matching_score), $this->matching_score),
        ];
    }
}
