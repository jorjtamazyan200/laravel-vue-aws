<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;
use Modules\Admin\Http\Resources\UserMinimalHttpResource;

class CommentHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'body' => $this->body,
            'authorId' => $this->author_id,
            'author' => new UserMinimalHttpResource($this->whenLoaded('author')),
            'pinToTop' => $this->pin_to_top,
            'createdAt' => $this->created_at->toIso8601String(),
            'updatedAt' => $this->updated_at->toIso8601String(),
            'type' => $this->type
        ];
    }
}
