<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class ProfileFieldHttpResource extends HttpResource
{
    /**
     * Unset the default "data" wrapper, so that we can return
     * a value without casting it to an array.
     *
     * @var string
     */
    public static $wrap = '';

    /**
     * Transform the resource.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->value
        ];
    }
}
