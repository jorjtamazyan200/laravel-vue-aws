<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class UserInvitationHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uid' => $this->uid,
            'inviting_user_id' => $this->inviting_user_id,
            'invited_user_email' => $this->invited_user_email,
            'invitation_sent_at' => $this->invitation_sent_at,
            'invited_user_id' => $this->invited_user_id,
            'invited_user' => new UserPublicHttpResource($this->invited_user),
            'state' => $this->state,

        ];
    }
}
