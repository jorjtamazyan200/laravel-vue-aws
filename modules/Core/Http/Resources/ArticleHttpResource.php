<?php

namespace Modules\Core\Http\Resources;

use App\Infrastructure\Http\HttpResource;

class ArticleHttpResource extends HttpResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'key' => $this->key,
            'title' => $this->subject,
            'content' => $this->content,
            'brandId' => $this->brand_id,
            'programId' => $this->program_id,
            'language' => $this->language,
            'audience' => $this->audience,
        ];
    }
}
