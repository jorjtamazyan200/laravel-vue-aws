<?php

namespace Modules\Core\Http\Requests;

class AddFeedbackRequest extends AbstractUserRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'response_text' => 'string|nullable',
            'response_scalar' => 'required|numeric',
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return true;
    }
}
