<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Support\Facades\Auth;

class UpdateUserRequest extends AbstractUserRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'email' => 'email|unique:users,email,' . Auth::id(),
            'phone_number' => 'unique:users,phone_number,' . Auth::id(),
            'phone_number_prefix' => 'numeric',
            'whatsapp_number' => 'string|max:255|nullable',
            'whatsapp_setup' => 'string|max:255|nullable',
            'about_me' => 'string|nullable',
            'whatsapp_number_prefix' => 'numeric|nullable',
            'new_password' => 'string|min:5',
            'password' => 'required_with:email,new_password',
            'community_enabled' => 'boolean|nullable',
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return true;
    }
}
