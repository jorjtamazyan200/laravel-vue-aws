<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Participation;

class PostponeParticipationRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'until' => 'required|date',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        if (Auth::user()->isAdmin()) {
            return true;
        }

        $participation = Participation::find($this->route('id'));
        $user = $participation->enrollment->user;

        return $participation && $this->user()->id === $user->id;
    }
}
