<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class ValidateRegistrationCodeRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules() : array
    {
        return [
            'code' => 'required|string',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize() : bool
    {
        return true;
    }
}
