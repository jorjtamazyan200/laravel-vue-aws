<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\GroupService;

class SubmitEnrollmentRequest extends DeserializedFormRequest
{
    protected $enrollmentService;

    public function __construct(
        EnrollmentService $enrollmentService

    ) {
        $this->enrollmentService = $enrollmentService;
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        $enrollment = $this->enrollmentService->get((int)$this->id);
        return Auth::id() == $enrollment->user_id;
    }
}
