<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class CommentRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'body' => 'required|string',
            'type' => 'required|string',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        // Authorization handled in Action.
        return true;
    }
}
