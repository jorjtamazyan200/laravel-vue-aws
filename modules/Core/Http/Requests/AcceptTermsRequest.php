<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;

class AcceptTermsRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'type' => 'required|in:STATEMENT_OF_GOOD_CONDUCT,PRIVACY_AND_GENERAL_BUSINESS_TERMS',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return true;
    }
}
