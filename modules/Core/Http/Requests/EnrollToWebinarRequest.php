<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;

class EnrollToWebinarRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        $enrollment = Enrollment::find($this->id);

        return $enrollment && $this->user()->id === $enrollment->user_id;
    }
}
