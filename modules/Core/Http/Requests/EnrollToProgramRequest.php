<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;

class EnrollToProgramRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'role' => 'in:mentee,mentor',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        // Authorization handled in Action.
        return true;
    }
}
