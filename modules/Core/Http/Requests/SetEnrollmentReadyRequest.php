<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\GroupService;

class SetEnrollmentReadyRequest extends DeserializedFormRequest
{
    protected $enrollmentService;
    protected $groupService;

    public function __construct(
        EnrollmentService $enrollmentService,
        GroupService $groupService
    ) {
        $this->enrollmentService = $enrollmentService;
        $this->groupService = $groupService;
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        if (Auth::user()->isAdmin()) {
            return true;
        }

        return $this->isSupervisorInASharedGroup();
    }

    /**
     * Check whether the current User shares at least one Group with the Enrollments User.
     *
     * @return boolean
     */
    private function isSupervisorInASharedGroup()
    {
        $enrolledUserGroupIds = $this->getUserForEnrollment()
            ->groups->pluck('id')->toArray();

        $sharedGroups = $this->groupService
            ->listSupervisingForUser(Auth::id())
            ->filter(function ($group) use ($enrolledUserGroupIds) {
                return in_array($group->id, $enrolledUserGroupIds);
            });

        return $sharedGroups->count() > 0;
    }

    /**
     * Get the User for the currently requested Enrollment.
     *
     * @return User
     */
    private function getUserForEnrollment()
    {
        $enrollment = $this->enrollmentService->get($this->route('id'));
        return $enrollment->user;
    }
}
