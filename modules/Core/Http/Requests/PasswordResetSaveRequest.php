<?php

namespace Modules\Core\Http\Requests;

use App\Infrastructure\Http\DeserializedFormRequest;

class PasswordResetSaveRequest extends DeserializedFormRequest
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email|exists:password_resets,email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
            'token' => 'required',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authorize()
    {
        return true;
    }
}
