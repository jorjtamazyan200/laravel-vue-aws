<?php

/*
|--------------------------------------------------------------------------
| Module routes
|--------------------------------------------------------------------------
|
| Here's the place to register API routes for the current module. These routes
| are loaded by the RouteServiceProvider within a group which is assigned
| the "api" middleware group and a version prefix, eg. `/api/v1`.
|
 */

use App\Http\Middleware\MapCommentable;

/**
 * Public Scope: Registration
 */
Route::post('/registration-codes/validate', 'Actions\ValidateRegistrationCodeAction');
Route::post('/users/register', 'Actions\RegisterUserAction');
Route::get('/translations/{scope}/{locale}', 'Actions\ListTranslationsAction');
Route::get('/registration-codes/{code}/article', 'Actions\ShowArticleForRegistrationCodeAction');



Route::post('/auth', 'Controllers\AccessTokenController@issueToken');
Route::post('/auth/loginRedirectByEmail', 'Controllers\AccessTokenController@loginRedirectByEmail');



/**
 * Public Scope: Password Reset
 */
Route::post('/auth/password-reset/request', 'Actions\PasswordResetSendEmailAction');
Route::post('/auth/password-reset/save', 'Actions\PasswordResetSaveAction');
Route::post('/auth/exists', 'Actions\UserExistsAction');
Route::get('/translations/{scope}/{locale}.json', 'Actions\ListTranslationsAction');
Route::get('/translations/{scope}/{locale}', 'Actions\ListTranslationsAction');


/**
 * Public Scope: Accepting Invitation
 */

Route::get('/invites/accept/{uid}', 'Actions\AcceptInvitationAction');

Route::get('/alive', 'Actions\AliveAction');
/**
 * Conference Callbacks
 */
// @todo: disable rate limiter here.


Route::post('/conference/authorize/room', 'Actions\Conference\AuthorizeRoomAction')->name('conference.auth.room');
Route::post('/conference/authorize/user', 'Actions\Conference\AuthorizeUserAction')->name('conference.auth.user');


Route::post('/conference/events/callback', 'Actions\Conference\HandleCallbackAction')->name('conference.callback');

Route::post('/conference/messages/rooms/{id}/appointments', 'Actions\Conference\AppointmentCallbackAction')->name('conference.appointmentcallback')->where('id', '[0-9]+');
Route::post('/conference/rooms/{id}/appointments', 'Actions\Conference\AppointmentCallbackAction')->name('conference.appointmentcallback')->where('id', '[0-9]+');

Route::post('/conference/messages/rooms/{id}/videoconnected', 'Actions\Conference\VideoconnectedCallbackAction')->name('conference.videoconnectedCallback')->where('id', '[0-9]+');
Route::post('/conference/rooms/{id}/videoconnected', 'Actions\Conference\VideoconnectedCallbackAction')->name('conference.videoconnectedCallback')->where('id', '[0-9]+');

Route::post('/conference/messages/rooms/{id}/tokboxcredentials', 'Actions\Conference\TokboxCredentialsCallbackAction')->name('conference.tokboxcredentials')->where('id', '[0-9]+');
Route::post('/conference/rooms/{id}/tokboxcredentials', 'Actions\Conference\TokboxCredentialsCallbackAction')->name('conference.tokboxcredentials')->where('id', '[0-9]+');

Route::post('/conference/messages/rooms/{id}/feedbacks', 'Actions\Conference\FeedbackCallbackAction')->name('conference.feedbackcallback')->where('id', '[0-9]+');


Route::post('/conference/rooms/{id}/feedbacks', 'Actions\Conference\FeedbackCallbackAction')->name('conference.feedbackcallback')->where('id', '[0-9]+');


// @deprecated;

// updated API for firebase client
Route::post('/conference2/authorize/room', 'Actions\Conference\AuthorizeRoomAction')->name('conference2.auth.room');
Route::post('/conference2/rooms/{id}/feedbacks', 'Actions\Conference\FeedbackCallbackAction')->name('conference2.feedbackcallback');
//Route::post('/conference2/rooms/{id}', 'Actions\Conference\AuthorizeRoomAction')->name('conference2.auth.room');
Route::get('/conference2/rooms/{id}', 'Actions\Conference\GetRoomAction')->name('conference2.auth.getroom');
Route::post('/conference2/rooms/{id}', 'Actions\Conference\GetRoomAction')->name('conference2.auth.getroomPost');
Route::post('/conference2/users/{id}', 'Actions\Conference\GetUserAction')->name('conference.usergetter')->where('id', '[0-9]+');
Route::post('/conference2/rooms/{id}/appointments', 'Actions\Conference\AppointmentCallbackAction')->name('conference2.appointmentcallback');




///rooms/994/feedbacks
/**
 * Private Scope
 */
Route::group(['middleware' => ['auth']], function () {
    /**
     * Articles
     */
    Route::get('/articles/{E}', 'Actions\ShowArticleAction');

    /**
     * User Profile (/users/me)
     */
    Route::group(['prefix' => '/users/me'], function () {
        // Accept the current terms

        //complete registration
        Route::post('/complete-registration', 'Actions\CompleteRegistrationAction');

        // Statistics about the users' browser
        Route::post('/launch', 'Actions\LaunchSessionAction');
        Route::post('/callcheck', 'Actions\AddCallcheckResultAction')->name('me.addCallcheck');


        // Retrieve/update own user object
        Route::get('/', 'Actions\ShowMeAction');
        Route::put('/', 'Actions\UpdateMeAction');

        // Retrieve all profile fields
        Route::get('/profile', 'Actions\ShowMyProfileAction');
        Route::get('/activities', 'Actions\ShowMyActivities');
        // Mass-update profile fields
        Route::put('/profile', 'Actions\UpdateMyProfileAction');
        Route::put('/avatar', 'Actions\UploadMyAvatarAction');
        Route::delete('/avatar', 'Actions\RemoveMyAvatarAction');
        Route::delete('/', 'Actions\RemoveMeAction');

        // Retrieve/create/update a single profile field
        Route::get('/profile/{code}', 'Actions\ShowMyProfileFieldAction');
        Route::put('/profile/{code}', 'Actions\UpdateMyProfileFieldAction');
        Route::post('/profile/{code}', 'Actions\UpdateMyProfileFieldAction');

        // List own groups
        Route::get('/groups', 'Actions\ListMyGroupsAction');

        Route::get('/nextStep', 'Actions\ShowNextStepAction');

        // List own enrollments
        Route::get('/enrollments', 'Actions\ListMyEnrollmentsAction');

        // List own participations
        Route::get('/participations', 'Actions\ListMyParticipationsAction');

        Route::get('/keyaccount', 'Actions\ShowKeyaccountAction');
    });

    /**
     * Groups
     */
    Route::get('/groups/{id}', 'Actions\ShowGroupAction')->where('id', '[0-9]+');
    Route::get('/groups/{id}/password', 'Actions\ShowGroupPasswordAction')->where('id', '[0-9]+');
    Route::get('/groups/{id}/users', 'Actions\ListUsersForGroupAction')->where('id', '[0-9]+');

    /**
     * Enrollments
     */
    Route::get('/enrollments/{id}', 'Actions\ShowEnrollmentAction')->where('id', '[0-9]+');
    Route::get('/enrollments/{id}/type', 'Actions\ShowEnrollmentTypeAction')->where('id', '[0-9]+');
    Route::get('/enrollments/{id}/videowebinar', 'Actions\ShowVideoWebinarOfEnrollmentAction')->where('id', '[0-9]+');
    Route::post('/enrollments/{id}/videowebinar', 'Actions\SubmitVideoWebinarOfEnrollmentAction')->where('id', '[0-9]+');

    //api/v2/enrollments/${key}/articles/${id}
    Route::get('/enrollments/{id}/articles/{slug}', 'Actions\ShowEnrollmentArticleAction')->where('id', '[0-9]+');
    Route::post('/enrollments/{id}/set-ready', 'Actions\SetEnrollmentReadyAction')->where('id', '[0-9]+');

    Route::post('/enrollments/{id}/submit', 'Actions\SubmitEnrollmentAction')->where('id', '[0-9]+');
//    Route::post('/enrollments/{id}/postpone', 'Actions\PostponeEnrollmentAction');

    /**
     * Participations
     */
    Route::get('/participations/{id}', 'Actions\ShowParticipationAction')->where('id', '[0-9]+');
    Route::post('/participations/{id}/postpone', 'Actions\PostponeParticipationAction')->where('id', '[0-9]+');

    /**
     * Programs
     */
    Route::get('/programs', 'Actions\ListProgramsAction');

    Route::get('/programs/{id}', 'Actions\ShowProgramAction')->where('id', '[0-9]+');
    Route::post('/programs/{id}/enroll', 'Actions\EnrollToProgramAction')->where('id', '[0-9]+');

    // This endpoint is not used yet, potentially remove it.
    Route::get('/programs/{id}/files', 'Actions\ListFilesForProgramAction')->where('id', '[0-9]+');
    Route::get('/files/{placement}', 'Actions\ListFilesForPlacementAction'); // optional: program && target_audience
    Route::put('/programs/{id}/logo', 'Actions\UploadProgramLogoAction')->where('id', '[0-9]+');
    Route::delete('/programs/{id}/logo', 'Actions\RemoveProgramLogoAction')->where('id', '[0-9]+');
    Route::put('/programs/{id}/cover', 'Actions\UploadProgramCoverAction')->where('id', '[0-9]+');
    Route::post('/programs/{id}/cover', 'Actions\UploadProgramCoverAction')->where('id', '[0-9]+');
    Route::delete('/programs/{id}/cover', 'Actions\RemoveProgramCoverAction')->where('id', '[0-9]+');
    Route::post('/programs/{id}/detail-photo', 'Actions\UploadProgramDetailPhotoAction')->where('id', '[0-9]+');
    Route::put('/programs/{id}/detail-photo', 'Actions\UploadProgramDetailPhotoAction')->where('id', '[0-9]+');
    Route::delete('/programs/{id}/detail-photo', 'Actions\RemoveProgramDetailPhotoAction')->where('id', '[0-9]+');

    /**
     * Webinars
     */
    Route::get('/programs/{id}/webinars', 'Actions\ListWebinarsForProgramAction')->where('id', '[0-9]+');
    Route::get('/enrollments/{id}/webinars', 'Actions\ListWebinarsForEnrollmentAction')->where('id', '[0-9]+');
    Route::post('/enrollments/{id}/webinars/{webinar_id}/enroll', 'Actions\EnrollToWebinarAction')->where('id', '[0-9]+');

    /**
     * Comments
     */
    Route::group(['middleware' => [MapCommentable::class], 'prefix' => '/{commentable_name}/{commentable_id}/comments'], function () {
        Route::get('/', 'Actions\ListCommentsAction');

        Route::get('/{comment_id}', 'Actions\ShowCommentAction')->where('comment_id', '[0-9]+');
        Route::post('/', 'Actions\AddCommentAction');
        Route::put('/{comment_id}', 'Actions\UpdateCommentAction')->where('comment_id', '[0-9]+');
        Route::delete('/{comment_id}', 'Actions\DeleteCommentAction')->where('comment_id', '[0-9]+');


        Route::get('/admin', 'Actions\ListAdminCommentsAction');
        Route::post('/admin', 'Actions\AddCommentAction');
        Route::put('/admin/{comment_id}', 'Actions\UpdateCommentAction');

    });

    /**
     * Admin Scope
     */
    Route::group(['middleware' => ['scope:admin']], function () {
        /**
         * Groups
         */
        Route::post('/groups/{id}/members', 'Actions\AddUserToGroupAction');

        /**
         * Webinars
         * @todo: shouldn't be those in admin bundle then?
         */
        Route::post(
            '/webinars/{webinarId}/attendees/{enrollmentId}/attended',
            'Actions\MarkWebinarAttendeeAsAttendedAction'
        );
        Route::post(
            '/webinars/{webinarId}/attendees/{enrollmentId}/unenroll',
            'Actions\MarkWebinarAttendeeAsUnenrolledAction'
        );
        Route::post(
            '/webinars/{webinarId}/attendees/{enrollmentId}/missed',
            'Actions\MarkWebinarAttendeeAsMissedAction'
        );
    });

    /**
     * State Machine
     */

    Route::get('/statemachine/{model_name}', 'Actions\ListStateMachineConfigrationForModel');
    Route::post('/statemachine/{model_name}/{id}/{transition}', 'Actions\ApplyTransitionOnModelAction');

    /**
     * User Invitation
     */
    Route::post('/invites', 'Actions\InviteUserAction');

    Route::get('/invites', 'Actions\ListInvitedUsersAction');

    Route::post('/feedbacks', 'Actions\AddFeedbackAction');
    Route::post('/acceptpolicies', 'Actions\AcceptPoliciesAction');

    Route::post('/me/acceptpolicies', 'Actions\AcceptPoliciesAction');
    Route::post('/me/requestoptin', 'Actions\RequestOptinAgainAction')
        ->name('me.requestoptin');



});

Route::get('/public/browserupdate', 'Actions\BrowserupdateAction')
    ->name('public.browserupdate');;

    Route::get('/public/privacypolicy', 'Actions\ShowPrivacyPolicyAction')
    ->name('public.privacypolicy');;


Route::get('/response/{action}/{userid}/{secret}', 'Actions\GeneralEmailResponseAction')
    ->where('userid', '[0-9]+')
    ->name('email.response.general');;

Route::get('/participation-response/{action}/{userid}/{participationId}/{secret}', 'Actions\ParticipationMailResponseAction')
    ->where('userid', '[0-9]+')
    ->where('participationId', '[0-9]+')
    ->name('email.response.participation');

    Route::get('/lead-response/{action}/{id}/{secret}', 'Actions\UserLeadMailResponseAction')
    ->where('id', '[0-9]+')
    ->name('email.response.userlead');;

Route::get('/confirmmigration/{secret}/{id}', 'Actions\ShowConfirmMigrationAction')
    ->where('id', '[0-9]+')
    ->name('email.migration.confirm');;

