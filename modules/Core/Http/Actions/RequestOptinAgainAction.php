<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Notifications\User\UserOptinAgainNotification;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Requests\EnrollToWebinarRequest;

class RequestOptinAgainAction extends Action
{
    protected $enrollmentService;
    protected $responder;

    public function __construct(
        EnrollmentService $enrollmentService,
        ResourceResponder $responder
    )
    {
        $this->enrollmentService = $enrollmentService;
        $this->responder = $responder;
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function __invoke()
    {

        $user = Auth::user();

        $notification = new UserOptinAgainNotification($user->brand, $user);

        $user->notify($notification);

        return response()->json(['status' => 'ok']);


    }
}
