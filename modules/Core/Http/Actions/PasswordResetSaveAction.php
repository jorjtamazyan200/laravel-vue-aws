<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\PasswordResetSaveRequest;

class PasswordResetSaveAction extends Action
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param PasswordResetSaveRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \App\Exceptions\Client\ValidationException
     */
    public function __invoke(PasswordResetSaveRequest $request)
    {
        $this->userService->resetPassword($request->only([
            'email', 'password', 'password_confirmation', 'token',
        ]));

        return response('ok', 200);
    }
}
