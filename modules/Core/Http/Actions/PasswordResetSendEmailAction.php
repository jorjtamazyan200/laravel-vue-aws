<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\PasswordResetSendEmailRequest;

class PasswordResetSendEmailAction extends Action
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(PasswordResetSendEmailRequest $request)
    {
        $email = strtolower($request->input('email'));

        $this->userService->sendPasswordResetEmail($email);

        return response('ok', 200);
    }
}
