<?php

namespace Modules\Core\Http\Actions;

use Illuminate\Http\Request;
use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Response;
use Modules\Core\Domain\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class DeleteCommentAction extends Action
{
    /**
     * @param Request $request
     * @throws NotAuthorizedException
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $user = Auth::user();
        $comment_id = $request->comment_id;
        if ($comment = Comment::find($comment_id)) {
            if ($user->cannot('deleteComment', $comment)) {
                throw new NotAuthorizedException;
            }

            $comment->delete();

            return response('', 200);
        }

        return abort(404);
    }
}
