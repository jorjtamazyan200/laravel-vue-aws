<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\TimeHelper;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\ProfileFieldService;
use Modules\Core\Http\Resources\ProfileFieldHttpResource;
use Modules\Matching\Domain\Models\Match;

class ShowNextStepAction extends Action
{


    protected $responder;

    public function __construct(ProfileFieldService $profileFieldService, ResourceResponder $responder)
    {
        $this->profileFieldService = $profileFieldService;
        $this->responder = $responder;
    }

    public function __invoke()
    {


        /**
         * @var Enrollment $enrollment
         * @var User $user
         * @var Match $match
         */
        $user = Auth::user();

        if ($user->primary_role === Role::ROLES['supervisor']) {
            return $this->response("SUPERVISOR", []);
        }

        list($code, $response) = $this->findEnrollmentIssues($user);

        if ($response) {
            return $this->response($code, $response);
        }

        list($code, $response) = $this->findUnconfirmedMatch($user);
        if ($response) {
            return $this->response($code, $response);
        }

        return $this->response("DEFAULT", ['action' => 'callcheck']);
    }

    private function response($code, $data)
    {

        return response()
            ->json(['data' => ['state' => $code, 'variables' => $data]])
            ->setStatusCode(200);
    }

    private function findEnrollmentIssues($user)
    {
        $enrollments = $user->enrollments;

        $enrollment = $enrollments->firstWhere('state', Enrollment::STATES['NEW']);
        if ($enrollment) {
            return [
                'INCOMPLETE_ENROLLMENT',
                ['id' => $enrollment->id, 'programTitle' => $enrollment->program->title, 'action' => 'enrollment']
            ];
        }
        $enrollment = $enrollments->firstWhere('state', Enrollment::STATES['WAITINGLIST']);
        if ($enrollment) {
            return [
                'ENROLLMENT_WAITINGLIST',
                ['id' => $enrollment->id, 'programTitle' => $enrollment->program->title]
            ];
        }

        $enrollment = $enrollments->firstWhere('state', Enrollment::STATES['TRAINING']);
        if ($enrollment) {
            $webinar = $enrollment->webinars()->where('starts_at', '>', Carbon::now())->first(); // @todo: where startsAt >  now
            if ($webinar) {
                return [
                    'WAITING_FOR_TRAINING',
                    [
                        'id' => $enrollment->id,
                        'startsAt' => TimeHelper::formatForUser($user, $webinar->starts_at),
                        'programTitle' => $enrollment->program->title,
                    ]
                ];
            }
            return [
                'NO_TRAINING',
                [
                    'id' => $enrollment->id,
                    'programTitle' => $enrollment->program->title,
                    'action' => 'enrollmentWebinar'
                ]
            ];
        }
        return [false, false];
    }


    private function findUnconfirmedMatch($user)
    {
        $enrollments = $user->enrollments;
        $foundMatch = null;
        $activeMentorMatches = new Collection(); // only mentor matches;
        $unconfirmedMatches = new Collection();

        if ($enrollments->count() === 0) {
            return ['NO_ENROLLMENT', ['action' => 'select_program']];
        }


        foreach ($enrollments as $enrollment) {
            if ($enrollment->role != Role::ROLES['mentor']) {
                continue;
            }
            foreach ($enrollment->participations as $participation) {
                if (!$participation->match) {
                    continue;
                }
                if ($participation->match->state === Match::STATES['UNCONFIRMED']) {
                    $unconfirmedMatches->push($participation->match);
                    continue;
                }
                if (!$participation->match->isActive()) {
                    continue;
                }

                $activeMentorMatches->push($participation->match);

            }
        }



        if ($unconfirmedMatches->count() > 0) {
            $unconfirmed = $unconfirmedMatches->first();
            return ['UNCONFIRMED_MATCH', ['id' => $unconfirmed->id, 'action' => 'match']];
        }

        $noUpcoming = $activeMentorMatches->first(function ($match, $key) {
            return $match->appointments()->where('planned_start', '>', Carbon::now())->count() === 0;
        });
        if ($noUpcoming) {
            return ['NO_UPCOMING_APPOINTMENT', ['id' => $noUpcoming->id, 'action' => 'match', 'state' => $noUpcoming->state]];
        }

        return [false, false];
    }
}
