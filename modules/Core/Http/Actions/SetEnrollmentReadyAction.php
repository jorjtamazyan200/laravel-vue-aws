<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Http\Requests\SetEnrollmentReadyRequest;

class SetEnrollmentReadyAction extends Action
{
    protected $enrollmentService;
    protected $groupService;

    public function __construct(
        EnrollmentService $enrollmentService,
        GroupService $groupService
    ) {
        $this->enrollmentService = $enrollmentService;
        $this->groupService = $groupService;
    }

    public function __invoke(int $id, SetEnrollmentReadyRequest $request)
    {
        $currentUser = Auth::user();
        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->get($id);

        //statemachine
        $enrollment->transition('complete_training');

        return response('', 200);
    }
}
