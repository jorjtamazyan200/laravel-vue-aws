<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Actions\UploadProgramImageAction;

class UploadProgramCoverAction extends UploadProgramImageAction
{
    protected $attachmentName = 'cover';
}
