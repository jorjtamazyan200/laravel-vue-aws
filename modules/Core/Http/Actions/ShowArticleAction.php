<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Core\Http\Resources\ArticleHttpResource;

class ShowArticleAction extends Action
{
    protected $responder;

    public function __construct(ResourceResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke(string $key, Request $request)
    {
        $user = Auth::user();
        $program = $request->program_id ?
            // Allow a user to brand documents only to programs available to their organization
            $user->organization->programs()->find($request->program_id) :
            null;

        $key = strtolower($key);

        $searchBy = new SearchObject(
            $key,
            'article',
            $user->preferred_language,
            $user->brand,
            $program
        );
        $brandedDocument = ContentChooser::findDocument($searchBy);

        // views
        if (!$brandedDocument) {
            return abort(404);
        }

        $placeholderData = ['user' => $user];
        if ($program) {
            $placeholderData['program'] = $program;
        }
        $brandedDocument->content = $brandedDocument->parseContent($placeholderData);
        $brandedDocument->views = $brandedDocument->views + 1;
        $brandedDocument->save();


        return $this->responder->send($brandedDocument, ArticleHttpResource::class);
    }
}
