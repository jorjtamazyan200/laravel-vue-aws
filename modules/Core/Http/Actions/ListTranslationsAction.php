<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Modules\Core\Domain\Services\TranslationService;

class ListTranslationsAction extends Action
{
    protected $translationService;
    protected $responder;

    public function __construct(TranslationService $translationService, ResourceResponder $responder)
    {
        $this->translationService = $translationService;
        $this->responder = $responder;
    }

    public function __invoke(string $scope, string $locale)
    {
        if (strlen($locale) > 2) {
            $locale = substr($locale, 0, 2);
        }
        $translations = $this->translationService::retrieveTranslationForLocaleAndScope($locale, $scope);

        return response($translations)
            ->header('Content-Type', 'application/json');
    }
}
