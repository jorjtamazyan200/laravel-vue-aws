<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;
use Modules\Core\Http\Requests\EnrollToProgramRequest;

class EnrollToProgramAction extends Action
{
    protected $enrollmentService;
    protected $programService;
    protected $responder;

    public function __construct(
        EnrollmentService $enrollmentService,
        ProgramService $programService,
        ResourceResponder $responder
    )
    {
        $this->enrollmentService = $enrollmentService;
        $this->programService = $programService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, EnrollToProgramRequest $request)
    {
        /** @var Program $program */
        $program = $this->programService->get($id);
        $user = Auth::user();

        if ($user->cannot('enroll', $program)) {
            return response('You are not allowed to enroll into this program', 403);
        }
        $role = $request->input('role') ?: $user->primary_role;

        // @todo:

        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->firstOrCreate($user->id, $program->id, $role);

        if ($enrollment->state === Enrollment::STATES['WAITINGLIST']) {
            return response('You are currently on the waiting list and therefore cannot enroll', 422);
        }

        $enrollment->last_state_change_at = Carbon::now();
        $enrollment->save();

        return $this->responder->send($enrollment, EnrollmentHttpResource::class);
    }
}
