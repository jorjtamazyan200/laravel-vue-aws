<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class RemoveProgramImageAction extends Action
{
    protected $programService;
    protected $responder;
    protected $attachmentName;

    public function __construct(ProgramService $programService, ResourceResponder $responder)
    {
        $this->programService = $programService;
        $this->responder = $responder;
    }

    public function __invoke(int $id)
    {
        $program = $this->programService->getForUser(Auth::id(), $id);
        $program->removeImageAttachment($this->attachmentName);

        return $this->responder->send($program, ProgramHttpResource::class);
    }
}
