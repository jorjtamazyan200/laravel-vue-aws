<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ProfileFieldService;
use Modules\Core\Http\Requests\UpdateProfileFieldRequest;
use Modules\Core\Http\Resources\ProfileFieldHttpResource;

class UpdateMyProfileFieldAction extends Action
{
    protected $profileFieldService;
    protected $responder;

    public function __construct(ProfileFieldService $profileFieldService, ResourceResponder $responder)
    {
        $this->profileFieldService = $profileFieldService;
        $this->responder = $responder;
    }

    public function __invoke(string $code, UpdateProfileFieldRequest $request)
    {
        $code = strtolower($code);
        $profileField = $this->profileFieldService->createOrUpdateForUser(
            Auth::id(),
            $code,
            $request->input('data')
        );

        return $this->responder->send($profileField, ProfileFieldHttpResource::class);
    }
}
