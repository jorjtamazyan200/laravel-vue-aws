<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ListStateMachineConfigrationForModel extends Action
{
    protected $responder;
    protected $stateMachine;

    public function __construct(ResourceResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke($model_name, Request $request)
    {
        $config = Config::get('state-machine');

        if (!array_key_exists($model_name, $config)) {
            throw new \Exception("Configuration File Not Found for ". $model_name);
        }

        $responseJson= $config[$model_name];
        $responseJson = [
                'states' => $responseJson['states'],
                'transitions' => $responseJson['transitions']
        ];

        return response()->json($responseJson);
    }
}
