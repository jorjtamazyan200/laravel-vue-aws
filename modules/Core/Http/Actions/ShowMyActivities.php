<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ActivityService;
use Modules\Core\Domain\Services\ProfileFieldService;
use Modules\Core\Http\Resources\ActivityHttpResource;
use Modules\Core\Http\Resources\ProfileFieldHttpResource;

class ShowMyActivities extends Action
{
    /**
     * @var ActivityService
     */
    protected $activityService;
    protected $responder;

    public function __construct(ActivityService $activityService, ResourceResponder $responder)
    {
        $this->activityService = $activityService;
        $this->responder = $responder;
    }

    public function __invoke()
    {


        $activities = $this->activityService->listForUser(Auth::id());


        return $this->responder->send($activities, ActivityHttpResource::class);
    }
}
