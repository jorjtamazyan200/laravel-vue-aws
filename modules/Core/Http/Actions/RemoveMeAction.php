<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\UserHttpResource;
use Illuminate\Http\Request;

class RemoveMeAction extends Action
{
    protected $userService;
    protected $responder;

    public function __construct(UserService $userService, ResourceResponder $responder)
    {
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke()
    {

        /** @var User $me */
        $me = $this->userService->get(Auth::id());

        $this->userService->quitUser($me);
        $me->anonymize();
        $me->save();

        return $this->responder->send($me, UserHttpResource::class);
    }
}
