<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AcceptTermsRequest;

class AliveAction extends Action
{
    public function __invoke(Request $request)
    {






        $response  = [
            'deployment' => $this->getDeploymentInfo(),
            'date' => Carbon::now()->toW3cString(),
            'version' => '2.0'
        ];

        return response()->json($response);
    }
    private function getDeploymentInfo(){

        $deploymentInfo = Storage::disk('local')->get('deploymentinfo.json');
        if (!$deploymentInfo){
            return [];
        }
        $deploymentInfo = json_decode($deploymentInfo);
        return $deploymentInfo;


    }
}
