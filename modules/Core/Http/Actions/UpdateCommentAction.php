<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Requests\CommentRequest;
use App\Infrastructure\Http\ResourceResponder;
use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class UpdateCommentAction extends Action
{
    public function __construct(ResourceResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke(CommentRequest $request)
    {
        $user = Auth::user();
        $comment_id = $request->comment_id;
        if ($comment = Comment::find($comment_id)) {
            if ($user->cannot('updateComment', $comment)) {
                throw new NotAuthorizedException;
            }

            $comment->update($request->except(['commentable_type', 'commentable_id', 'author_id']));

            return $this->responder->send($comment, CommentHttpResource::class);
        }

        return abort(404);
    }
}
