<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\FileService;
use Modules\Core\Http\Resources\FileHttpResource;

/**
 *
 * Class ListFilesForProgramAction
 * @deprecated
 * @package Modules\Core\Http\Actions
 */
class ListFilesForProgramAction extends Action
{
    protected $enrollmentService;
    protected $fileService;
    protected $responder;

    public function __construct(
        FileService $fileService,
        ResourceResponder $responder
    ) {
        $this->fileService = $fileService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        $files = $this->fileService->listForProgram($id);

        return $this->responder->send($files, FileHttpResource::class);
    }
}
