<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Http\Requests\AddUserToGroupRequest;

class AddUserToGroupAction extends Action
{
    protected $groupService;

    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    public function __invoke(int $id, AddUserToGroupRequest $request)
    {
        $group = $this->groupService->get($id);

        $group->users()->attach($request->input('user_id'), [
            'role' => (bool) $request->input('is_supervisor') ? 'supervisor' : 'member'
        ]);

        return response('', 200);
    }
}
