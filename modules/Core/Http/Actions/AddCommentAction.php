<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Http\Requests\CommentRequest;
use App\Infrastructure\Http\ResourceResponder;
use App\Infrastructure\Http\Action;
use Modules\Core\Domain\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class AddCommentAction extends Action
{
    public function __construct(ResourceResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke(CommentRequest $request)
    {
        $user = Auth::user();

        // commentable_name

        $commentable_instance = $request->commentable_instance;

        if ($user->cannot('addComment', $commentable_instance)) {
            throw new NotAuthorizedException;
        }

        $comment = new Comment($request->all());
        $comment->commentable_type = $request->commentable_fqn;
        $comment->commentable_id = $commentable_instance->id;
        $comment->type = $request->type;
        $comment->author_id = $user->id;
        $comment->save  ();

        $comment->refresh(); // Retreive default value for pin_to_top instead of null

        return $this->responder->send($comment, CommentHttpResource::class);
    }
}
