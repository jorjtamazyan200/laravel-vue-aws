<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Http\Resources\EnrollmentHttpResource;

class ShowEnrollmentTypeAction extends Action
{
    protected $enrollmentService;
    protected $responder;

    public function __construct(EnrollmentService $enrollmentService, ResourceResponder $responder)
    {
        $this->enrollmentService = $enrollmentService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->getForUser($user->id, $id);
        // @todo find if the pivottable between organization and program contains a special enrollemt ue this, otherwise us the one from program


        /** @var Program $program */
        $program = $enrollment->program;
        $enrollmentType = $program->getEnrollmentTypeAttribute();

        // has custom enrollment in pivot table?
        try {
            $customEnrollment = $user->organization->programs()->findOrFail($program->id, ['program_id'])->pivot->enrollment_type;
            if ($customEnrollment){
                $enrollmentType = $customEnrollment;
            }
        } catch (ModelNotFoundException $exception){
            Log::error("Program " . $program->id ." is not avaialble to organiazation " . $user->organization_id ." but user enrolls!!!");
        }



        return response()->json([
            'data' => [
                'type' => $enrollmentType
            ]
        ])->setStatusCode(200);

        return $this->responder->send($enrollment, EnrollmentHttpResource::class);
    }
}
