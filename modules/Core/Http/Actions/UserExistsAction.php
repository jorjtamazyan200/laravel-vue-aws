<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\PasswordResetSaveRequest;

class UserExistsAction extends Action
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(Request $request)
    {


        $exists = User::where('email', $request->input('email'))->exists();

        if ($exists === false) {
            return response('not found', 404);
        }

        return response('ok', 200);
    }
}
