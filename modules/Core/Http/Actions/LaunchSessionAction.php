<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\UserLaunch;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\LaunchSessionRequest;

class LaunchSessionAction extends Action
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Update the User's timezone and log their Browser Version to the console.
     *
     * @param LaunchSessionRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(LaunchSessionRequest $request)
    {
        $myId = Auth::id();

        $timezone = $request->input('timezone');
        $timezone2 = $request->input('timezone2');
        $offset = $request->input('offset');

        if ($timezone && $this->isValidTimezone($timezone)) {
            $me = $this->userService->update($myId, [
                'timezone' => $timezone,
            ]);
        }

        $userAgent = $request->header('User-Agent');

        $userLaunch = new UserLaunch();
        $userLaunch->fill([
            'useragent' => $userAgent,
            'timezone' => $timezone,
            'timezone2' => $timezone2,
            'user_id' => $myId,
            'offset' => $offset
        ]);
        $userLaunch->save();

//        Log::info("New session for User #$myId with User-Agent \"$userAgent\" and timezone $timezone");

        return response('', 200);
    }
    /**
     * Check if a string is a valid timezone
     *
     * timezone_identifiers_list() requires PHP >= 5.2
     *
     * @param string $timezone
     * @return bool
     */
    private function isValidTimezone($timezone) {
        return in_array($timezone, timezone_identifiers_list());
    }

}
