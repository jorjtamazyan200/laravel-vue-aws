<?php

namespace Modules\Core\Http\Actions;

use Illuminate\Http\Request;
use App\Infrastructure\Http\ResourceResponder;
use App\Infrastructure\Http\Action;
use Modules\Core\Http\Requests\CommentRequest;
use Modules\Core\Http\Resources\CommentHttpResource;
use Modules\Core\Domain\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class ShowCommentAction extends Action
{
    public function __construct(ResourceResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke(CommentRequest $request)
    {
        $user = Auth::user();
        $comment_id = $request->comment_id;
        $commentable_instance = $request->commentable_instance;

        if ($comment = Comment::find($comment_id)) {
            if ($user->cannot('showComment', [$comment, $commentable_instance])) {
                throw new NotAuthorizedException;
            }

            return $this->responder->send($comment, CommentHttpResource::class);
        }

        return abort(404);
    }
}
