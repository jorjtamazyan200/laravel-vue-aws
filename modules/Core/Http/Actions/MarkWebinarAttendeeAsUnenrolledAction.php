<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\WebinarService;

class MarkWebinarAttendeeAsUnenrolledAction extends Action
{
    protected $enrollmentService;
    protected $webinarService;

    public function __construct(EnrollmentService $enrollmentService, WebinarService $webinarService)
    {
        $this->enrollmentService = $enrollmentService;
        $this->webinarService = $webinarService;
    }

    public function __invoke(int $webinarId, int $enrollmentId, Request $request)
    {
        /** @var Webinar $webinar */
        $webinar = $this->webinarService->get($webinarId);
        /** @var Enrollment $enrollment */
        $enrollment = $this->enrollmentService->get($enrollmentId);

        $this->webinarService->unenrollUser($enrollment->id, $webinar);

        return response('', 200);
    }
}
