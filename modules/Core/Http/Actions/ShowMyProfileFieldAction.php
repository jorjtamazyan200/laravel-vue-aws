<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ProfileFieldService;
use Modules\Core\Http\Resources\ProfileFieldHttpResource;

class ShowMyProfileFieldAction extends Action
{
    protected $profileFieldService;
    protected $responder;

    public function __construct(ProfileFieldService $profileFieldService, ResourceResponder $responder)
    {
        $this->profileFieldService = $profileFieldService;
        $this->responder = $responder;
    }

    public function __invoke(string $code)
    {
        $code = strtolower($code);


        try {
            $profileField = $this->profileFieldService->getForUser(Auth::id(), $code);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['data' => null]);
        }

        return $this->responder->send($profileField, ProfileFieldHttpResource::class);
    }
}
