<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Services\ContentChooser\ContentChooser;
use Modules\Core\Domain\Services\ContentChooser\SearchObject;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Http\Resources\ArticleHttpResource;

class ShowEnrollmentArticleAction extends Action
{
    protected $responder;
    protected $enrollmentService;

    public function __construct(ResourceResponder $responder, EnrollmentService $enrollmentService)
    {
        $this->responder = $responder;
        $this->enrollmentService = $enrollmentService;
    }

    public function __invoke(int $id, string $slug , Request $request)
    {
        $user = Auth::user();
        $enrollment = $this->enrollmentService->get($id);

        $program = $enrollment->program;

        $key = strtolower($slug);


        $searchBy = new SearchObject(
            $key,
            'article',
            $user->preferred_language,
            $user->brand,
            $program,
            $enrollment->role
        );

        if ($brandedDocument = ContentChooser::findDocument($searchBy)) {
            $placeholderData = ['user' => $user];
            if ($program) {
                $placeholderData['program'] = $program;
            }
            $brandedDocument->content = $brandedDocument->parseContent($placeholderData);

            return $this->responder->send($brandedDocument, ArticleHttpResource::class);
        }

        return abort(404);
    }
}
