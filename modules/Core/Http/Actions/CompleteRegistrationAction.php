<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;

class CompleteRegistrationAction extends Action
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(Request $request)
    {
        $user_id = Auth::id();

        /** @var User $user */
        $user = $this->userService->get($user_id);

        // this allows to run the transition twice in case the user clicks multiple times, the
        // transition will only be run once.


        if ($user->transitionAllowed('complete_registration')) {
            $user->transition('complete_registration');
        }

        return response('', 200);
    }
}
