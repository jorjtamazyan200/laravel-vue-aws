<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ProfileFieldService;
use Modules\Core\Http\Resources\ProfileFieldResourceCollection;

class UpdateMyProfileAction extends Action
{
    protected $profileFieldService;
    protected $responder;

    public function __construct(ProfileFieldService $profileFieldService, ResourceResponder $responder)
    {
        $this->profileFieldService = $profileFieldService;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $myId = Auth::id();
        $profileFields = $this->profileFieldService
            ->createOrUpdateManyForUser($myId, $request->all());

        return $this->responder->send($profileFields, ProfileFieldResourceCollection::class);
    }
}
