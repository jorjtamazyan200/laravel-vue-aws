<?php

namespace Modules\Core\Http\Actions\Conference;

use App\Infrastructure\Http\Action;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

class VideoconnectedCallbackAction extends Action
{
    protected $matchService;
    protected $appointmentService;

    /**
     * Construct an instance of the action.
     */
    public function __construct(MatchService $matchService, AppointmentService $appointmentService)
    {
        $this->middleware('verify-conference-signature');
        $this->matchService = $matchService;
        $this->appointmentService = $appointmentService;
    }

    /**
     * Handle the request.
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request, $id)
    {

        /** @var Match $match */
        $match = $this->matchService->getByExternalRoomId($id);

        if (!$match) {
            Log::error("[Conference Callback] Got Video Connected event for room " . $id . " but did not find any match!");
            return;
        }
        /** @var Appointment $appointment */
        $appointment = $this->appointmentService->getCurrentAppointmentForMatch($match->id);

        if (!$appointment) {
            // create appointment if users meet and no appointment exists;
            $data = ['planned_start' => Carbon::now(), 'state' => Appointment::STATES['PLANNED'], 'match_id' => $match->id];
            $appointment = $this->appointmentService->create($data);
        }

        if ($appointment->transitionAllowed('start')) {
            $appointment->transition('start');
        }

        Log::info("[Conference Callback] Got Video Connected event for room " . $id);


        return response()->json([
            'appointment' => $appointment->id
        ])->setStatusCode(200);
    }
}
