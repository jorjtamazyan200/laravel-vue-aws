<?php

namespace Modules\Core\Http\Actions\Conference;

use App\Infrastructure\Http\Action;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class HandleCallbackAction extends Action
{
    /**
     * Construct an instance of the action.
     */
    public function __construct()
    {
        $this->middleware('verify-conference-signature');
    }

    /**
     * Handle the request.
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $event = $request->input('event');



        switch ($event['type']) {
            case 'appointment.start':
                Log::debug('[HandleCallbackAction] Appointment started:', ['event' => $event]);

                break;

            case 'appointment.end':
                // TODO: Save actual_start and duration to Appointment
                $log = sprintf('[ConferenceCallback] %s with parameter: %s'.PHP_EOL, json_encode($event), json_encode($request->all()));
                Log::info($log);

                // {"type":"appointment.end","payload":{"room":25,"duration":3782926},"timestamp":"2018-06-19T16:54:00.052Z"} with parameter: {"event":{"type":"appointment.end","payload":{"room":25,"duration":3782926},"timestamp":"2018-06-19T16:54:00.052Z"}}  []
                /**
                 * parametrs: v
                 * room: roomId
                 * duration: (seconds? or ms?)
                 */
//                Log::debug('[HandleCallbackAction] Appointment ended:', ['event' => $event]);
                break;
            case 'user.join':
                $log = sprintf('[ConferenceCallback] %s with parameter: %s'.PHP_EOL, json_encode($event), json_encode($request->all()));
                Log::info($log);

//                Log::debug('[HandleCallbackAction] User joined room:', ['event' => $event]);
                break;

            case 'user.leave':
//                Log::debug('[HandleCallbackAction] User left room:', ['event' => $event]);
                break;

            default:
                Log::warning(
                    '[HandleCallbackAction] Unidentified event from the conference room:',
                    ['event' => $event]
                );
                break;
        }
        return response()->json(['message' => 'ok'])->setStatusCode(200);
    }
}
