<?php

namespace Modules\Core\Http\Actions\Conference;

use App\Exceptions\Client\NotAuthorizedException;
use App\Exceptions\Client\NotFoundException;
use App\Infrastructure\Http\Action;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetRoomAction extends Action
{
    /**
     * An instance of the MatchService.
     *
     * @var MatchService
     */
    protected $matchService;

    /**
     * Construct an instance of the action.
     *
     * @param MatchService $matchService
     */
    public function __construct(MatchService $matchService)
    {
        $this->matchService = $matchService;

    }

    /**
     * Handle the request.
     *
     * @param Request $request
     * @return Response
     * @throws NotAuthorizedException
     */
    public function __invoke(Request $request, $id)
    {

        // @todo: also handle demo rooms here;

        /** @var Match $match */
        $match = $this->find_and_try_match($id);

        if (!$match) {
            throw new NotFoundHttpException('This roomId is unknown');
        }

        /** @var Enrollment $enrollment */
        $enrollment = $match->getMentorParticipation()->enrollment;

        /** @var Program $program */
        $program = $enrollment->program;

        /** @var Organization $organisation */
        $organisation = $enrollment->user->organization;

        $provider = $organisation->video_provider ?: 'twilio';


        $response = [
            'content' =>
                [
                    'entry' => $program->conference_entry_url,
                    'root' => $program->conference_entry_url
                ],
            'access' => 'private', // should be public for demo;
            'video' => ['provider' => $provider]
        ];

        return response()
            ->json(['data' => $response]);


    }

    private function find_and_try_match($externalRoomId)
    {
        try {
            $match = $this->matchService->getByExternalRoomId($externalRoomId);
        } catch (ModelNotFoundException $e) {
            // match does not exist
            return false;
        }
        return $match;
    }

}
