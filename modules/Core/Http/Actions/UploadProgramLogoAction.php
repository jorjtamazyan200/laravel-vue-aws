<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Actions\UploadProgramImageAction;

class UploadProgramLogoAction extends UploadProgramImageAction
{
    protected $attachmentName = 'logo';
}
