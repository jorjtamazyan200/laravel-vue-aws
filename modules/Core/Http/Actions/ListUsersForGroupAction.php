<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\NotAuthorizedException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\GroupService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Resources\GroupedUserHttpResource;

class ListUsersForGroupAction extends Action
{
    protected $userService;
    protected $responder;

    public function __construct(
        GroupService $groupService,
        UserService $userService,
        ResourceResponder $responder
    ) {
        $this->groupService = $groupService;
        $this->userService = $userService;
        $this->responder = $responder;
    }

    public function __invoke($id, Request $request)
    {
        $group = $this->groupService->get($id);

        if (Auth::user()->cannot('supervise', $group)) {
            throw new NotAuthorizedException('USER_IS_NOT_SUPERVISOR_OF_GROUP');
        }

        return $this->responder->send($group->users, GroupedUserHttpResource::class);
    }
}
