<?php

namespace Modules\Core\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Core\Http\Requests\AddFeedbackRequest;
use Modules\Core\Http\Requests\RegisterUserRequest;
use Modules\Core\Http\Resources\UserHttpResource;
use Modules\Scheduling\Domain\Services\AppointmentService;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class ShowPrivacyPolicyAction extends Action
{


    protected  $brandedDocumentService;

    public function __construct(BrandedDocumentService $brandedDocumentService)
    {
        $this->brandedDocumentService = $brandedDocumentService;

    }

    public function __invoke(Request $request)
    {

        $document = $this->brandedDocumentService->findDocument('privacypolicy', 'en');

        $frontendUrl = Brand::query()->first()->frontend_url;

        if ($request->path) {
            $frontendUrl .=  $request->path;
        }

        return view('public.privacypolicy', [
            'document' => $document,
            'frontendUrl' => $frontendUrl
        ]);


    }

}
