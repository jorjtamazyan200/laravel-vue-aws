<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Actions\RemoveProgramImageAction;

class RemoveProgramCoverAction extends RemoveProgramImageAction
{
    protected $attachmentName = 'cover';
}
