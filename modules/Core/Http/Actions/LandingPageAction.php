<?php

namespace Modules\Core\Http\Actions;

use Illuminate\Http\Request;
use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Response;
use Modules\Core\Domain\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class LandingPageAction extends Action
{
    /**
     * @param Request $request
     * @throws NotAuthorizedException
     * @return Response
     */
    public function __invoke(Request $request)
    {
        return response('Default', 200);
    }
}
