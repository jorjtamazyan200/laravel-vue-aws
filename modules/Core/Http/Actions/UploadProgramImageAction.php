<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;
use Modules\Core\Http\Requests\FileUploadRequest;

class UploadProgramImageAction extends Action
{
    protected $programService;
    protected $responder;
    protected $attachmentName;

    public function __construct(ProgramService $programService, ResourceResponder $responder)
    {
        $this->programService = $programService;
        $this->responder = $responder;
    }

    /**
     * @param int $id
     * @param FileUploadRequest $request
     * @return mixed
     */
    public function __invoke(int $id, FileUploadRequest $request)
    {
        /** @var Program $program */
        $program = $this->programService->getForUser(Auth::id(), $id);

        $program->updateImageAttachment($this->attachmentName, $request->fileContent(), $request->fileType());
        return $this->responder->send($program, ProgramHttpResource::class);
    }
}
