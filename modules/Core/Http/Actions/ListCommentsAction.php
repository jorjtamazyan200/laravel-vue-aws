<?php

namespace Modules\Core\Http\Actions;

use Modules\Core\Http\Resources\CommentHttpResource;
use Illuminate\Http\Request;
use App\Infrastructure\Http\ResourceResponder;
use App\Infrastructure\Http\Action;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Client\NotAuthorizedException;

class ListCommentsAction extends Action
{
    public function __construct(ResourceResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $user = Auth::user();
        $commentable_instance = $request->commentable_instance;

        if ($user->cannot('listComments', $commentable_instance)) {
            throw new NotAuthorizedException;
        }

        // @todo: neeed to comment the idea of this.
        $query = $commentable_instance->comments()->where(function ($query) use ($user, $commentable_instance) {
            $query->where('type', null);

            if ($user->can('listSupervisorComments', $commentable_instance)) {
                $query->orWhere('type', 'SUPERVISOR_COMMENT');
            }
        });

        $query->with('author');

        return $this->responder->send($query->get(), CommentHttpResource::class);
    }
}
