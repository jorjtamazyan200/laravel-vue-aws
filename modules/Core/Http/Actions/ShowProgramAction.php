<?php

namespace Modules\Core\Http\Actions;

use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Domain\Services\ProgramService;
use Modules\Core\Http\Resources\ProgramHttpResource;

class ShowProgramAction extends Action
{
    protected $programService;
    protected $responder;

    public function __construct(ProgramService $programService, ResourceResponder $responder)
    {
        $this->programService = $programService;
        $this->responder = $responder;
    }

    public function __invoke(int $id, Request $request)
    {
        $program = $this->programService->getForUser(Auth::id(), $id);

        return $this->responder->send($program, ProgramHttpResource::class);
    }
}
