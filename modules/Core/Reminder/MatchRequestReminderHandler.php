<?php

namespace Modules\Core\Reminder;

use App\Exceptions\Client\ClientException;


use App\Notifications\Match\MatchNotConfirmedReminder2Notification;
use App\Notifications\Match\MatchNotConfirmedReminder3Notification;
use App\Notifications\Match\MatchNotConfirmedReminderNotification;
use App\Notifications\Match\MatchRequestReminderNotification;
use App\Reminders\Handler\AbstractReminderHandler;
use Illuminate\Support\Carbon;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchRequestReminderHandler extends AbstractReminderHandler
{


    protected $coordinatorTodoService;

    public function __construct(CoordinatorTodoService $coordinatorTodoService)
    {
        parent::__construct();
        $this->coordinatorTodoService = $coordinatorTodoService;


        $this->setDateCallback(function ($match) {
            return $match->last_state_change_at;
        });

        $this->registerEvents();
    }

    public function registerEvents()
    {
        $this->registerEvent(3 * 24, [$this, 'firstReminder']);
        $this->registerEvent( 6 * 24, [$this, 'rejectMatch']);

    }

    /**
     * @param Match $match
     */
    public function firstReminder($match)
    {
        $this->sendReminder($match, 'first');
    }


    public function rejectMatch(Match $match)
    {

        $match->user_comment = '[reject_request] because the mentee did not confirm';
        $match->save();

        try {
            $match->transition('mentee_reject_auto');
        } catch (ClientException $e) {
            report($e);
        }


    }

    private function sendReminder(Match $match, $type)
    {
        /**
         * @var Enrollment $enrollment
         * @var User $user
         */
        $enrollment = $match->getMenteeParticipation();
        $user = $enrollment->user;
        if (!$user) {
            return;
        }

        // tdb: needed?
        $user->last_reminder = Carbon::now();
        $user->save();

        $notification = new MatchRequestReminderNotification($user->brand, $match, $enrollment);
        $user->notify($notification);

    }
}