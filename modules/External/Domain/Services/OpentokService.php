<?php

namespace Modules\External\Domain\Services;

use App\Exceptions\Client\NotAuthenticatedException;
use App\Exceptions\Server\ServerException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

use OpenTok\OpenTok;
use OpenTok\Role;

use OpenTok\MediaMode;

/**
 * Class WaboboxService
 * @docs: https://www.waboxapp.com/assets/doc/waboxapp-API-v2.pdf
 * @package App\Services
 */
class OpentokService
{


    private $initialized = false;


    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $apiKey;

    /**
     * @var OpenTok
     */
    protected $opentok;


    /**
     * Construct an instance of the Service.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct()
    {

    }

    private function init()
    {
        if ($this->initialized) {
            return true;
        }

        $this->apiKey = Config::get('tokbox.key');
        $secret = Config::get('tokbox.secret');

        $this->opentok = new OpenTok($this->apiKey, $secret);


    }

    public function getApiKey()
    {
        $this->init();
        return $this->apiKey;
    }

    /**
     * @return array
     */
    public function getSampleSessionWithToken()
    {
        $this->init();

        $session = $this->opentok->createSession(['mediaMode' => MediaMode::ROUTED]);
        $sessionId = $session->getSessionId();

        return [
            'sessionId' => $sessionId,
            'token' => $this->opentok->generateToken($sessionId),
            'apiKey' => $this->getApiKey()
        ];

    }

    public function getCredentialsForMatch(Match $match)
    {

        $this->init();
        if (empty($match->tokbox_session_id)) {
            $match->tokbox_session_id = $this->opentok->createSession(['mediaMode' => MediaMode::ROUTED])->getSessionId();
            $match->save();
        }


        return [
            'sessionId' => $match->tokbox_session_id,
            'token' => $this->opentok->generateToken($match->tokbox_session_id),
            'apiKey' => $this->getApiKey()
        ];
    }

}
