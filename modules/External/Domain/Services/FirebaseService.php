<?php

namespace Modules\External\Domain\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


/**
 * Class WaboboxService
 * @docs: https://www.waboxapp.com/assets/doc/waboxapp-API-v2.pdf
 * @package App\Services
 */
class FirebaseService
{
    private $init = false;


    /**
     * @var \Kreait\Firebase
     */
    protected $firebase;
    protected $databaseUrl;


    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * Construct an instance of the Service.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->databaseUrl = Config::get('firebase.uri', 'https://volunteer-vision.firebaseio.com');


    }

    protected function init()
    {
        if ($this->init) {
            return;
        }
        $firebaseConfig = "firebase/firebase-" .  Config::get('firebase.env', 'development') . ".json";

        $serviceAccount = ServiceAccount::fromJsonFile(storage_path($firebaseConfig));

        $this->firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
//            ->withDatabaseUri($this->databaseUrl)
            ->create();

    }

    public function getAuthTokenForUser(User $user): string
    {
        $this->init();
        $uid = $user->id;
        $additionalClaims = [
            'consumerUid' => $user->id,
            'role' => $user->primary_role
        ];

        $customToken = $this->firebase
            ->getAuth()
            ->createCustomToken($uid, $additionalClaims)
        ;

        return (string)$customToken;


    }


}
