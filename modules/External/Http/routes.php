<?php


Route::group(['prefix' => '/external'], function () {
    // Retrieve availabilities
    Route::post('/twilio/smshook', 'Actions\TwilioSmsHookAction');
    Route::post('/wabobox/hook', 'Actions\WaboboxHookAction');
    Route::post('/landingpages/codes/{code}/register', 'Actions\LandingRegisterAction');
    Route::get('/landingpages/codes/{code}/webinars', 'Actions\LandingpageWebinarsAction');
    // only temporary;
//    Route::post('/users/{id}/sendMigrationMail', 'Actions\SendMigrationMailAction');
    Route::post('/emails/handlebounce', 'Actions\HandleBounceAction')->name('external.handlebounces');

});
Route::group(['prefix' => '/conference3'], function () {


    Route::get('/public-preview/{hash}/{programid}', 'Actions\Conference3\RedirectPublicPreviewAction')->name('conference3.auth.getpublicroom');

    Route::post('/authorize/room', 'Actions\Conference3\AuthorizeRoomAction')->name('conference3.auth.room');

    Route::get('/rooms/{id}', 'Actions\Conference3\GetRoomAction')->name('conference3.auth.getroom');

    Route::post('/rooms/{id}', 'Actions\Conference3\GetRoomAction')->name('conference3.auth.getroomPost');
    Route::post('/users/{id}', 'Actions\Conference3\GetUserAction')->name('conference3.usergetter')->where('id', '[0-9]+');

    Route::post('/rooms/{id}/feedbacks', 'Actions\Conference3\FeedbackCallbackAction')->name('conference3.feedbackcallback');
    Route::post('/rooms/{id}/appointments', 'Actions\Conference3\AppointmentCallbackAction')->name('conference3.appointmentcallback');
    Route::post('/rooms/{id}/videoconnected', 'Actions\Conference3\VideoconnectedCallbackAction')->name('conference3.videoconnectedCallback');
});





Route::group(['prefix' => '/callcheck', 'middleware' => ['throttle:60,1']], function () {
    Route::get('/tokboxcredentials', 'Actions\ShowTokboxCredentialsAction')->name('callcheck.gettokboxcredentials');

});

Route::group(['prefix' => '/', 'middleware' => ['auth','throttle:60,1']], function () {
    Route::get('/me/firebasetoken', 'Actions\ShowSampleFirebaseTokenAction')->name('me.firebasetoken');
});