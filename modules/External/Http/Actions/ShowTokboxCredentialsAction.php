<?php

namespace Modules\External\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Exceptions\Server\ServerException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;

use App\Services\WaboboxService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Log;


use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;

use Modules\External\Domain\Services\OpentokService;
use Modules\External\Http\Resources\PartnerUserHttpResource;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Scheduling\Domain\Models\Appointment;

use OpenTok\MediaMode;
use OpenTok\ArchiveMode;


/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class ShowTokboxCredentialsAction extends Action
{


    private $opentokService = null;

    public function __construct(OpentokService $opentokService)
    {
        $this->opentokService = $opentokService;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke(Request $request)
    {


        $user = Auth::user();

        $session = $this->opentokService->getSampleSessionWithToken();

        return response()->json(['data' => $session]);


    }


}
