<?php

namespace Modules\External\Http\Actions\Conference3;


use App\Infrastructure\Http\Action;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\External\Domain\Services\Conference3Service;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

class AppointmentCallbackAction extends Action
{
    protected $matchService;
    protected $appointmentService;
    protected $conference3Service;

    /**
     * Construct an instance of the action.
     */
    public function __construct(MatchService $matchService, AppointmentService $appointmentService, Conference3Service $conference3Service)
    {
        $this->middleware('verify-conference-signature');
        $this->matchService = $matchService;
        $this->appointmentService = $appointmentService;
        $this->conference3Service = $conference3Service;


    }

    /**
     * Handle the request.
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request, $id)
    {


        $lectionCompleted = $request->input('currentLection');
        $date = Carbon::parse($request->get('date'));
        // demo mode
        if ($this->conference3Service->isDemoRoom($id)){
            return response()->json([
                'id' => rand(0,100),
                'planned_start' => $date->toIso8601String()
            ])->setStatusCode(200);
        }

        /** @var Match $match */
        $match = $this->matchService->getByExternalRoomId($id);


        /** @var Appointment $pastAppointment */
        $pastAppointment = $this->appointmentService->getCurrentAppointmentForMatch($match->id);
        if ($pastAppointment) {
            $pastAppointment->current_lecture = $lectionCompleted;
            $pastAppointment->save();
            if ($pastAppointment->transitionAllowed('finish')) {
                $pastAppointment->transition('finish');
            }
        } else {
            Log::info("[AppointmentCallbackAction] Could not find current Appointment for Match $match->id");
        }

        $appointment = $this->appointmentService->planFutureAppointmentForMatch($match->id, $date);
        $appointment->state = Appointment::STATES['CONFIRMED'];
        $appointment->save();

        return response()->json([
            'id' => $appointment->id,
            'planned_start' => $appointment->planned_start->toIso8601String()
        ])->setStatusCode(200);
    }
}
