<?php

namespace Modules\External\Http\Actions\Conference3;

use App\Infrastructure\Http\Action;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\External\Domain\Services\Conference3Service;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

class FeedbackCallbackAction extends Action
{
    protected $matchService;
    protected $appointmentService;
    protected $userFeedbackService;

    protected $conference3Service;


    /**
     * Construct an instance of the action.
     */
    public function __construct(MatchService $matchService, UserFeedbackService $userFeedbackService, AppointmentService $appointmentService, Conference3Service $conference3Service)
    {
        $this->middleware('verify-conference-signature');
        $this->matchService = $matchService;
        $this->appointmentService = $appointmentService;
        $this->conference3Service = $conference3Service;
        $this->userFeedbackService = $userFeedbackService;
    }

    /**
     * Handle the request.
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request, $id)
    {

        // demo mode
        if ($this->conference3Service->isDemoRoom($id)) {
            return response()->json([
                'added' => 3
            ])->setStatusCode(200);
        }

        /** @var Match $match */
        $match = $this->matchService->getByExternalRoomId($id);
        $feedbacks = $request->input('feedbacks'); //array of feedbacks;
        $userId = $request->header("X-VV-User-Id");

        /** @var Appointment $appointment */
        $appointment = $this->appointmentService->getCurrentAppointmentForMatch($match->id);
        $appointmentId = null;
        if ($appointment) {
            $appointmentId = $appointment->id;
        }
        $added = 0;
        foreach ($feedbacks as $requestFeedback) {
            $added++;

            $this->userFeedbackService->create(
                [
                    'question_code' => $requestFeedback['code'],
                    'response_scalar' => $requestFeedback['rating'],
                    'response_text' => $requestFeedback['text'],
                    'user_id' => $userId,
                    'appointment_id' => $appointmentId
                ]
            );


//            $feedback = new UserFeedback();
//            $feedback->fill([
//                'question_code' => $requestFeedback['code'],
//                'response_scalar' => $requestFeedback['rating'],
//                'response_text' => $requestFeedback['text'],
//                'user_id' => $userId,
//                'appointment_id' => $appointmentId,
//            ]);
//            $feedback->save();
        }

        return response()->json([
            'added' => $added
        ])->setStatusCode(200);
    }
}
