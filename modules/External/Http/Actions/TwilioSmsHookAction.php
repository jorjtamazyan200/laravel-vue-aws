<?php

namespace Modules\External\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class TwilioSmsHookAction extends Action
{
    public function __construct(
        MatchService $matchService,
        UserFeedbackService $userFeedbackService

    ) {
        $this->matchService = $matchService;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke(Request $request)
    {
        $body = $request->body ? $request->body : $request->Body;
        $from = $request->from ? $request->from : $request->From;

        Log::info("received SMS from " . $from . " with ". $body);

        $log = SmsLog::create([
            'type' => 'sms',
            'direction' => 'incoming',
            'channel' => 'sms',
            'from' => $from,
            'to' => $request->to ? $request->to : $request->To,
            'body' => substr($body, 0 , 255)
        ]);


        return response()->json(['message' => 'ok']);
    }
}
