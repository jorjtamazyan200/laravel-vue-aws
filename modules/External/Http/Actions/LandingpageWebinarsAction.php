<?php

namespace Modules\External\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Modules\Core\Domain\Models\RegistrationCode;
use Modules\Core\Domain\Services\WebinarService;
use Modules\Core\Domain\Services\RegistrationCodeService;
use Modules\Core\Http\Resources\WebinarHttpResource;
use Modules\Matching\Http\Requests\AddFeedbackRequest;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class LandingpageWebinarsAction extends Action
{

    /**
     * @var RegistrationCodeService
     */
    private $codeService;

    /**
     * @var WebinarService
     */
    private $webinarService;

    /**
     * @var
     */
    private $responder;


    public function __construct(
        RegistrationCodeService $codeService,
        WebinarService $webinarService,
        ResourceResponder $responder
    ) {
        $this->codeService = $codeService;
        $this->webinarService = $webinarService;
        $this->responder = $responder;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke(string $code, Request $request)
    {

        /** @var RegistrationCode $registrationCode */
        $registrationCode = $this->codeService->getByCode($code);


        $webinars = $this->webinarService->listBy(
            $registrationCode->auto_enroll_in,
            $registrationCode->group->organization,
            $registrationCode->primary_role
        );

        return $this->responder->send($webinars, WebinarHttpResource::class);
    }
}
