<?php

namespace Modules\External\Http\Actions;

use App\Exceptions\Client\ClientException;
use App\Infrastructure\Http\Action;
use App\Infrastructure\Http\ResourceResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserFeedback;
use Modules\Core\Domain\Services\ConferenceService;
use Modules\Core\Domain\Services\UserFeedbackService;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Matching\Http\Requests\AddFeedbackRequest;
use Modules\Matching\Http\Resources\MatchHttpResource;
use App\Exceptions\Client\NotAuthorizedException;
use Modules\Core\Domain\Services\ParticipationService;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class CallMatchAction
 * @package Modules\Matching\Http\Actions
 */
class HandleBounceAction extends Action
{
    protected $userService;
    protected $coordinatorTodoService;

    public function __construct(
        UserService $userService,
        CoordinatorTodoService $coordinatorTodoService
    )
    {
        $this->userService = $userService;
        $this->coordinatorTodoService = $coordinatorTodoService;
    }

    /**
     *
     * @param $id
     * @param AddFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ClientException
     */
    public function __invoke(Request $request)
    {

        $type = $request->type;
        $email = strtolower($request->email);
        $message = $request->message;

        Log::info("[External Email Response] received  " . $type . "  from " . $email);

        $user = User::query()->where('email', '=', $email)
            ->firstOrFail();

        $data = [
            'description' => 'Email delivery problem:' . ($message ? $message : ''),
            'type' => 'EMAIL_' . $type,
            'customer_id' => $user->id,
        ];
        $todo = new CoordinatorTodo($data);
        $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 14);

        $user->addComment(null, 'E-Mail Problem detected: ' . $type . PHP_EOL . $message);

        if ($type === 'complain') {
            $user->accept_email = false;
            $user->save();
        }

        return response()->json(['message' => 'ok']);
    }
}
