<?php

namespace Modules\External\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use App\Services\ChatbotService;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class WaboboxHookTest extends EndpointTest
{
    public function test_it_receives_ack_event()
    {

        /**
         *
         * {
         * "event":"message",
         * "token":"5f8395e79198a5d595ab82debe6540725a66fb9ec0fc9",
         * "uid":"4915208656328",
         * "contact":{
         * "uid":"491776706937",
         * "name":"mentee-demo GS (mentee)",
         * "type":"user"
         * },
         * "message":{
         * "dtm":"1542387939",
         * "uid":"6C0BCDA51F366CAFAA426708E5F5C6AF",
         * "cuid":null,
         * "dir":"i",
         * "type":"chat",
         * "body":{
         * "text":"Of course!"
         * },
         * "ack":"3"
         * }
         * }
         */
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();

        $smslog = SmsLog::create([
            'user_id' => 1,
            'from' => '49177',
            'to' => '491234',
            'body' => 'body',
            'type' => 'whatsapp',
            'unique_message_id' => 'abcedf',
            'whatsapp_status' => -1,
        ]);


        $data = [
            'event' => 'ack',
            'token' => 'abcd1234',
            'uid' => '74397B58E3ELZ',
            'muid' => '1234',
            'cuid' => $smslog->unique_message_id,
            'ack' => 3
        ];


        $response = $this->postJson("/api/v2/external/wabobox/hook", $data);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $this->assertDatabaseHas('sms_logs', [
            'id' => $smslog->id,
            'whatsapp_status' => 3
        ]);
    }


    public function test_it_sets_user_state_to_exists()
    {

        $user = $this->user;
        $user->whatsapp_status = null;
        $user->phone_number_prefix = '+49';
        $user->phone_number = '176423222';
        $user->save();

        $data = $this->getDefaultMessage();
        $data['event'] = 'message';
        $data['message']['dir'] = 'o';
        $data['contact']['uid'] = $user->phone_number_prefix . $user->phone_number;
        $data['message']['ack'] = 3; //

        $response = $this->postJson("/api/v2/external/wabobox/hook", $data);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $userAfter = $user::query()->find($user->id);

        $this->assertEquals($userAfter->whatsapp_status, 'exists');


    }

    public function test_it_confirms_delivered_message()
    {

//        {
//            "event":"message",
        //   "token":"5f8395e79198a5d595ab82debe6540725a66fb9ec0fc9",
        //   "uid":"4915208656328",
        //   "contact":{
        //            "uid":"491776706937",
        //      "name":"mentee-demo GS (mentee)",
        //      "type":"user"
        //   },
        //   "message":{
        //            "dtm":"1542387919",
        //      "uid":null,
        //      "cuid":"400c7d8e0adaa6193aa7537054f54284",
        //      "dir":"o",
        //      "type":"chat",
        //      "body":{
        //                "text":"Hello Simon! thanks for reaching out"
        //      },
        //      "ack":"-1"
        //   }
//}
        $cuid = '900c7d8e0adaa6193aa7537054f54285';

        $smslog = SmsLog::create([
            'user_id' => 1,
            'from' => '49177',
            'to' => '491234',
            'body' => 'body',
            'type' => 'manual_admin',
            'unique_message_id' => $cuid,
            'whatsapp_status' => 0,
        ]);


        $data = $this->getDefaultMessage();
        $data['message']['cuid'] = $cuid;

        $data['message']['ack'] = -1;

        $response = $this->postJson("/api/v2/external/wabobox/hook", $data);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


        $this->assertDatabaseHas('sms_logs', [
            'id' => $smslog->id,
            'unique_message_id' => $cuid,
            'whatsapp_status' => -1
        ]);

    }

    public function test_it_receives_incoming_whatsapp()
    {
//
//        {
//            "event":"message",
        //   "token":"5f8395e79198a5d595ab82debe6540725a66fb9ec0fc9",
        //   "uid":"4915208656328",
        //   "contact":{
        //            "uid":"491776706937",
        //      "name":"mentee-demo GS (mentee)",
        //      "type":"user"
        //   },
        //   "message":{
        //            "dtm":"1542387939",
        //      "uid":"6C0BCDA51F366CAFAA426708E5F5C6AF",
        //      "cuid":null,
        //      "dir":"i",
        //      "type":"chat",
        //      "body":{
        //                "text":"Of course!"
        //      },
//      "ack":"3"
//   }
//}

        $data = $this->getDefaultMessage();
        $response = $this->postJson("/api/v2/external/wabobox/hook",
            $data
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());


    }

    public function test_it_confirms_on_hello()
    {
        $this->user->phone_number_prefix = 49;
        $this->user->phone_number = 99999966;
        $this->user->whatsapp_status = false;
        $this->user->save();

        $number = $this->user->phone_number_prefix . $this->user->phone_number;


//        $messageJson = '{"event": "message",
//                "token": "abcd1234",
//                "contact[uid]": "' . $number . '",
//                "contact[name]": "Peter",
//                "contact[type]": "user",
//                "message[dtm]": 1487082303,
//                "message[uid]": "62397B58E3E0B",
//                "message[cuid]": "",
//                "message[dir]": "i",
//                "message[type]": "chat",
//                "message[body][text]": "asdsaddas",
//                "message[ack]": 3}';
//
//
        $data = $this->getDefaultMessage();
        $data['contact']['uid'] = $number;
        $data['message']['body']['text'] = 'adasdasad';

//        $data = json_decode($messageJson, True);
        $response = $this->postJson("/api/v2/external/wabobox/hook",
            $data
        );


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $user2 = User::find($this->user->id);
        $this->assertEquals($user2->whatsapp_status, 'exists');

    }

    public function test_it_accepts_on_message()
    {
        $this->user->phone_number_prefix = 49;
        $this->user->phone_number = 99999999;
        $this->whatsapp_status = null;

        /** @var ChatbotService $chatbotService */
        $chatbotService = app(ChatbotService::class);

        $chatbotService->setUserState($this->user, ChatbotService::STATES['account_confirmation'], []);

        $this->user->save();

        $number = $this->user->phone_number_prefix . $this->user->phone_number;

        $data = $this->getDefaultMessage();
        $data['contact']['uid'] = $number;
        $data['message']['body']['text'] = 'hola';

        $response = $this->postJson("/api/v2/external/wabobox/hook",
            $data
        );

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $user2 = User::find($this->user->id);
        $this->assertEquals($user2->whatsapp_status, 'confirmed');
    }


    private function getDefaultMessage()
    {
        return [
            'event' => 'message',
            'token' => 'abcd1234',
            'contact' => [
                'uid' => '34666123456',
                'name' => 'Peter',
                'type' => 'user'
            ],
            'message' => [
                'dtm' => '34666123456',
                'uid' => '62397B58E3E0B',
                'cuid' => '',
                'dir' => 'i',
                'type' => 'chat',
                'body' => ['text' => "Hey! How are you doing?"],
                'ack' => 3
            ]
        ];
    }
}
