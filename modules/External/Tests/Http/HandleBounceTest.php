<?php

namespace Modules\External\Tests\Http;

use App\Infrastructure\AbstractTests\EndpointTest;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/matches/{id}/confirm` endpoint.
 */
class HandleBounceTest extends EndpointTest
{
    public function test_it_handles_a_bounce()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();

        /** @var User $user */
        $user = $this->user;

        $route = route('external.handlebounces');

        $response = $this->postJson($route, [
            'type' => 'bounce',
            'email' => $user->email,
        ]);
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
    public function test_it_handles_complaint()
    {
        // Disable model events, as we don't want to call the ConferenceService during this test
        Match::flushEventListeners();

        /** @var User $user */
        $user = $this->user;

        $route = route('external.handlebounces');

        $response = $this->postJson($route, [
            'type' => 'complain',
            'email' => $user->email,
        ]);

        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }
}
