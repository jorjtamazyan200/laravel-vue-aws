<?php

namespace Modules\External\Tests\Http\Conference3;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

/**
 * @todo: add real events (e.g. appointment online, appointment end)
 *
 * Test the functionality of the `/api/v1/conference/callback` endpoint.
 *
 * method: 'POST',
 * path: `/appointments/for-room/${this.room.id}`,
 * payload: {
 * date: date
 * }
 * })
 */
class ConferenceAppointmentCallbackTest extends ConferenceEndpointTest
{

    public function test_it_creates_a_new_appointment()
    {

        $user = $this->user;

        $match = $user->enrollments->first()->participations->first()->match;
        $match->external_room_id = 12345;
        $match->save();
        $date = Carbon::now()->addDays(7);

        $payload = [
            'date' => $date->toIso8601String(),
            'currentLection' => 3
        ];
        $signature = $this->getRequestSignature($payload);

        $response = $this->json(
            'POST',
            '/api/v2/conference3/rooms/' . $match->external_room_id . '/appointments',
            $payload,
            ['X-VV-Signature' => $signature, "X-VV-User-Id" => $user->id]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'planned_start' => $date->toIso8601String()
        ], $response->getContent());
    }
    public function test_it_mocks_demo_rooms()
    {
        $date = Carbon::now();

        $payload = [
            'date' => $date->toIso8601String(),
            'currentLection' => 3
        ];
        $signature = $this->getRequestSignature($payload);

        $response = $this->json(
            'POST',
            '/api/v2/conference3/rooms/DEMO-3-1231/appointments',
            $payload,
            ['X-VV-Signature' => $signature, "X-VV-User-Id" => 1]
        );
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $response->assertJson([
            'planned_start' => $date->toIso8601String()
        ], $response->getContent());
    }
}
