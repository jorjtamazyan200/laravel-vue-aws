<?php

namespace Modules\External\Tests\Http\Conference3;

use App\Infrastructure\AbstractTests\EndpointTest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Modules\Core\Domain\Models\UserFeedback;

/**
 * @todo: add real events (e.g. appointment online, appointment end)
 *
 * Test the functionality of the `/api/v1/conference/callback` endpoint.
 *
 * method: 'POST',
 * path: `/appointments/for-room/${this.room.id}`,
 * payload: {
 * date: date
 * }
 * })
 */
abstract class ConferenceEndpointTest extends EndpointTest
{


    protected function getRequestSignature($payload)
    {
        $algo = Config::get('conference.signature_algo');
        $secret = Config::get('conference.shared_secret');
        $signature = hash_hmac($algo, json_encode($payload), $secret);

        return $signature;
    }


}
