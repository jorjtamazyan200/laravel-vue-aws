<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Base URL of the conference backend
    |--------------------------------------------------------------------------
    */

    'backend_url' => env('CONFERENCE_BACKEND_URL', 'http://localhost.test'),

    /*
    |--------------------------------------------------------------------------

    | Base URL of the conference backend
    |--------------------------------------------------------------------------
    */

    'frontend_url' => env('CONFERENCE_FRONTEND_URL', 'http://localhost.test'),
    'frontend3_url' => env('CONFERENCE3_FRONTEND_URL', 'http://missing.configuration'),
        /*
         *
    | Access Token for the conference backend
    |--------------------------------------------------------------------------
    */

    'access_token' => env('CONFERENCE_BACKEND_TOKEN', ''),


    /*
    |--------------------------------------------------------------------------
    | Shared secret for verifying the signature of conference callbacks.
    |--------------------------------------------------------------------------
    */

    'shared_secret' => env('CONFERENCE_SHARED_SECRET'),

    /*
    |--------------------------------------------------------------------------
    | Algorithm for computing the signature of signed callbacks
    |--------------------------------------------------------------------------
    */

    'signature_algo' => 'sha1',
];
