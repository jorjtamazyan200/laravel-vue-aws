<?php

return array(


    'pdf' => [
        'enabled' => true,
        'binary' => env('WKPDFPATH', 'xvfb-run /usr/bin/wkhtmltopdf'), // '/usr/local/bin/wkhtmltopdf',
        'timeout' => false,
        'options' => ['disable-smart-shrinking' => true],
        'env' => [],
    ],
    'image' => [
        'enabled' => true,
        'binary' => env('WKIMGPATH', 'xvfb-run /usr/bin/wkhtmltoimage'), //'/usr/local/bin/wkhtmltoimage',
        'timeout' => false,
        'options' => [],
        'env' => [],
    ],


);
