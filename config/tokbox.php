<?php
return [
    'key' => env('TOKBOX_API_KEY'),
    'secret' => env('TOKBOX_API_SECRET')
];
