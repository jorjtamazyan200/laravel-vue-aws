<?php
return [
    'user' => env('ZENDESK_USER'),
    'enabled' => env('ZENDESK_ENABLED'),
    'token' => env('ZENDESK_TOKEN'),
    'instancename'=> env('ZENDESK_INSTANCENAME'),
];
