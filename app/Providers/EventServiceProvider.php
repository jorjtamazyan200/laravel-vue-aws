<?php

namespace App\Providers;

use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use Aacotroneo\Saml2\Events\Saml2LogoutEvent;
use App\Listeners\BackupZipWasCreatedListener;
use App\Listeners\LogFailedLoginAttempt;
use App\Listeners\StateMachine\StateMachineEventHandler;
use App\Listeners\TwilioNotificationErrorListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Modules\Core\Listeners\Enrollment\EnrollmentStateSubscriber;
use Modules\Core\Listeners\Organization\OrganizationStateSubscriber;
use Modules\Core\Listeners\User\UserStateSubscriber;
use Modules\Core\Listeners\Webinar\WebinarStateSubscriber;
use Modules\Matching\Listeners\Match\MatchStateSubscriber;
use Modules\Scheduling\Listeners\Appointment\AppointmentStateSubscriber;
use SM\Event\SMEvents;
use Spatie\Backup\Events\BackupZipWasCreated;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Handle all state machine events
        SMEvents::POST_TRANSITION => [StateMachineEventHandler::class],
        'Aacotroneo\Saml2\Events\Saml2LoginEvent' => [Saml2LoginEvent::class],
        'Aacotroneo\Saml2\Events\Saml2LogoutEvent' => [Saml2LogoutEvent::class],

        // Log failed notifications
        'Illuminate\Notifications\Events\NotificationFailed' => [
            'App\Listeners\LogNotificationErrors',
            TwilioNotificationErrorListener::class
        ],
        'Illuminate\Auth\Events\Failed' => [
            LogFailedLoginAttempt::class
        ],
        'Spatie\Backup\Events\BackupZipWasCreated' => [
            BackupZipWasCreatedListener::class
        ]

    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        UserStateSubscriber::class,
        EnrollmentStateSubscriber::class,
        AppointmentStateSubscriber::class,
        MatchStateSubscriber::class,
        WebinarStateSubscriber::class,
        OrganizationStateSubscriber::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
