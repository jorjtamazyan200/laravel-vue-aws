<?php

namespace App\Providers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Blade;


class LogQueriesProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!config('database.log_queries')) {
            return;
        }
        DB::listen(function ($query) {

                Log::debug("[sql] [query] " . $query->sql);
                Log::debug("[sql] [bindings] " . print_r($query->bindings, true));

        });
    }

}
