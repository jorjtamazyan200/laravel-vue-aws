<?php

namespace App\Exceptions;

use Exception;

class OauthException extends BaseException
{
    /**
     * Create an App exception from an League\OAuthServerException.
     *
     * @param Exception $exception
     * @return OauthException
     */
    public static function fromBase(Exception $exception) : BaseException
    {
        $type = $exception->getErrorType();
        if ($type === 'server_error') {
            $code = 'server.oauth';
        } else {
            $code = 'client.' . $type;
        }

        $instance = new static($exception->getMessage(), $code);
        $instance->setHttpStatus($exception->getHttpStatusCode());

        return $instance;
    }
}
