<?php

namespace App\Exceptions\Client;

class WrongPasswordException extends NotAuthorizedException
{
    /** {@inheritDoc} */
    protected $code = 'client.wrong_password';

    /** {@inheritDoc} */
    protected $httpStatus = 403;

    /** {@inheritDoc} */
    protected $message = 'Wrong password for user.';
}
