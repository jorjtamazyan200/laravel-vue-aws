<?php

namespace App\Exceptions\Client;

use App\Exceptions\BaseException;

class ClientException extends BaseException
{
    /** {@inheritDoc} */
    protected $httpStatus = 400;

    /** {@inheritDoc} */
    protected $code = 'client.generic';
}
