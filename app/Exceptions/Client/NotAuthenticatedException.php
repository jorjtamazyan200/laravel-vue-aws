<?php

namespace App\Exceptions\Client;

class NotAuthenticatedException extends ClientException
{
    /** {@inheritDoc} */
    protected $code = 'client.not_authenticated';

    /** {@inheritDoc} */
    protected $httpStatus = 401;

    /** {@inheritDoc} */
    protected $message = 'You need to be authenticated in order to perform this action.';
}
