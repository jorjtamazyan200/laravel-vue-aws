<?php

namespace App\Exceptions\Client;

class InvalidVimeoUrlException extends ClientException
{
    /** {@inheritDoc} */
    protected $code = 'client.invalid_vimeo_url';

    /** {@inheritDoc} */
    protected $httpStatus = 401;

    /** {@inheritDoc} */
    protected $message = 'Provided url is not valid "Vimeo" url';
}
