<?php

namespace App\Exceptions\Server;

use App\Exceptions\BaseException;

class ServerException extends BaseException
{
    /** {@inheritDoc} */
    protected $httpStatus = 500;

    /** {@inheritDoc} */
    protected $code = 'server.generic';

    /**
     * Get an array representation of the Exception.
     *
     * @return array
     */
    public function toArray() : array
    {
        $return = parent::toArray() ;
        return $return;

    }


}
