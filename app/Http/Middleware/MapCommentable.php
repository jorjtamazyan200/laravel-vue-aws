<?php

namespace App\Http\Middleware;

use Closure;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;
use Illuminate\Support\Facades\Auth;
use Modules\Matching\Domain\Models\Match;

class MapCommentable
{
    /**
     * Map the endpoint's param 'commentable_name' to a commentable model
     * Info: Use the existing Commentable Trait on the model!
     *
     * @var array
     */
    protected $model_mapping = [
        'users' => User::class,
        'organization' => Organization::class,

        'matches' => Match::class,
    ];

    /**
     * Check if the request can be mapped to a commentable model.
     * If no instance can be created by class name and id return a 404 response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $commentable_name = $request->route('commentable_name');
        $commentable_id = $request->route('commentable_id');

        if (!isset($this->model_mapping[$commentable_name])) {
            throw new \InvalidArgumentException("'$commentable_name' can't be mapped to a commentable class.");
        }

        $model_fqn = $this->model_mapping[$commentable_name];
        if ($instance = $model_fqn::find($commentable_id)) {
            $request->merge(['commentable_instance' => $instance, 'commentable_fqn' => $model_fqn]);
            return $next($request);
        }

        return abort(404);
    }
}
