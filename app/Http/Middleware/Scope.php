<?php

namespace App\Http\Middleware;

use App\Exceptions\Client\NotAuthorizedException;
use Closure;
use Modules\Core\Domain\Models\Role;

class Scope
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $scope
     * @return mixed
     */
    public function handle($request, Closure $next, $scope)
    {
        if ($scope === 'admin' &&
            !$request->user()->hasAdminAccess()
        ) {
            throw new NotAuthorizedException();
        }
        if ($scope === 'superadmin' &&
            !$request->user()->hasRole(Role::ROLES['superadmin'])
        ) {
            throw new NotAuthorizedException();
        }

        if ($scope === 'manager' &&
            !$request->user()->isAdmin() &&
            !$request->user()->hasRole(Role::ROLES['manager'])
        ) {
            throw new NotAuthorizedException();
        }

        return $next($request);
    }
}
