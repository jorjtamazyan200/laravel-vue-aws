<?php

namespace App\Http\Middleware;

// use Monolog\Logger;
// use Monolog\Handler\StreamHandler;
use Closure;
use DB;
use Log;

class LogDatabaseQueries
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!config('database.log_queries')) {
            return $next($request);
        }

        DB::enableQueryLog();

        // not used at the moment cause this might log later debug logging to db.log???
        // Use separate log file for database queries
        //Log::getMonolog()->pushHandler(new StreamHandler(storage_path('logs/db.log'), Logger::DEBUG, false));

        // We wanna log all queries that the request triggered first and return after all
        $response = $next($request);


        Log::debug('--- SQL Queries for: ' . $request->fullUrl() . ' ---');
        foreach (DB::getQueryLog() as $log) {
            Log::debug($log['query'], ['bindings' => $log['bindings'], 'time' => $log['time']]);
        }
        Log::debug('---');

        return $response;
    }
}
