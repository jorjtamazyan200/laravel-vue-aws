<?php

namespace App\Service\Tests\MultiTenant;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Services\MultiTenant\TenantState;

class TenantStateTest extends LaravelTest
{
    public function test_it_stores_the_tenant_id()
    {
        $subject = new TenantState();
        $tenantId = 'Some_Tenant_ID';
        $subject->setTenantId($tenantId);

        $this->assertEquals($tenantId, $subject->getTenantId());
    }
}
