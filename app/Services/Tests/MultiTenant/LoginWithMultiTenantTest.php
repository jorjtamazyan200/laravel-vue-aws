<?php

namespace App\Services\Tests\MultiTenant;

use App\Infrastructure\AbstractTests\EndpointTest;
use App\Infrastructure\AbstractTests\LaravelTest;
use App\Services\MultiTenant\Facades\MultiTenant;
use App\Services\MultiTenant\TenantScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mockery;

class LoginWithMultiTenantTest extends EndpointTest
{
    /**
     * Check that the scope is properly applied to a query builder.
     *
     * In this test we're basically checking, that the following
     * is called on the builder:
     *
     * $builder->where(fuction ($query) {
     *     $query->where('tenant_id', '=', $the_global_tenant_id)
     *         ->orWhereNull('tenant_id')
     * });
     *
     * @return void
     */
    public function test_i_can_login_and_it_finds_my_tenant()
    {

        $user = $this->user;
        //no tenant is set on public endpoint;
        MultiTenant::setTenantId(null);

        $response = $this->actingAs($this->user)->postJson('/api/v2/auth', [
            'username' => 'user@volunteer-vision.com',
            'password' => 'start123',
            'client_id' => 1,
            'grant_type' => 'password',
            'client_secret' => 'non-secret'
        ]);


        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

        $response->assertJsonStructure(
            ['token_type','expires_in','access_token']
        );

        //@todo: response contains tolen..


    }
}
