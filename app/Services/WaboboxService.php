<?php

namespace App\Services;

use App\Exceptions\Client\NotAuthenticatedException;
use App\Exceptions\Server\ServerException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

/**
 * Class WaboboxService
 * @docs: https://www.waboxapp.com/assets/doc/waboxapp-API-v2.pdf
 * @package App\Services
 */
class WaboboxService
{

    private $token;
    private $uid;
    protected $enabled;
    private $baseUrl = 'https://www.waboxapp.com/api';


    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * Construct an instance of the Service.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->enabled = Config::get('wabox.enabled');
        $this->token = Config::get('wabox.token');
        $this->uid = Config::get('wabox.uid');
    }


    public function queueMessage(User $user, $content, $key = 'custom')
    {
        if (!$user->qualifiedWhatsappNumber) {
            return;
        }
        $log = $this->createLog($user, $content, $key);
        $this->sendQueuedMessage($log);

    }

    public function getWhatsappDeviceStatus()
    {

        $url = 'status/' . $this->uid;

        try {
            $status = $this->sendRequest($url, [], 'POST');
        } catch (\Exception $exception) {
            return 'offline';
        }

        return 'ok';
    }
    /*
     * object(stdClass)#1628 (8) {
  ["success"]=>
  bool(true)
  ["uid"]=>
  string(13) "4915208656328"
  ["hook_url"]=>
  string(62) "https://api2.volunteer-vision.com/api/v2/external/wabobox/hook"
  ["alias"]=>
  string(16) "Volunteer Vision"
  ["platform"]=>
  string(7) "android"
  ["battery"]=>
  string(3) "100"
  ["plugged"]=>
  string(1) "1"
  ["locale"]=>
  string(5) "de-DE"
}

     */

    /**
     * @param SmsLog $log
     * @param $text
     * @param $reciver
     * @return mixed
     * @throws ServerException
     */
    private function doSendRequest(SmsLog $log, $text, $reciver)
    {
        $parameters = [
            'text' => $text,
            'custom_id' => $log->id,
            'to' => $reciver
        ];

        if (!$this->enabled) {
//            echo 'skipped sending because of config' . PHP_EOL;
            return null;
        }

        $response = $this->sendRequest('send/chat', $parameters); // {success:true , custom_uid: 'xxx'}

        if ($response->custom_uid) {
            $log->unique_message_id = $response->custom_uid;
            $log->sent_at = Carbon::now();
            $log->save();
        }

        return $response;
    }

    public function sendQueuedMessage(SmsLog $smsLog)
    {
        //
        $wasSent = false;
        try {
            $this->doSendRequest($smsLog, $smsLog->body, $smsLog->to);
            $wasSent = true;
        } catch (\App\Exceptions\Client\ClientException $clientException) {
            report($clientException);
        } catch (ServerException $serverException) {
            report($serverException);
        }
        return $wasSent;


    }

    private function createLog(User $user, $content, $type)
    {
        $log = new SmsLog();
        $log->user_id = $user->id;
        $log->direction = 'outgoing';
        $log->from = 'outgoing';
        $log->to = $user->qualifiedWhatsappNumber;
        $log->body = $content;
        $log->channel = 'whatsapp';
        $log->type = $type;
        $log->save();
        return $log;
    }

    public function logOutgoingWhatsapp($contaactuid, $user, $body)
    {


    }

    public function logIncomingWhatsapp($contactuid, $user, $body, $ack, $direction, $whatsapp_cuid): SmsLog
    {

        $data = [
            'from' => $contactuid,
            'to' => '--',
            'direction' => $direction === 'i' ? 'incoming' : 'outgoing',
            'type' => 'hook_generated',
            'channel' => 'whatsapp',
            'body' => $body,
            'user_id' => $user ? $user->id : null,
            'whatsapp_uid' => $whatsapp_cuid,
            'unique_message_id' => $whatsapp_cuid,
            'whatsapp_status' => $ack
        ];

        // handle responses of already existing messages;
        if ($whatsapp_cuid) {
            $smsLog = SmsLog::query()->where('unique_message_id', $whatsapp_cuid)->first();
            if ($smsLog) {
                $smsLog->whatsapp_status = $ack;
                $smsLog->save();
                return $smsLog;
            }
        }

        // create new log;
        return SmsLog::create($data);

    }


    /**
     * @param $action
     * @param array $parameter
     * @throws ServerException
     */
    private function sendRequest($action, array $parameters, $method = 'POST')
    {
        $additionalParamrs = ['token' => $this->token, 'uid' => $this->uid];
        $parameters = array_merge($additionalParamrs, $parameters);

        $url = $this->baseUrl . '/' . $action;


        Log::debug("[whatsapp request to] " . $url);

        try {
            $response = $this->httpClient->request($method, $url, ['form_params' => $parameters]);
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            if ($body && strstr($body, ' is not connected with waboxapp right now') !== false) {
                throw new \App\Exceptions\Client\ClientException('The Whatsapp device or whatsapp web is not connected. Please check the mobile phone');
            }

            if ($response->getStatusCode() === 400) {
                throw new ServerException("wabobox parameters invalid!" . json_encode($parameters));
            }
        }


        if ($response->getStatusCode() === 403) {
            throw new ServerException("wabobox not configured correctly (access denied; 403)");
        }

        Log::debug("[whatapp response] Repsonse: " . $response->getStatusCode() . $response->getBody());

        $responseData = json_decode($response->getBody());

        if (!$responseData->success) {
            Log::error("[whatsapp box response] Invalid response!! " . $response->getBody());
        }
        return $responseData;


    }


}
