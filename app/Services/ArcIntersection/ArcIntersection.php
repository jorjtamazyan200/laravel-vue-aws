<?php

namespace App\Services\ArcIntersection;

use App\Infrastructure\Contracts\Interval;

class ArcIntersection
{
    /**
     * The last value of the full "circle"
     *
     * @var int
     */
    public $maxValue;

    public function __construct(int $maxValue = 360)
    {
        $this->maxValue = $maxValue;
    }

    /**
     * Get intersection of two intervals
     *
     * @param App\Infrastructure\Contracts\Interval $arc1
     * @param App\Infrastructure\Contracts\Interval $arc2
     * @return int
     */
    public function computeIntersection(Interval $arc1, Interval $arc2) : int
    {
        $arcs1 = $this->breakupArcAtZero($arc1);
        $arcs2 = $this->breakupArcAtZero($arc2);

        $intersection = 0;
        foreach ($arcs1 as $subarc1) {
            foreach ($arcs2 as $subarc2) {
                $intersection += $this->computeSimpleIntersection($subarc1, $subarc2);
            }
        }

        return $intersection;
    }

    /**
     * Break a zero "crossing" arc in two pieces
     *
     * @param App\Infrastructure\Contracts\Interval $arc
     * @return array
     */
    protected function breakupArcAtZero(Interval $arc) : array
    {
        $arcs = [];

        if ($arc->intervalStart() > $arc->intervalEnd()) {
            $arcs[] = new Arc($arc->intervalStart(), $this->maxValue);
            $arcs[] = new Arc(0, $arc->intervalEnd());
        } else {
            $arcs[] = $arc;
        }

        return $arcs;
    }

    /**
     * Calculate intersection of two arcs
     *
     * @param App\Infrastructure\Contracts\Interval $arc1
     * @param App\Infrastructure\Contracts\Interval $arc2
     * @return int
     */
    protected function computeSimpleIntersection(Interval $arc1, Interval $arc2) : int
    {
        $s1 = $arc1->intervalStart();
        $s2 = $arc2->intervalStart();

        $e1 = $arc1->intervalEnd();
        $e2 = $arc2->intervalEnd();

        if (
            ($s1 >= $s2 && $s1 <= $e2) ||
            ($e1 >= $s2 && $e1 <= $e2) ||
            ($s2 >= $s1 && $s2 <= $e1) ||
            ($e2 >= $s1 && $e2 <= $e1)
        ) {
            $intersection = min($e1, $e2)-max($s1, $s2);
            return $intersection;
        }

        return 0;
    }
}
