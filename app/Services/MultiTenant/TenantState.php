<?php

namespace App\Services\MultiTenant;

class TenantState
{
    /**
     * The id of the tenant that the current request is scoped to.
     *
     * @var mixed
     */
    protected $tenantId;

    /**
     * Set the tenantId for the current request.
     *
     * @param mixed $tenantId
     */
    public function setTenantId($tenantId) : void
    {
        $this->tenantId = $tenantId;
    }

    /**
     * Get the tenantId for the current request.
     *
     * @return mixed
     */
    public function getTenantId()
    {
        return $this->tenantId;
    }
}
