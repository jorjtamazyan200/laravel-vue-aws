<?php

namespace App\Services\MultiTenant;

use App\Services\MultiTenant\Facades\MultiTenant;

/*
 * @similar to https://github.com/HipsterJazzbo/Landlord/blob/master/src/BelongsToTenants.php
 */
trait BelongsToTenant
{
    /**
     * The "booting" method of the trait.
     *
     * @return void
     */
    protected static function bootBelongsToTenant()
    {
        // Scope queries
        static::addGlobalScope(new TenantScope());

        // Add tenant_id to newly created resources
        static::creating(function ($model) {
            if (!isset($model->tenant_id)) {
                $model->tenant_id = MultiTenant::getTenantId();
            }
        });
    }
}
