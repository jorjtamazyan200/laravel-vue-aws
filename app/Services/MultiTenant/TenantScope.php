<?php

namespace App\Services\MultiTenant;

use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

class TenantScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        return;

//
//        /**
//         * This implies that we ignore the multitenancy for all
//         * public requests. Not sure yet if this is a good idea.
//         */
//        if (MultiTenant::getTenantId() == null) {
//            return;
//        }
//
//
//        switch (true) {
//            case $model instanceof User:
//                $builder->where($model->getTable() . '.tenant_id', '=', MultiTenant::getTenantId());
//                return;
//            case $model instanceof Organization:
//                $builder->where($model->getTable() . '.tenant_id', '=', MultiTenant::getTenantId());
//                return;
//            case $model instanceof Webinar:
//                $builder->where($model->getTable() . '.tenant_id', '=', MultiTenant::getTenantId());
//                return;
//            default:
//                $builder->where(function (Builder $query) use ($model) {
//                    return $query->where($model->getTable() . '.tenant_id', '=', MultiTenant::getTenantId())
//                        ->orWhereNull($model->getTable() . '.tenant_id');
//                });
//
//        };

    }

}
