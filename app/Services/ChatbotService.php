<?php

namespace App\Services;

use App\Exceptions\Client\NotAuthenticatedException;
use App\Exceptions\Server\ServerException;
use App\Notifications\User\ConfirmationNotification;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Models\SmsLog;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Modules\Scheduling\Domain\Models\Appointment;

/**
 * Class WaboboxService
 * @docs: https://www.waboxapp.com/assets/doc/waboxapp-API-v2.pdf
 * @package App\Services
 */
class ChatbotService
{

    private $token;
    private $uid;
    protected $enabled;
    private $baseUrl = 'https://www.waboxapp.com/api';


    const MESSAGE_TYPES = [
        'CONFIRM' => 'CONFIRM',
        'REJECT' => 'REJECT',
        'HELLO' => 'HELLO',
        'UNKNOWN' => 'UNKNOWN'
    ];

    const STATES = [
        'default' => 'default',
        'appointment_confirmation' => 'appointment_confirmation',
        'account_confirmation' => 'account_confirmation',
        'waiting_for_match' => 'waiting_for_match',// @todo:
        'match_request' => 'match_request'
    ];

    const SEARCH_KEYWORDS = [
        'CONFIRM' => ['yes', 'ja', 'si', 'sicher', 'jawoll', 'Confirm', '👍', 'ok'],
        'REJECT' => ['no', 'nein'],
        'HELLO' => ['hello', 'hallo', 'hola', 'start'],
        'UNKNOWN' => []
    ];

    /**
     * An HTTP client instance for making requests to the conference service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;
    protected $userService;

    /**
     * Construct an instance of the Service.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient, UserService $userService)
    {
        $this->httpClient = $httpClient;
        $this->userService = $userService;
    }

    public function setUserState(User $user, $state, $params)
    {
        if (!in_array($state, array_keys(self::STATES))) {
            throw  new  InvalidArgumentException("Invalid chatbot state", $state);
        }
        $user->chatbot_state = ['state' => $state, 'params' => $params];
        $user->save();
    }

    /**
     * so far this is only made for confirmations;
     * @param User $user
     * @param $message
     */
    public function handleOutgoingMessage(User $user, $message)
    {
        if (!$user->whatsapp_status) {
            $user->whatsapp_status = 'exists';
            $user->save();
        }

        Log::debug("[Chatbot:debug] Handle Outgoing message: " . json_encode($message));

        // maybe log this?


    }

    /**
     * Actual entry;
     * @param User $user
     * @param $message
     */
    public function handleIncomingMessage(User $user, $message)
    {
        $messageType = $this->convertTextIntoMessageType($message);
        return $this->handleMessageType($user, $messageType, $message);

    }

    /**
     * @param $message
     * @return mixed
     */
    public function convertTextIntoMessageType($message)
    {

        $trimmed = StringHelper::trimOnlyLetters($message);
        $trimmed = strtolower($trimmed);
        $trimmed = StringHelper::strip_introduction($trimmed);


        foreach (array_keys(self::MESSAGE_TYPES) as $message_type) {
            $searchWords = self::SEARCH_KEYWORDS[$message_type];

            foreach ($searchWords as $searchWord) {

                if (StringHelper::startsWith($trimmed, $searchWord)) {
                    // exit point if found;
                    return self::MESSAGE_TYPES[$message_type];
                }
            }
        }

        return self::MESSAGE_TYPES['UNKNOWN'];


    }

    /***
     * @param User $user
     * @param string $messageType self::MESSAGE_TYPE
     * @param $originalMessage
     */
    public function handleMessageType(User $user, $messageType, $originalMessage = '')
    {
        if (!in_array($messageType, self::MESSAGE_TYPES)) {
            throw new InvalidArgumentException("messagetype has to be one of ChatbotService::MESSAGE_TYPES");
        }
        $state = $this->getStateFromUser($user);

        if (!$user->whatsapp_status) {
            $user->whatsapp_status = 'exists';
            $user->save();
        }

        //
        //  Possible options...
        //
        Log::debug("[Chatbot:debug] Current User State: " . json_encode($state) . " >>> Action Message Applying: " . $messageType);
        $handled = false;

        switch ($state) {
            case self::STATES['account_confirmation']:
                if ($messageType === self::MESSAGE_TYPES['CONFIRM'] ||
                    $messageType === self::MESSAGE_TYPES['HELLO']
                ) {
                    $this->userService->confirmWhatsappNumber($user);
                    $this->clearUserState($user, false);
                    $handled = true;
                }

                break;
            case self::STATES['appointment_confirmation']:
                $params = $this->getParamsFromUser($user);
                /** @var Appointment $app */
                $app = Appointment::query()->find($params['id']); // maybe better as service..?
                // maybe only future appiontments?
                if ($messageType === self::MESSAGE_TYPES['CONFIRM'] && $app->transitionAllowed('confirm')) {
                    $app->transition('confirm');
                    $this->clearUserState($user);
                    $handled = true;
                } else if ($messageType === self::MESSAGE_TYPES['CONFIRM'] && $app->state === Appointment::STATES['CONFIRMED']) {
                    $this->clearUserState($user);
                    $handled = true;
                }

                if ($messageType === self::MESSAGE_TYPES['REJECT'] && $app->transitionAllowed('cancel')) {
                    $app->transition('cancel');
                    $this->clearUserState($user);
                    $handled = true;
                }
                break;
            case self::STATES['match_request']:

                $params = $this->getParamsFromUser($user);
                /** @var Match $match */
                $match = Match::query()->find($params['id']); // maybe better as service..?

                if ($messageType === self::MESSAGE_TYPES['CONFIRM']) {
                    if ($match->transitionAllowed('mentee_approve')) {
                        $match->transition('mentee_approve');
                        $this->clearUserState($user);
                    } else {
                        Log::warning("[Chatbot:debug] tried to approve " . $match->id . " but it was not in the right state." . $match->state);
                    }
                    $handled = true;
                } else if ($messageType === self::MESSAGE_TYPES['CONFIRM'] && $match->state === Match::STATES['UNCONFIRMED']) {
                    $this->clearUserState($user);
                    $handled = true;
                }

                if ($messageType === self::MESSAGE_TYPES['REJECT']) {
                    if ($match->transitionAllowed('mentee_reject')) {
                        $match->transition('mentee_reject');
                        $this->clearUserState($user);
                        $handled = true;
                    } else {
                        Log::warning("[Chatbot:debug] tried to reject " . $match->id . " but it was not in the right state:" . $match->state);
                    }

                }

                break;
        }

        return $handled;


    }

    private function getParamsFromUser(User $user)
    {
        if (!$user->chatbot_state || !$user->chatbot_state['params']) {
            return [];
        }
        return $user->chatbot_state['params'];

    }

    private function clearUserState(User $user, $autoRespond = true)
    {
        $user->chatbot_state = null;
        $user->save();
        if ($autoRespond) {
            $user->notify(new ConfirmationNotification($user->brand, $user));
        }

    }

    private function getStateFromUser(User $user)
    {

        if (!$user->chatbot_state) {
            return self::STATES['default'];
        }
        if (!$user->chatbot_state['state']) {
            return self::STATES['default'];
        }
        return $user->chatbot_state['state'];

    }


}
