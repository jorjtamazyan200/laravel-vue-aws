<?php

namespace App\Console;

use App\Console\Commands\AppointmentReminderSender;
use App\Console\Commands\AppointmentSetToSuccessCommand;
use App\Console\Commands\CalculateMatchingScoreForNewParticipationsCommand;
use App\Console\Commands\DeleteAwwWhiteobardsCommand;
use App\Console\Commands\EnrollmentIncompleteReminderSender;
use App\Console\Commands\EnrollmentTrainingReminderSender;
use App\Console\Commands\FindCoordinatorTodosDailyCommand;
use App\Console\Commands\FindGeoLocationOfMissingUsersCommand;
use App\Console\Commands\MatchNoFirstAppointmentReminderSenderCommand;
use App\Console\Commands\MatchNotConfirmedReminderSender;
use App\Console\Commands\MatchPausedReminderSenderCommand;
use App\Console\Commands\MatchRequestReminderSenderCommand;
use App\Console\Commands\NightlyDeleteTestUsersCommand;
use App\Console\Commands\NoFutureAppointmentReminderSenderCommand;
use App\Console\Commands\NoWebinarReminderSenderCommand;
use App\Console\Commands\ParticipationWaitingSenderCommand;
use App\Console\Commands\ProcessQueuedWhatsappCommand;
use App\Console\Commands\SendUserLeadInvitesCommand;
use App\Console\Commands\SupervisorDailySender;
use App\Console\Commands\SyncZendeskUsersCommand;
use App\Console\Commands\UserCompleteRegistrationReminderSender;
use App\Console\Commands\WaitingMatchableParticipationsCommand;
use App\Console\Commands\WebinarInvitiationSender;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Config;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // @docs: https://laravel.com/docs/master/scheduling

        // email reminder
//        $schedule->command(UserCompleteRegistrationReminderSender::class)->daily();

        $schedule->command(MatchNotConfirmedReminderSender::class)->daily()->at('18:00')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(WebinarInvitiationSender::class)->hourly()
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(AppointmentReminderSender::class)->everyTenMinutes()
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(AppointmentSetToSuccessCommand::class)->everyTenMinutes()
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(SupervisorDailySender::class)->hourly()
            ->appendOutputTo(storage_path('logs/task.mails.log'));


        // should state at night
        $schedule->command(NoWebinarReminderSenderCommand::class)->daily()->at('05:00')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(ParticipationWaitingSenderCommand::class)->daily()->at('05:30');


//
//        $schedule->command(WaitingMatchableParticipationsCommand::class)->weekly()
//            ->appendOutputTo(storage_path('logs/task.mails.log'));


        $schedule->command(NightlyDeleteTestUsersCommand::class)->daily()->at('00:15')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(NoFutureAppointmentReminderSenderCommand::class)->daily()->at('13:15')
            ->appendOutputTo(storage_path('logs/task.mails.log'));


        $schedule->command(MatchNoFirstAppointmentReminderSenderCommand::class)->daily()->at('13:30')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(MatchRequestReminderSenderCommand::class)->daily()->at('15:30')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(MatchPausedReminderSenderCommand::class)->daily()->at('18:00')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(EnrollmentIncompleteReminderSender::class)->daily()->at('13:45')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(EnrollmentTrainingReminderSender::class)->daily()->at('9:45')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(SendUserLeadInvitesCommand::class)->daily()->at('14:00')
            ->appendOutputTo(storage_path('logs/task.mails.log'));

        $schedule->command(ProcessQueuedWhatsappCommand::class)->everyTenMinutes()
            ->appendOutputTo(storage_path('logs/task.whatsapp.log'));


        $schedule->command(FindCoordinatorTodosDailyCommand::class)->hourly()
            ->appendOutputTo(storage_path('logs/task.coordinatortask.log'));


        $schedule->command(CalculateMatchingScoreForNewParticipationsCommand::class)->hourly()
            ->appendOutputTo(storage_path('logs/task.matching.log'));

        $schedule->command(SyncZendeskUsersCommand::class)->everyFiveMinutes()
            ->appendOutputTo(storage_path('logs/zendesk.sync.log'));

        $schedule->command(DeleteAwwWhiteobardsCommand::class)->dailyAt('02:30')
            ->appendOutputTo(storage_path('logs/whiteboard.sync.log'));


        if (Config::get('app.env', false) === 'production') {
            $schedule->command('backup:clean')->daily()->at('01:00');
            $schedule->command('backup:run')->daily()->at('02:00');
            $schedule->command('backup:monitor')->daily()->at('03:00');
        }


        $schedule->command(FindGeoLocationOfMissingUsersCommand::class)
            ->daily()->at('03:30')
            ->appendOutputTo(storage_path('logs/task.general.log'));


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
