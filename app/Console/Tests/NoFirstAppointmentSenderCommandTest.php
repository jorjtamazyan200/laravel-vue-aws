<?php

namespace Modules\Supervisor\Test\Http;

use App\Console\Commands\NoWebinarReminderSenderCommand;
use App\Infrastructure\AbstractTests\EndpointTest;
use App\Notifications\Match\MatchNoFirstAppointmentNotification;
use App\Notifications\Match\MatchNoFutureAppointmentNotification;
use App\Notifications\Webinar\WebinarChoiceReminderNotification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class NoFirstAppointmentSenderCommandTest extends EndpointTest
{
    /** @test */
    public function it_creates_a_coordinator_todo_after_six_days()
    {

        Notification::fake();

        $this->assertTrue(true);
        // Assert a notification was sent to the given users...

        $otherMatch =        Match::find(10);
        $otherMatch->state = Match::STATES['ACTIVE'];
        $otherMatch->save();


        $user = User::where('email', 'mentor@volunteer-vision.com')->firstOrFail();
        $enrollment = $user->enrollments->first();


        $eventTime = Carbon::now()->subHours((24 * 6) + 13);
        /** @var Enrollment $enrollment */
        $match = factory(Match::class)->create([
            'state' => Match::STATES['CONFIRMED'],
            'last_reminder' => (new Carbon($eventTime))->addHours(24 * 4.5),
            'last_state_change_at' => $eventTime
        ]);


        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);



        Artisan::call('vv:reminder:nofirstappointment');
        $resultAsText = Artisan::output();

        $this->assertDatabaseHas('coordinator_todos', [
            'type' => CoordinatorTodo::TYPES['NO_FIRST_APPOINTMENT'],
            'customer_id' => $user->id,
        ]);


    }

    /** @test */
    public function test_it_sends_for_match_without_future_appointments()
    {

        Notification::fake();

        $this->assertTrue(true);
        // Assert a notification was sent to the given users...


        $user = User::where('email', 'mentor@volunteer-vision.com')->firstOrFail();
        $enrollment = $user->enrollments->first();

        /** @var Enrollment $enrollment */
        $match = factory(Match::class)->create([
            'state' => Match::STATES['CONFIRMED'],
            'last_reminder' => null,
            'last_state_change_at' => Carbon::now()->subHours((24 * 2) + 1)
        ]);


        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);


        Artisan::call('vv:reminder:nofirstappointment');

        $resultAsText = Artisan::output();
        Notification::assertSentTo(
            [$user], MatchNoFirstAppointmentNotification::class
        );


    }

//    /** @test */
//    public function test_()
//    {
//
//        $this->markTestSkipped();
//        Notification::fake();
//
//
//        $this->assertTrue(true);
//        // Assert a notification was sent to the given users...
//
//
//        $user = User::where('email', 'mentor@volunteer-vision.com')->firstOrFail();
//        $enrollment = $user->enrollments->first();
//
//        /** @var Enrollment $enrollment */
//        $match = factory(Match::class)->create([
//            'state' => Match::STATES['CONFIRMED'],
//            'last_reminder' => null,
//            'last_state_change_at' => Carbon::now()->subHours((24 * 2) + 1)
//        ]);
//
//
//        factory(Participation::class)->create([
//            'enrollment_id' => $enrollment->id,
//            'match_id' => $match->id
//        ]);
//
//
//        Artisan::call('vv:reminder:nofirstappointment');
//
//        $resultAsText = Artisan::output();
//        var_dump($resultAsText);
//        Notification::assertSentTo(
//            [$user], MatchNoFirstAppointmentNotification::class
//        );
//
//
//    }

}


