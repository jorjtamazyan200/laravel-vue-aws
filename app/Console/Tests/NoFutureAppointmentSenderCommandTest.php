<?php

namespace Modules\Supervisor\Test\Http;

use App\Console\Commands\NoWebinarReminderSenderCommand;
use App\Infrastructure\AbstractTests\EndpointTest;
use App\Notifications\Match\MatchNoFutureAppointmentNotification;
use App\Notifications\Webinar\WebinarChoiceReminderNotification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;
use Modules\Matching\Domain\Models\Match;

/**
 * Test the functionality of the `/api/v1/users/me/groups` endpoint.
 */
class NoFutureAppointmentSenderCommandTest extends EndpointTest
{
    /** @test */
    public function test_it_sends_for_match_without_future_appointments()
    {

        Notification::fake();



        $this->assertTrue(true);
        // Assert a notification was sent to the given users...


        $user = User::where('email', 'mentor@volunteer-vision.com')->firstOrFail();
        $enrollment = $user->enrollments->first();

        /** @var Enrollment $enrollment */
        $match = factory(Match::class)->create([
//            'enrollment_id' => $enrollment->id,
            'state' => Match::STATES['ACTIVE'],
            'last_reminder' => null
        ]);

        factory(Participation::class)->create([
            'enrollment_id' => $enrollment->id,
            'match_id' => $match->id
        ]);


        Artisan::call('vv:reminder:nofutureappointment');

//        $resultAsText = Artisan::output();
        Notification::assertSentTo(
            [$user], MatchNoFutureAppointmentNotification::class
        );


    }


    public function test_it_sends_webinar_info_if_webinar_is_in_past()
    {

        Notification::fake();


        $resultAsText = Artisan::output();

        $this->assertTrue(true);
        // Assert a notification was sent to the given users...

        $pastWebinar = Webinar::query()->where('program_id', 1)
            ->where('starts_at', '<', Carbon::now())->firstOrFail();


        /** @var Enrollment $enrollment */
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['TRAINING'],
            'program_id' => 1,
            'last_reminder' => null
        ]);

        $enrollment->webinars()->attach($pastWebinar);


        Artisan::call('vv:reminder:nowebinar');

        Notification::assertSentTo(
            [$this->user], WebinarChoiceReminderNotification::class
        );


    }

    public function test_it_does_not_send_on_future_webinars()
    {

        Notification::fake();


        $resultAsText = Artisan::output();

        $this->assertTrue(true);
        // Assert a notification was sent to the given users...

        $pastWebinar = Webinar::query()->where('program_id', 1)
            ->where('starts_at', '>', Carbon::now())->firstOrFail();


        /** @var Enrollment $enrollment */
        $enrollment = factory(Enrollment::class)->create([
            'user_id' => $this->user->id,
            'state' => Enrollment::STATES['TRAINING'],
            'program_id' => 1,
            'last_reminder' => null
        ]);

        $enrollment->webinars()->attach($pastWebinar);


        Artisan::call('vv:reminder:nowebinar');

        Notification::assertNotSentTo(
            [$this->user], WebinarChoiceReminderNotification::class
        );


    }
}


