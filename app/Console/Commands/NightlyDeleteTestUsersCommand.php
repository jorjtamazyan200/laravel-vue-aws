<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\Core\Domain\Services\UserService;

class NightlyDeleteTestUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:delete:testusers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $userService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();
        $this->userService = $userService;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $users = $this->userService->findTestUsers();
        $count = count($users);
        $this->info("[delete test users] found $count entries to delete");


        foreach ($users as $user) {
            try {
                $this->userService->forceDeleteUser($user);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                report($exception);
            }
        }
        return;
    }
}
