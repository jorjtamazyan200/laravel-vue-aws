<?php

namespace App\Console\Commands;

use App\Notifications\Appointment\AppointmentReminderNotification;
use App\Notifications\Enrollment\UserQuitNotification;
use App\Notifications\Organization\ContractEndedNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

/**
 * @todo: test
 */
class TestNotFoundNotificationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:notfound';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Reminder to Appointemnts';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(' sending UserQuitNotification to simon fakir');

        $user = User::where('email','=','simon.fakir@volunteer-vision.com')->firstOrFail();
        $organization = Organization::find(1);

        $user->notify(new ContractEndedNotification($user->brand, $organization));

        $this->info(sprintf('[] done. actually you should no see an error log in laravel.log' ));

    }
}
