<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\UserLaunch;

class RenderMissingBrowsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:missing:browser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $launches = UserLaunch::query()->whereNull('client_type')->get();

        foreach ($launches as $launch) {
            $launch->useragent = $launch->useragent; // trigger setter;
            $launch->save();
        }

        $this->info('done with ' . $launches->count() . 'rows ');
    }
}
