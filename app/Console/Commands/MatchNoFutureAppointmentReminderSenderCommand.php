<?php

namespace App\Console\Commands;

use App\Services\MultiTenant\Facades\MultiTenant;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Reminder\FutureAppointmentsReminderHandler;
use Modules\Scheduling\Reminder\ConfirmedMatchesReminderHandler;

class MatchNoFutureAppointmentReminderSenderCommand extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:nofutureappointment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $matchService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( MatchService $matchService)
    {
        parent::__construct();
        $this->matchService = $matchService;


    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();


        $now = Carbon::now();

        $query = $this->matchService
            ->findMatchesWithoutPlannedAppointments(Match::STATES['ACTIVE'])
            ->where(function ($query) {
                $query->where('last_reminder', '<', Carbon::now()->subDays(2))
                    ->orWhereNull('last_reminder'); // @todo first
            });


        $matches = $query->get();

        /** @var ConfirmedMatchesReminderHandler $handler */
        $handler = App(FutureAppointmentsReminderHandler::class);
        $eventsFired = $handler->applyActions($matches);
//        $this->info('[reminder] Events fired: ' . $eventsFired);
        $this->end(' %s events founds ', $eventsFired);

    }

}
