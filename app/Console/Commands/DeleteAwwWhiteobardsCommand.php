<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class DeleteAwwWhiteobardsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aww:cleanboards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $httpClient;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->httpClient = $client;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        //
        // hack, should actually run in cloudfunctions etc; but was faster implemented here;
        //
        $secret = '2293224b-8537-440b-81c0-db02045f7cfd';
        $baseUri = 'https://awwapp.com/api/v2/';

        $data = ['secret' => $secret];
        $response = $this->httpClient->post($baseUri . 'admin/boards', ['form_params' => $data]);
        $responseBody = $response->getBody()->getContents();
        $boards = json_decode($responseBody);

        foreach ($boards->boards as $board) {

            $modifiedAt = Carbon::parse($board->modifiedAt);
            $daysAgo = $modifiedAt->diff(Carbon::now())->days;
            $id = $board->_id;

            if ($daysAgo <= 8) {
                return;
            }

            // DELETE /api/v2/admin/boards/< board_id >/delete
            $response = $this->httpClient->delete($baseUri . 'admin/boards/' . $id . '/delete', ['form_params' => $data]);
            if ($response->getStatusCode() !== 200) {
                throw new \Exception($response->getBody()->getContents());
            }

        }


    }
}
