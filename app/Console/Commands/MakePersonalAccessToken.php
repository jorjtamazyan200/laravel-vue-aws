<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\User;

class MakePersonalAccessToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'personal-access-token:make {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a personal access token for a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('user');
        $user = User::find($userId);

        if (is_null($user)) {
            $this->error("No user found with id $userId");

            return;
        }

        $token = $user->createToken('Personal Access Token');
        $expiry = $token->token['expires_at'];

        $this->info("Token: $token->accessToken\n");
        $this->info("Expires: $expiry");
    }
}
