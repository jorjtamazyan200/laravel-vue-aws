<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;

class FindOrCreateDistrubtionUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'distribution:make:client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $userService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();
        $this->userService = $userService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $email = 'distribution@volunteer-vision.com';


        $password = $this->userService->generateNewPassword();

        $user = $this->findOrCreateUser($email, $password);


        $text = <<<EOT
        
the user was created with the following details:
  ---------------------
  id    \t %s
  email \t %s
  password\t %s
  expiry\t %s
  token  \t %s
  ---------------------
EOT;


        $token = $user->createToken('Personal Access Token');
        $expiry = $token->token['expires_at'];

        $this->info(sprintf($text, $user->id, $user->email, $password, $expiry, $token->accessToken));


    }

    private function findOrCreateUser($email, $password): User
    {

        $user = User::where('email', '=', $email)->first();
        if ($user) {
            return $user;
        }


        $user = factory(User::class)->create([
            'first_name' => 'Platform',
            'last_name' => 'User',
            'email' => $email,
            'gender' => 'male',
            'password' => bcrypt($password),
            'organization_id' => 1,
            'brand_id' => 1,
            'primary_role' => Role::ROLES['distributionservice']
        ]);

        $primaryRoleObject = Role::whereName(Role::ROLES['distributionservice'])->first();
        $user->roles()->attach($primaryRoleObject);

        return $user;

    }
}
