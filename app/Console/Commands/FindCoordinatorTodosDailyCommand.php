<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Admin\Domain\Models\CoordinatorTodo;
use Modules\Admin\Domain\Services\CoordinatorTodoService;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\EnrollmentService;
use Modules\Core\Domain\Services\UserService;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class FindCoordinatorTodosDailyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:todos:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $coordinatorTodoService;
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var MatchService
     */
    protected $matchService;


    /**
     * @var
     */
    protected $enrollmentService;

    protected $todosAdded = 0;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CoordinatorTodoService $coordinatorTodoService, UserService $userService, MatchService $matchService, EnrollmentService $enrollmentService)
    {
        parent::__construct();
        $this->coordinatorTodoService = $coordinatorTodoService;
        $this->userService = $userService;
        $this->enrollmentService = $enrollmentService;
        $this->matchService = $matchService;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


//        $this->findUsersOnRegisterState();

//        $user = User::query()->inRandomOrder()->firstOrFail();
//
//        $this->addTodo($user, CoordinatorTodo::TYPES['INCOMPLETE_REGISTRATION'], Carbon::now());

        // find invalid webinars
//        $this->findNoWebinarScheduledEnrollment();
//        $this->findNotMatchedUsers();
//        $this->findMatchesWithoutFirstAppointment();


        $this->info('done. added ' . $this->todosAdded . ' Todos');


    }

    private function findUsersOnRegisterState()
    {
        $users = $this->userService->listHangingOnState(User::STATES['NEW'], 24 * 5);
        foreach ($users as $user) {
//            $this->addTodo($user, CoordinatorTodo::TYPES['INCOMPLETE_REGISTRATION'], $user->last_state_change_at);
        }
    }


    private function findNoWebinarScheduledEnrollment()
    {

        // @otdo: to check;
//        $enrollments = $this->enrollmentService->findWithoutFutureTraining();
//        foreach ($enrollments as $enrollment) {
//            $this->addTodo($enrollment->user,
//                CoordinatorTodo::TYPES['NO_FUTURE_WEBINAR'],
//                $enrollment->last_state_change_at, ['enrollment_id' => $enrollment->id]);
//        }
    }

//
//    private function findNotMatchedUsers()
//    {
//
//        $enrollments = $this->enrollmentService->findWithoutMatchFor7Days();
//        foreach ($enrollments as $enrollment) {
//
//            $this->addTodo($enrollment->user,
//                CoordinatorTodo::TYPES['WITHOUT_MATCH'],
//                Carbon::now(),
//                ['enrollment_id' => $enrollment->id, 'last_status_change' => $enrollment->last_state_change_at]
//            );
//        }
//    }


    /**
     *
     */
//    private function findMatchesWithoutFirstAppointment()
//    {
//        /**
//         * @var Match $match ;
//         */
//        $matches = $this->matchService->findMatchesWithoutPlannedAppointments(Match::STATES['CONFIRMED'])->get();
//        foreach ($matches as $match) {
//            $user = $match->getMentorParticipation()->enrollment->user;
//            if ($user) {
//                $this->addTodo($user, CoordinatorTodo::TYPES['NO_FIRST_APPOINTMENT'], $match->last_state_change_at, ['match_id' => $match->id]);
//            }
//
//        }
//
//        $matches = $this->matchService->findMatchesWithoutPlannedAppointments(Match::STATES['ACTIVE'], 24 * 3)->get();
//        foreach ($matches as $match) {
//            $user = $match->getMentorParticipation()->enrollment->user;
//            if ($user) {
//                $this->addTodo($user, CoordinatorTodo::TYPES['NO_FUTURE_APPOINTMENT'], $match->last_state_change_at, ['match_id' => $match->id]);
//            }
//        }
//    }


    private function addTodo(User $user, $type, $lastUserAction, $meta = []) // @todo: last user seen...
    {


        $duedate = Carbon::parse($lastUserAction);
        $duedate->addHours(CoordinatorTodo::getReactionTimeByType($type));

        $data = [
            'meta' => $meta,
            'type' => $type,
            'customer_id' => $user->id,
            'duedate' => $duedate,
        ];
        $todo = new CoordinatorTodo($data);
        $saved = $this->coordinatorTodoService->createTodoIfNotExistsWithinDays($todo, 7);
        if ($saved) {
            $this->todosAdded++;
        }
    }

}
