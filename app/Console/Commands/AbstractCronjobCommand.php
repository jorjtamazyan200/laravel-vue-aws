<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Carbon;
use Modules\Core\Domain\Models\CronjobLog;

abstract class AbstractCronjobCommand extends Command
{
//    /**
//     * The name and signature of the console command.
//     *
//     * @var string
//     */
//    protected $signature = 'deployment:after';

//    /**
//     * The console command description.
//     *
//     * @var string
//     */
//    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();

    }

    protected $cronjobLog = null;
    protected $startTime = null;
    protected $name = null;

    protected function init()
    {
        $name = $this->signature ? $this->signature : 'unknown job';

        $this->startTime = microtime(true);

        $this->cronjobLog = new CronjobLog();
        $this->cronjobLog->name = $name;
        $this->cronjobLog->save();

    }

    /**
     * $args can be an array or multiple parameters;
     * @param $string
     * @param mixed ...$args
     */
    protected function sprintfInfo($string, ...$args)
    {
        if (is_array($args[0])) {
            $args = $args[0];
        }
        $name = $this->signature ? $this->signature : 'unknown job';
        $text = $args ? vsprintf($string, $args) : $string;
        $formattedText  = Carbon::now()->toDateTimeString() . ' [' . $name . '] ' . $text;
        $this->info($formattedText);
        return $text;
    }

    /**
     * @param $text
     * @param mixed ...$args
     *
     */
    protected function end($text, ... $args)
    {
        $result = $this->sprintfInfo($text, $args);

        $time_end = microtime(true);

        $execution_time = ($time_end - $this->startTime);

        if (!$this->cronjobLog) {
            throw new \Exception("this->init() was not called in command;");
        }

        $this->cronjobLog->result = $result;
        $this->cronjobLog->success = true;
        $this->cronjobLog->duration = (int)($execution_time * 1000);
        $this->cronjobLog->save();

    }
}
