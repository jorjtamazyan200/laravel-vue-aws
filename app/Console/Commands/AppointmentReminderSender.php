<?php

namespace App\Console\Commands;

use App\Notifications\Appointment\AppointmentReminderNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

/**
 * @todo: test
 */
class AppointmentReminderSender extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:appointment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Reminder to Appointemnts';


    protected $appointmentService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AppointmentService $appointmentService)
    {
        parent::__construct();
        $this->appointmentService = $appointmentService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {

        $this->init();
        $now = Carbon::now();
        /**
         * @var Collection $appointments
         * @var Appointment $appointment
         * @var User $user
         * @var Enrollment $enrollment
         */
        $appointments = $this->appointmentService->findAppointmentsToRemind();

        $appointments->load(['enrollments']);
        $counter = 0;

        $appointments->each(function ($appointment) use ($counter) {
//
            $appointment->last_notification_at = Carbon::now();
            $appointment->save();


            $enrollments = $appointment->enrollments;
            $enrollments->each(function ($enrollment) use ($appointment, $counter) {

                $user = $enrollment->user;
                if ($user->id == 30) return;
                if ($enrollment->role === Role::ROLES['mentee']) {
                    $user->notify(new AppointmentReminderNotification($user->brand, $appointment, $enrollment));
                }
            });
        });

        $this->end('Send %s appoinemtnts ', $appointments->count());

    }
}
