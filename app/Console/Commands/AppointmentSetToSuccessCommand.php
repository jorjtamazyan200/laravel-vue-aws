<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Scheduling\Domain\Models\Appointment;
use Modules\Scheduling\Domain\Services\AppointmentService;

class AppointmentSetToSuccessCommand extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:appointments:autosuccess';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets appointemnts to success if they are on ONLINE sice more then 1,5 hrs.';

    protected $appointmentService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AppointmentService $appointmentService)
    {
        parent::__construct();
        $this->appointmentService = $appointmentService;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->init();

        //
        $total = 0;
        $appointments = $this->appointmentService->getOnlineAppointmentsWithoutConferenceInformation();

        /** @var Appointment $appointment */
        foreach ($appointments as $appointment) {
            $total++;
            try {
                $this->appointmentService->finishAppointment($appointment, 'timeout');
            } catch (\Exception $e) {
                report($e);
            }
        }


        $appointments = $this->appointmentService->getPastAppointmentsWithStatus(
            [Appointment::STATES['PLANNED'], Appointment::STATES['CONFIRMED']]
        );

        /** @var Appointment $appointment */
        foreach ($appointments as $appointment) {
            $total++;
            try {
                if ($appointment->transitionAllowed('report_noattendance')) {
                    $appointment->transition('report_noattendance');
                }
            } catch (\Exception $e) {
                report($e);
            }
        }

        $this->end('[AppointmentAutoSucess] moved %s appoinemnts via timeout or noattendance', $total);

    }
}
