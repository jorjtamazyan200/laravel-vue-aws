<?php

namespace App\Console\Commands;

use App\Notifications\User\UserRegistrationCompleteMenteeNotification;
use App\Notifications\User\UserRegistrationCompleteNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use Faker\Factory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;

class SendSampleMailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:sample:mail {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $email = $this->argument('email');
        $user = User::where('email', '=', $email)->first();


        if ($user) {
            $notification = new UserRegistrationCompleteMenteeNotification($user->brand, $user);
            $this->info('Sending sample mail to  user ' . $user->email);
            $user->notify($notification);
            return;
        }


        $user =  factory(User::class)->create([
            'email' => $email,
        ]);


        $notification = new UserRegistrationCompleteMenteeNotification($user->brand, $user);

        $this->info('Sending sample mail to custom email ' . $email);

        $user
            ->notify($notification);


        $user->forceDelete();

        $this->info('done');

        //
    }
}
