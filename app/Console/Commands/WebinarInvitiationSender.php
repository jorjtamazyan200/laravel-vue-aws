<?php

namespace App\Console\Commands;

use App\Notifications\Webinar\WebinarInvitationNotification;
use App\Notifications\Webinar\WebinarReferentNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Domain\Services\WebinarService;

/**
 * @todo: no test
 * Class WebinarInvitiationSender
 * @package App\Console\Commands
 */
class WebinarInvitiationSender extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:webinar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Reminder for webinar Invitiation';


    protected $webinarService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WebinarService $webinarService)
    {
        $this->webinarService = $webinarService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //


        $this->init();

        MultiTenant::setTenantId(null);

        $webinars = $this->webinarService->findWebinarsToRemind();
        $notifications = 0;

        foreach ($webinars as $webinar) {
            $enrollments = $webinar->enrollments;
            foreach ($enrollments as $enrollment) {
                if ($enrollment->pivot->attended) {
                    continue;
                }
                if ($enrollment->pivot->invitation_sent) {
                    continue;
                }

                $enrollment->pivot->invitation_sent = Carbon::now();
                $enrollment->pivot->save();

                $user = $enrollment->user;
                $notification = new WebinarInvitationNotification($user->brand, $webinar);
                $user->notify($notification);
                $notifications++;
            }
            $referent = $webinar->referent;
            if (!empty($referent)) {
                $notification = new WebinarReferentNotification($user->brand, $webinar);
                $referent->notify($notification);
            }
        }

        $this->end('[webinar reminder] done send %s notifications to %s webinars', $notifications, $webinars->count());


    }
}