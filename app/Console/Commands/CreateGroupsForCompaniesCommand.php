<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\Group;
use Modules\Core\Domain\Models\User;

/**
 * One time Command
 *
 * Class CreateGroupsForCompaniesCommand
 * @package App\Console\Commands
 */
class CreateGroupsForCompaniesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:create:groups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $users = User::all();
        foreach ($users as $user) {

            if ($user->groups()->count() > 0) {
                continue;
            }
            $orgName = $user->organization->name;

            $group = Group::firstOrCreate(['name' => $orgName . ' Gruppe', 'organization_id' => $user->organization_id]);
            $user->groups()->attach($group);

        }


    }
}
