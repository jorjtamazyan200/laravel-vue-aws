<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\Translation;
use Modules\Core\Domain\Services\DeeplService;
use Modules\Core\Domain\Services\TranslationService;

class TranslateMissingTranslationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:missing:translations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $translationService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TranslationService $translationService)
    {
        parent::__construct();
        $this->translationService = $translationService;


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->translationService->findMissingTranslationsFromEnglish('conference3','de');


    }
}
