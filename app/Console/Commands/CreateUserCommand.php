<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Domain\Models\Role;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;

class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:make:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $userService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();
        $this->userService = $userService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $email = $this->ask('Please enter the email adress?');
        $firstName = $this->ask('First Name?');
        $lastName = $this->ask('Last Name?');

        $primaryRole = $this->choice('What is users role?', [
            Role::ROLES['coordinator'], Role::ROLES['admin'], Role::ROLES['supervisor']
        ]);

        $gender = $this->choice('Gender?', [
            'male', 'female'
        ]);

        $password = $this->userService->generateNewPassword();


        // create user;

        $user = factory(User::class)->create([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'gender' => $gender,
            'password' => bcrypt($password),
            'organization_id' => 1,
            'brand_id' => 1,
            'primary_role' => $primaryRole
        ]);

        $primaryRoleObject = Role::whereName($primaryRole)->first();
        $user->roles()->attach($primaryRoleObject);

        $useRoleObject = Role::whereName(Role::ROLES['user'])->first();
        $user->roles()->attach($useRoleObject);



        $this->info(sprintf('user was created with id %s following login credentials: %s',  $user->id, $password));

        $this->info('-----------------------');
        $this->info(sprintf('email: %s',  $user->email));
        $this->info(sprintf('password: %s',  $password));
        $this->info('-----------------------');



        //
    }
}
