<?php

namespace App\Console\Commands;

use App\Notifications\Supervisor\WeeklyReportNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Domain\Services\GroupService;
use Modules\Matching\Domain\Services\MatchService;

class SupervisorWeeklySender extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:mail:supervisor:weekly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a weekly report to supervisor';


    protected $groupService;
    protected $matchService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GroupService $groupService, MatchService $matchService)
    {
        parent::__construct();
        $this->groupService = $groupService;
        $this->matchService = $matchService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groups = $this->groupService->findGroupsToSendReport();

        $this->init();
        $groups->each(/**
         * @param $group
         */
            function ($group) {
            $group->report_weekly_sent_at = Carbon::now();
            $group->save();
            $matches = $this->matchService->findNewMatchesForSupervisorInGroup($group, 7);

            $supervisors = $group->supervisors;


            foreach ($supervisors as $supervisor) {
                if (!$supervisor->supervisor_daily_enabled) {
                    continue;
                }
                $supervisor->notify(new WeeklyReportNotification($supervisor->brand, $matches));
            }

        });
        $this->end();

        //
    }
}
