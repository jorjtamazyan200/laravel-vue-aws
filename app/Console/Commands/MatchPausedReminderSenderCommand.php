<?php

namespace App\Console\Commands;

use App\Notifications\Match\MatchPausedCheckinNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Domain\Models\Participation;
use Modules\Core\Domain\Models\User;
use Modules\Core\Reminder\MatchPausedReminderHandler;
use Modules\Core\Reminder\MatchRequestReminderHandler;
use Modules\Matching\Domain\Models\Match;
use Modules\Matching\Domain\Services\MatchService;

class MatchPausedReminderSenderCommand extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:matchPaused';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $matchService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MatchService $matchService)
    {
        parent::__construct();
        $this->matchService = $matchService;


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $this->init();

        $matches = $this->matchService
            ->listHangingOnState(Match::STATES['PAUSED'], 14 * 24);

        /** @var MatchRequestReminderHandler $handler */
        $handler = App(MatchPausedReminderHandler::class);
        $eventsFired = $handler->applyActions($matches);
        $this->end(' %s events founds ', $eventsFired);

    }
}
