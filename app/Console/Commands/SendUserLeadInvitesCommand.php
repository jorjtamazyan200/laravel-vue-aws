<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\UserLead;
use Modules\Core\Domain\Services\UserLeadService;
use Modules\Core\Reminder\UserLeadReminderHandler;

class SendUserLeadInvitesCommand extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:reminder:userlead:invites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected  $userleadService;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserLeadService $userLeadService)
    {
        parent::__construct();
        $this->userleadService = $userLeadService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {

        $this->init();

        /** @var Collection $leads */
        $leads = $this->userleadService->getLeadsOnState([UserLead::STATES['CONTACTED'], UserLead::STATES['NEW']]);

        /** @var UserLeadReminderHandler $handler */
        $handler = App(UserLeadReminderHandler::class);
        $eventsFired = $handler->applyActions($leads);
//        $this->info('[user leads reminder] );

        $this->end(' Events fired: %s of checked entries %s ',$eventsFired, $leads->count());


    }
}
