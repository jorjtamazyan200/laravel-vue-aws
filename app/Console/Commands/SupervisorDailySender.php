<?php

namespace App\Console\Commands;

use App\Notifications\Supervisor\SupervisorDailyReportNotification;
use App\Notifications\Supervisor\WeeklyReportNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Domain\Services\GroupService;
use Modules\Matching\Domain\Services\MatchService;
use Modules\Scheduling\Domain\Services\AppointmentService;

class SupervisorDailySender extends AbstractCronjobCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:mail:supervisor:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a daily report to supervisor';


    protected $groupService;
    protected $appointmentService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GroupService $groupService, AppointmentService $appointmentService)
    {
        parent::__construct();
        $this->groupService = $groupService;
        $this->appointmentService = $appointmentService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->init();
        $groups = $this->groupService->findGroupsForDailyReport();


        $groups->each(/**
         * @param $group
         */
            function ($group) {
                $group->report_daily_sent_at = Carbon::now();
                $group->save();
//
                $appointments = $this->appointmentService->listWithUserForSupervisorOfGroup($group, 2);

                $supervisors = $group->supervisors;
                if ($appointments->count() === 0) {
                    return;
                }

                foreach ($supervisors as $supervisor) {
                    if (!$supervisor->supervisor_daily_enabled) {
                        continue;
                    }
                    $supervisor->notify(new SupervisorDailyReportNotification($supervisor->brand, $appointments));
                }

            });
        //
        $this->end('done');

    }
}
