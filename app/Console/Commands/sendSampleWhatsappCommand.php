<?php

namespace App\Console\Commands;

use App\Services\WaboboxService;
use Illuminate\Console\Command;
use Modules\Core\Domain\Models\User;

class SendSampleWhatsappCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vv:sample:whatsapp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Whatsapp';

    protected $waboboxService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WaboboxService $waboboxService)
    {
        parent::__construct();
        $this->waboboxService = $waboboxService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $number = '1776706937';

        /** @var User $user */
        $user = User::where('whatsapp_number', '=', $number)
            ->firstOr(function () use ($number) {
                return Factory(User::class)->create(['whatsapp_number' => $number, 'whatsapp_number_prefix' => 49]);
            });

//        $user->accept_sms = true;
        // accept whatsapp
        $user->save();

        $this->waboboxService->queueMessage($user, 'Hello Queue');

//        $this->waboboxService->send($user, 'Was geht ab?');

        $this->info('Sending sample sms to  ' . $user->qualifiedWhatsappNumber);

        $this->info('done');

    }
}
