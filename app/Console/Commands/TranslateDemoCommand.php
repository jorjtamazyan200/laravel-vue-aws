<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Services\BrandedDocumentService;
use Modules\Core\Domain\Services\DeeplService;
use Symfony\Component\Routing\Route;

class TranslateDemoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $deeplService;
    protected $brandedDocumentService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DeeplService $deeplService, BrandedDocumentService $brandedDocumentService)
    {
        parent::__construct();
        $this->deeplService = $deeplService;
        $this->brandedDocumentService = $brandedDocumentService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        // find all documents with missing / old tanslations;
        // get translations;
        // create / update these documents;
        // render template to make sure the translation worked;

        $i = 0;

        foreach (['de', 'es'] as $checkLanuage) {
            $enDocuments = $this->brandedDocumentService->findDocumentKeysWithMissingTranslations($checkLanuage);
            foreach ($enDocuments as $document) {
                if ($i > 5) {
                    echo ('stopped after ' . $i . ' documents. If you want to translate more, please run the command again');
                    return;
                }

                $this->info('Trying to translate ' . $document->key . '( id: '.$document->id.' ) from ' . $document->language . ' to ' . $checkLanuage);
                try {
                    $i++;
                    $route = route('admin.previewbrandeddocument', ['id' => $document->id]);
                    $request = Request::create($route, 'GET');
                    $response = app()->handle($request);

                    if ($response->getStatusCode() != 200) {
                        $this->error('could not render source document: '. $document->id . ' Please check the template');
                        continue;
                    }

                    if (empty($document->markdown) && empty($document->content)){
                        throw new \Exception('Document has no content id:'. $document->id);
                    }

                    $brandedDocument = $this->brandedDocumentService->findOrCreateTranslationForDocument($checkLanuage, $document);
                    // try to parse excample request:

                    $route = route('admin.previewbrandeddocument', ['id' => $brandedDocument->id]);
                    $request = Request::create($route, 'GET');

                    $response = app()->handle($request);

                    if ($response->getStatusCode() != 200) {
                        $this->error('Render of document ' . $brandedDocument->id . 'failed, please check if manually!');
                        $this->info('--------------------------------------------------');
                        continue;
                    }

                    $this->info('successfully created ' . $brandedDocument->id);
                    $this->info('--------------------------------------------------');

                } catch (\Exception $exception) {
                    $this->error('Failed because:: ' . $exception->getMessage());
                }


            }
        }

    }
}
