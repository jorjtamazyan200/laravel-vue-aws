<?php

namespace App\Console\Commands;

use App\Notifications\User\CompleteRegistrationReminderNotification;
use App\Services\MultiTenant\Facades\MultiTenant;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;

class UserCompleteRegistrationReminderSender extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:user:registration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * @var UserService
     */
    protected $userService;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();
        $this->userService = $userService;


    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // this was replaced by enrollment incomplete

//        MultiTenant::setTenantId(null);
//        $users = $this->userService->listHangingOnStateWithoutReminder(User::STATES['NEW'], 24);
//        /** @var User $user */
//        foreach ($users as $user){
//            $user->last_reminder = Carbon::now();
//            $user->save();
//            $user->notify(new CompleteRegistrationReminderNotification($user->brand, $user));
//        }

    }
}
