<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;
use Modules\External\Domain\Services\ZendeskService;


class SyncZendeskUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zendesk:sync {--limit=5}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $zendeskService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ZendeskService $zendeskService)
    {
        parent::__construct();
        $this->zendeskService = $zendeskService;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        //

        if (!$this->isEnabled()) {
            $this->info('Zendesk Module not enabled; skipping');
            return;
        }

        $limit = (int) $this->option('limit');

        $organizationsProcessed = 0;
        $usersProcessed = 0;


        $organizations = Organization::query()
            ->inRandomOrder()
            ->where('state', Organization::STATES['ACTIVE'])
            ->whereNull('zendesk_last_sync')
            ->limit($limit)
            ->get();



        if ($organizations->count() > 0) {
            foreach ($organizations as $organization) {
                $this->info("Processing organization $organization->name ");
                $this->zendeskService->createOrUpdateOrganization($organization);
                $organizationsProcessed++;
            }
            $this->info("done: $organizationsProcessed Orgnizations processed ");
            return;
        }


        $users = User::query()
            ->whereNull('zendesk_last_sync')
            ->whereIn('state', [User::STATES['REGISTERED'], User::STATES['INMIGRATION']])
            ->where('count_for_reporting', true)
            ->limit($limit)
            ->with('organization')
            ->get();

        if ($users->count() === 0){
            $this->info('[zendesk sync] no users found');
            return;

        }

        foreach ($users as $user) {
            $this->info("Processing user $user->email ");
            $this->zendeskService->updateOrCreateUser($user);
            $usersProcessed++;
        }


        $this->info("done: $usersProcessed Users processed | $organizationsProcessed Orgnizations processed ");

    }
}
