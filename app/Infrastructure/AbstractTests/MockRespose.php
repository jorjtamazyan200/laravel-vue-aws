<?php

namespace App\Infrastructure\AbstractTests;

/**
 * Data container class for mocking HTTP responses from Guzzle.
 *
 * Unfortunately, guzzles responses neither are of any know class, nor does
 * the PSR-7/Response interface they implement provide the required
 * `getBody` method, rendering it impossible to mock them.
 */
class MockResponse
{
    private $rawBody;

    public function __construct(string $rawBody)
    {
        $this->rawBody = $rawBody;
    }

    public function getBody() : string
    {
        return $this->rawBody;
    }
}
