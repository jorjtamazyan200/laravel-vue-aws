<?php

namespace App\Infrastructure\Domain;

use App\Infrastructure\Contracts\Query;
use Illuminate\Database\Eloquent\Builder;
use InvalidArgumentException;
use Misiuziu\Bruno\EloquentBuilderTrait;

/**
 * Interprets a query as defined by a App\Infrastructure\Contracts\Query
 * and applies it to an Eloquent Builder.
 */
trait AppliesQueryArgsToEloquent
{
    use EloquentBuilderTrait;

    /**
     * Apply sorting, filtering, pagination & eager loads to $builder according to $query.
     *
     * @param  Builder $builder
     * @param  Query $query
     * @return void
     */
    public function apply(Builder $builder, Query $query)
    {
        $this->applyResourceOptions($builder, $query->options());
    }

    /**
     * Apply only includes to $builder according to $query.
     *
     * @param  Builder $builder
     * @param  Query $query
     * @return void
     */
    public function applyIncludes(Builder $builder, Query $query)
    {
        if ($includes = $query->options()['includes']) {
            if (!is_array($includes)) {
                throw new InvalidArgumentException('Includes should be an array.');
            }

            $builder->with($includes);
        }
    }
    /**
     * Apply only includes to $builder according to $query.
     *
     * @param  Builder $builder
     * @param  Query $query
     * @return void
     */
    public function applyWithCount(Builder $builder, Query $query)
    {

        if ($withCount = $query->options()['withCount']) {
            if (!is_array($withCount)) {
                throw new InvalidArgumentException('Includes should be an array.');
            }

            $builder->withCount($withCount);
        }
    }



    /**
     *
     */

    public function applySearch(Builder $builder, Query $query)
    {
        $options = $query->options();
        if (!$options['search'] || $options['search'] == '') {
            return;
        }

        if (!isset($this->model->searchable)) {
            return;
        }

        $keyword = $this->trimSearch($options['search']);

        $searchable = $this->model->searchable;

        
        $builder->where( function($qb) use ($searchable, $keyword) {
            foreach ($searchable as $field){
                $qb->orWhereRaw( "lower($field) LIKE ?", [ '%' . $keyword . '%']);
//                $qb->orWhereRaw("? % " . $field, [$keyword]);
            }
        });

    }

    /**
     * @param $search string
     * @return array
     */
    private function trimSearch(string $search): string
    {
        // @todo: maybe remove special character etc.
        return strtolower($search);
    }
}
