<?php

namespace App\Infrastructure\Traits;

use App\Exceptions\Client\ClientException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Sebdesign\SM\Facade as StateMachineFacade;
use SM\SMException;
use SM\StateMachine\StateMachine;

/**
 * Helpers for models that maintain a "state" using `SM\StateMachine`.
 *
 * Contains proxies for commonly used methods on the state machine
 * like `can` or `apply`. Provides a relationship to the model
 * that tracks the history of state changes on the object.
 *
 * Each model using this Trait needs to have a  'state' and 'last_state_change_at'
 */
trait Statable
{
    /**
     * The state machine instance used by this object.
     *
     * @var SM\StateMachine\StateMachine
     */
    protected $stateMachine;

    /**
     * Determine whether the model tracks the history of state changes.
     *
     * @return boolean
     */
    public static function logsTransitions(): bool
    {
        return defined(static::class . '::HISTORY_MODEL') && !!static::HISTORY_MODEL;
    }

    /**
     * Get the current state of the state machine.
     *
     * @return string
     */
    public function currentState(): string
    {
        return $this->stateMachine()->getState();
    }

    /**
     * Get the state machine instance for this object.
     * @return StateMachine
     */
    public function stateMachine(): StateMachine
    {
        if (!$this->stateMachine) {
            $this->stateMachine = StateMachineFacade::get($this);
        }

        return $this->stateMachine;
    }

    /**
     * Apply a state transition.
     *
     * @param  string $transition Name of the transition to apply
     * @return bool  Whether or not the transition has been applied
     */
    public function transition(string $transition): bool
    {
        try {
            if (!$this->transitionAllowed($transition)) {
                $error = sprintf("Transition -%s- is not allowed from state -%s- in [%s:%s]", $transition, $this->state, class_basename($this), $this->id);
                throw new ClientException($error);
            }
        } catch (SMException $e) {
            throw new ClientException("Transition does not exist: " . $transition . ' - ' . $e->getMessage());
        }

        return $this->stateMachine()->apply($transition);
    }

    /**
     * Check if given transition is currently possible.
     *
     * @param string $transition
     * @return boolean
     */
    public function transitionAllowed(string $transition): bool
    {
        return $this->stateMachine()->can($transition);
    }

    /**
     * Create a new entry in the models' state change log.
     *
     * @param array $transitionData
     * @return Illuminate\Database\Eloquent\Model
     */
    public function logTransition(array $transitionData): Model
    {
        //updating the model itself
        $this->update(['last_state_change_at' => Carbon::now()]);

        //creating an entry in the Model's log
        return $this->history()->create($transitionData);
    }


    /**
     * Retrieve a the state change log.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history(): HasMany
    {
        return $this->hasMany(static::HISTORY_MODEL);
    }

    /**
     * Returns an array of all possible states ['NEW',..]
     *
     * @return array
     */

    public static function allStates(): array
    {
        return array_map(function ($a) {
            return $a;
        }, self::STATES);
    }

}
