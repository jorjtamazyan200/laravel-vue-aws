<?php

namespace App\Infrastructure\Traits;

use Modules\Core\Domain\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Common methods for models that act as "state change history" for other models.
 *
 * Each model using this Trait needs to have a nullable `actor_id` referring
 * to the id of the user that issued the logged state change.
 */
trait LogsState
{
    /**
     * Boot the trait on Eloquent Models.
     *
     * @return void
     */
    public static function bootLogsState() : void
    {
        // Populate the actor_id with the id of the authenticated user, if it
        // hasn't already been set by the code that issued the creation.
        static::creating(function (Model $model) {
            if (!$model->actor_id && Auth::check()) {
                $model->actor_id = Auth::id();
            }
        });
    }

    /**
     * The "actor" who issued the state change.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function actor() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
