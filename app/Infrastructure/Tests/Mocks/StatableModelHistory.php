<?php

namespace App\Infrastructure\Tests\Mocks;

use App\Infrastructure\Traits\LogsState;

use Illuminate\Database\Eloquent\Model;

/**
 * Helper class for testing the `Statable` traits' history feature.
 *
 * @property-read \Modules\Core\Domain\Models\User $actor
 * @mixin \Eloquent
 */
class StatableModelHistory extends Model
{
    use LogsState;

    /**
     * The attributes that are mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['transition', 'to', 'user_id'];

    /**
     * Override this method because the table doesn't exist.
     *
     * @return void
     */
    public function save(array $options = []) : void
    {
        //
    }
}
