<?php

namespace App\Infrastructure\Tests\Mocks;

use App\Infrastructure\Traits\Statable;

use Illuminate\Database\Eloquent\Model;

/**
 * Helper class for testing the `Statable` trait.
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Infrastructure\Tests\Mocks\StatableModelHistory[] $history
 * @mixin \Eloquent
 */
class StatableModel extends Model
{
    use Statable;

    const HISTORY_MODEL = StatableModelHistory::class;

    /**
     * The attributes that are mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['state'];

    /**
     * Override this method because the table doesn't exist.
     *
     * @return void
     */
    public function save(array $options = []) : void
    {
        //
    }
}
