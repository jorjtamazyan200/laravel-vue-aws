<?php

namespace App\Infrastructure\Http;

use Illuminate\Foundation\Http\FormRequest as IlluminateFormRequest;

/**
 * Base FormRequest class that handles deserialization of request payloads.
 */
abstract class FormRequest extends IlluminateFormRequest
{
    /**
     * Deserialization step for the input payload.
     *
     * @param  array  $input Raw payload
     * @return array  Transformed payload
     */
    abstract protected function deserialize(array $input) : array;

    /**
     * Retrieve an input item from the request.
     *
     * @param  string  $key
     * @param  string|array|null  $default
     * @return string|array
     */
    public function input($key = null, $default = null)
    {
        return data_get(
            $this->deserialize($this->getInputSource()->all() + $this->query->all()), $key, $default
        );
    }
}
