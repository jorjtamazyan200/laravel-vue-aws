<?php

namespace App\Infrastructure\Http;

use App\Infrastructure\Contracts\BreadService;
use ArrayAccess;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use UnexpectedValueException;

abstract class BreadServiceController extends BaseController
{
    /**
     * The service to which the controller defers the work.
     *
     * @var App\Infrastructure\Contracts\BreadService
     */
    protected $service;

    /**
     * The constructor must be declared to require a service via dependency injection.
     */
    public function __construct(BreadService $service)
    {
        $this->service = $service;
        $this->validateResourceClasses();
    }

    /**
     * A way for the implementing class to tell us which "API Resource"
     * to use for transforming single records at this endpoint.
     *
     * This is necessary because conditional dependency injection in
     * laravel works only on class constructor methods.
     *
     * Must return the FQN of a subclass of Illuminate\Http\Resources\Json\Resource
     * @param $view string default | detail | $_GET['view']
     * @return string
     */
    abstract protected function getResourceClass(string $view): string;

    /**
     * A way for the implementing class to tell us which FormRequest to use for
     * deserializing/validating an incoming request for a 'store' operation.
     *
     * This is necessary because conditional dependency injection in
     * laravel works only on class constructor methods.
     *
     * Must return the FQN of a subclass of Illuminate\Http\Request
     *
     * @return string
     */
    abstract protected function getStoreRequestClass(): string;

    /**
     * A way for the implementing class to tell us which FormRequest to use for
     * deserializing/validating an incoming request for a 'update' operation.
     *
     * This is necessary because conditional dependency injection in
     * laravel works only on class constructor methods.
     *
     * Must return the FQN of a subclass of Illuminate\Http\Request
     *
     * @return string
     */
    abstract protected function getUpdateRequestClass(): string;

    /**
     * Handle a GET request to the / endpoint.
     *
     * @return \ArrayAccess
     */
    public function index(Request $request): ArrayAccess
    {
        $query = new Query($request);

        if ($query->isPaginated()) {
            $paginator = $this->service->paginate($query);

            return $this->makeCollection($paginator, $request);
        }

        $results = $this->service->list($query);

        return $this->makeCollection($results, $request);
    }

    /**
     * Handle a POST request to the / endpoint.
     *
     * @return \ArrayAccess
     */
    public function store(): ArrayAccess
    {
        $request = $this->getStoreRequest();

        return $this->makeResource($this->service->create($request->all()));
    }

    /**
     * Handle a GET request to the /{id} endpoint.
     *
     * @param  mixed $id Unique ID of the record to retrieve
     * @return \ArrayAccess
     */
    public function show($id, Request $request): ArrayAccess
    {
        $query = new Query($request);

        return $this->makeResource($this->service->get($id, $query));
    }

    /**
     * Handle a PUT/PATCH request to the /{id} endpoint.
     *
     * @param  mixed $id Unique ID of the record to delete
     * @return \ArrayAccess
     */
    public function update($id): ArrayAccess
    {
        $request = $this->getUpdateRequest();
        return $this->makeResource($this->service->update($id, $request->all()));

    }

    /**
     * Handle a DELETE request to the /{id} endpoint.
     *
     * @param  mixed $id Unique ID of the record to delete
     * @return \ArrayAccess
     */
    public function destroy($id): ArrayAccess
    {
        return $this->makeResource($this->service->delete($id));
    }

    /**
     * Resolve the Request for a 'store' operation through the container.
     *
     * @return Request
     */
    protected function getStoreRequest(): Request
    {
        return App::make($this->getStoreRequestClass());
    }

    /**
     * Resolve the Request for an 'update' operation through the container.
     *
     * @return Request
     */
    protected function getUpdateRequest(): Request
    {
        return App::make($this->getStoreRequestClass());
    }

    /**
     * Create an API Resource Collection from an array of entities.
     *
     * @param  Collection|Paginator $items
     * @param  Request $request
     * @return ArrayAccess
     */
    protected function makeCollection($entities, Request $request): ArrayAccess
    {
        $view = $this->getViewClass($request);
        $class = $this->getResourceClass($view);

        return $class::collection($entities);
    }

    /**
     * Create an API Resource from a single entity.
     *
     * @param  ArrayAccess $item
     * @return ArrayAccess
     */
    protected function makeResource(ArrayAccess $entity): ArrayAccess
    {

        $class = $this->getResourceClass('detail');

        return $class::make($entity);
    }

    /**
     * @param Request $request
     * @return array|string
     */
    protected function getViewClass(Request $request)
    {
        return $request->query('view', 'list');
    }

    /**
     * Ensure that a subclass properly implements the abstract methods
     * by checking the return value of `getResourceClass()`.
     *
     * @throws \UnexpectedValueException if one of the return values is invalid
     * @return void
     */
    private function validateResourceClasses()
    {
        $resource_class = $this->getResourceClass('default');
        if (!is_a($resource_class, Resource::class) && !is_subclass_of($resource_class, Resource::class)) {
            throw new UnexpectedValueException(sprintf(
                '`%s` must return "%s" or the FQN of a descendant. It returned "%s" instead.',
                'getResourceClass()',
                Resource::class,
                $resource_class
            ));
        }
    }
}
