<?php

namespace App\Infrastructure\Http;

/**
 * Base FormRequest class that converts all input keys from camelCase to snake_case.
 */
abstract class DeserializedFormRequest extends FormRequest
{
    /**
     * Deserialization step for the input payload.
     *
     * @param  array  $input Raw payload
     * @return array  Transformed payload
     */
    protected function deserialize(array $input) : array
    {
        return $this->keysToSnakeCase($input);
    }

    /**
     * Convert an array's keys to camel_case.
     *
     * Returns a completely new array, leaving the input untouched.
     *
     * @param  array  $array
     * @return array
     */
    protected function keysToSnakeCase(array $array) : array
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[snake_case($key)] = $value;
        }

        return $result;
    }
}
