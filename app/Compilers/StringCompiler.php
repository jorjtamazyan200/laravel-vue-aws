<?php

namespace App\Compilers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class StringCompiler
{
    /**
     * Renders Blade template syntax string into HTML
     *
     * @param string $string
     * @param array $data ['recipient' => $user]
     * @return string
     */
    public static function render(string $string, array $data) : string
    {
        $output = self::parseBladeWithPlaceholders($string, $data);

        return $output;
    }

    /**
     * Replaces placeholders in blade syntax e.g. {{ $recipient->name }} with data
     *
     * @param string $string
     * @param array $data ['recipient' => $user]
     * @return string
     */
    public static function parseBladeWithPlaceholders(string $string, array $data) : string
    {

        $data['now'] = Carbon::now();

        $php = Blade::compileString($string);
        $__env = app(\Illuminate\View\Factory::class);


        $obLevel = ob_get_level();
        ob_start();
        extract($data, EXTR_SKIP);

        try {
            eval('?>' . $php);
        } catch (\Exception $e) {
            while (ob_get_level() > $obLevel) {
                ob_end_clean();
            }
            throw $e;
        } catch (\Throwable $e) {
            while (ob_get_level() > $obLevel) {
                ob_end_clean();
            }
            throw new FatalThrowableError($e);
        }

        return ob_get_clean();
    }
}
