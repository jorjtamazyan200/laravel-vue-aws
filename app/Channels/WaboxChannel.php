<?php

namespace App\Channels;

use App\Notifications\BrandedNotification;
use App\Services\WaboboxService;

use Illuminate\Notifications\Events\NotificationFailed;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Events\Dispatcher;
use Modules\Core\Domain\Models\User;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WaboxChannel
{

    /**
     * @var WaboboxService
     */
    protected $waboboxService;


    /**
     * @var Dispatcher
     */
    protected $events;


    public function __construct(WaboboxService $waboboxService, Dispatcher $events)
    {
        $this->waboboxService = $waboboxService;
        $this->events = $events;
    }

    /**
     * Send the given notification.
     *
     * @param  User $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {

        if (!$notifiable instanceof User) {
            $class = get_class($notifiable);
            Log::warning("Trying to send a whatsapp to a non-User : ${$class}");
            return;
        }

        /** @var User $user */
        $user = $notifiable;
        $organization = $user->organization;
        if ($organization && !$organization->whatsapp_enabled) {
            // skip if whatsapp is not available
            return;
        }

        $this->queueWhatsapp($notifiable, $notification);
    }

    private function queueWhatsapp(User $user, Notification $notification)
    {


        try {
            /** @var BrandedNotification $notification */
            $message = $notification->toWhatsapp($user);
            $key = 'custom';
            if ($notification instanceof BrandedNotification){
                $key = $notification->getDocumentKey();
            }

            if (!is_string($message)) {
                throw new NotFoundHttpException("Template has no content! of ${message}");
            }
            $this->waboboxService->queueMessage($user, $message, $key);

        } catch (\Exception $exception) {
            $event = new NotificationFailed($user, $notification, 'wabox', ['message' => $exception->getMessage(), 'exception' => $exception]);
            $this->events->dispatch($event);
            report($exception);
        }


    }

}