<?php

namespace App\Reminders\Handler;

use Carbon\Carbon;
use Illuminate\Support\Collection;


/**
 * Please see ReminderHandlerTest how to use it;
 *
 * Requirements to use this:
 *  - their is datetime field like 'last_reminder' on the entity
 *  - the field will not be changed from anyone else.
 *  - the field should be changed to NULL on any status change
 *
 *  - if the last_reminder value is LOWER then the event value
 *  (e.g.: last_reminder Monday, but status_change is Tuesday; the last reminder will be ignored)
 *
 * Class AbstractReminderHandler
 * @package App\Reminder\Handler
 */
abstract class AbstractReminderHandler
{

    protected $events = [];

    protected $lastReminderFieldName = 'last_reminder';
    protected $dateCallback = null;

    public function __construct()
    {

    }

    public function registerEvent($hours, $action)
    {
        $this->events[$hours] = $action;
    }

    /**
     * @param Collection $targetEntities
     * @return \Exception|int
     * @throws \Exception
     */
    public function applyActions(Collection $targetEntities)
    {

        if (!is_callable($this->dateCallback)) {
            return new \Exception('date callback not set or not callable');
        }


        $lastReminderFieldName = $this->lastReminderFieldName;

        $eventsFired = 0;

        /**
         *
         */
        foreach ($targetEntities as $entity) {


            // class as to use 'Remindable' trait
            if (!$entity->hasAttribute($lastReminderFieldName)) {
                throw new \Exception("Entity does not have a the reminder field: " . $lastReminderFieldName . ':' . get_class($entity));
            }

            $lastReminder = $entity->$lastReminderFieldName;
            $reasonDate = call_user_func($this->dateCallback, $entity);


            if ($reasonDate === null) {
                continue;
            }

            if (!$reasonDate instanceof Carbon) {
                throw new \Exception("Date callback has to be null or Carbon");
            }

            // ignore old reminders;
            if ($lastReminder < $reasonDate) {
                $lastReminder = null;
            }

            $triggerHours = $this->findDoAbleAction($reasonDate, $lastReminder);
            if ($triggerHours === null) {
                continue;
            }

            if (!isset($this->events[$triggerHours])) {
                throw new \Exception("Unknown event for time" . $triggerHours . " in " . get_class($this));
            }

            $action = $this->events[$triggerHours];

            if ($action === null) {
                continue;
            }
            // apply action;
            $entity->$lastReminderFieldName = Carbon::now();
            $entity->last_reminder_type = $triggerHours;
            $entity->save();
            $eventsFired++;
            try {
                if (!is_callable($action)) {
                    return new \Exception(' reminder - action on Handler is not callable');
                }
                call_user_func($action, $entity);
            } catch (\Exception $exception) {
                echo $exception->getMessage();
                echo $exception->getTraceAsString();
                report($exception);
            }

        }

        return $eventsFired;


    }

    /**
     * This function returns the base date we where talking about.
     * For example:
     *    return $enrollment->last_state_change_at;
     *
     * to measure how long sb. stood on this status
     *
     * @param $fn \Closure function
     */
    public function setDateCallback($fn)
    {

        $this->dateCallback = $fn;
    }

    private function findDoAbleAction(Carbon $reasonDate, $lastReminder)
    {
        // here the magic happens.
        // check time ago until last remim

        // condition: calculate time between the 'reasonDate' and the lastReminder
        // if lastReminder == null -> lastReminderHoursAfterEvent is 0.
        // foreach possibleEvent
        //    if timeSince Event (25hrs since event) > possibleEvent.triggerTime (24hrs) // condition
        //    and lastReminderHoursAfterEvent (e.g. 1hrs || null) < possibleEvent.triggerTime (24hrs) -> dont to it again condition
        //
        // ... next time

        $hoursSinceEvent = $reasonDate->diffInHours();
        $lastReminderAfterEvent = $lastReminder ? $reasonDate->diffInHours($lastReminder) : 0;

        foreach ($this->events as $triggerHours => $action) {
            if ($hoursSinceEvent < $triggerHours) {
                continue;
            }
            if ($lastReminderAfterEvent > $triggerHours) {
                continue;
            }
            echo '[Abstract Reminder] firing event: ' . $triggerHours . PHP_EOL; // @todo: should be log;

            return $triggerHours;
        }
        return null;


    }


}