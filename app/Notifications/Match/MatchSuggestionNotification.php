<?php

namespace App\Notifications\Match;

use Modules\Core\Domain\Models\User;

class MatchSuggestionNotification extends MatchNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'match_suggestion_notification';

    public static function getComment()
    {
        return 'Mentor gets a mathc offered';
    }


    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        $data = parent::placeholderData($user);
        $data['technicalInfo'] = $user->organization->technical_usage_comment;
        return $data;

    }

    public static function samplePlaceholderData($user)
    {
        $data = parent::samplePlaceholderData($user);
        $data['technicalInfo'] = "Please use Google Chrome";
        return $data;
    }


}