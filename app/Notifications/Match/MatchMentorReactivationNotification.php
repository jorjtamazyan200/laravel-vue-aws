<?php

namespace App\Notifications\Match;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchMentorReactivationNotification extends MatchNotification
{
    public $comment = "Will be sent 3-4 weeks after the match was set to done;";
}