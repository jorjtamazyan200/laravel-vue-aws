<?php

namespace App\Notifications\Match;

use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchRequestNotification extends MatchNotification
{

    public static function getComment()
    {
        return 'Mentee match offer';
    }

    protected $available_channels = ['email'];
    protected $additional_mobile_channels = ['whatsapp']; //@todo:add sms

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        $data = parent::placeholderData($user);

        $match = $this->match;

        $data['confirmLink'] = self::getLink($match->id, $user, 'mentee_approve');
        $data['rejectLink'] = self::getLink($match->id, $user, 'mentee_reject');

        return $data;
    }

    public static function samplePlaceholderData($user)
    {

        $data = parent::samplePlaceholderData($user);
        /** @var Match $match */
        $match = Match::query()->where('state', Match::STATES['ACTIVE'])->firstOrFail();

        $enrollment = $match->getMentorParticipation()->enrollment;

        $data['confirmLink'] = self::getLink($match->id, $enrollment->user, 'mentee_approve');
        $data['rejectLink'] = self::getLink($match->id, $enrollment->user, 'mentee_reject');

        return $data;
    }


    private static function getLink($matchId, User $user, $action)
    {
        $linkParameters = [
            'id' => $matchId,
            'action' => $action,
            'userid' => $user->id,
            'secret' => $user->emailSecret(strtoupper($action) . '_MATCH')
        ];
        return route('match.email.response', $linkParameters);


    }


}


