<?php

namespace App\Notifications\Match;

use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchRequestReminderNotification extends MatchRequestNotification
{
    public static function getComment()
    {
        return '3days match request unanswered';
    }

}




