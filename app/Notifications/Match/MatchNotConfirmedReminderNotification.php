<?php

namespace App\Notifications\Match;

class MatchNotConfirmedReminderNotification extends MatchNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'match_not_confirmed_reminder_notification';

    public static function getComment()
    {
        return '3days match unconfirmed';
    }

}
