<?php

namespace App\Notifications\Match;

class MatchNotConfirmedReminder2Notification extends MatchNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'match_not_confirmed_reminder_2_notification';

    public static function getComment()
    {
        return '6 days match unconfirmed';
    }

}