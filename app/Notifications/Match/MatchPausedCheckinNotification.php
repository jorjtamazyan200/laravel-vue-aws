<?php

namespace App\Notifications\Match;

use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchPausedCheckinNotification extends MatchNotification
{

    public static function getComment()
    {
        return '??days match paused';
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'match' => $this->match,
            'enrollment' => $this->enrollment,
            'pauseagain' => self::getLink($this->match, $user, 'pause_again')
        ];
    }

    public static function samplePlaceholderData($user)
    {

        /** @var Match $match */
        $match = Match::query()->where('state', Match::STATES['ACTIVE'])->firstOrFail();

        $enrollment = $match->getMentorParticipation()->enrollment;

        return [
            'user' => $enrollment->user,
            'brand' => $enrollment->user->brand,
            'match' => $match,
            'enrollment' => $match->getMentorParticipation()->enrollment,
            'pauseagain' => self::getLink($match, $enrollment->user, 'pause_again')
        ];
    }

    private static function getLink(Match $match, User $user, $action)
    {
        $secret = $user->emailSecret($action . '_match');

        return route('match.email.response',
            [
                'userid' => $user->id,
                'id' => $match->id,
                'action' => 'pause_again',
                'secret' => $secret,
            ]
        );

    }


}