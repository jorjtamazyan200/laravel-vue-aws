<?php

namespace App\Notifications\Match;

class ConfirmMatchReminder extends MatchNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'match_confirm_reminder';
}