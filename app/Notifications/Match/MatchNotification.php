<?php

namespace App\Notifications\Match;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchNotification extends BrandedNotification
{
    /**
     *
     * @var Match
     */
    protected $match;
    protected $enrollment;

    public function __construct(Brand $brand, Match $match, Enrollment $enrollment)
    {
        $this->brand = $brand;
        $this->match = $match;
        $this->enrollment = $enrollment;
        $this->role = $enrollment->role;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'match' => $this->match,
            'enrollment' => $this->enrollment,
        ];
    }

    public static function samplePlaceholderData($user)
    {

        /** @var Match $match */
        $match = Match::query()->where('state', Match::STATES['ACTIVE'])->firstOrFail();

        $enrollment = $match->getMentorParticipation()->enrollment;

        return [
            'user' => $enrollment->user,
            'brand' => $enrollment->user->brand,
            'match' => $match,
            'enrollment' => $match->getMentorParticipation()->enrollment
        ];
    }

}