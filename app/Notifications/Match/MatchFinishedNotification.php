<?php

namespace App\Notifications\Match;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Matching\Domain\Models\Match;

class MatchFinishedNotification extends MatchNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'match_finished_notification';


    /** {@inheritDoc}
     * @throws \Exception
     */
    protected function placeholderData(User $user)
    {
        /** @var Program $program */
        $program = $this->enrollment->program;
        $link = $program->getCustomLink('EVALUATION_LINK', $this->enrollment->role, $user->language);

        $participation = $this->match->getParticipationOfUser($user);

        $certificateLink = route('certificate_pdf',
            [
                'id' => $participation->id,
                'hash' => $this->match->getHash()
            ]);


        return [
            'user' => $user,
            'brand' => $this->brand,
            'match' => $this->match,
            'enrollment' => $this->enrollment,
            'evaluationLink' => $link,
            'certificateLink' => $certificateLink
        ];
    }

    public static function samplePlaceholderData($user)
    {

        $parentPlaceholder = parent::samplePlaceholderData($user);

        $parentPlaceholder['evaluationLink'] = 'evaluationLink';
        $parentPlaceholder['certificateLink'] = 'certificateLink';
        return $parentPlaceholder;

    }


}

//EVALUATION_LINK