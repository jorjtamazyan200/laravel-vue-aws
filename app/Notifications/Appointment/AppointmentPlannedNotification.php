<?php

namespace App\Notifications\Appointment;

use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;

class AppointmentPlannedNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_planned_notification';

    protected $available_channels = ['email'];

    protected $additional_mobile_channels = ['sms','whatsapp'];


}