<?php

namespace App\Notifications\Appointment;

class AppointmentNoShowSenderNotification extends AppointmentNotification
{
    public static function getComment()
    {
        return 'the person who clicks no show';
    }

}
