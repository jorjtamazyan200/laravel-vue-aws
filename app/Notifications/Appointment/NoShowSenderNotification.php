<?php

namespace App\Notifications\Appointment;

class NoShowSenderNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_no_show_sender_notification';

}
