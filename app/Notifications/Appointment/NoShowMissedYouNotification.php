<?php

namespace App\Notifications\Appointment;

class NoShowMissedYouNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_no_show_missed_you_notification';
}
