<?php

namespace App\Notifications\Appointment;

class AppointmentReminderNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_reminder_notification';

//    protected $force_delivery_channel = 'sms';

    protected $available_channels = ['email'];

    protected $additional_mobile_channels = ['sms', 'whatsapp'];

}