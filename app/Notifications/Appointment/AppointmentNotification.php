<?php

namespace App\Notifications\Appointment;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;

class AppointmentNotification extends BrandedNotification
{
    /**
     * The appointment nobody came to ...
     *
     * @var Appointment
     */
    protected $appointment;
    protected $enrollment;
    protected $link;

    public function __construct(Brand $brand, Appointment $appointment, Enrollment $enrollment, $link = false)
    {
        $this->brand = $brand;
        $this->appointment = $appointment;
        $this->enrollment = $enrollment;
        $this->link = $link;

    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'appointment' => $this->appointment,
            'enrollment' => $this->enrollment,
            'link' => $this->link
        ];
    }
    public static function samplePlaceholderData($user)
    {
        $appointment = Appointment::query()->where('state', Appointment::STATES['PLANNED'])->firstOrFail();

        return [
            'user' => $appointment->match->participations->first()->enrollment->user,
            'brand' => $appointment->match->participations->first()->enrollment->user->brand,
            'appointment' => $appointment,
            'enrollment' => $appointment->match->participations->first()->enrollment,
            'link' => 'http://www.sample.com'
        ];
    }


}
