<?php

namespace App\Notifications\Appointment;

class AppointmentCanceledNotification extends AppointmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'appointment_canceled_notification';

    public static function getComment()
    {
        return 'Will be sent, when user cancels a notification';
    }


}