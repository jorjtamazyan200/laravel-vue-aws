<?php

namespace App\Notifications\External;

use App\Notifications\AnonymousBrandedNotification;
use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;
use Modules\Core\Domain\Models\Webinar;

class LandingPageRegistrationNotification extends AnonymousBrandedNotification
{
    protected  $webinar;
    protected  $program;
    protected  $name;
    protected  $email;
    protected  $code;



    /**
     * @var string
     */
    protected $document_key = 'landingpage_registration_notification';

    public function __construct(Brand $brand, string $code, $name, $email, Program $program, $webinar)
    {
        $this->brand = $brand;
        $this->webinar = $webinar;
        $this->program = $program;
        $this->name = $name;
        $this->code = $code;
        $this->email = $email;

    }

    /** {@inheritDoc} */
    protected function placeholderData()
    {
        return [
            'brand' => $this->brand,
            'webinar' => $this->webinar,
            'program' => $this->program,
            'name' => $this->name,
            'email' => $this->email,
            'code' => $this->code
        ];
    }

    protected function samplePlaceholderData()
    {
        return [];
    }

    /** {@inheritDoc} */
    public function via($notifiable)
    {
        return ['mail'];
    }
}