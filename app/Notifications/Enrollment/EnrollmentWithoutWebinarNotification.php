<?php

namespace App\Notifications\Enrollment;

/**
 * Class EnrollmentWithoutWebinarNotification
 * @package App\Notifications\Enrollment
 * @deprecated
 */
class EnrollmentWithoutWebinarNotification extends EnrollmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'enrollment_without_webinar_notification';

}