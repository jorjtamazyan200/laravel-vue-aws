<?php

namespace App\Notifications\Enrollment;

class UserQuitNotification extends EnrollmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'enrollment_user_quit_notification';
}