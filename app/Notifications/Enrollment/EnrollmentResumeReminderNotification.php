<?php

namespace App\Notifications\Enrollment;

class EnrollmentResumeReminderNotification extends EnrollmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'enrollment_resume_reminder';
}