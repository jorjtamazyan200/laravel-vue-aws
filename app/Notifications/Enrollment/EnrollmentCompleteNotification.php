<?php

namespace App\Notifications\Enrollment;

use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;
use Modules\Core\Domain\Services\UserService;

/**
 * Class EnrollmentCompleteNotification
 * @deprecated replaced by Enrollmentenrolled?
 * @package App\Notifications\Enrollment
 */
class EnrollmentCompleteNotification extends EnrollmentNotification
{

    public static function getComment()
    {
        return 'Enrollment state becomes active';
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        /** @var Webinar $webinar */
        $webinar = $this->enrollment->webinars()->first();

        return [
            'user' => $user,
            'brand' => $this->brand,
            'enrollment' => $this->enrollment,
            'webinar' => $webinar,
            'webinarLink' => $webinar->getLoginLink(),
            'optinLink' => UserService::getOptInLink($user)
        ];
    }

    public static function samplePlaceholderData($user)
    {

        $enrollment = Enrollment::query()->where('state', Enrollment::STATES['TRAINING'])->firstOrFail();

        /** @var Webinar $webinar */
        $webinar = $enrollment->webinars()->first();

        return [
            'user' => $enrollment->user,
            'brand' => $enrollment->user->brand,
            'enrollment' => $enrollment,
            'webinar' => $webinar,
            'webinarLink' => $webinar->getLoginLink(),
            'optinLink' => UserService::getOptInLink($enrollment->user)
        ];
    }


}