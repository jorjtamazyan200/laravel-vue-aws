<?php

namespace App\Notifications\Enrollment;

class EnrollmentIncompleteReminderNotification extends EnrollmentNotification
{
    public static function getComment()
    {
        return '3days Enrollment State = NEW';
    }

}