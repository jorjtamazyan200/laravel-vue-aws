<?php

namespace App\Notifications\Enrollment;

use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;

class ProgramEnrolledNotification extends EnrollmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'enrollment_enrolled_notification';

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'enrollment' => $this->enrollment,
            'webinar' => $this->enrollment->webinars()->first()
        ];
    }
    public static function samplePlaceholderData($user)
    {

        $enrollment = Enrollment::query()->where('state', Enrollment::STATES['TRAINING'])->firstOrFail();
        return [
            'user' => $enrollment->user,
            'brand' => $enrollment->user->brand,
            'enrollment' => $enrollment,
            'webinar' => $enrollment->webinars()->first()
        ];
    }

}