<?php

namespace App\Notifications\Enrollment;

class ProgramCompletedNotification extends EnrollmentNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'enrollment_program_completed_notification';
}