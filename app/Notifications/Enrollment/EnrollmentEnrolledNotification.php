<?php

namespace App\Notifications\Enrollment;

use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

class EnrollmentEnrolledNotification extends EnrollmentNotification
{
    public static function getComment()
    {
        return 'User completed the enrollment - next is training or active';
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {

        /** @var Webinar $webinar */
        $webinar = $this->enrollment->webinars()->orderBy("starts_at","desc")->first();

        return [
            'user' => $user,
            'brand' => $this->brand,
            'enrollment' => $this->enrollment,
            'webinar' => $webinar,
            'webinarLink' => $webinar ? $webinar->getLoginLink() : null
        ];
    }

    public static function samplePlaceholderData($user)
    {

        $enrollment = Enrollment::query()->where('state', Enrollment::STATES['TRAINING'])->firstOrFail();
        $webinar = $enrollment->webinars()->first();

        return [
            'user' => $enrollment->user,
            'brand' => $enrollment->user->brand,
            'enrollment' => $enrollment,
            'webinar' => $webinar,
            'webinarLink' => $webinar ? $webinar->getLoginLink() : null
        ];
    }

}