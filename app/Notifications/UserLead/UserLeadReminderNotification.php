<?php

namespace App\Notifications\UserInvitation;

use App\Notifications\AnonymousBrandedNotification;
use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;
use Modules\Core\Domain\Models\UserLead;

class UserLeadReminderNotification extends UserLeadAbstractNotification
{

    public $comment = "First Reminder (5days)";
}