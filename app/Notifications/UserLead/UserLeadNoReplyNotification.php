<?php

namespace App\Notifications\UserInvitation;

use App\Notifications\AnonymousBrandedNotification;
use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\UserInvitation;
use Modules\Core\Domain\Models\UserLead;

class UserLeadNoReplyNotification extends UserLeadAbstractNotification
{
    public $comment = "Second Reminder (14days)";
}