<?php

namespace App\Notifications\Supervisor;

use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;
use Modules\Scheduling\Domain\Models\Appointment;

class SupervisorDailyReportNotification extends SupervisorNotification
{

    protected $supervisor_user;
    protected $upcoming_appointments;

    public function __construct(Brand $brand, Collection $upcoming_appointments)
    {
        $this->brand = $brand;
        $this->upcoming_appointments = $upcoming_appointments;
    }

    public function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'upcomingAppointments' => $this->upcoming_appointments
        ];
    }
    public static function samplePlaceholderData($user)
    {
        $appointment = Appointment::query()->where('state', Appointment::STATES['PLANNED'])->firstOrFail();

        return [
            'user' => $appointment->match->participations->first()->enrollment->user,
            'brand' => $appointment->match->participations->first()->enrollment->user->brand,
            'upcomingAppointments' => Appointment::query()->inRandomOrder()->limit(10)->get()
        ];
    }

}