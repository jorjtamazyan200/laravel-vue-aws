<?php

namespace App\Notifications\Supervisor;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;

class SupervisorNotification extends BrandedNotification
{
    protected $supervisorUser;

    public function __construct(Brand $brand, User $supervisorUser)
    {
        $this->brand = $brand;
        $this->supervisorUser = $supervisorUser;
    }

    public function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'supervisor' => $this->supervisorUser,
        ];
    }

    public static function samplePlaceholderData($user)
    {
        return [
            'user' => $user,
            'brand' => $user->brand,
            'supervisor' => $user,
        ];
    }


}