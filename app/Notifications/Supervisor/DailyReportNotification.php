<?php

namespace App\Notifications\Supervisor;

use Illuminate\Support\Collection;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;

class DailyReportNotification extends SupervisorNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'supervisor_daily_report_notification';

    protected $supervisor_user;
    protected $upcoming_appointments;

    public function __construct(Brand $brand, Collection $upcoming_appointments)
    {
        $this->brand = $brand;
        $this->upcoming_appointments = $upcoming_appointments;
    }

    public function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'upcomingAppointments' => $this->upcoming_appointments
        ];
    }


}