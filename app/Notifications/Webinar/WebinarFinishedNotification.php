<?php

namespace App\Notifications\Webinar;

class WebinarFinishedNotification extends WebinarNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'webinar_finished_notification';
}