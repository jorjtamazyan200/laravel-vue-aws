<?php

namespace App\Notifications\Webinar;

class WebinarInvitationNotification extends WebinarNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'webinar_invitation_notification';
}