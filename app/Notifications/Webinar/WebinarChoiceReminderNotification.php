<?php

namespace App\Notifications\Webinar;

use App\Notifications\User\UserNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Enrollment;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Webinar;

class WebinarChoiceReminderNotification extends UserNotification
{

    protected $webinars;
    protected $enrollment;


    public function __construct(Brand $brand, User $user, Enrollment $enrollment, $webinars)
    {
        parent::__construct($brand, $user);
        $this->webinars = $webinars;
        $this->enrollment = $enrollment;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {

        return [
            'brand' => $this->brand,
            'user' => $this->user,
            'webinars' => $this->webinars,
            'enrollment' => $this->enrollment,
            'videoLink' => self::getVideoLink($this->brand, $this->user, $this->enrollment),
            'webinarLink' => self::getVideoLink($this->brand, $this->user, $this->enrollment),
        ];
    }

    private static function getVideoLink(Brand $brand, User $user, Enrollment $enrollment)
    {

        $videolink = false;
        try {
            $videolink = $enrollment->program->getCustomLink('WEBINAR_VIDEO', $enrollment->role, $user->getPreferredLanguageAttribute());
        } catch (\Exception $e) {
        }

        if (!$videolink){
            return null;
        }
        // mentoring/enrollment/292/videoWebinar
        return $brand->frontend_url . '/mentoring/enrollment/' . $enrollment->id .'/videoWebinar';

    }

    public static function samplePlaceholderData($user)
    {
        /** @var Enrollment $enrollment */
        $enrollment = Enrollment::query()->where('state', Enrollment::STATES['TRAINING'])->firstOrFail();

        return [
            'user' => $user,
            'brand' => $user->brand,
            'webinars' => Webinar::query()->inRandomOrder()->limit(5)->get(),
            'videoLink' => self::getVideoLink($user->brand, $user, $enrollment),
            'enrollment' => $enrollment
        ];

    }


}