<?php

namespace App\Notifications\Webinar;

class WebinarReferentNotification extends WebinarNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'webinar_referent_notification';
}