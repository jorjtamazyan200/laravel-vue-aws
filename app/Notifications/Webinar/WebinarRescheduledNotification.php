<?php

namespace App\Notifications\Webinar;

class WebinarRescheduledNotification extends WebinarNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'webinar_rescheduled_notification';
}