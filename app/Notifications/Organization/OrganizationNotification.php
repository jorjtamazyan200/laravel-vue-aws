<?php

namespace App\Notifications\Organization;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Organization;
use Modules\Core\Domain\Models\User;

class OrganizationNotification extends BrandedNotification
{
    /**
     *
     * @var Organization
     */
    protected $organization;

    public function __construct(Brand $brand, Organization $organization)
    {
        $this->brand = $brand;
        $this->organization = $organization;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'organization' => $this->organization,
        ];
    }

}