<?php

namespace App\Notifications\Organization;

class ITCheckNotification extends OrganizationNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'organization_it_check_notification';
}