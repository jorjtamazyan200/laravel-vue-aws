<?php

namespace App\Notifications\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Notifications\UserInvitation\UserInvitationNotification;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\UserInvitation;

class UserInvitationNotificationTest extends LaravelTest
{
    public function test_it_creates_a_correctly_branded_notification_mail()
    {
        Notification::fake();

        $brand = factory(Brand::class)->create();
        $invited_user_email = 'simon@fakir-it.de';

        $invite = factory(UserInvitation::class)->create();
        $notification = new UserInvitationNotification($brand, $invite);

        Notification::route('mail', $invited_user_email)
        ->notify($notification);

        Notification::assertSentTo(
            new AnonymousNotifiable(),
            UserInvitationNotification::class
        );
    }
}