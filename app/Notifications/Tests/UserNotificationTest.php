<?php

namespace App\Notifications\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;
use App\Notifications\User\UserRegistrationCompleteNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\User;

class UserNotificationTest extends LaravelTest
{

    public function test_it_creates_a_correctly_branded_notification_mail()
    {
        $user = factory(User::class)->create(['first_name' => 'Tom', 'language' => 'de', 'accept_email' => true]);
        $brand = factory(Brand::class)->create(['name' => 'Company']);

        /** @var BrandedDocument $document */
        $document = BrandedDocument::where('key', '=', 'user_registration_complete_notification')->firstOrFail();

        $notification = new UserRegistrationCompleteNotification($brand, $user);

        $mail = $notification->toMail($user);
        $this->assertEquals($document->subject, $mail->subject);
    }
}