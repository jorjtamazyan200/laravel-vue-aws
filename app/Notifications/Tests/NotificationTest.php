<?php

namespace App\Services\Tests;

use App\Infrastructure\AbstractTests\LaravelTest;

use Illuminate\Support\Facades\Notification;
use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\Program;
use App\Notifications\BaseNotification;
use App\Notifications\BrandedNotificationForProgram;
use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\BrandedDocument;
use NotificationChannels\Twilio\TwilioChannel;


class TestBrandedNotification extends BrandedNotification
{
    protected $document_key = 'test';
    protected $available_channels = ['email', 'sms'];

    public function toDatabase(User $user): array
    {
        // not needed but it's defined as abstract in parent ...
    }
}


class TestWhatsappNotification extends BrandedNotification
{
    protected $document_key = 'whatsapp_sample';
    protected $additional_mobile_channels = ['whatsapp'];


}


class TestBrandedNotificationForProgram extends BrandedNotificationForProgram
{
    protected $document_key = 'test';

    public function toDatabase(User $user): array
    {
        // not needed but it's defined as abstract in parent ...
    }
}

class TestForceMailChannelNotification extends BaseNotification
{
    protected $force_delivery_channel = 'email';

    public function toDatabase(User $user): array
    {
        // not needed but it's defined as abstract in parent ...
    }
}

class NotificationTest extends LaravelTest
{
    public function test_it_creates_a_correctly_branded_notification_mail()
    {
        $user = factory(User::class)->create(['first_name' => 'Tom', 'language' => 'de', 'accept_email' => true]);
        $brand = factory(Brand::class)->create(['name' => 'Company']);
        $branded_document = factory(BrandedDocument::class)->create(['brand_id' => $brand->id, 'key' => 'test', 'type' => 'email', 'subject' => 'Test subject by {{ $brand->name }} for user {{ $user->first_name }}', 'content' => 'Test content by {{ $brand->name }} for user {{ $user->first_name }}']);

        $notification = new TestBrandedNotification($brand);

        $mail = $notification->toMail($user);
        $this->assertEquals('Test subject by Company for user Tom', $mail->subject);
        $this->assertEquals('Test content by Company for user Tom', $mail->introLines[0]);
    }

    public function test_it_creates_a_correctly_branded_notification_sms()
    {
        $user = factory(User::class)->create(['first_name' => 'Tom', 'language' => 'de', 'accept_sms' => true]);
        $brand = factory(Brand::class)->create(['name' => 'Company']);
        $branded_document = factory(BrandedDocument::class)->create(['brand_id' => $brand->id, 'key' => 'test', 'type' => 'sms', 'content' => 'Test content by {{ $brand->name }} for user {{ $user->first_name }}']);

        $notification = new TestBrandedNotification($brand);

        $sms = $notification->toTwilio($user);
        $this->assertEquals('Test content by Company for user Tom', $sms->content);
    }

    public function test_it_creates_whatsapp_queue()
    {
        $user = factory(User::class)->create(['first_name' => 'Tom', 'language' => 'de', 'accept_sms' => true, 'whatsapp_setup' => 'mobile']);
        $brand = factory(Brand::class)->create(['name' => 'Company']);
        $branded_document = factory(BrandedDocument::class)->create(['brand_id' => $brand->id, 'key' => 'whatsapp_sample', 'type' => 'whatsapp', 'content' => 'Test content by {{ $brand->name }} for user {{ $user->first_name }}']);

        $notification = new TestWhatsappNotification($brand);
        $smsText = $notification->toWhatsapp($user);
        $this->assertEquals('Test content by Company for user Tom', $smsText);

        $user->notify($notification);

        $this->assertDatabaseHas('sms_logs', [
            'channel' => 'whatsapp',
            'type' => 'whatsapp_sample',
            'body' => $smsText
        ]);


    }

    public function test_it_creates_a_correctly_branded_program_notification_mail()
    {
        $user = factory(User::class)->create(['first_name' => 'Tom', 'language' => 'de', 'accept_email' => true]);
        $brand = factory(Brand::class)->create(['name' => 'Company']);
        $program = factory(Program::class)->create(['title' => 'Test']);
        $branded_document = factory(BrandedDocument::class)->create(['brand_id' => $brand->id, 'program_id' => $program->id, 'key' => 'test', 'type' => 'email', 'subject' => 'Program {{ $program->title }} by {{ $brand->name }} for user {{ $user->first_name }}', 'content' => 'Program content for {{ $program->title }} by {{ $brand->name }} for user {{ $user->first_name }}']);

        $notification = new TestBrandedNotificationForProgram($brand, $program);

        $mail = $notification->toMail($user);
        $this->assertEquals('Program Test by Company for user Tom', $mail->subject);
        $this->assertEquals('Program content for Test by Company for user Tom', $mail->introLines[0]);
    }

    public function test_it_creates_a_correctly_branded_program_notification_sms()
    {
        $user = factory(User::class)->create(['first_name' => 'Tom', 'language' => 'de', 'accept_sms' => true]);
        $brand = factory(Brand::class)->create(['name' => 'Company']);
        $program = factory(Program::class)->create(['title' => 'Test']);
        $branded_document = factory(BrandedDocument::class)->create(['brand_id' => $brand->id, 'program_id' => $program->id, 'key' => 'test', 'type' => 'sms', 'content' => 'Program content for {{ $program->title }} by {{ $brand->name }} for user {{ $user->first_name }}']);

        $notification = new TestBrandedNotificationForProgram($brand, $program);

        $sms = $notification->toTwilio($user);
        $this->assertEquals('Program content for Test by Company for user Tom', $sms->content);
    }

    public function test_it_uses_forced_delivery_channel()
    {
        $user = factory(User::class)->create(['accept_sms' => true, 'accept_email' => true, 'preferred_channel' => 'sms']);

        $notification = new TestForceMailChannelNotification();

        $result = $notification->via($user);


        $this->assertEquals(['mail', 'database'], $result);
    }

    public function test_it_uses_additional_whatsapp()
    {
        $user = factory(User::class)->create(['accept_sms' => true, 'accept_email' => true, 'preferred_channel' => 'sms', 'whatsapp_setup' => 'mobile']);

        $brand = factory(Brand::class)->create(['name' => 'Company']);
        $branded_document = factory(BrandedDocument::class)->create(['brand_id' => $brand->id, 'key' => 'whatsapp_sample', 'type' => 'whatsapp', 'content' => 'Test content by {{ $brand->name }} for user {{ $user->first_name }}']);

        $notification = new TestWhatsappNotification($brand);

        $result = $notification->via($user);

        $this->assertEquals(['mail', 'App\Channels\WaboxChannel', 'database'], $result);
    }

//    public function test_it_uses_users_preferred_delivery_channel()
//    {
//
//        $user = factory(User::class)->create(['accept_sms' => true, 'accept_email' => true, 'preferred_channel' => 'sms']);
//        $brand = factory(Brand::class)->create();
//
//        $notification = new TestBrandedNotification($brand);
//
//        $this->assertEquals([TwilioChannel::class, 'database'], $notification);
//    }

    public function test_it_uses_intersection_of_accepted_and_available_delivery_channels()
    {
        $brand = factory(Brand::class)->create();
        $notification = new TestBrandedNotification($brand);

        $user1 = factory(User::class)->create(['accept_sms' => true, 'accept_email' => true, 'preferred_channel' => null,'whatsapp_setup' => 'notused']);

        $this->assertEquals(['mail', TwilioChannel::class, 'database'], $notification->via($user1));

    }

    public function test_it_uses_intersection_of_accepted_and_available_delivery_channels2()
    {
        $brand = factory(Brand::class)->create();
        $notification = new TestBrandedNotification($brand);


        $user2 = factory(User::class)->create(['accept_sms' => true, 'accept_email' => false, 'preferred_channel' => null, 'whatsapp_setup' => 'notused']);

        $this->assertEquals([TwilioChannel::class, 'database'], $notification->via($user2));
    }

    // Important: This test simply mocks a notification and checks general stuff.
    //  It doesn't even invoke toMail() function which would fail because of missing BrandedDocument
    public function test_it_notifies_a_user()
    {
        Notification::fake();

        $user = factory(User::class)->create(['preferred_channel' => 'email']);
        $brand = factory(Brand::class)->create();

        $notification = new TestBrandedNotification($brand);
        $user->notify($notification);

        Notification::assertSentTo(
            $user,
            TestBrandedNotification::class
        );
    }
}
