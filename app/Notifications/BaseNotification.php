<?php

namespace App\Notifications;

use App\Channels\WaboxChannel;
use App\Services\StringHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use NotificationChannels\Twilio\TwilioChannel;
use Modules\Core\Domain\Models\User;
use App\Interfaces\Notifications\HasNotificationSettings;

abstract class BaseNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The brand for which a template will be searched
     *
     * @var \Modules\Core\Domain\Models\Brand
     */
    protected $brand;


    /**
     * Overide any channel logic for a notification.
     *
     * @var string
     */
    protected $force_delivery_channel = null;

    /**
     * Overide to send this message via multiple channel
     * Multichannel tries to send one mobile (push|whatsapp|sms)
     * and email.
     *
     * Chooses only ONE of those;
     *
     * @var boolean
     */
    protected $additional_mobile_channels = [];

    /**
     * Define the available channels of a notification.
     *
     * @var array
     */
    protected $available_channels = ['email'];

    /**
     * Maps channel types to an implementing provider, e.g. SMS to Twilio
     *
     * @var array
     */
    protected $delivery_channel_implementations = [
        'sms' => TwilioChannel::class,
        'email' => 'mail',
        'whatsapp' => WaboxChannel::class
    ];

    private function getPreferedChannel($notifiable): array
    {

        $channels = [];
        if ($this->force_delivery_channel) {
            return [$this->force_delivery_channel];
        }

        // no force channel for this notification, so check if the user has a prefered channel set.
//        if (method_exists($notifiable, 'getPreferredNotificationChannelAttribute') &&
//            $notifiable->getPreferredNotificationChannelAttribute()) {
//            $preferedChannel = $notifiable->getPreferredNotificationChannelAttribute();
//            if (in_array($preferedChannel, $this->available_channels)) {
//                return [$preferedChannel];
//            }
//        }

        // @Todo: in fallback case, should it use ANY or only ONE of the availale channels?
        // okay, no channel force, no prefered channel, lets use any channel available;
        return $this->available_channels;
    }

    private function getAvailableChannels($notifiable)
    {
        $channels = $this->getPreferedChannel($notifiable);

        // add additional mobile only channels;
        if ($this->additional_mobile_channels) {
            $channels = array_merge($channels, $this->additional_mobile_channels);
            $channels = array_unique($channels);
        }
        if (method_exists($notifiable, 'getAcceptedChannelsAttribute')) {
            $channels = array_intersect($channels, $notifiable->getAcceptedChannelsAttribute());
        }



        return $channels;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $channels = $this->getAvailableChannels($notifiable);


        if (count($channels) < 1) {
            Log::debug("No channels could be set for the notification! user:", [$notifiable->id, json_encode($channels)]);
            return ['database'];
        }

//        $channels = $this->removeDisabledChannels($channels);


        $channel_implementations = [];
        foreach ($channels as $channel) {
            if (!$this->isHandableChannel($channel)) {
                throw new \Exception("The channel '$channel' can\'t be handled!");
            }
            $channel_implementations[] = $this->channelImplementedThrough($channel);
        }
        $channel_implementations[] = 'database'; // always log to database


        return $channel_implementations;
    }

    private
    function removeDisabledChannels($channels)
    {

        $twilioEnabled = getEnv('TWILIO_ENABLED') === 'true';
        $key = array_search('sms', $channels);

        if (!$twilioEnabled && $key !== false) {
            //@tood: log, twilio skipped
            unset($channels[$key]);
        }
        return $channels;

    }

    /**
     * Decides if a channel has a delivery implementation.
     *
     * @param String $channel
     * @return boolean
     */
    protected
    function isHandableChannel(
        String $channel
    )
    {
        return array_key_exists($channel, $this->delivery_channel_implementations);
    }

    /**
     * Returns the name of the delivery service for a channel, e.g. 'amazon-ses' for e-mails.
     *
     * @param String $channel
     * @return String
     */
    protected
    function channelImplementedThrough(
        String $channel
    )
    {
        return $this->delivery_channel_implementations[$channel];
    }

    protected function getDefaultVariables($user)
    {

        $greetings = Lang::get('email.greetings');

        $signature = '&copy; ' . Config::get('app.name');

        if ($this->brand) {
            $greetings .= $this->brand->email_greetings;
            $signature = $this->brand->email_signature;
        }


        return [
            'salutation' => $this->getSalutation($user),
            'plattformName' => Config::get('app.name'),
            'greetings' => $greetings,
            'signature' => $signature
        ];
    }

    protected function getSalutation($user)
    {
        $greetings = Lang::get('email.hello');

        if ($user) {
            $greetings .= ' ' . $user->first_name;
        }
        $greetings .= '';

        return $greetings;
    }


    /**
     * Document key is Class name in Snake Case
     * @todo: replace all direct access and remove keys;

     * @return string
     */
    public function getDocumentKey()
    {
        if (isset($this->document_key)) {
            return $this->document_key;
        }
        return StringHelper::classNameToDocumentKey(get_class($this));
    }

    public static function getComment() {
        return 'no comment';
    }

    /** {@inheritDoc} */
    abstract public function toDatabase(User $user): array;
}
