<?php

namespace App\Notifications\User;

use Modules\Core\Domain\Models\User;
use Modules\Core\Domain\Services\UserService;

class UserOptinAgainNotification extends UserNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'user_optin_again_notification';
    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'brand' => $this->brand,
            'user' => $this->user,
            'optinLink' => UserService::getOptInLink($user)
        ];
    }
    public static function samplePlaceholderData($user)
    {
        return [
            'user' => $user,
            'brand' => $user->brand,
            'optinLink' => UserService::getOptInLink($user)
        ];
    }

}