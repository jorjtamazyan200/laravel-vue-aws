<?php

namespace App\Notifications\User;

class ConfirmationNotification extends UserNotification
{
    protected $available_channels = ['whatsapp'];
    protected $additional_mobile_channels = ['whatsapp'];
}