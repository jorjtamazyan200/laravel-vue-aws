<?php

namespace App\Notifications\User;

class CompleteRegistrationReminderNotification extends UserNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'user_complete_registration_reminder';
}
