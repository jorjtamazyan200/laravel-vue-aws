<?php

namespace App\Notifications\User;

use App\Notifications\BrandedNotification;
use Carbon\Carbon;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\BrandedDocument;
use Modules\Core\Domain\Models\User;

class SmsSampleNotification extends BrandedNotification
{
    protected $user;
    /**
     * requrire sms
     * @var string
     */
    protected $force_delivery_channel = 'sms';


    public function __construct(Brand $brand, User $user)
    {
        $this->brand = $brand;
        $this->user = $user;
    }

    public function getTemplateDocument(User $user, String $type)
    {
        if ($type != 'sms'){
            throw new \Exception('this notification is only avaialble as SMS!');
        }
        $document = new BrandedDocument();
        $document->content = 'Sample SMS ' . (Carbon::now()->toW3cString());
        return $document;
    }

    /** {@inheritDoc} */
    protected function placeholderData(User $user)
    {
        return [
            'brand' => $this->brand,
            'user' => $this->user,
        ];
    }
}