<?php

namespace App\Notifications\User;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;

class PasswordResetNotification extends BrandedNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'user_password_reset_notification';

    /**
     * The reset token that needs to be passed in the reset link.
     *
     * @var string
     */
    protected $token;

    /**
     * Override the constructor to collect the token in addition to the brand.
     *
     * @param Brand  $brand
     * @param string $token
     */
    public function __construct(Brand $brand, string $token)
    {
        $this->brand = $brand;
        $this->token = $token;
    }

    /**
     * Decides which data is passed as a nameable var to the view file.
     *
     * @param User $user
     * @return array
     */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'token' => $this->token,
            'reset_link' => sprintf(
                '%s/auth/changepassword/%s',
                $this->brand->frontend_url,
                $this->token
            ),
        ];
    }
}
