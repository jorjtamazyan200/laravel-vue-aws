<?php

namespace App\Notifications\User;

use App\Notifications\BrandedNotification;
use Modules\Core\Domain\Models\Brand;
use Modules\Core\Domain\Models\User;

class UserMigratedNotification extends BrandedNotification
{
    /** {@inheritDoc} */
    protected $document_key = 'user_migrated_notification';

    /**
     * The reset token that needs to be passed in the reset link.
     *
     * @var string
     */
    protected $password;
    protected $link;

    /**
     * Override the constructor to collect the token in addition to the brand.
     *
     * @param Brand  $brand
     * @param string $token
     */
    public function __construct(Brand $brand, string $password , string $link)
    {
        $this->brand = $brand;
        $this->password = $password;
        $this->link = $link;
    }

    /**
     * Decides which data is passed as a nameable var to the view file.
     *
     * @param User $user
     * @return array
     */
    protected function placeholderData(User $user)
    {
        return [
            'user' => $user,
            'brand' => $this->brand,
            'password' => $this->password,
            'link' => $this->link
        ];
    }
}
