<?php

namespace App\Listeners;

use Illuminate\Notifications\Events\NotificationFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\RestException;

class TwilioNotificationErrorListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationFailed $event
     * @return void
     */
    public function handle(NotificationFailed $event)
    {
        /**
         * See  Twilio Channel.php Line 60
         * $event = new NotificationFailed($notifiable, $notification, 'twilio', ['message' => $exception->getMessage(), 'exception' => $exception]);
         */
        Log::warning('Twilio Notification failed', ['event' => $event]);


        $exception = $event->data['exception'];


        // [HTTP 400] Unable to create record: The 'To' number +49+33758631040 is not a valid phone number.


        /** @var RestException $exception */
        if ($exception instanceof RestException) {
            report($exception);
        }

        // [2018-06-22 17:18:34] local.WARNING: Twilio Notification failed {"event":"[object] (Illuminate\\Notifications\\Events\\NotificationFailed:
        // {\"notifiable\":{
        //      \"id\":31,\"gender\":\"female\",\"first_name\":\"Jasmin\",\"last_name\":\"Mentee\",\"email\":\"mentee@volunteer-vision.com\",\"phone_number\":\"(802) 658-9664\",\"whatsapp_number\":\"1-618-416-3610\",\"address\":\"98456 Darby Falls\",\"city\":\"Port Monicaton\",\"postcode\":\"87253-5804\",\"country\":\"PR\",\"country_of_origin\":\"CN\",\"birthday\":\"2017-11-24\",\"organization_id\":1,\"brand_id\":1,\"accept_sms\":true,\"accept_email\":true,\"accept_push\":true,\"is_anonymized\":false,\"last_reminder\":null,\"last_login\":null,\"language\":\"en\",\"timezone\":\"Europe/Berlin\",\"avatar\":{\"small\":\"https://vvapi2-staging.s3.eu-central-1.amazonaws.com/uploads/user/avatar/defaults/small.jpg\",\"medium\":\"https://vvapi2-staging.s3.eu-central-1.amazonaws.com/uploads/user/avatar/defaults/medium.jpg\",\"large\":\"https://vvapi2-staging.s3.eu-central-1.amazonaws.com/uploads/user/avatar/defaults/large.jpg\"},\"state\":\"REGISTERED\",\"last_state_change_at\":\"2018-05-18 11:11:35\",\"registration_code_id\":null,\"preferred_channel\":null,\"color_code\":\"#00ddbb\",\"servicelevel\":\"priority\",\"points\":861,\"level\":1,\"about_me\":\"\",\"position\":\"\",\"phone_number_prefix\":49,\"whatsapp_number_prefix\":49,\"community_enabled\":false,\"tenant_id\":1,\"primary_role\":\"mentee\",\"count_for_reporting\":true,\"invite_sent\":null
        //},\"notification\":
        //  {\"id\":\"f4907a78-bd19-4980-9008-b535f96491ed\",\"connection\":null,\"queue\":null,\"chainConnection\":null,\"chainQueue\":null,\"delay\":null,\"chained\":[]},\"channel\":\"twilio\",
        //\"data\":{\"message\":\"[HTTP 400] Unable to create record: To number: +498026589664, is not a mobile number\",\"exception\":{}}})"
        //}
        try {
            /** @var User $user */
            $user = $event->notifiable;

            $notification = $event->notification;
            $user->sms_disabled = true;
            $user->save();


            $data = json_encode($event->data);
            Log::info("disabled SMS for user id {$user->id} because of {$data}");


        } catch (\Exception $exception) {
            report($exception);
            Log::warning('Twilio Notification handler failed again ;)', ['e' => $exception]);
        }


    }
}
