<?php

namespace App\Listeners;

use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Core\Domain\Models\SecurityIncident;
use Modules\Core\Domain\Services\SecurityIncidentService;

class LogFailedLoginAttempt
{

    protected $securityIncidentService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SecurityIncidentService $securityIncidentService)
    {
        //
        $this->securityIncidentService = $securityIncidentService;
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     */
    public function handle($event)
    {
        //

        $userid = $event->user->id;
        $ip = ''; //Request::getClientIp();

        $this->securityIncidentService->saveSecurityIncident(
            SecurityIncident::TYPES['INVALID_LOGIN'],
            ['ip' => $ip],
            $userid
        );

    }
}
